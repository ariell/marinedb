{
  "collection": "campaign_file",
  "endpoints": [
    {
      "endpoint": "/api/campaign_file",
      "method": "POST",
      "preprocessors": [
        {
          "name": "post_auth_preprocessor",
          "doc": "Restrict request to LOGGED IN users only"
        }
      ],
      "postprocessors": [],
      "description": "Create new [campaign_file](#campaign_file) object. Expects JSON. Fields shown in `MODEL COLUMNS` below. Add user_id to post payload if not supplied"
    },
    {
      "endpoint": "/api/campaign_file/<id>",
      "method": "PATCH",
      "preprocessors": [
        {
          "name": "patch_auth_preprocessor",
          "doc": "Restrict request to OWNER only"
        }
      ],
      "postprocessors": [],
      "description": "Update single [campaign_file](#campaign_file) object with pk field matching param `id`. Expects JSON. Fields shown in `MODEL COLUMNS` below."
    },
    {
      "endpoint": "/api/campaign_file",
      "method": "GET",
      "fields": [
        "user_id",
        "description",
        "iscompressed",
        "campaign_id",
        "file_url",
        "id",
        "name"
      ],
      "preprocessors": [
        {
          "name": "get_many_auth_preprocessor",
          "doc": "Allow all requests"
        }
      ],
      "postprocessors": [],
      "description": "Get list of [campaign_file](#campaign_file) objects matching API query. See [Making API search queries](#api_query) for instructions on making search queries"
    },
    {
      "endpoint": "/api/campaign_file/<id>",
      "method": "GET",
      "fields": [
        "user_id",
        "description",
        "iscompressed",
        "campaign_id",
        "file_url",
        "id",
        "name"
      ],
      "preprocessors": [
        {
          "name": "get_single_auth_preprocessor",
          "doc": "Allow all requests"
        }
      ],
      "postprocessors": [],
      "description": "Get single [campaign_file](#campaign_file) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/campaign_file/<id>",
      "method": "DELETE",
      "preprocessors": [
        {
          "name": "delete_auth_preprocessor",
          "doc": "Restrict request to OWNER only"
        }
      ],
      "postprocessors": [],
      "description": "Delete single [campaign_file](#campaign_file) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/campaign_file/<int:fid>/data",
      "method": "GET",
      "description": "\n        Get the data for the file resource matching `fid`. This endpoint also provides options to manipulate, transform\n        and save data to the database. The querystring takes the following form:\n        ```\n        ?f={...}&save={...}&disposition=...&template=...\n        ```\n        where `f` is a JSON dictionary containing a sequence of operations for transforming the data\n        (see [Data transformation API](#api_data_transform) for more info),\n        `disposition` is optional and defines the download type which can be `inline` (default, displays in\n        the browser) or `attachment` (triggers a download) ,\n        `template` is optional depending on the output of the steps in `f` and can be used to define the export format,\n        `save` is a JSON dictionary that defines batch save operations. This can be used to save the file content or the\n        output of the operations in `f` to a related database model collection.\n\n        "
    },
    {
      "endpoint": "/api/campaign_file/data",
      "method": "POST",
      "description": "\n        Save file data to database. Use this if it is not possible to dynamically link the file using the `file_url` or\n        if the `file_url` is potentially unstable / ephemeral. This should be a multipart form post with `file`\n        parameter set to the uploaded file and all other fields as in the object definition below.\n        "
    }
  ],
  "description": "\n    Campaign File resource: a file associated with a [campaign](#campaign) object.\n    ",
  "columns": [
    {
      "name": "id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "Primary key `id`",
      "primary_key": true,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "last_save",
      "type": "JSON()",
      "nullable": true,
      "unique": null,
      "doc": "Sequence of file operations performed at last save",
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "name",
      "type": "Text()",
      "nullable": false,
      "unique": false,
      "doc": "The name of the file (optional). Defaults to `basename` of either `file_url` or the path of the uploaded file",
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "file_url",
      "type": "Text()",
      "nullable": true,
      "unique": false,
      "doc": "The URL of the file to be loaded dynamically (if `data` field is not set)",
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "description",
      "type": "Text()",
      "nullable": true,
      "unique": null,
      "doc": "The description of the file (optional)",
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "_rawfiledata",
      "type": "LargeBinary()",
      "nullable": true,
      "unique": null,
      "doc": "The raw file storage for the file. This is not directly accessible through the API",
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "iscompressed",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": "Whether or not the file is compressed or not. If so, it will be automatically decompressed upon access.",
      "primary_key": false,
      "foreign_key": null,
      "default": "False"
    },
    {
      "name": "user_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": "users.id",
      "default": null
    },
    {
      "name": "campaign_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": "campaign.id",
      "default": null
    },
    {
      "name": "created_at",
      "type": "DateTime()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    },
    {
      "name": "updated_at",
      "type": "DateTime()",
      "nullable": true,
      "unique": null,
      "doc": "Updated time (UTC)",
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    }
  ],
  "relations": [
    {
      "model": "users",
      "key": "user",
      "doc": null,
      "direction": "MANYTOONE"
    },
    {
      "model": "campaign",
      "key": "campaign",
      "doc": null,
      "direction": "MANYTOONE"
    }
  ],
  "hybrid_properties": [],
  "methods": [
    {
      "name": "data",
      "doc": null,
      "type": "property"
    }
  ],
  "association_proxies": []
}
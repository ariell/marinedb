{
  "collection": "label_scheme",
  "endpoints": [
    {
      "endpoint": "/api/label_scheme",
      "method": "POST",
      "preprocessors": [
        {
          "name": "post_auth_preprocessor",
          "doc": "Restrict request to LOGGED IN users only"
        }
      ],
      "postprocessors": [],
      "description": "Create new [label_scheme](#label_scheme) object. Expects JSON. Fields shown in `MODEL COLUMNS` below. "
    },
    {
      "endpoint": "/api/label_scheme/<id>",
      "method": "PATCH",
      "preprocessors": [
        {
          "name": "patch_auth_preprocessor",
          "doc": "Assert object edit permission from shared `usergroups`. Checks current_user has edit permission."
        }
      ],
      "postprocessors": [],
      "description": "Update single [label_scheme](#label_scheme) object with pk field matching param `id`. Expects JSON. Fields shown in `MODEL COLUMNS` below."
    },
    {
      "endpoint": "/api/label_scheme",
      "method": "GET",
      "fields": [
        "user_id",
        "description",
        "is_hierarchy",
        "id",
        "name",
        "user",
        "user.username",
        "user.first_name",
        "user.last_name",
        "user.id",
        "label_scheme_files",
        "label_scheme_files.description",
        "label_scheme_files.file_url",
        "label_scheme_files.id",
        "label_scheme_files.name",
        "current_user_can_view",
        "current_user_is_owner",
        "current_user_is_member",
        "current_user_can_edit",
        "is_public",
        "is_child",
        "parent_label_scheme",
        "parent_label_scheme.id",
        "parent_label_scheme.name",
        "created_at",
        "updated_at",
        "usergroup_count",
        "annotation_set_count",
        "exemplar_annotation_set_count"
      ],
      "preprocessors": [
        {
          "name": "get_many_auth_preprocessor",
          "doc": "Assert object view permission from shared `usergroups`. Restricts objects to the ones that current_user can view"
        }
      ],
      "postprocessors": [],
      "description": "Get list of [label_scheme](#label_scheme) objects matching API query. See [Making API search queries](#api_query) for instructions on making search queries"
    },
    {
      "endpoint": "/api/label_scheme/<id>",
      "method": "GET",
      "fields": [
        "user_id",
        "description",
        "is_hierarchy",
        "id",
        "name",
        "user",
        "user.username",
        "user.first_name",
        "user.last_name",
        "user.id",
        "label_scheme_files",
        "label_scheme_files.description",
        "label_scheme_files.file_url",
        "label_scheme_files.id",
        "label_scheme_files.name",
        "current_user_can_view",
        "current_user_is_owner",
        "current_user_is_member",
        "current_user_can_edit",
        "is_public",
        "is_child",
        "parent_label_scheme",
        "parent_label_scheme.id",
        "parent_label_scheme.name",
        "created_at",
        "updated_at",
        "usergroup_count",
        "annotation_set_count",
        "exemplar_annotation_set_count",
        "parent_label_scheme_ids"
      ],
      "preprocessors": [
        {
          "name": "get_single_auth_preprocessor",
          "doc": "Assert object view permission from shared `usergroups`. Checks current_user has view permission."
        }
      ],
      "postprocessors": [],
      "description": "Get single [label_scheme](#label_scheme) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/label_scheme/<id>",
      "method": "DELETE",
      "preprocessors": [
        {
          "name": "delete_preprocessor",
          "doc": "\n        checks that the scheme does not contain annotation sets or children.\n        "
        },
        {
          "name": "delete_auth_preprocessor",
          "doc": "Assert object delete permission from shared `usergroups`. Checks current user is owner and object is not\n        shared in a `usergroup`"
        }
      ],
      "postprocessors": [],
      "description": "Delete single [label_scheme](#label_scheme) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/label_scheme/<int:instance_id>/group/<int:group_id>",
      "method": "POST",
      "description": "Add object with id `instance_id` to group with id `group_id`"
    },
    {
      "endpoint": "/api/label_scheme/<int:instance_id>/group/<int:group_id>",
      "method": "DELETE",
      "description": "Remove resource with id `instance_id` from group with id `group_id`"
    },
    {
      "endpoint": "/api/label_scheme/<int:label_scheme_id>/reset_label_parents",
      "method": "PATCH",
      "description": "\n        Reset [labels](#label) parents for `label_scheme` with `id` matching `label_scheme_id`.\n        Uses `origin_code` and `parent_origin_code` to match up [labels](#label). Accepts optional query string argument\n        `replace_existing=true`, which will reset parent_ids for labels that are already set. By default this is set to `false`\n        "
    },
    {
      "endpoint": "/api/label_scheme/<int:label_scheme_id>/export",
      "method": "GET",
      "description": "\n        Export [labels](#label) collection from [label_scheme](#label_scheme) matching `id`.\n        Search queries are executed on the [label](#label) model.\n\n        \n        Querystring parameters are:\n\n        ```?q={...}&f={...}&disposition=...&template=...&include_columns=[...]```\n\n        Where `q` is a JSON search query (see [Making API search queries](#api_query)) on the collection model,\n        where `f` is a JSON dictionary containing a sequence of operations for transforming the data\n        (see [Data transformation API](#api_data_transform) for more info),\n        `disposition` defines the download type which can be `attachment` (default, triggers a download) or `inline` (displays in\n        the browser),\n        `template` is optional depending on the output of the steps in `f` and can be used to define the export format,\n        `include_columns` is a JSON list of column fields for the exported model.\n        Nested fields for related models delimited with a `.`\n        If `include_columns` is omitted, all allowed columns will be returned.\n\n        Metadata for the [annotation_set](#annotation_set) matching `id` is also returned in the response. Depending on the output\n        format, this could be as part of the response body in a `metadata` field or in the response headers with the key\n        `X-Content-Metadata`.\n    \n\n        Allowed fields include:\n\n        `id`, `parent_id`, `uuid`, `name`, `color`, `user.id`, `user.full_name`, `label_scheme.id`,\n        `label_scheme.name`, `lineage_names`, `is_approved`, `tags`, `vocab_registry`,\n        `created_at`, `updated_at`, `origin_code`, `parent_origin_code`, `is_mapped`\n\n        See [label](#label) model columns, relations and attributes for more information on each of the included fields.\n\n        **TRANSLATING TO OTHER LABEL_SCHEMES**\n\n        This endpoint also supports the translation of labels from a source to a target label_scheme.\n        another label_scheme using the semantic translation framework. In order to do this, you need to specify the\n        additional `translate` url query parameter:\n\n        ```\n        &translate={\"target_label_scheme_id\":..., \"vocab_registry_keys\": ..., \"mapping_override\": ...}\n        ```\n        Where `target_label_scheme_id` [required] is an INT with the `id` of the target label scheme,\n        `vocab_registry_keys` [optional] is a list containing the priority order of keys for `vocab_registries` for\n        which to perform the semantic translation and `mapping_override` defines a set of key-value pairs containing\n        `source label.id : target label.id` for which to override the translation.\n        Note: for extended schemes, labels are translated through `tree_traversal` and no semantic translation is\n        required.\n\n        NOTE: you also need to add the `translated.*` columns to obtain the translation output. These columns are\n        ignored if no `translate` query parameter is supplied.\n\n        **EXAMPLES**\n\n        ```\n        # Example1: get all columns as an HTML table in the browser (including all labels from base scheme)\n        /api/label_scheme/7/export?template=data.html&disposition=inline\n\n        # Example2: get all columns as a downloaded CSV file (including all labels from base scheme)\n        /api/label_scheme/7/export?template=data.csv&disposition=inline\n\n        # Example3: get all columns as an HTML table in the browser, but only for labels defined in the extended scheme (not the base scheme)\n        /api/label_scheme/7/export?template=data.html&disposition=inline&q={\"filters\":[{\"name\":\"label_scheme_id\",\"op\":\"eq\",\"val\":7}]}\n\n        # Example4: get translation lookup between schemes, and only return specific columns\n        /api/label_scheme/8/export?disposition=inline&translate={\"vocab_registry_keys\":[\"worms\",\"caab\",\"catami\"],\"target_label_scheme_id\":\"7\"}\n          &include_columns=[\"label_scheme.name\",\"lineage_names\",\"name\",\"id\",\"translated.id\",\"translated.name\",\"translated.lineage_names\",\n          \"translated.translation_info\",\"translated.label_scheme.name\"]\n\n        # MORE TO COME... Need something else or an example added here, please ask...\n        ```\n        "
    },
    {
      "endpoint": "/api/label_scheme/<int:label_scheme_id>/labels",
      "method": "GET",
      "description": null
    },
    {
      "endpoint": "/api/label_scheme/<int:label_scheme_id>/labels/<int:label_parent_id>",
      "method": "GET",
      "description": null
    }
  ],
  "description": "\n    Label Scheme resource. This resource is for managing and define label schemes otherwise known as (annotation scheme,\n    taxonomic hierarchy, category lists, label lists, classification scheme)\n    ",
  "columns": [
    {
      "name": "id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "Primary key `id`",
      "primary_key": true,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "name",
      "type": "String(length=80)",
      "nullable": false,
      "unique": false,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "description",
      "type": "Text()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "user_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "Foreign key reference for ID of related owner [users](#users) model",
      "primary_key": false,
      "foreign_key": "users.id",
      "default": null
    },
    {
      "name": "is_hierarchy",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": "False"
    },
    {
      "name": "created_at",
      "type": "DateTime()",
      "nullable": true,
      "unique": null,
      "doc": "Creation time (UTC)",
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    },
    {
      "name": "updated_at",
      "type": "TIMESTAMP()",
      "nullable": true,
      "unique": null,
      "doc": "Updated time (UTC)",
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    },
    {
      "name": "parent_label_scheme_id",
      "type": "Integer()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": "label_scheme.id",
      "default": null
    }
  ],
  "relations": [
    {
      "model": "users",
      "key": "user",
      "doc": "Related owner [users](#users) model",
      "direction": "MANYTOONE"
    },
    {
      "model": "label_scheme",
      "key": "parent_label_scheme",
      "doc": null,
      "direction": "MANYTOONE"
    },
    {
      "model": "annotation_set",
      "key": "annotation_sets",
      "doc": null,
      "direction": "ONETOMANY"
    },
    {
      "model": "groups",
      "key": "usergroups",
      "doc": "List of [groups](#groups) objects that contain this resource",
      "direction": "MANYTOMANY"
    },
    {
      "model": "label_scheme_file",
      "key": "label_scheme_files",
      "doc": null,
      "direction": "ONETOMANY"
    },
    {
      "model": "label_scheme",
      "key": "children",
      "doc": null,
      "direction": "ONETOMANY"
    },
    {
      "model": "label",
      "key": "labels",
      "doc": null,
      "direction": "ONETOMANY"
    }
  ],
  "hybrid_properties": [
    {
      "name": "is_child",
      "doc": "BOOLEAN, whether or not this class label has a parent class label",
      "args": [],
      "type": "property"
    },
    {
      "name": "is_public",
      "doc": "Whether or not this object is public (i.e. shared in a public group)",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_is_owner",
      "doc": "Whether or not the currently logged in user is the owner of this object",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_is_member",
      "doc": "Whether or not the current logged in user is a member of a group that includes this object",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_is_edit_member",
      "doc": "Whether or not the current logged in user is a member with edit permission of a group that includes this object",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_can_view",
      "doc": "Whether or not the current user has permission to view this object",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_can_edit",
      "doc": "Whether or not the current logged in user has edit permission (owner or edit_member)",
      "args": [],
      "type": "property"
    }
  ],
  "methods": [
    {
      "name": "annotation_set_count",
      "doc": null,
      "type": "method"
    },
    {
      "name": "exemplar_annotation_set_count",
      "doc": null,
      "type": "method"
    },
    {
      "name": "label_count",
      "doc": null,
      "type": "method"
    },
    {
      "name": "usergroup_count",
      "doc": null,
      "type": "method"
    }
  ],
  "association_proxies": []
}
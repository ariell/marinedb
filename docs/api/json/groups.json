{
  "collection": "groups",
  "endpoints": [
    {
      "endpoint": "/api/groups",
      "method": "POST",
      "preprocessors": [
        {
          "name": "group_data_preprocessor",
          "doc": "Handle group_type_name parameter and secure against membership/dataset tampering"
        }
      ],
      "postprocessors": [
        {
          "name": "post_postprocessor",
          "doc": null
        }
      ],
      "description": "Create new [groups](#groups) object. Expects JSON. Fields shown in `MODEL COLUMNS` below. "
    },
    {
      "endpoint": "/api/groups/<id>",
      "method": "PATCH",
      "preprocessors": [
        {
          "name": "group_data_preprocessor",
          "doc": "Handle group_type_name parameter and secure against membership/dataset tampering"
        },
        {
          "name": "patch_auth_preprocessor",
          "doc": "Restrict request to OWNER only"
        }
      ],
      "postprocessors": [],
      "description": "Update single [groups](#groups) object with pk field matching param `id`. Expects JSON. Fields shown in `MODEL COLUMNS` below."
    },
    {
      "endpoint": "/api/groups",
      "method": "GET",
      "fields": [
        "id",
        "name",
        "is_public",
        "dua_link",
        "user",
        "user.id",
        "user.first_name",
        "user.last_name",
        "user.username",
        "is_restricted",
        "group_type",
        "groupinfo",
        "created_at",
        "current_user_is_edit_member",
        "current_user_is_member",
        "current_user_is_owner",
        "current_user_can_share_data",
        "current_user_can_add_member",
        "current_user_has_agreed",
        "current_user_is_pending_approval",
        "requires_agreement",
        "requires_approval",
        "member_count",
        "pending_member_count"
      ],
      "preprocessors": [
        {
          "name": "get_many_auth_preprocessor",
          "doc": "Allow all requests"
        }
      ],
      "postprocessors": [],
      "description": "Get list of [groups](#groups) objects matching API query. See [Making API search queries](#api_query) for instructions on making search queries"
    },
    {
      "endpoint": "/api/groups/<id>",
      "method": "GET",
      "fields": [
        "id",
        "name",
        "is_public",
        "dua_link",
        "user",
        "user.id",
        "user.first_name",
        "user.last_name",
        "user.username",
        "is_restricted",
        "group_type",
        "groupinfo",
        "created_at",
        "current_user_is_edit_member",
        "current_user_is_member",
        "current_user_is_owner",
        "current_user_can_share_data",
        "current_user_can_add_member",
        "current_user_has_agreed",
        "current_user_is_pending_approval",
        "requires_agreement",
        "requires_approval",
        "member_count",
        "pending_member_count",
        "description",
        "dataset_counts"
      ],
      "preprocessors": [
        {
          "name": "get_single_auth_preprocessor",
          "doc": "Allow all requests"
        }
      ],
      "postprocessors": [],
      "description": "Get single [groups](#groups) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/groups/<id>",
      "method": "DELETE",
      "preprocessors": [
        {
          "name": "delete_auth_preprocessor",
          "doc": "Restrict request to OWNER only"
        }
      ],
      "postprocessors": [],
      "description": "Delete single [groups](#groups) object with pk field matching param `id`"
    }
  ],
  "description": "\n    Group resource: user groups for managing shared resources and permissions.\n    ",
  "columns": [
    {
      "name": "id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "Primary key `id`",
      "primary_key": true,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "name",
      "type": "String(length=128)",
      "nullable": false,
      "unique": null,
      "doc": "Name of Group",
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "is_public",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": "Whether or not contents are publicly viewable",
      "primary_key": false,
      "foreign_key": null,
      "default": "False"
    },
    {
      "name": "is_restricted",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": "Whether or not members can see other member's datasets",
      "primary_key": false,
      "foreign_key": null,
      "default": "True"
    },
    {
      "name": "requires_approval",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": "Whether or not custodian approval is required",
      "primary_key": false,
      "foreign_key": null,
      "default": "True"
    },
    {
      "name": "requires_agreement",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": "Whether or not members need to agree to Data Usage Agreement (DUA)",
      "primary_key": false,
      "foreign_key": null,
      "default": "False"
    },
    {
      "name": "description",
      "type": "Text()",
      "nullable": true,
      "unique": null,
      "doc": "Extra information about the Group",
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "dua_link",
      "type": "Text()",
      "nullable": true,
      "unique": null,
      "doc": "Data Usage Agreement (DUA) link. Must be an HTTP accessible, embeddable document",
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "user_id",
      "type": "Integer()",
      "nullable": true,
      "unique": null,
      "doc": "`id` of group custodian [user](#users)",
      "primary_key": false,
      "foreign_key": "users.id",
      "default": null
    },
    {
      "name": "group_type_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "`id` of related [group_type](#group_type)",
      "primary_key": false,
      "foreign_key": "group_types.id",
      "default": null
    },
    {
      "name": "groupinfo",
      "type": "Text()",
      "nullable": true,
      "unique": null,
      "doc": "Shorter description / tagline of Group",
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "created_at",
      "type": "DateTime()",
      "nullable": false,
      "unique": null,
      "doc": "Datetime of creation",
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    }
  ],
  "relations": [
    {
      "model": "users",
      "key": "user",
      "doc": "Related [user](#users) for group custodian",
      "direction": "MANYTOONE"
    },
    {
      "model": "group_types",
      "key": "group_type",
      "doc": "related [group_type](#group_type) object",
      "direction": "MANYTOONE"
    },
    {
      "model": "users",
      "key": "members",
      "doc": null,
      "direction": "MANYTOMANY"
    },
    {
      "model": "usergroups",
      "key": "usergroups",
      "doc": null,
      "direction": "ONETOMANY"
    },
    {
      "model": "label_scheme",
      "key": "shared_label_schemes",
      "doc": null,
      "direction": "MANYTOMANY"
    },
    {
      "model": "annotation_set",
      "key": "shared_annotation_sets",
      "doc": null,
      "direction": "MANYTOMANY"
    },
    {
      "model": "media_collection",
      "key": "shared_media_collections",
      "doc": null,
      "direction": "MANYTOMANY"
    }
  ],
  "hybrid_properties": [
    {
      "name": "current_user_is_owner",
      "doc": "\n\n        :return:\n        ",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_is_pending_approval",
      "doc": "\n\n        :return:\n        ",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_is_member",
      "doc": "\n\n        :return:\n        ",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_is_edit_member",
      "doc": "\n\n        :return:\n        ",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_can_share_data",
      "doc": "BOOL, whether current_user has permission to share",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_can_add_member",
      "doc": "BOOL, whether current_user has permission to share",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_has_agreed",
      "doc": "BOOL, whether current_user has permission to share",
      "args": [],
      "type": "property"
    }
  ],
  "methods": [
    {
      "name": "dataset_counts",
      "doc": "the number of different datasets that are shared in this group",
      "type": "method"
    },
    {
      "name": "member_count",
      "doc": null,
      "type": "method"
    },
    {
      "name": "pending_member_count",
      "doc": null,
      "type": "method"
    }
  ],
  "association_proxies": []
}
{
  "collection": "media",
  "endpoints": [
    {
      "endpoint": "/api/media/<id>",
      "method": "PATCH",
      "preprocessors": [
        {
          "name": "patch_auth_preprocessor",
          "doc": "Restrict request to object OWNER only"
        }
      ],
      "postprocessors": [
        {
          "name": "_clear_cache",
          "doc": "Clears the request cache object for this resource (to cause cache refresh on next request)"
        }
      ],
      "description": "Update single [media](#media) object with pk field matching param `id`. Expects JSON. Fields shown in `MODEL COLUMNS` below."
    },
    {
      "endpoint": "/api/media",
      "method": "GET",
      "fields": [
        "created_at",
        "timestamp_start",
        "is_valid",
        "key",
        "deployment",
        "deployment.id",
        "deployment.key",
        "campaign",
        "campaign.id",
        "campaign.key",
        "deployment_id",
        "data",
        "id",
        "media_type",
        "media_type.id",
        "media_type.name",
        "path_best",
        "path_best_thm"
      ],
      "preprocessors": [
        {
          "name": "get_many_auth_preprocessor",
          "doc": "Allow all requests"
        }
      ],
      "postprocessors": [],
      "description": "Get list of [media](#media) objects matching API query. See [Making API search queries](#api_query) for instructions on making search queries"
    },
    {
      "endpoint": "/api/media/<id>",
      "method": "GET",
      "fields": [
        "created_at",
        "timestamp_start",
        "is_valid",
        "key",
        "deployment",
        "deployment.id",
        "deployment.key",
        "campaign",
        "campaign.id",
        "campaign.key",
        "deployment_id",
        "data",
        "id",
        "media_type",
        "media_type.id",
        "media_type.name",
        "path_best",
        "path_best_thm",
        "current_user_can_edit"
      ],
      "preprocessors": [
        {
          "name": "get_single_auth_preprocessor",
          "doc": "Allow all requests"
        }
      ],
      "postprocessors": [
        {
          "name": "get_single_postprocessor",
          "doc": null
        }
      ],
      "description": "Get single [media](#media) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/media/<id>",
      "method": "DELETE",
      "preprocessors": [
        {
          "name": "delete_auth_preprocessor",
          "doc": null
        }
      ],
      "postprocessors": [],
      "description": "Delete single [media](#media) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/media/<int:media_id>/annotations/<int:annotation_set_id>",
      "method": "GET",
      "description": null
    },
    {
      "endpoint": "/api/media/save",
      "method": "POST",
      "description": "\n        Media UPLOAD resource.\n\n        Used to upload Media to predefined location set out in server config file.\n\n        `file`: file data input containing image file to upload.\n        `json`: json string containing parameters for new [media](#media) item\n        "
    },
    {
      "endpoint": "/api/media",
      "method": "POST",
      "description": "\n        In addition to the `MODEL COLUMNS` below, the JSON body can contain the following parameters:\n        `pose`: a dict containing parameters for a single [pose](#pose) including associated pose data,\n        `poses`: a list of dicts as above if linking multiple poses to a single media object (eg: video),\n        `media_type`: OPTIONAL (default=\"image\"), a string containing the [media_type](#media_type) name.\n\n        Here are some example payloads for creating [media](#media) objects:\n        ```\n        # example JSON payload for media item with a single pose (eg: still image)\n        {\n            \"pose\": {\"lat\":-35.53153264, \"lon\":150.43962917, \"alt\":3.558, \"dep\":28.85, \"data\":[\n                {\"name\":\"temperature\",\"value\":16.602},\n                {\"name\":\"salinity\",\"value\":35.260222}\n            ]},\n            \"key\": \"PR_20101117_002241_698_LC16\",\n            \"deployment_id\": 5,\n            \"timestamp_start\": \"2018-06-07T00:47:17.273514\"\n        }\n\n        # Same as above, but with dict data\n        {\n            \"pose\": {\"lat\":-35.53153264, \"lon\":150.43962917, \"alt\":3.558, \"dep\":28.85, \"data\":{\n                \"temperature\":16.602,\n                \"salinity\":35.260222\n            }},\n            \"key\": \"TEST_20101117_002241_698_LC16\",\n            \"deployment_id\": 13170,\n            \"timestamp_start\": \"2018-06-07T00:47:17.273514\"\n        }\n\n        # To add multiple poses to a media object (i.e. video)\n        {\n            \"poses\":\n                [{\n                    \"timestamp\": \"2018-06-07T00:47:17.273517\",\n                    \"lat\":-35.53153264,\n                    \"lon\":150.43962917,\n                    \"alt\":3.558,\n                    \"dep\":28.85,\n                    \"data\":{\n                        \"temperature\":16.602,\n                        \"salinity\":35.260222\n                    }\n                },{\n                    \"timestamp\": \"2018-06-07T00:47:18.273517\",\n                    \"lat\":-35.531532654,\n                    \"lon\":150.439629345,\n                    \"alt\":6.558,\n                    \"dep\":29.85,\n                    \"data\":{\n                        \"temperature\":16.602,\n                        \"salinity\":35.260222\n                    }\n                },{\n                    \"timestamp\": \"2018-06-07T00:47:19.273517\",\n                    \"lat\":-35.531534564,\n                    \"lon\":150.43962934,\n                    \"alt\":4.558,\n                    \"dep\":28.85,\n                    \"data\":{\n                        \"temperature\":16.60256,\n                        \"salinity\":35.260234\n                    }\n                }],\n            \"key\": \"KEY_OF_VIDEO_FILE_NAME\",\n            \"deployment_id\": 13170,\n            \"timestamp_start\": \"2018-06-07T00:47:17.273514\",\n            \"media_type\": \"rawvideo\"\n        }\n        ```\n        "
    },
    {
      "endpoint": "/api/media/<int:id>/poses",
      "method": "GET",
      "description": "\n        The list of [pose](#pose) for the [media](#media) item matching `id`. For still images, it will normally just\n        be a single [pose](#pose), but video-like [media_types](#media_type) can have more.\n        "
    },
    {
      "endpoint": "/api/media/<int:id>/thumbnail",
      "method": "GET",
      "description": "\n        Dynamically create thumbnail.\n\n        If user is logged in, then they can request different sizes\n        "
    },
    {
      "endpoint": "/api/media/<int:id>/download",
      "method": "GET",
      "description": "Download media object with associated metadata as a zip file"
    }
  ],
  "description": "\n    Media resource: a generic media object definition that can be used to link in media in a variety of formats,\n    including images, videos, photo-mosaics and other media formats. Note that at the moment only images and video\n    framegrabs have annotation support. Annotation viewers for video and mosaics coming soon.\n    ",
  "columns": [
    {
      "name": "id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "Primary key `id`",
      "primary_key": true,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "data",
      "type": "JSON()",
      "nullable": true,
      "unique": null,
      "doc": "Text field containing JSON properties for this resource",
      "primary_key": false,
      "foreign_key": null,
      "default": "{}"
    },
    {
      "name": "key",
      "type": "String(length=80)",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "path",
      "type": "Text()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "path_thm",
      "type": "Text()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "is_valid",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": "True"
    },
    {
      "name": "deployment_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": "deployment.id",
      "default": null
    },
    {
      "name": "media_type_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": "media_type.id",
      "default": null
    },
    {
      "name": "timestamp_start",
      "type": "DateTime()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    },
    {
      "name": "created_at",
      "type": "DateTime()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    },
    {
      "name": "parent_id",
      "type": "Integer()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": "media.id",
      "default": null
    }
  ],
  "relations": [
    {
      "model": "deployment",
      "key": "deployment",
      "doc": null,
      "direction": "MANYTOONE"
    },
    {
      "model": "media_type",
      "key": "media_type",
      "doc": null,
      "direction": "MANYTOONE"
    },
    {
      "model": "media",
      "key": "parent",
      "doc": null,
      "direction": "MANYTOONE"
    },
    {
      "model": "point",
      "key": "annotations",
      "doc": "List of [point](#point) objects",
      "direction": "ONETOMANY"
    },
    {
      "model": "pose",
      "key": "poses",
      "doc": null,
      "direction": "ONETOMANY"
    },
    {
      "model": "media",
      "key": "children",
      "doc": null,
      "direction": "ONETOMANY"
    },
    {
      "model": "media_collection_media",
      "key": "media_collection_media",
      "doc": null,
      "direction": "ONETOMANY"
    },
    {
      "model": "deployment_event",
      "key": "events",
      "doc": null,
      "direction": "ONETOMANY"
    }
  ],
  "hybrid_properties": [
    {
      "name": "geom",
      "doc": null,
      "args": [],
      "type": "property"
    },
    {
      "name": "is_child",
      "doc": "BOOLEAN, whether or not this has a parent",
      "args": [],
      "type": "property"
    },
    {
      "name": "has_unlabeled_annotations",
      "doc": "BOOL, whether or not the [media object](#media) contains unlabeled annotations from the\n        [annotation_set](annotation_set) with the id `annotation_set_id`",
      "args": [
        "annotation_set_id"
      ],
      "type": "method"
    },
    {
      "name": "has_annotations",
      "doc": "BOOL, whether or not the [media object](#media) contains any annotations from the\n        [annotation_set](annotation_set) with the id `annotation_set_id`",
      "args": [
        "annotation_set_id"
      ],
      "type": "method"
    },
    {
      "name": "distance",
      "doc": "FLOAT, distance (in meters) of closest linked [pose](#pose) relative to `lat` and `lon` (which are both in decimal degrees)",
      "args": [
        "lat",
        "lon"
      ],
      "type": "method"
    },
    {
      "name": "timestamp_proximity",
      "doc": "Absolute difference in seconds of the in-situ timestamp and input `ts`, which is datetime string\n        eg: `\"2020-09-25T00:57:11.945009\"` (or in a format that is parsable by `dateutil.parser.parse`)",
      "args": [
        "ts"
      ],
      "type": "method"
    }
  ],
  "methods": [
    {
      "name": "current_user_can_edit",
      "doc": "Whether or not the current user owns the deployment and can edit this media item",
      "type": "method"
    },
    {
      "name": "event_log",
      "doc": null,
      "type": "method"
    },
    {
      "name": "color",
      "doc": null,
      "type": "property"
    },
    {
      "name": "path_best",
      "doc": null,
      "type": "property"
    },
    {
      "name": "path_best_thm",
      "doc": null,
      "type": "property"
    },
    {
      "name": "pixel_height",
      "doc": "Height of media item in pixels. Not implemented yet...",
      "type": "property"
    },
    {
      "name": "pixel_width",
      "doc": "Width of media item in pixels. Not implemented yet...",
      "type": "property"
    },
    {
      "name": "pose",
      "doc": "\n        The first pose from related `poses` or if child media item, it gets the closest matching pose from the\n        `parent_media` item based on `timestamp_start`\n        ",
      "type": "property"
    },
    {
      "name": "timestamp_start_local",
      "doc": "LOCAL datetime, converted using `pose.lat`, `pose.lon` and `timestamp_start`",
      "type": "property"
    }
  ],
  "association_proxies": [
    {
      "name": "media_collections",
      "model": "media_collection",
      "target_collection": "media_collection_media"
    }
  ]
}
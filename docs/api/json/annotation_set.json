{
  "collection": "annotation_set",
  "endpoints": [
    {
      "endpoint": "/api/annotation_set",
      "method": "POST",
      "preprocessors": [
        {
          "name": "post_auth_preprocessor",
          "doc": "Restrict request to LOGGED IN users only"
        }
      ],
      "postprocessors": [],
      "description": "Create new [annotation_set](#annotation_set) object. Expects JSON. Fields shown in `MODEL COLUMNS` below. "
    },
    {
      "endpoint": "/api/annotation_set/<id>",
      "method": "PATCH",
      "preprocessors": [
        {
          "name": "patch_auth_preprocessor",
          "doc": null
        }
      ],
      "postprocessors": [],
      "description": "Update single [annotation_set](#annotation_set) object with pk field matching param `id`. Expects JSON. Fields shown in `MODEL COLUMNS` below."
    },
    {
      "endpoint": "/api/annotation_set",
      "method": "GET",
      "fields": [
        "id",
        "name",
        "parent_id",
        "description",
        "user",
        "user.id",
        "user.username",
        "user.first_name",
        "user.last_name",
        "created_at",
        "children",
        "media_collection",
        "media_collection.id",
        "media_collection.name",
        "label_scheme",
        "label_scheme.id",
        "label_scheme.name",
        "data",
        "current_user_can_view",
        "current_user_is_owner",
        "current_user_is_member",
        "current_user_can_edit",
        "is_public",
        "is_exemplar",
        "is_full_bio_score",
        "is_real_science",
        "is_qaqc",
        "is_final",
        "annotation_count",
        "usergroup_count"
      ],
      "preprocessors": [
        {
          "name": "get_many_auth_preprocessor",
          "doc": "Assert object view permission from shared `usergroups`. Restricts objects to the ones that current_user can view"
        }
      ],
      "postprocessors": [],
      "description": "Get list of [annotation_set](#annotation_set) objects matching API query. See [Making API search queries](#api_query) for instructions on making search queries"
    },
    {
      "endpoint": "/api/annotation_set/<id>",
      "method": "GET",
      "fields": [
        "id",
        "name",
        "parent_id",
        "description",
        "user",
        "user.id",
        "user.username",
        "user.first_name",
        "user.last_name",
        "created_at",
        "children",
        "media_collection",
        "media_collection.id",
        "media_collection.name",
        "label_scheme",
        "label_scheme.id",
        "label_scheme.name",
        "data",
        "current_user_can_view",
        "current_user_is_owner",
        "current_user_is_member",
        "current_user_can_edit",
        "is_public",
        "is_exemplar",
        "is_full_bio_score",
        "is_real_science",
        "is_qaqc",
        "is_final",
        "annotation_count",
        "usergroup_count",
        "files",
        "files.id",
        "files.name",
        "files.file_url",
        "files.created_at",
        "media_count",
        "point_count",
        "unannotated_point_count"
      ],
      "preprocessors": [
        {
          "name": "get_single_auth_preprocessor",
          "doc": "Assert object view permission from shared `usergroups`. Checks current_user has view permission."
        }
      ],
      "postprocessors": [],
      "description": "Get single [annotation_set](#annotation_set) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/annotation_set/<id>",
      "method": "DELETE",
      "preprocessors": [
        {
          "name": "delete_auth_preprocessor",
          "doc": null
        }
      ],
      "postprocessors": [],
      "description": "Delete single [annotation_set](#annotation_set) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/annotation_set/<int:instance_id>/group/<int:group_id>",
      "method": "POST",
      "description": "\n        Add object with id `instance_id` to group with id `group_id`\n        "
    },
    {
      "endpoint": "/api/annotation_set/<int:instance_id>/group/<int:group_id>",
      "method": "DELETE",
      "description": "Remove resource with id `instance_id` from group with id `group_id`"
    },
    {
      "endpoint": "/api/annotation_set/<int:id>/export",
      "method": "GET",
      "description": "\n        Export [annotations](#annotation) from [annotation_set](#annotation_set) matching `id`.\n        \n        Export [annotations](#annotation) matching query define in `q`.\n        Search queries are executed on the [annotation](#annotation) model. This endpoint also supports translating\n        to a target `label_scheme` (when possible)\n\n        \n        Querystring parameters are:\n\n        ```?q={...}&f={...}&disposition=...&template=...&include_columns=[...]```\n\n        Where `q` is a JSON search query (see [Making API search queries](#api_query)) on the collection model,\n        where `f` is a JSON dictionary containing a sequence of operations for transforming the data\n        (see [Data transformation API](#api_data_transform) for more info),\n        `disposition` defines the download type which can be `attachment` (default, triggers a download) or `inline` (displays in\n        the browser),\n        `template` is optional depending on the output of the steps in `f` and can be used to define the export format,\n        `include_columns` is a JSON list of column fields for the exported model.\n        Nested fields for related models delimited with a `.`\n        If `include_columns` is omitted, all allowed columns will be returned.\n\n        Metadata for the [annotation_set](#annotation_set) matching `id` is also returned in the response. Depending on the output\n        format, this could be as part of the response body in a `metadata` field or in the response headers with the key\n        `X-Content-Metadata`.\n    \n\n        Allowed fields include:\n\n        `id`, `tag_names`, `comment`, `label.id`, `label.name`, `label.lineage_names`, `label.uuid`, `label.color`,\n        `updated_at`, `user.username`, `needs_review`, `likelihood`,\n        `point.id`, `point.x`, `point.y`, `point.t`, `point.data`, `point.media.id`, `point.media.key`,\n        `point.media.path_best`, `point.media.timestamp_start`, `point.media.timestamp_start_local`,\n        `point.media.path_best_thm`, `point.pose.timestamp`, `point.pose.timestamp_local`, `point.pose.lat`,\n        `point.pose.lon`, `point.pose.alt`, `point.pose.dep`, `point.pose.data`, `point.pose.id`,\n        `point.media.deployment.id`, `point.media.deployment.key`, `point.media.deployment.name`,`point.polygon`,\n        `point.media.deployment.campaign.id`, `point.media.deployment.campaign.key`,\n        `point.media.deployment.campaign.name`, `object_id`, `supplementary_label`\n\n        See [annotation](#annotation) model columns, relations and attributes for more information on each of the included fields.\n\n        **TRANSLATING TO OTHER LABEL_SCHEMES**\n\n        This endpoint also supports the translation of labels from a source to a target label_scheme.\n        another label_scheme using the semantic translation framework. In order to do this, you need to specify the\n        additional `translate` url query parameter:\n\n        ```\n        &translate={\"target_label_scheme_id\":..., \"vocab_registry_keys\": ..., \"mapping_override\": ...}\n        ```\n        Where `target_label_scheme_id` [required] is an INT with the `id` of the target label scheme,\n        `vocab_registry_keys` [optional] is a list containing the priority order of keys for `vocab_registries` for\n        which to perform the semantic translation and `mapping_override` defines a set of key-value pairs containing\n        `source label.id : target label.id` for which to override the translation.\n        Note: for extended schemes, labels are translated through `tree_traversal` and no semantic translation is\n        required. With translation parameters you will also need to specify additional columns in the `include_columns`\n        parameter to obtain the translation output.\n\n        Additional allowed columns include:\n\n        `label.translated.id`, `label.translated.name`, `label.translated.lineage_names`,\n        `label.translated.uuid`, `label.translated.translation_info`,\n\n        These columns are ignored if no `translate` query parameter is supplied.\n        \n\n        **EXAMPLES**\n\n        ```\n        # Example1: get all columns as a downloaded CSV file, only point annotations - filter out whole frame labels\n        /api/annotation_set/2194/export?template=dataframe.csv&f={\"operations\":[{\"module\":\"pandas\", \"method\":\"json_normalize\"}]}&q={\"filters\":[{\"name\":\"point\", \"op\":\"has\", \"val\":{\"name\":\"has_xy\", \"op\":\"eq\",\"val\":true}}]}\n\n        # Example2: aggregate label counts per frame (percent cover, no point information) as a downloaded csv\n        /api/annotation_set/2171/export?template=dataframe.csv&include_columns=[\"label.name\", \"label.lineage_names\", \"point.media.key\", \"point.pose.timestamp\", \"point.pose.lat\", \"point.pose.lon\", \"point.pose.alt\", \"point.pose.dep\"]&f={\"operations\":[{\"module\":\"pandas\", \"method\":\"json_normalize\"},{\"method\":\"fillna\", \"kwargs\":{\"value\":\"-\"}},{\"module\":\"pandas\", \"method\":\"count_unstack\", \"kwargs\":{\"columns\":[\"label.id\", \"label.uuid\", \"label.name\", \"label.lineage_names\", \"comment\", \"tag_names\"]}}]}\n\n        # Example3: show selected columns as an HTML table in the browser, filter out unlabeled annotations\n        /api/annotation_set/2171/export?template=dataframe.html&disposition=inline&include_columns=[\"label.name\",\"label.lineage_names\", \"updated_at\", \"point.x\", \"point.y\", \"point.media.key\", \"point.pose.timestamp\", \"point.pose.lat\", \"point.pose.lon\", \"point.pose.alt\", \"point.pose.dep\", \"point.media.deployment.key\", \"point.media.deployment.campaign.key\"]&f={\"operations\":[{\"module\":\"pandas\", \"method\":\"json_normalize\"}]}&q={\"filters\":[{\"name\":\"label_id\", \"op\":\"is_not_null\"}]}\n\n        # Example4: show HTML table in browser with tally of label counts (can be refreshed for readable live summary)\n        /api/annotation_set/2171/export?template=dataframe.html&disposition=inline&include_columns=[\"label.uuid\",\"label.name\",\"label.lineage_names\",\"label.id\"]&f={\"operations\":[{\"module\":\"pandas\",\"method\":\"json_normalize\"},{\"module\":\"pandas\",\"method\":\"groupby_count\"}]}&q={\"filters\":[{\"name\":\"label_id\",\"op\":\"is_not_null\"}]}\n        ```\n\n        NB: the UI contains an \"ADVANCED\" option which can help build more complex export queries.\n        "
    },
    {
      "endpoint": "/api/annotation_set/<int:id>/media",
      "method": "GET",
      "description": null
    },
    {
      "endpoint": "/api/annotation_set/<int:id>/annotations",
      "method": "GET",
      "description": "\n        Returns list of [annotation](#annotation) objects contained in the `annotation_set` with an ID matching `id`.\n        The results can be filtered and searched. See [Making API search queries](#api_query) for instructions on making search queries.\n        In addition it accepts `include_columns` and `include_methods` query parameters which override returned\n        fields of [annotation](#annotation) model.\n        "
    },
    {
      "endpoint": "/api/annotation_set/<int:id>/annotations/<group_by>",
      "method": "GET",
      "description": "\n        **THIS ENDPOINT IS DEPRECATED! USE `/api/annotation/tally/<group_by>` in the [annotation](#annotation) resource**\n\n        Returns lists of the labels used in the `annotation_set` with an ID matching `id`. The `group_by` parameter\n        must one of [`label_updated`, `label_frequency`, `deployment`] which returns the most recently used labels,\n        counts by label (most frequently used) or counts by deployment, respectively.\n        The number of results and page can be controlled with the `results_per_page` and `page` query parameters,\n        respectively.\n        "
    },
    {
      "endpoint": "/api/annotation_set/<int:annotation_set_id>/media/<int:media_id>",
      "method": "POST",
      "description": "\n        Add the [media](#media) object with the ID `media_id` to the [media_collection](#media_collection) of the [annotation_set](#annotation_set) matching `annotation_set_id`\n        "
    },
    {
      "endpoint": "/api/annotation_set/<int:annotation_set_id>/media/<int:media_id>",
      "method": "DELETE",
      "description": "\n        Remove the [media](#media) object with the ID `media_id` from the [media_collection](#media_collection) of the [annotation_set](#annotation_set) matching `annotation_set_id`\n        "
    },
    {
      "endpoint": "/api/annotation_set/<int:id>/clone",
      "method": "POST",
      "description": "\n        Clone the [annotation_set](#annotation_set) matching `id` to create a new one under the same collection.\n        This will clone all annotation_set properties along with all [points](#point), [annotations](#annotation),\n        [tags](#tag), [labels](#labels) including all annotation properties (`needs_review`, `likelihood`, `comment`).\n        **Note:** this does not currently preserve linking between annotations in observation groups.\n\n        This endpoint also supports the translation of labels from a source to a target label_scheme using the semantic\n        translation framework. In order to do this, you need to specify the additional `translate` url query parameter:\n\n        ```\n        &translate={\"target_label_scheme_id\":..., \"vocab_registry_keys\": ..., \"mapping_override\": ...}\n        ```\n        Where `target_label_scheme_id` [required] is an INT with the `id` of the target label scheme,\n        `vocab_registry_keys` [optional] is a list containing the priority order of keys for `vocab_registries` for\n        which to perform the semantic translation and `mapping_override` defines a set of key-value pairs containing\n        `source label.id : target label.id` for which to override the translation.\n        Note: for extended schemes, labels are translated through `tree_traversal` and no semantic translation is\n        required.\n        "
    }
  ],
  "description": "\n    Annotation Set resource: defines how the [media](#media) objects contained in a [media_collection](#media_collection) should be annotated /\n    analysed. Each [media_collection](#media_collection) can have multiple [annotation_sets](#annotation_set) attached to it.\n    ",
  "columns": [
    {
      "name": "id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "Primary key `id`",
      "primary_key": true,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "data",
      "type": "JSON()",
      "nullable": true,
      "unique": null,
      "doc": "Text field containing JSON properties for this resource",
      "primary_key": false,
      "foreign_key": null,
      "default": "{}"
    },
    {
      "name": "name",
      "type": "String(length=80)",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "description",
      "type": "Text()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "media_collection_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": "media_collection.id",
      "default": null
    },
    {
      "name": "label_scheme_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": "label_scheme.id",
      "default": null
    },
    {
      "name": "user_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "Foreign key reference for ID of related owner [users](#users) model",
      "primary_key": false,
      "foreign_key": "users.id",
      "default": null
    },
    {
      "name": "created_at",
      "type": "DateTime()",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    },
    {
      "name": "parent_id",
      "type": "Integer()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": "annotation_set.id",
      "default": null
    },
    {
      "name": "is_exemplar",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": "False"
    },
    {
      "name": "is_full_bio_score",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": "whether or not the intent was full biodiversity scoring, or just targeted scoring",
      "primary_key": false,
      "foreign_key": null,
      "default": "False"
    },
    {
      "name": "is_real_science",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": "whether or not this is for science, or was for education / training / testing",
      "primary_key": false,
      "foreign_key": null,
      "default": "False"
    },
    {
      "name": "is_qaqc",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": "whether or not the annotation set has been QA/QC'd",
      "primary_key": false,
      "foreign_key": null,
      "default": "False"
    },
    {
      "name": "is_final",
      "type": "Boolean()",
      "nullable": true,
      "unique": null,
      "doc": "whether or not the annotator has completed annotating",
      "primary_key": false,
      "foreign_key": null,
      "default": "False"
    },
    {
      "name": "type",
      "type": "String(length=80)",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": "base"
    }
  ],
  "relations": [
    {
      "model": "media_collection",
      "key": "media_collection",
      "doc": null,
      "direction": "MANYTOONE"
    },
    {
      "model": "label_scheme",
      "key": "label_scheme",
      "doc": null,
      "direction": "MANYTOONE"
    },
    {
      "model": "users",
      "key": "user",
      "doc": "Related owner [users](#users) model",
      "direction": "MANYTOONE"
    },
    {
      "model": "annotation_set",
      "key": "parent",
      "doc": null,
      "direction": "MANYTOONE"
    },
    {
      "model": "groups",
      "key": "usergroups",
      "doc": "List of [groups](#groups) objects that contain this resource",
      "direction": "MANYTOMANY"
    },
    {
      "model": "point",
      "key": "points",
      "doc": "List of [point](#point) objects",
      "direction": "ONETOMANY"
    },
    {
      "model": "annotation",
      "key": "annotations",
      "doc": "List of related [annotation](#annotation) objects",
      "direction": "ONETOMANY"
    },
    {
      "model": "annotation_set",
      "key": "children",
      "doc": null,
      "direction": "ONETOMANY"
    },
    {
      "model": "annotation_set_file",
      "key": "files",
      "doc": null,
      "direction": "ONETOMANY"
    }
  ],
  "hybrid_properties": [
    {
      "name": "media_count",
      "doc": "Number of media items in the media_collection",
      "args": [],
      "type": "property"
    },
    {
      "name": "is_child",
      "doc": "BOOLEAN, whether or not this annotation has a linked parent annotation",
      "args": [],
      "type": "property"
    },
    {
      "name": "is_public",
      "doc": "Whether or not this object is public (i.e. shared in a public group)",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_is_owner",
      "doc": "Whether or not the currently logged in user is the owner of this object",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_is_member",
      "doc": "Whether or not the current logged in user is a member of a group that includes this object",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_is_edit_member",
      "doc": "Whether or not the current logged in user is a member with edit permission of a group that includes this object",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_can_view",
      "doc": "Whether or not the current user has permission to view this object",
      "args": [],
      "type": "property"
    },
    {
      "name": "current_user_can_edit",
      "doc": "Whether or not the current logged in user has edit permission (owner or edit_member)",
      "args": [],
      "type": "property"
    }
  ],
  "methods": [
    {
      "name": "annotation_count",
      "doc": null,
      "type": "method"
    },
    {
      "name": "point_count",
      "doc": null,
      "type": "method"
    },
    {
      "name": "unannotated_point_count",
      "doc": null,
      "type": "method"
    },
    {
      "name": "usergroup_count",
      "doc": null,
      "type": "method"
    }
  ],
  "association_proxies": []
}
{
  "collection": "pose",
  "endpoints": [
    {
      "endpoint": "/api/pose",
      "method": "POST",
      "preprocessors": [
        {
          "name": "post_auth_preprocessor",
          "doc": "Restrict request to LOGGED IN users only"
        }
      ],
      "postprocessors": [],
      "description": "Create new [pose](#pose) object. Expects JSON. Fields shown in `MODEL COLUMNS` below. "
    },
    {
      "endpoint": "/api/pose",
      "method": "GET",
      "fields": [
        "id",
        "dep",
        "alt",
        "lat",
        "lon",
        "events",
        "timestamp",
        "data",
        "media",
        "media.key",
        "media.id",
        "media.timestamp_start",
        "media.path_best_thm",
        "deployment",
        "campaign",
        "platform",
        "color",
        "media_path_best_thm"
      ],
      "preprocessors": [
        {
          "name": "get_many_auth_preprocessor",
          "doc": "Allow all requests"
        }
      ],
      "postprocessors": [],
      "description": "Get list of [pose](#pose) objects matching API query. See [Making API search queries](#api_query) for instructions on making search queries"
    },
    {
      "endpoint": "/api/pose/<id>",
      "method": "GET",
      "fields": [
        "id",
        "dep",
        "alt",
        "lat",
        "lon",
        "events",
        "timestamp",
        "data",
        "media",
        "media.key",
        "media.id",
        "media.timestamp_start",
        "media.path_best_thm",
        "deployment",
        "campaign",
        "platform",
        "color",
        "media_path_best_thm"
      ],
      "preprocessors": [
        {
          "name": "get_single_auth_preprocessor",
          "doc": "Allow all requests"
        }
      ],
      "postprocessors": [],
      "description": "Get single [pose](#pose) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/pose/<id>",
      "method": "DELETE",
      "preprocessors": [
        {
          "name": "delete_auth_preprocessor",
          "doc": "Restrict request to OWNER only"
        }
      ],
      "postprocessors": [],
      "description": "Delete single [pose](#pose) object with pk field matching param `id`"
    }
  ],
  "description": "\n    Pose resource: associates nav / sensor data to a [media](#media) item.\n    ",
  "columns": [
    {
      "name": "id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "Primary key `id`",
      "primary_key": true,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "lat",
      "type": "Float(precision=53)",
      "nullable": true,
      "unique": false,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "lon",
      "type": "Float(precision=53)",
      "nullable": true,
      "unique": false,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "alt",
      "type": "Float()",
      "nullable": true,
      "unique": false,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "dep",
      "type": "Float()",
      "nullable": true,
      "unique": false,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "timestamp",
      "type": "DateTime()",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    },
    {
      "name": "media_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": "media.id",
      "default": null
    },
    {
      "name": "geom",
      "type": "Geometry(geometry_type='POINT', srid=4326, from_text='ST_GeomFromEWKT', name='geometry')",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    }
  ],
  "relations": [
    {
      "model": "media",
      "key": "media",
      "doc": null,
      "direction": "MANYTOONE"
    },
    {
      "model": "posedata",
      "key": "_data",
      "doc": null,
      "direction": "ONETOMANY"
    },
    {
      "model": "deployment_event",
      "key": "events",
      "doc": null,
      "direction": "ONETOMANY"
    }
  ],
  "hybrid_properties": [
    {
      "name": "distance",
      "doc": "FLOAT, distance (in meters) to `lat` and `lon` (which are both in decimal degrees)",
      "args": [
        "lat",
        "lng"
      ],
      "type": "method"
    },
    {
      "name": "proximity",
      "doc": null,
      "args": [
        "lat",
        "lon"
      ],
      "type": "method"
    },
    {
      "name": "timestamp_proximity",
      "doc": "Absolute difference in seconds of the in-situ timestamp and input `ts`, which is datetime string\n        eg: `\"2020-09-25T00:57:11.945009\"` (or in a format that is parsable by `dateutil.parser.parse`)",
      "args": [
        "ts"
      ],
      "type": "method"
    }
  ],
  "methods": [
    {
      "name": "campaign",
      "doc": null,
      "type": "property"
    },
    {
      "name": "color",
      "doc": null,
      "type": "property"
    },
    {
      "name": "data",
      "doc": "Properties for related [pose](#pose) item including any linked [posedata](#posedata) fields",
      "type": "property"
    },
    {
      "name": "deployment",
      "doc": null,
      "type": "property"
    },
    {
      "name": "media_path_best_thm",
      "doc": null,
      "type": "property"
    },
    {
      "name": "platform",
      "doc": null,
      "type": "property"
    },
    {
      "name": "timestamp_local",
      "doc": "LOCAL datetime, converted using `lat`, `lon` and `timestamp`. SETTER converts Local to UTC",
      "type": "property"
    }
  ],
  "association_proxies": []
}
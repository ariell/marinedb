{
  "collection": "vocab_element",
  "endpoints": [
    {
      "endpoint": "/api/vocab_element",
      "method": "POST",
      "preprocessors": [
        {
          "name": "post_auth_preprocessor",
          "doc": "Restrict request to LOGGED IN users only"
        }
      ],
      "postprocessors": [],
      "description": "Create new [vocab_element](#vocab_element) object. Expects JSON. Fields shown in `MODEL COLUMNS` below. "
    },
    {
      "endpoint": "/api/vocab_element/<id>",
      "method": "PATCH",
      "preprocessors": [
        {
          "name": "patch_auth_preprocessor",
          "doc": "Restrict request to OWNER only"
        }
      ],
      "postprocessors": [],
      "description": "Update single [vocab_element](#vocab_element) object with pk field matching param `id`. Expects JSON. Fields shown in `MODEL COLUMNS` below."
    },
    {
      "endpoint": "/api/vocab_element",
      "method": "GET",
      "fields": [
        "id",
        "name",
        "key",
        "parent_key",
        "vocab_registry",
        "vocab_registry.key",
        "vocab_registry.vocab_element_key_name",
        "vocab_registry.name",
        "vocab_registry.description",
        "created_at",
        "updated_at",
        "user",
        "user.id",
        "lineage",
        "user.first_name",
        "mapped_registries",
        "user.last_name",
        "other_names",
        "other_names.id",
        "other_names.name",
        "origin_updated_at",
        "reference_url",
        "reference_url_parent"
      ],
      "preprocessors": [
        {
          "name": "get_many_auth_preprocessor",
          "doc": "Allow all requests"
        }
      ],
      "postprocessors": [],
      "description": "Get list of [vocab_element](#vocab_element) objects matching API query. See [Making API search queries](#api_query) for instructions on making search queries"
    },
    {
      "endpoint": "/api/vocab_element/<id>",
      "method": "GET",
      "fields": [
        "id",
        "name",
        "key",
        "parent_key",
        "vocab_registry",
        "vocab_registry.key",
        "vocab_registry.vocab_element_key_name",
        "vocab_registry.name",
        "vocab_registry.description",
        "created_at",
        "updated_at",
        "user",
        "user.id",
        "lineage",
        "user.first_name",
        "mapped_registries",
        "user.last_name",
        "other_names",
        "other_names.id",
        "other_names.name",
        "origin_updated_at",
        "reference_url",
        "reference_url_parent",
        "data",
        "description",
        "mapped_labels"
      ],
      "preprocessors": [
        {
          "name": "get_single_auth_preprocessor",
          "doc": "Allow all requests"
        }
      ],
      "postprocessors": [],
      "description": "Get single [vocab_element](#vocab_element) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/vocab_element/<id>",
      "method": "DELETE",
      "preprocessors": [
        {
          "name": "delete_auth_preprocessor",
          "doc": "Restrict request to OWNER only"
        }
      ],
      "postprocessors": [],
      "description": "Delete single [vocab_element](#vocab_element) object with pk field matching param `id`"
    },
    {
      "endpoint": "/api/vocab_element/<registry_key>/<element_key>",
      "method": "GET",
      "description": null
    },
    {
      "endpoint": "/api/vocab_element/<registry_key>/<element_key>",
      "method": "POST",
      "description": null
    },
    {
      "endpoint": "/api/vocab_element/<registry_key>/<element_key>",
      "method": "PATCH",
      "description": null
    },
    {
      "endpoint": "/api/vocab_element/<registry_key>/<element_key>/label/<label_id>",
      "method": "POST",
      "description": null
    },
    {
      "endpoint": "/api/vocab_element/<registry_key>/<element_key>/label/<label_id>",
      "method": "DELETE",
      "description": null
    },
    {
      "endpoint": "/api/vocab_element/nested",
      "method": "GET",
      "description": null
    }
  ],
  "description": "\n    Vocab Element resource: locally cached version of element from [vocab_registry](#vocab_registry)\n    ",
  "columns": [
    {
      "name": "id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "Primary key `id`",
      "primary_key": true,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "name",
      "type": "String(length=128)",
      "nullable": false,
      "unique": false,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "key",
      "type": "String(length=96)",
      "nullable": false,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "lineage",
      "type": "JSON()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "data",
      "type": "JSON()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "mapped_registries",
      "type": "JSON()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "description",
      "type": "Text()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "parent_key",
      "type": "String(length=96)",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    },
    {
      "name": "vocab_registry_key",
      "type": "String(length=96)",
      "nullable": false,
      "unique": null,
      "doc": "The `id` of the `vocab_registry`",
      "primary_key": false,
      "foreign_key": "vocab_registry.key",
      "default": null
    },
    {
      "name": "user_id",
      "type": "Integer()",
      "nullable": false,
      "unique": null,
      "doc": "The `id` of the user who created and owns this record",
      "primary_key": false,
      "foreign_key": "users.id",
      "default": null
    },
    {
      "name": "created_at",
      "type": "DateTime()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    },
    {
      "name": "updated_at",
      "type": "TIMESTAMP()",
      "nullable": true,
      "unique": null,
      "doc": "Updated time (UTC)",
      "primary_key": false,
      "foreign_key": null,
      "default": "function"
    },
    {
      "name": "origin_updated_at",
      "type": "DateTime()",
      "nullable": true,
      "unique": null,
      "doc": null,
      "primary_key": false,
      "foreign_key": null,
      "default": null
    }
  ],
  "relations": [
    {
      "model": "vocab_registry",
      "key": "vocab_registry",
      "doc": "The `vocab_registry` that contains this `vocab_element`",
      "direction": "MANYTOONE"
    },
    {
      "model": "users",
      "key": "user",
      "doc": "The user who owns or created and owns this record",
      "direction": "MANYTOONE"
    },
    {
      "model": "label",
      "key": "labels",
      "doc": "A list of `labels` linked to this `vocab_element`",
      "direction": "MANYTOMANY"
    },
    {
      "model": "vocab_element_names",
      "key": "other_names",
      "doc": null,
      "direction": "ONETOMANY"
    },
    {
      "model": "label_vocab_element",
      "key": "label_vocab_element",
      "doc": null,
      "direction": "ONETOMANY"
    }
  ],
  "hybrid_properties": [],
  "methods": [
    {
      "name": "mapped_labels",
      "doc": null,
      "type": "method"
    },
    {
      "name": "reference_url",
      "doc": null,
      "type": "method"
    },
    {
      "name": "reference_url_parent",
      "doc": null,
      "type": "method"
    }
  ],
  "association_proxies": []
}
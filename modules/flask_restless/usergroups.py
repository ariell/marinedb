import json

from flask import current_app, jsonify, request
from flask_login import current_user
from sqlalchemy import inspect
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import backref

from marinedb.database import ReferenceCol, relationship
from marinedb.extensions import db
from marinedb.user.models import Group, UserGroupAssociation
from marinedb.user.api import GroupAPI
from modules.flask_restless import ProcessingException
from modules.flask_restless.helpers import to_dict, get_relations, count
from modules.flask_restless.mixins import APIModelEndpoint, assert_authenticated_user
from modules.flask_restless.search import search
from modules.flask_restless.views import render_data_response, _parse_includes

# def current_user_id():
#     if current_user.

class APIUserGroupMixin(object):
    """
    # query to list instances in group
    q={"filters":[{"name":"usergroups","op":"any","val":{"name":"id","op":"eq","val":6}}]}
    """
    #__table_args__ = {'extend_existing': True}
    _group_association_table = None
    allow_anonymous_viewing = True

    # def __init__(self):
    #     pass

    @declared_attr
    def user_id(cls):
        """Foreign key reference for ID of related owner [users](#users) model"""
        return ReferenceCol('users', nullable=True)

    @declared_attr
    def user(cls):
        """Related owner [users](#users) model"""
        return relationship('User')  # , backref='campaigns')

    @declared_attr
    def usergroups(cls):
        """List of [groups](#groups) objects that contain this resource"""
        association_table, column = cls._get_group_association_table()
        return relationship(
            'Group',
            secondary=association_table,
            lazy='dynamic',
            backref=backref(cls._backrefname(), lazy="dynamic", enable_typechecks=False),
            enable_typechecks=False
        )

    # @staticmethod
    # def get_preprocessors():
    #     return dict(
    #         GET_SINGLE=["get_single_auth_preprocessor"],
    #         GET_MANY=["get_many_auth_preprocessor"],
    #         PATCH_SINGLE=["patch_auth_preprocessor"],
    #         PATCH_MANY=["patch_many_auth_preprocessor"],
    #         DELETE_SINGLE=["delete_auth_preprocessor"],
    #         DELETE_MANY=["delete_many_auth_preprocessor"]
    #     )

    @classmethod
    def _table_name(cls):
        return getattr(cls, '__tablename__')

    @classmethod
    def _backrefname(cls):
        return "shared_"+cls._table_name()+"s"

    @classmethod
    def _get_group_association_table(cls):
        # table_name = getattr(cls, '__tablename__')
        column_name = '%s_id' % cls._table_name()
        if cls._group_association_table is None:
            cls._group_association_table = db.Table(
                "usergroups_%s" % cls._table_name(),
                db.Column('group_id', db.Integer, db.ForeignKey('groups.id'), index=True),
                db.Column(column_name, db.Integer, db.ForeignKey('%s.id' % cls._table_name()), index=True),
                db.PrimaryKeyConstraint('%s_id' % cls._table_name(), 'group_id')
            )
            # with current_app.app_context():
            #     GroupType.get_or_create(commit=True, name=cls._table_name())
        column = getattr(cls._group_association_table.c, column_name)

        return cls._group_association_table, column

    @classmethod
    def get_single_auth_preprocessor(cls, instance_id=None, instance=None, **kw):
        """Assert object view permission from shared `usergroups`. Checks current_user has view permission."""
        if instance is None:
            instance = cls.get_by_id(instance_id)
        if instance is None:
            raise ProcessingException(description="Record not found", code=404)
        if not instance.current_user_can_view:
            raise ProcessingException(description="Unauthorised. You don't have permission to view this {}"
                                      .format(cls._table_name()), code=401)
        # return instance

    @classmethod
    def get_many_auth_preprocessor(cls, search_params, **kw):
        """Assert object view permission from shared `usergroups`. Restricts objects to the ones that current_user can view"""
        if not current_user.is_authenticated() or not current_user.is_admin:
            cls._append_filter(search_params, {"name": "current_user_can_view", "op": "eq", "val": True})
        # print(search_params)

    @classmethod
    def patch_auth_preprocessor(cls, instance_id=None, instance=None, data=None, **kw):
        """Assert object edit permission from shared `usergroups`. Checks current_user has edit permission."""
        if instance is None:
            instance = cls.get_by_id(instance_id)
        if instance is None:
            raise ProcessingException(description="Record not found", code=404)
        if not instance.current_user_can_edit:
            raise ProcessingException(description="Unauthorised. You don't have permission to edit this {}"
                                      .format(cls._table_name()), code=401)

    @classmethod
    def patch_many_auth_preprocessor(cls, search_params, **kw):
        """Assert object edit permission from shared `usergroups`. Restricts objects to the ones that current_user can edit"""
        cls._append_filter(search_params, {"name": "current_user_can_edit", "op": "eq", "val": True})

    @classmethod
    def delete_auth_preprocessor(cls, instance_id=None, instance=None, **kw):
        """Assert object delete permission from shared `usergroups`. Checks current user is owner and object is not
        shared in a `usergroup`"""
        if instance is None:
            instance = cls.get_by_id(instance_id)
        if instance is None:
            raise ProcessingException(description="Record not found", code=404)
        if not instance.current_user_is_owner:
            raise ProcessingException(description="Unauthorised. You don't have permission to delete this {}"
                                      .format(cls._table_name()), code=401)
        if instance.usergroup_count() > 0:
            raise ProcessingException(description="Cannot delete shared dataset. This {} is shared in {} Group(s). You need to unshare it before deleting."
                                      .format(cls._table_name(), instance.usergroup_count()), code=400)

    @classmethod
    def delete_many_auth_preprocessor(cls, search_params, **kw):
        cls._append_filter(search_params, {"name": "current_user_is_owner", "op": "eq", "val": True})

    # @classmethod
    # def post_auth_preprocessor(cls, data=None, **kw):
    #     if not cls.get_by_id(instance_id).current_user_can_edit:
    #         raise ProcessingException(description="You do not have access to edit this record", code=401)

    @classmethod
    def build_api_endpoints(cls):
        return [
            APIModelEndpoint("add_to_group_api", endpoint='{tablename}/<int:instance_id>/group/<int:group_id>', methods=['POST']),
            APIModelEndpoint("remove_from_group_api", endpoint='{tablename}/<int:instance_id>/group/<int:group_id>', methods=['DELETE']),
            # APIModelEndpoint("get_resource_groups_api", endpoint='{tablename}/groups', methods=['GET'], defaults=dict(instance_id=None)),
            # APIModelEndpoint("get_resource_groups_api", endpoint='{tablename}/<int:instance_id>/groups', methods=['GET'])
        ]

    # @classmethod
    # def get_resource_groups_api(cls, instance_id=None):
    #     """
    #     Get all [groups](#groups) that contain this resource or collection of resources.
    #     """
    #     if instance_id is None:
    #         # Only groups that current user has access to and this object type has been shared with
    #         # Filter using standard query parameters for models
    #         q = json.loads(request.args.get("q", "{}") or "{}")
    #         if current_user.is_authenticated():
    #             filts = db.or_(GroupAPI.current_user_is_owner.is_(True), GroupAPI.current_user_is_member.is_(True),
    #                         GroupAPI.is_public.is_(True))
    #         else:
    #             filts = GroupAPI.is_public.is_(True)
    #         grps = search(db.session, GroupAPI, q).join(cls._group_association_table).filter(filts)
    #         # grps = db.session.query(GroupAPI).join(cls._group_association_table) \
    #         #     .filter(or_(GroupAPI.current_user_is_owner.is_(True), GroupAPI.current_user_is_member.is_(True),
    #         #                 GroupAPI.is_public.is_(True)))
    #
    #         response = GroupAPI.paginated_results(grps.all())  # Need to pass in list (.all()) rather than query because for some reason the count is wrong with a query
    #         # TODO: work out why count is wrong with query: response = GroupAPI.paginated_results(grps)
    #     else:
    #         # all groups that this is in (whether or not current user is member)
    #         isin = db.session.query(GroupAPI).join(cls._group_association_table).join(cls).filter(cls.id == instance_id)
    #         ref_col = getattr(GroupAPI, cls._backrefname())
    #         notin = db.session.query(GroupAPI).filter(db.not_(ref_col.any(cls.id == instance_id)),
    #             db.or_(GroupAPI.current_user_can_share_data == True, GroupAPI.current_user_is_owner == True))
    #         response = dict(groups_in=[gi.instance_to_dict() for gi in isin], groups_out=[go.instance_to_dict() for go in notin],
    #                         **cls.get_by_id(instance_id).instance_to_dict())
    #     return render_data_response(response)

    @classmethod
    def add_to_group_api(cls, instance_id, group_id, **kw):
        """Add object with id `instance_id` to group with id `group_id`"""
        current_app.logger.debug("add_to_group_api: {} instance_id:{} group_id:{} current_user_id:{}"
                                 .format(cls._table_name(), instance_id, group_id, current_user.id))
        assert_authenticated_user()
        instance = cls.get_by_id(instance_id)
        instance.add_to_group(group_id, added_by_user_id=current_user.id)
        return jsonify(instance_id=instance_id, group_id=group_id, resource=cls._table_name())

    @classmethod
    def remove_from_group_api(cls, instance_id, group_id):
        """Remove resource with id `instance_id` from group with id `group_id`"""
        assert_authenticated_user()
        instance = cls.get_by_id(instance_id)
        instance.remove_from_group(group_id, removed_by_user_id=current_user.id)
        return jsonify()

    def add_to_group(self, group_id, added_by_user_id=None, commit=True):
        if added_by_user_id is None:
            added_by_user_id = current_user.id

        if self.usergroups.filter(Group.id == group_id).count() > 0:
            raise ProcessingException(description="Already in group. Cannot re-add.", code=409)
        # TODO revise sharing policy - can non-owners share if they have edit access?
        if not self.user_is_owner(added_by_user_id):
            raise ProcessingException(description="Unauthorised. You are not the owner of this dataset. Only the owner "
                                                  "can share it to a group. You may be able to add members to the "
                                                  "current group (if you are a member with rights).", code=401)
        group = Group.get_by_id(group_id)
        if not group.user_can_share_data(added_by_user_id):
            raise ProcessingException(description="Unauthorised. You do not have permission to share data "
                                                  "to this group. Contact the Custodian.", code=401)

        try:
            self.usergroups.append(group)
            if commit:
                db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            raise ProcessingException(description="Error adding to group: {}.".format(e), code=500)

    def remove_from_group(self, group_id, removed_by_user_id=None, commit=True):
        if removed_by_user_id is None:
            removed_by_user_id = current_user.id
        if self.usergroups.filter(Group.id == group_id).count() <= 0:
            raise ProcessingException(description="Not in group. Cannot remove something that does not exist!", code=404)
        group = Group.get_by_id(group_id)
        # TODO revise sharing policy - can non-owners share if they have edit access?
        if not group.user_is_owner(removed_by_user_id):
            if not self.user_is_owner(removed_by_user_id):
                raise ProcessingException(description="Unauthorised. You are not the owner of this item. Only the "
                                                      "owner or group custodian can remove this item from a group", code=401)
        try:
            group = Group.get_by_id(group_id)
            self.usergroups.remove(group)
            if commit:
                db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            raise ProcessingException(description="Error adding to group: {}.".format(e), code=500)

    # @classmethod
    # def share(cls, instance_id, group_id, user_id=None):
    #     instance = cls.get_by_id(instance_id)
    #     group = Group.get_by_id(group_id)
    #     instance.add_to_group(group_id, commit=False)
    #     group.add_user(user_id, commit=False)
    #     db.session.commit()

    def is_in_group(self, group_id):
        return db.session.query(self.usergroups.filter(Group.id == group_id).exists()).scalar()

    def user_is_member(self, user_id):
        return db.session.query(self.usergroups.filter(
            UserGroupAssociation.group_id == Group.id, UserGroupAssociation.approved.is_(True),
            UserGroupAssociation.user_id == user_id).exists()).scalar()

    def user_is_group_owner(self, user_id):
        return db.session.query(self.usergroups.filter(Group.user_id == user_id).exists()).scalar()

    def user_is_edit_member(self, user_id):
        return db.session.query(self.usergroups.filter(
            UserGroupAssociation.group_id == Group.id, UserGroupAssociation.approved.is_(True),
            UserGroupAssociation.can_edit.is_(True), UserGroupAssociation.user_id == user_id).exists()).scalar()

    def user_can_view(self, user_id):
        """Returns true is user is owner, dataset belongs to a public group, user owns a group or dataset belongs to an
        unrestricted group that user is a member of"""
        return self.user_is_owner(user_id) or self.is_public or self.user_is_group_owner(user_id) or db.session.query(
            self.usergroups.filter(
                db.not_(db.and_(Group.requires_agreement.is_(True), UserGroupAssociation.has_agreed.is_(False))),
                UserGroupAssociation.user_id == user_id, UserGroupAssociation.approved.is_(True),
                Group.is_restricted.is_(False), UserGroupAssociation.group_id == Group.id).exists()).scalar()

    def user_is_owner(self, user_id):
        return user_id == self.user_id

    def usergroup_count(self):
        return self.usergroups.count()

    @hybrid_property
    def is_public(self):
        """Whether or not this object is public (i.e. shared in a public group)"""
        q = self.usergroups.filter(Group.is_public.is_(True)).limit(1).with_entities(Group.is_public)
        return q.scalar() or False

    @is_public.expression
    def is_public(cls):
        """
        q={"filters":[{"name":"is_public","op":"eq","val":true}]}
        q={"order_by":[{"field":"is_public","direction":"desc"}]}
        :return:
        """
        return cls.usergroups.any(Group.is_public.is_(True))

    # Add apifield for current user denoting whether or not they own the group
    @hybrid_property
    def current_user_is_owner(self):
        """Whether or not the currently logged in user is the owner of this object"""
        return self.user_is_owner(current_user.id) \
            if current_user.is_authenticated() \
            else False

    @current_user_is_owner.expression
    def current_user_is_owner(cls):
        """
        q={"filters":[{"name":"current_user_is_owner","op":"eq","val":true}]}
        q={"order_by":[{"field":"current_user_is_owner","direction":"desc"}]}
        :return:
        """
        primary_key = inspect(cls).primary_key[0]
        if current_user.is_authenticated():
            subquery = db.session.query(primary_key).filter(cls.user_id == current_user.id)
            return primary_key.in_(subquery)
        else:
            return False

    @hybrid_property
    def current_user_is_member(self):
        """Whether or not the current logged in user is a member of a group that includes this object"""
        return self.user_is_member(current_user.id) or self.user_is_group_owner(current_user.id) \
            if current_user.is_authenticated() else False

    @current_user_is_member.expression
    def current_user_is_member(cls):
        """
        """
        if current_user.is_authenticated():
            # subquery to check if current user is approved member or owner
            primary_key = inspect(cls).primary_key[0]
            subquery = db.session.query(primary_key).join(cls._group_association_table, Group) \
                .filter(db.or_(db.and_(UserGroupAssociation.group_id == Group.id, UserGroupAssociation.user_id == current_user.id,
                        UserGroupAssociation.approved.is_(True)), Group.user_id == current_user.id))
            return primary_key.in_(subquery)
        else:
            return False

    @hybrid_property
    def current_user_is_edit_member(self):
        """Whether or not the current logged in user is a member with edit permission of a group that includes this object"""
        return self.user_is_edit_member(current_user.id) or self.user_is_group_owner(current_user.id) \
            if current_user.is_authenticated() else False

    @current_user_is_edit_member.expression
    def current_user_is_edit_member(cls):
        """
        """
        if current_user.is_authenticated():
            # subquery to check if current user is approved member or owner
            primary_key = inspect(cls).primary_key[0]
            subquery = db.session.query(primary_key).join(cls._group_association_table, Group).filter(
                db.or_(db.and_(
                    UserGroupAssociation.group_id == Group.id, UserGroupAssociation.user_id == current_user.id,
                    UserGroupAssociation.approved.is_(True), UserGroupAssociation.can_edit.is_(True)
                ), Group.user_id == current_user.id ))
            return primary_key.in_(subquery)
        else:
            return False

    @hybrid_property
    def current_user_can_view(self):
        """Whether or not the current user has permission to view this object"""
        # admin can view everything
        return current_user.is_admin or self.user_can_view(current_user.id)  \
            if current_user.is_authenticated() \
            else self.is_public if self.allow_anonymous_viewing else False
        # return False

    @current_user_can_view.expression
    def current_user_can_view(cls):
        """
        Return true if user is an APPROVED group member or the item owner or it is part of a public group

        q={"filters":[{"name":"current_user_can_view","op":"eq","val":true}]}
        q={"order_by":[{"field":"current_user_can_view","direction":"desc"}]}
        :return:
        """
        # # admin can view everything
        # return current_user.is_admin or db.or_(cls.is_public, cls.current_user_is_owner, cls.current_user_is_member) \
        #     if current_user.is_authenticated() \
        #     else cls.is_public if cls.allow_anonymous_viewing else False

        if current_user.is_authenticated():
            # subquery to check if current user is approved member or owner
            primary_key = inspect(cls).primary_key[0]
            subquery = db.session.query(primary_key).join(cls._group_association_table, Group).filter(db.or_(db.and_(
                db.not_(db.and_(Group.requires_agreement.is_(True), UserGroupAssociation.has_agreed.is_(False))),
                UserGroupAssociation.group_id == Group.id, UserGroupAssociation.user_id == current_user.id,
                UserGroupAssociation.approved.is_(True), Group.is_restricted.is_(False)
                ), Group.user_id == current_user.id))
            return db.or_(current_user.is_admin, cls.current_user_is_owner, cls.is_public, primary_key.in_(subquery))
        else:
            return cls.is_public if cls.allow_anonymous_viewing else False

    @hybrid_property
    def current_user_can_edit(self):
        """Whether or not the current logged in user has edit permission (owner or edit_member)"""
        return self.current_user_is_owner or (self.current_user_is_edit_member and self.current_user_can_view) \
            if current_user.is_authenticated() \
            else False

    @current_user_can_edit.expression
    def current_user_can_edit(cls):
        """
        Return true if user is an APPROVED group member with edit permissions or the item owner or the item

        q={"filters":[{"name":"current_user_can_edit","op":"eq","val":true}]}
        q={"order_by":[{"field":"current_user_can_edit","direction":"desc"}]}
        :return:
        """
        return db.or_(cls.current_user_is_owner, db.and_(cls.current_user_is_edit_member, cls.current_user_can_view)) \
            if current_user.is_authenticated() \
            else False
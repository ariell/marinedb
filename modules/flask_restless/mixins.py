
import hashlib
import json

import os

import math
from inspect import getfullargspec
from time import time

from flask import current_app, request, jsonify, g
from flask_login import current_user
from sqlalchemy import inspect
from sqlalchemy.exc import DataError, IntegrityError, ProgrammingError
from sqlalchemy.ext.associationproxy import AssociationProxy, ObjectAssociationProxyInstance
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.orm import Query
from sqlalchemy.orm.exc import NoResultFound
from werkzeug.contrib.cache import FileSystemCache

# from marinedb.user.utils import assert_authenticated_user
from marinedb.extensions import db
from modules.flask_restless import ProcessingException
from savalidation import ValidationMixin, ValidationError

# wrapper that populates an API_REGISTER for generating help documentation
from modules.flask_restless.helpers import get_relations, to_dict, primary_key_names
from modules.flask_restless.search import search
from modules.flask_restless.views import create_exception_response, _parse_includes, extract_error_messages

# def get_class_that_defined_method(meth):
#     for cls in inspect.getmro(meth.im_class):
#         if meth.__name__ in cls.__dict__:
#             return cls
#     return None
#
#
# def api_custom_endpoint(endpoint, cls, methods=["GET"], defaults=None):
#     print("DECORATOR 1")
#     def api_custom_endpoint_decorator(func):
#         print("DECORATOR 2")
#         m = importlib.import_module(cls)
#         print(m)
#         m.api_custom_endpoints.append(
#             APIModelEndpoint(func, model=None, apiprefix="/api", endpoint=endpoint, methods=methods, defaults=defaults))
#         def func_wrapper(cls, *args, **kwargs):
#             print("DECORATOR 3")
#             func(cls, *args, **kwargs)
#         return func_wrapper
#     return api_custom_endpoint_decorator


# def api_request_processor(resource, request_type, procesor_type="preprocessor"):
#     print("DECORATOR 1")
#     assert procesor_type in ["preprocessor", "postprocessor"]
#     assert request_type in ["GET_SINGLE", "GET_MANY", "POST", "DELETE", "PATCH_SINGLE", "PATCH_MANY"], \
#         "Invalid request type for preprocessor"
#
#     def api_request_processor_decorator(func):
#         print("DECORATOR 2")
#         if resource not in api_processor_register[procesor_type]:
#             api_processor_register[procesor_type][resource] = {}
#         if request_type not in api_processor_register[procesor_type][resource]:
#             api_processor_register[procesor_type][resource][request_type] = []
#         api_processor_register[procesor_type][resource][request_type].append(func)
#
#         def func_wrapper(cls, *args, **kwargs):
#             print("DECORATOR 3")
#             func(cls, *args, **kwargs)
#         return func_wrapper
#     return api_request_processor_decorator
#
#
# api_processor_register = {
#     "preprocessor": {},
#     "postprocessor": {}
# }

_api_module_register = []


def register_api(apimanager, app, build_docs=False):
    for m in _api_module_register:
        m.register_api(apimanager, app, build_docs=build_docs)


def add_to_api(m):
    if m not in _api_module_register:
        _api_module_register.append(m)


def assert_authenticated_user():
    if not current_user.is_authenticated():
        current_app.logger.debug("User is not authenticated!")
        raise ProcessingException(description='You are not logged in!', code=401)


def catch_api_request_exceptions(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ProcessingException as e:
            message = e.description
            current_app.logger.exception(message)
            return create_exception_response(message, e.code)
        except (DataError, IntegrityError, ProgrammingError, AssertionError) as e:
            db.session.rollback()
            message = "{}: {}".format(type(e).__name__, e)
            current_app.logger.exception(str(e))
            return create_exception_response(message, code=400)
        except NoResultFound as e:
            message = "No result found"
            current_app.logger.exception(message)
            return create_exception_response(message, code=404)
        except ValidationError as e:
            db.session.rollback()
            error_messages = extract_error_messages(e)
            errors = dict(error_messages) if error_messages else 'Could not determine specific validation errors'
            return jsonify(validation_errors=errors, message="Please correct the validation errors."), 400
        except Exception as e:
            message = "{}: {}".format(type(e).__name__, e)
            current_app.logger.exception(message)
            return create_exception_response(message, code=500)

    return func_wrapper


class APIModel():

    # Key-function pairs containing custom endpoint methods
    api_build_restless_endpoints = True
    api_restless_methods = ['GET']  # , 'POST', 'DELETE', 'PATCH'

    api_custom_endpoints = []  # This defines whether to use a custom method, otherwise if None use restless API model

    # Optional for restless (or custom method)
    api_collection_name = None
    api_url_prefix = '/api'
    api_exclude_columns = None
    api_include_methods = None
    api_include_columns = None
    api_add_columns_single=None
    api_add_methods_single=None
    api_max_results_per_page = 1000
    api_results_per_page = 10
    api_allow_patch_many = False
    api_allow_delete_many = False
    api_allow_functions = False  # eg: GET /api/eval/person?q={"functions": [{"name": "count", "field": "id"}]}
    api_validation_exceptions = [ValidationError]
    api_preprocessors = {}
    api_postprocessors = {}
    api_post_form_preprocessor = None
    api_allow_anonymous_post = False
    api_allow_anonymous_patch = False
    api_allow_nonowner_delete = False

    api_cache_threshold = None  # no. items to cache - set to None/False to ignore cache for this model
    _cache = None

    # api_primary_key = None

    # # Handle validation #######################
    # val.validates_constraints()

    # def __init__(self):
    #     pass

    def instance_to_dict(self, is_single=True, **kw):
        cls = self.__class__
        return cls.model_to_dict(self, is_single=is_single, **kw)

    @classmethod
    def model_to_dict(cls, instance, is_single=True, include_columns=None, include_methods=None, **kw):
        """

        :param instance:
        :param is_single: whether or not to default single processors for response
        :param include_columns: list of strings | False > empty | None > default
        :param include_methods: list of strings | False > empty | None > default
        :param kw:
        :return:
        """
        relations = frozenset(get_relations(cls))

        if include_columns is None:
            include_columns = (cls.api_include_columns or []) + ((cls.api_add_columns_single or []) if is_single else [])
        elif include_columns is False:
            include_columns = []
        if include_methods is None:
            include_methods = (cls.api_include_methods or []) + ((cls.api_add_methods_single or []) if is_single else [])
        elif include_methods is False:
            include_methods = []

        # include_columns = include_columns or ([] if include_columns is False else cls.api_include_columns) # + (cls.api_add_columns_single or []) if run_get_single_processors else [])
        # methods = include_methods or ([] if include_methods is False else cls.api_include_methods) # + (cls.api_add_methods_single or []) if run_get_single_processors else [])

        cols, rels = _parse_includes(include_columns)
        relations &= (frozenset(cols) | frozenset(rels))
        deep = dict((r, {}) for r in relations)
        result = to_dict(instance, deep=deep, include=cols, include_relations=rels, include_methods=include_methods)
        if is_single:
            for postprocessor in cls._get_classmethods_from_string(cls._get_postrocessors().get('GET_SINGLE', [])):
                postprocessor(result=result, instance=instance)
        return result

    @classmethod
    def paginated_results(cls, results, convert_to_dicts=True, max_results_per_page=None, default_results_per_page=None, **kw):
        if isinstance(results, list):
            num_results = len(results)
        elif isinstance(results, Query):
            num_results = results.count()  #count(db.session, results)
        else:
            raise ProcessingException("Unknown results object type. Expects `list` or `Query`")
        results_per_page = min(
            max_results_per_page or cls.api_max_results_per_page,
            int(request.args.get('results_per_page', default_results_per_page or cls.api_results_per_page))
        )
        total_pages = int(math.ceil(float(num_results) / float(results_per_page)))
        page_num = min(total_pages, int(request.args.get('page', 1)))   # get the page number (first page is page 1)
        start = max(0, (page_num - 1) * results_per_page)
        end = min(num_results, start + results_per_page)
        if isinstance(results, list):
            objects = results[start:end]
        elif isinstance(results, Query):
            objects = results.limit(results_per_page).offset(start)   # is this better than just results[start:end] on the query? Seems similar...
        if convert_to_dicts:
            objects = [cls.model_to_dict(x, is_single=False, **kw) for x in objects]
        return dict(page=page_num, objects=objects, total_pages=total_pages, num_results=num_results)

    @classmethod
    def get_search_results(cls, filters=None, allow_qs_includes=False, paginate_results=True, convert_to_dicts=True, **kw):
        args = request.args
        q = json.loads(args.get("q", "{}") or "{}")
        query = search(db.session, cls, q)
        if filters is not None:
            query = query.filter(filters)
        if allow_qs_includes:   # get from QS, but limit to what is allowable through API definition
            allowed_columns = set((cls.api_include_columns or []) + (cls.api_add_columns_single or []))
            #allowed_methods = set((cls.api_include_methods or []) + (cls.api_add_methods_single or []))
            include_columns = list(set(json.loads(args.get("include_columns", "[]")) or args.getlist("include_column")) & allowed_columns)
            # include_methods = list(set(json.loads(args.get("include_methods", "[]")) or args.getlist("include_method")) & allowed_methods)
            include_methods = json.loads(args.get("include_methods", "[]")) or args.getlist("include_method")
        else:
            include_columns, include_methods = None, None
        if paginate_results:
            return cls.paginated_results(query, include_columns=include_columns, include_methods=include_methods, convert_to_dicts=convert_to_dicts, **kw)
        elif convert_to_dicts:
            return [cls.model_to_dict(x, is_single=False, include_columns=include_columns, include_methods=include_methods, **kw) for x in query]
        else:
            return query


    @classmethod
    def _clear_cache(cls, **kw):
        """Clears the request cache object for this resource (to cause cache refresh on next request)"""
        if cls._cache is not None:
            cls._cache.clear()

    @staticmethod
    def _cache_key():
        return hashlib.md5("{}{}".format(request.path, request.query_string).encode('utf-8')).hexdigest()

    @classmethod
    def get_cached_data(cls):
        cachekey = cls._cache_key()
        if cls._cache.get(cachekey):
            return cls._cache.get(cachekey)
        return None

    @classmethod
    def request_cache(cls, cache_name, key, *value):
        """

        @param cache_name:
        @param key:
        @param value:
        @return: value, is_cached (bool)
        """
        _cache = getattr(g, cache_name, None)
        if _cache is None:
            _cache = dict()
            setattr(g, cache_name, _cache)
        if value:
            _cache[key] = value[0]
        if key in _cache:
            return _cache.get(key), True
        return None, False

    @classmethod
    def set_cached_data(cls, data=None, timeout=None):  # cache with no timeout
        cachekey = cls._cache_key()
        cls._cache.set(cachekey, data, timeout)

    @classmethod
    def append_processor(cls, processors, key, class_method_name):
        # if hasattr(cls, class_method_name):
        #     if key not in processors:
        #         processors[key] = []
        #     if class_method_name not in processors[key]:
        #         processors[key].append(class_method_name)
        if key not in processors:
            processors[key] = []
        if class_method_name not in processors[key]:
            processors[key].append(class_method_name)
        return processors

    # def add_api_endpoint(self, clsmethod, model=None, apiprefix="/api", endpoint="{tablename}", methods=["GET"], defaults=None):
    #     self.api_custom_endpoints.append(
    #         APIModelEndpoint(clsmethod, model=model, apiprefix=apiprefix,
    #                          endpoint=endpoint, methods=methods, defaults=defaults))

    @classmethod
    def _get_preprocessors(cls):
        # resource = getattr(cls, '__tablename__')
        # return api_processor_register["preprocessor"].get(resource, {})
        return cls.api_preprocessors.copy()

    @classmethod
    def _get_postrocessors(cls):
        # resource = getattr(cls, '__tablename__')
        # return api_processor_register["postprocessor"].get(resource, {})
        return cls.api_postprocessors.copy()

    @classmethod
    def _get_classmethods_from_string(cls, methods):
        return [f if callable(f) else getattr(cls, f) for f in methods]

    @staticmethod
    def _append_filter(search_params, filt):
        if "filters" not in search_params:
            search_params["filters"] = []
        search_params["filters"].append(filt)

    @classmethod
    def assert_user_login(cls, *args, **kw):
        """Restrict request to logged in users only"""
        assert_authenticated_user()

    @classmethod
    def assert_user_is_owner(cls, instance_id=None, *args, **kw):
        """Restrict request to object OWNER only"""
        assert_authenticated_user()
        if hasattr(cls, "user_id"):
            if getattr(cls, "get_by_id")(instance_id).user_id == current_user.id:
                return
        raise ProcessingException(description="Unauthorised. Only the object creator/owner can do this.", code=401)



    @classmethod
    def assert_current_user_is_admin(cls, *args, **kw):
        """Restrict request to ADMIN user only"""
        if not current_user.is_authenticated() or not current_user.is_admin:
            raise ProcessingException(description="Unauthorised. Only admin users can do this.", code=401)

    @classmethod
    def patch_auth_preprocessor(cls, instance_id=None, instance=None, data=None, *args, **kw):
        """Restrict request to OWNER only"""
        cls.assert_user_is_owner(instance_id=instance_id, *args, **kw)

    @classmethod
    def patch_many_auth_preprocessor(cls, search_params=None, data=None, *args, **kw):
        """Not implemented - needs to be implemented on model"""
        raise NotImplemented("Unauthorised. PATCH_MANY preprocessor is not implemented for this resource")

    @classmethod
    def delete_auth_preprocessor(cls, instance_id=None, *args, **kw):
        """Restrict request to OWNER only"""
        cls.assert_user_is_owner(instance_id=instance_id, *args, **kw)

    @classmethod
    def delete_many_auth_preprocessor(cls, instance_id=None, *args, **kw):
        """Not implemented - needs to be implemented on model"""
        raise NotImplemented("Unauthorised. DELETE_MANY preprocessor is not implemented for this resource")

    @classmethod
    def post_auth_preprocessor(cls, data, *args, **kw):
        """Restrict request to LOGGED IN users only"""
        cls.assert_user_login()
        # Check if model has user field and that it is set, otherwise set to current user
        if hasattr(cls, "user_id") and "user_id" not in data:
            data["user_id"] = current_user.id

    @classmethod
    def get_single_auth_preprocessor(cls, instance_id=None, **kw):
        """Allow all requests"""
        pass

    @classmethod
    def get_many_auth_preprocessor(cls, search_params, **kw):
        """Allow all requests"""
        pass

    # @classmethod
    # def get_many_search_preprocessor(cls, search_params, **kw):
    #
    #
    #     fields = request.args.getlist("f")
    #     search_terms = request.args.get("search_term", "").split()
    #     filters = []
    #     for f_str in fields:
    #         filt_search = {"or": []}
    #         for term in search_terms:
    #             f_list = f_str.split(".")
    #             filt = dict(name=f_list[0], val=None, op=None)
    #             o = getattr(cls, f_list[0])
    #             filt_search["or"].append(filt)
    #             if len(f_list) > 1:
    #                 for f in f_list[1:]:
    #                     filt["val"] = dict(name=f, val=None, op=None)
    #                     filt["op"] = "any" if getattr(o, f).property.uselist else "has"
    #                     filt["val"] = dict(name=f_list[0], val=None, op=None)
    #                     filt = filt["val"]
    #             filt["val"] = term
    #             filt["op"] = "ilike"
    #         filters.appen(filt_search)
    #     return filters

    # @classmethod
    # def get_single_add_fields_postprocessor(cls, result=None, instance=None, **kw):
    #     """Add extra fields to the `GET_SINGLE` response"""
    #     if instance is not None and cls.api_add_columns_single:
    #         for field_string in cls.api_add_columns_single:
    #             fields = field_string.split(".")
    #             result_field = result
    #             instance_field = instance
    #             for f in fields[:-1]:
    #                 if f not in result_field:
    #                     result_field[f] = {}
    #                 instance_field = getattr(instance_field, f, None)
    #             instance_value = getattr(instance_field, fields[-1], None)
    #             result_field[fields[-1]] = instance_value() if callable(instance_value) else instance_value

    @classmethod
    def register_api(cls, apimanager, app, build_docs=False):
        #api_endpoints = []
        tablename = getattr(cls, '__tablename__')
        apiprefix = apimanager.APINAME_FORMAT.format("/")  # "/api"
        preprocessors = {}
        postprocessors = {}

        # PATCH_SINGLE = ["patch_auth_preprocessor"],
        # PATCH_MANY = ["patch_many_auth_preprocessor"],
        # DELETE_SINGLE = ["delete_auth_preprocessor"],
        # DELETE_MANY = ["delete_many_auth_preprocessor"]

        if cls.api_build_restless_endpoints:
            preprocessors = cls._get_preprocessors()
            postprocessors = cls._get_postrocessors()

            if cls.api_collection_name is None:
                cls.api_collection_name = tablename

            # add cache if applicable
            if isinstance(cls.api_cache_threshold, (int, float)):
                cls._cache = FileSystemCache(  # default 7-day, 500 item cache
                    os.path.join(app.config.get("CACHE_DIR"), tablename), threshold=cls.api_cache_threshold, default_timeout=60*60*24*7)
                cls.append_processor(postprocessors, "POST", "_clear_cache")
                cls.append_processor(postprocessors, "PATCH_SINGLE", "_clear_cache")

            # attach GET preprocessors
            cls.append_processor(preprocessors, "GET_SINGLE", "get_single_auth_preprocessor")
            cls.append_processor(preprocessors, "GET_MANY", "get_many_auth_preprocessor")

            if not cls.api_allow_anonymous_patch:
                # TODO: this needs to be more rigorious: check for edit access, and if not: check for ownership
                cls.append_processor(preprocessors, "PATCH_SINGLE", "patch_auth_preprocessor")
                cls.append_processor(preprocessors, "PATCH_MANY", "patch_many_auth_preprocessor")

            if not cls.api_allow_nonowner_delete:
                cls.append_processor(preprocessors, "DELETE_SINGLE", "delete_auth_preprocessor")
                # TODO: should delete many be allowed? Should filter to only owned sets.
                cls.append_processor(preprocessors, "DELETE_MANY", "delete_many_auth_preprocessor")

            if not cls.api_allow_anonymous_post:
                cls.append_processor(preprocessors, "POST", "post_auth_preprocessor")

            # if isinstance(cls.api_add_columns_single, list):
            #     cls.append_processor(postprocessors, "GET_SINGLE", "get_single_add_fields_postprocessor")

            if preprocessors is not None:
                preprocessors = {k: cls._get_classmethods_from_string(preprocessors[k]) for k in list(preprocessors.keys())}
            if postprocessors is not None:
                postprocessors = {k: cls._get_classmethods_from_string(postprocessors[k]) for k in list(postprocessors.keys())}

            apimanager.create_api(
                cls, methods=cls.api_restless_methods, url_prefix=cls.api_url_prefix,
                collection_name=cls.api_collection_name, allow_patch_many=cls.api_allow_patch_many,
                allow_delete_many=cls.api_allow_delete_many,
                allow_functions=cls.api_allow_functions, exclude_columns=cls.api_exclude_columns,
                include_columns=cls.api_include_columns, include_methods=cls.api_include_methods,
                add_columns_single=cls.api_add_columns_single, add_methods_single=cls.api_add_methods_single,
                validation_exceptions=cls.api_validation_exceptions, results_per_page=cls.api_results_per_page,
                max_results_per_page=cls.api_max_results_per_page, post_form_preprocessor=cls.api_post_form_preprocessor,
                preprocessors=preprocessors, postprocessors=postprocessors, app=app
            )  # , primary_key=None)

            current_app.logger.debug("Registered endpoint for: '{}/{}' {}".format(apiprefix, cls.api_collection_name, cls.api_restless_methods))

        for ep in cls.api_custom_endpoints:
            ep.load_model(cls)
            app.add_url_rule(ep.url, endpoint=ep.endpoint, view_func=ep.view_func, methods=ep.methods, defaults=ep.defaults)
            current_app.logger.debug("Registered endpoint for: '{}' {}".format(ep.url, ep.methods))

        if build_docs:
            assert os.path.isdir(current_app.config.get("API_DOC_DIR")), "API documentation directory is missing!"
            collection_doc_file = os.path.join(current_app.config.get("API_DOC_DIR"), "{}.json".format(tablename))
            doc_data = cls.api_docs_to_dict(apiprefix, tablename, preprocessors, postprocessors)
            with open(collection_doc_file, "w") as f:
                json.dump(doc_data, f, indent=2)

    @classmethod
    def api_docs_to_dict(cls, apiprefix, tablename, preprocessors, postprocessors):
        columns = []
        relations = []
        hybrid_props = []
        collection_docs = []
        methods = []
        association_proxies = []

        if cls.api_build_restless_endpoints:
            base_endpoint = '{}/{}'.format(apiprefix, tablename)
            # endpoints = []
            columns = cls.columns_to_dict()
            relations = cls.relations_to_dict()
            hybrid_props = cls.hybrid_properties_to_dict()
            association_proxies = cls.association_proxies_to_dict()
            methods = cls.methods_to_dict()
            primary_key_name = primary_key_names(cls)[0]

            if "POST" in cls.api_restless_methods:
                collection_docs.append(dict(
                    endpoint=base_endpoint,
                    method="POST",
                    preprocessors=[dict(name=p.__name__, doc=p.__doc__) for p in preprocessors.get("POST", [])],
                    postprocessors=[dict(name=p.__name__, doc=p.__doc__) for p in postprocessors.get("POST", [])],
                    description="Create new [{0}](#{0}) object. Expects JSON. Fields shown in "
                                "`MODEL COLUMNS` below. {1}".format(cls.api_collection_name, cls.__init__.__doc__ or "")))
            if "PATCH" in cls.api_restless_methods:
                collection_docs.append(dict(
                    endpoint="{}/<{}>".format(base_endpoint, primary_key_name),
                    method="PATCH",
                    preprocessors=[dict(name=p.__name__, doc=p.__doc__) for p in preprocessors.get("PATCH_SINGLE", [])],
                    postprocessors=[dict(name=p.__name__, doc=p.__doc__) for p in postprocessors.get("PATCH_SINGLE", [])],
                    description="Update single [{0}](#{0}) object with pk field matching param `{1}`. "
                                "Expects JSON. Fields shown in `MODEL COLUMNS` below.".format(cls.api_collection_name, primary_key_name)))
            if "GET" in cls.api_restless_methods:
                collection_docs.append(dict(
                    endpoint=base_endpoint,
                    method="GET",
                    fields=(cls.api_include_columns or []) + (cls.api_include_methods or []),
                    # methods=cls.api_include_methods,
                    preprocessors=[dict(name=p.__name__, doc=p.__doc__) for p in preprocessors.get("GET_MANY", [])],
                    postprocessors=[dict(name=p.__name__, doc=p.__doc__) for p in postprocessors.get("GET_MANY", [])],
                    description="Get list of [{0}](#{0}) objects matching API query. See [Making API search queries](#api_query) for "
                                "instructions on making search queries".format(cls.api_collection_name)))

                collection_docs.append(dict(
                    endpoint="{}/<{}>".format(base_endpoint,primary_key_name),
                    method="GET",
                    fields=(cls.api_include_columns or []) + (cls.api_include_methods or []) +
                           (cls.api_add_columns_single or []) + (cls.api_add_methods_single or []) ,
                    # methods=cls.api_include_methods,
                    preprocessors=[dict(name=p.__name__, doc=p.__doc__) for p in preprocessors.get("GET_SINGLE", [])],
                    postprocessors=[dict(name=p.__name__, doc=p.__doc__) for p in postprocessors.get("GET_SINGLE", [])],
                    description="Get single [{0}](#{0}) object with pk field matching param `{1}`".format(cls.api_collection_name, primary_key_name)))
            if "DELETE" in cls.api_restless_methods:
                collection_docs.append(dict(
                    endpoint="{}/<{}>".format(base_endpoint, primary_key_name),
                    method="DELETE",
                    preprocessors=[dict(name=p.__name__, doc=p.__doc__) for p in preprocessors.get("DELETE_SINGLE", [])],
                    postprocessors=[dict(name=p.__name__, doc=p.__doc__) for p in postprocessors.get("DELETE_SINGLE", [])],
                    description="Delete single [{0}](#{0}) object with pk field matching param `{1}`".format(cls.api_collection_name, primary_key_name)))

            # for e in endpoints:
            #     collection_docs.append(dict(
            #         endpoint=e.get("endpoint"),
            #         method=e.get("method"),
            #         description=e.get("description"),
            #         fields=e.get("fields", []),
            #         methods=e.get("methods", []),
            #         preprocessors=e.get("preprocessors", []),
            #         postprocessors=e.get("postprocessors", []),
            #     ))
        for ep in cls.api_custom_endpoints:
            for m in ep.methods:
                collection_docs.append(dict(
                    endpoint='{}'.format(ep.url),
                    method=m,
                    description=ep.data_func.__doc__
                ))

        return dict(collection=tablename, endpoints=collection_docs, description=cls.__doc__, columns=columns,
                    relations=relations, hybrid_properties=hybrid_props, methods=methods, association_proxies=association_proxies)

    @classmethod
    def columns_to_dict(cls):
        return [dict(
            name=c.name, type=c.type.__repr__(), nullable=c.nullable, unique=c.unique, doc=c.doc,
            primary_key=c.primary_key, foreign_key="{}.{}".format(list(c.foreign_keys)[0].column.table.key, list(c.foreign_keys)[0].column.key) if len(list(c.foreign_keys))>0 else None,
            default=str(c.default.arg if not callable(c.default.arg) else "function") if c.default else None)
                for c in inspect(cls, raiseerr=False).columns
        ]

    @classmethod
    def hybrid_properties_to_dict(cls):
        return [dict(name=i.__name__, doc=i.__doc__, args=[], type='property')
                for i in inspect(cls).all_orm_descriptors if isinstance(i,hybrid_property)] + \
               [dict(name=i.func.__name__, doc=i.func.__doc__, args=[a for a in list(getfullargspec(i.func).args) if a !='self'], type='method')
                for i in inspect(cls).all_orm_descriptors if isinstance(i,hybrid_method)]

    @classmethod
    def association_proxies_to_dict(cls):
        # NOTE: needed to use `dir` instead of `inspect(cls).all_orm_descriptors` because had no way of obtaining `name` or attribute
        return [dict(name=k, model=getattr(cls, k).value_attr, target_collection=getattr(cls, k).target_collection)
                for k in dir(cls) if isinstance(getattr(cls, k), (ObjectAssociationProxyInstance, AssociationProxy)) and not k.startswith("_")]
        # return [dict(name=i.value_attr, model="`[{0}](#{0})`".format(i.value_attr), target_collection=i.target_collection)
        #         for i in inspect(cls).all_orm_descriptors if isinstance(i, AssociationProxy)]

    @classmethod
    def relations_to_dict(cls):
        return [dict(model=r.target.key, key=r.key, doc=r.doc, direction=r.direction.name) for r in inspect(cls).relationships]

    @classmethod
    def methods_to_dict(cls):
        return [dict(name=func, doc=getattr(cls,func).__doc__, type='method')
                for func in dir(cls) if callable(getattr(cls, func)) and not func.startswith("_")
                and len(getfullargspec(getattr(cls, func)).args)==1
                and getfullargspec(getattr(cls, func)).args[0]=="self"] + \
               [dict(name=prop, doc=getattr(cls,prop).__doc__, type='property')
                for prop in dir(cls) if isinstance(getattr(cls, prop), property)
                and prop != 'validation_errors']



class APIModelEndpoint:
    """

    """
    def __init__(self, clsmethod, apiprefix="/api", endpoint="{tablename}", methods=None, defaults=None):
        """

        :param clsmethod: classmethod string for APIModel or can be callable function
        :param apiprefix:
        :param endpoint:
        :param methods:
        :param defaults:
        """
        if methods is None:
            methods = ["GET"]
        self.model = None
        self.tablename = None
        self.data_func = None
        self.apiresource = endpoint
        self.apiprefix = apiprefix
        self.methods = methods
        self.classmethod = clsmethod
        self.defaults = defaults
        # self.load_model(model)

    @property
    def url(self):
        apiresource = self.apiresource.format(tablename=self.tablename)
        return "{apiprefix}/{apiresource}".format(apiprefix=self.apiprefix, apiresource=apiresource)

    @property
    def endpoint(self):
        return "{}.{}_{}".format(self.tablename, self.classmethod, self.apiresource)

    def load_model(self, model):
        if model is not None:
            self.model = model
            self.tablename = model.__tablename__
            self.data_func = self.classmethod if callable(self.classmethod) else getattr(model, self.classmethod)

    # decorate all custom view functions to catch processing exceptions and display a pretty JSON message
    @catch_api_request_exceptions
    def view_func(self, **kw):
        tic = time()
        data = self.data_func(**kw)
        # data = self.data_func(**kw)
        # render_data_response(data)
        current_app.logger.debug("{}, done in {}".format(self.data_func.__name__, time() - tic))
        return data


def docstring_parameter(*sub):
    def dec(obj):
        obj.__doc__ = obj.__doc__ % sub
        return obj
    return dec
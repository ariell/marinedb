import requests
from ..search import Registry


class WoRMS(Registry):

    def __init__(self, domain_url="http://www.marinespecies.org", *args, **kwargs):
        super().__init__(domain_url=domain_url, *args, **kwargs)
        # URLs used for matching operations
        self.match_synonym_url = None  # no apparent endpoint for matching synonyms, but can get synonyms for AphiaID
        self.match_vernacular_url = domain_url + "/rest/AphiaRecordsByVernacular/{search_term}?like=true&offset={page}"
        self.match_name_url = domain_url + "/rest/AphiaRecordsByName/{search_term}?like=true&marine_only=true&offset={page}"
        #self.match_name_url = api_base + "/rest/AphiaRecordsByNames?scientificnames[]={search_term}&like=true&marine_only=true"
        self.match_fuzzy_name_url = domain_url + "/rest/AphiaRecordsByMatchNames?scientificnames[]={search_term}&marine_only=true"

        # URLs for getting records
        self.get_element_url = domain_url + "/rest/AphiaRecordByAphiaID/{key}"
        self.get_children_url = domain_url + "/rest/AphiaChildrenByAphiaID/{key}?marine_only=true&offset={page}"
        self.get_root_url = domain_url + "/rest/AphiaChildrenByAphiaID/1?marine_only=true&offset={page}"
        self.get_reference_url = domain_url + "/aphia.php?p=taxdetails&id={key}"

        # Private URLs used in this class for getting 'other_names'
        self._synonym_url = domain_url + "/rest/AphiaSynonymsByAphiaID/{key}?offset=1"
        self._vernacular_url = domain_url + "/rest/AphiaVernacularsByAphiaID/{key}"

        self.lineage_fields = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus']

        # self.root_node_key = 1

    def get_match_results(self, url, *args, **kwargs):
        result = self.get_json(url)
        objects = []
        if isinstance(result, dict):
            objects = self.result_to_dict(result)   # single element
        elif isinstance(result, list):
            key_dict = {}                           # keep dict lookup to manage duplicates for accepted taxon
            if bool(result) and isinstance(result[0], list):  # some endpoints return list of list - flatten to lodicts
                result = [item for sublist in result for item in sublist]
            for r in result:
                status = r.get("status")
                key = r.get("valid_AphiaID")
                if key is None or status == "deleted":
                    print("Skipping result: {}, status: {}".format(r.get("scientificname"), status))
                    pass                # skip taxon that do not have a mapping to a valid record, or have been deleted
                elif key in key_dict:      # add scientific names as 'other_names' for duplicated valid name
                    key_dict[key]["other_names"].append(self.get_other_name("scientificname", r))
                else:
                    key_dict[key] = self.result_to_dict(r)
            objects = list(key_dict.values())
        return objects

    def result_to_dict(self, result):
        # get lineage
        # lineage_fields = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'valid_name']
        lineage = self.get_lineage(result)
        other_names = [self.get_other_name("scientificname", result)] \
            if result.get('scientificname') != result.get('valid_name') else []
        return self.result2dict(name=result.get('valid_name'), key=result.get("valid_AphiaID"), lineage=lineage,
                           other_names=other_names, parent_key=result.get("parentNameUsageID"),
                           origin_updated_at=result.get("modified"))

    def get_extended_element_attributes(self, result, data):
        super().get_extended_element_attributes(result, data)
        result['other_names'] += self._get_other_names(result.get("key"))
        return result

    def _get_other_names(self, key):
        print(f"GET_SYNONYMS, ", end="")
        synonym_results = self.get_json(self._synonym_url.format(key=key))
        print(f"GET_VERNACULARS, ", end="")
        vernacular_results = self.get_json(self._vernacular_url.format(key=key))
        return [self.get_other_name("scientificname", r) for r in synonym_results] + \
            [self.get_other_name("vernacular", r, vernacular_code=r.get("language_code")) for r in vernacular_results]

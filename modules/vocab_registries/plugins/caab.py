from ..search import Registry, ProcessingException


class CAAB(Registry):
    def __init__(self, domain_url="https://www.marine.csiro.au", *args, **kwargs):
        super().__init__(domain_url=domain_url, *args, **kwargs)

        # URLs used for matching operations
        self.match_synonym_url = domain_url + "/data/services/caab/?q={search_term}&type=synonym"   # optional: already appear in results fields: otherVernacularNames, vernacularName, marketingName
        self.match_name_url = domain_url + "/data/services/caab/?q={search_term}&cat=&type=scientific"  # required: does this search vernacular & common names?
        self.match_fuzzy_name_url = None  # optional: doesn't currently exist
        self.match_vernacular_url = domain_url + "/data/services/caab/?q={search_term}&type=vernacular"  # optional: seems that 'match_name_url' already searches vernaculars?

        # URLs for getting records
        self.get_element_url = domain_url + "/data/caab/api/?caab_code={key}"  # required
        self.get_children_url = None  # required: we'd like to be able to traverse up/down the tree from a match
        self.get_root_url = None
        self.get_reference_url = domain_url + "/data/caab/taxon_report.cfm?caab_code={key}"  # &noheader=true"   # without header

        self.lineage_fields = ['kingdom', 'phylum', 'class', 'order', 'family']
        self._other_name_fields = ['otherVernacularNames', 'vernacularName', 'marketingName']

    def get_match_results(self, url, *args, **kwargs):
        result = self.get_json(url)
        objects = []
        if isinstance(result, dict):
            objects = self.result_to_dict(result)
        elif isinstance(result, list):
            # convert results to dict, but ignore records without 'scientificName' values
            objects = [self.result_to_dict(r) for r in result if r.get("scientificName")]
        return objects

    def result_to_dict(self, result):
        # get lineage
        # lineage_fields = ['kingdom', 'phylum', 'class', 'order', 'family', 'scientificName']
        if result.get("CAAB_status") == "unknown CAAB code":
            raise ProcessingException(result.get("CAAB_status"), code=400)
        lineage = self.get_lineage(result)
        other_names = self._get_other_names(result)
        mapped_registries = []
        if result.get("scientificNameID_WoRMS") and isinstance(result.get("scientificNameID_WoRMS"), str):
            identifier = result.get("scientificNameID_WoRMS", "")
            mapped_registries.append(self.registry_mapping(registry_key="worms", key=identifier.split(":")[-1], identifier=identifier))
        if result.get("scientificNameID_AFD") and isinstance(result.get("scientificNameID_AFD"), str):
            identifier = result.get("scientificNameID_AFD", "")
            mapped_registries.append(self.registry_mapping(registry_key="afd", key=identifier.split(":")[-1], identifier=identifier))

        return self.result2dict(name=result.get("scientificName"), key=result.get("CAAB_code"),
                           parent_key=result.get('Parent_Id'), origin_updated_at=result.get("RecordModifiedOn"),
                           lineage=lineage, other_names=other_names, mapped_registries=mapped_registries)

    # def get_extended_element_attributes(self, result, data):
    #     super().get_extended_element_attributes(result, data)
    #     # ATTACH other_names?

    def _get_other_names(self, result):
        other_names = []
        for f in self._other_name_fields:
            if f in result:
                other_names += [i.capitalize() for i in result.get(f, "").split("|") if i]
        return [self.get_other_name(name=i, vernacular_code="eng") for i in list(set(other_names))]

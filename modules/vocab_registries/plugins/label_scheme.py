from ..search import Registry, ProcessingException

default_url = "https://squidle.org"
# default_url = "http://localhost:5000"


class LabelScheme(Registry):
    def __init__(self, label_scheme_id, key=None, domain_url=default_url, *args, **kwargs):
        super().__init__(domain_url=domain_url, key=key, *args, **kwargs)

        # URLs used for matching operations
        self.match_synonym_url = domain_url + '/api/label?q={{"filters":[{{"name":"label_scheme_id","op":"eq","val":"%i"}},{{"name":"vocab_elements","op":"any","val":{{"name":"labels","op":"any","val":{{"name":"name","op":"ilike","val":"%%{search_term}%%"}}}}}}]}}&page={page}&results_per_page=100' % label_scheme_id
        self.match_name_url = domain_url + '/api/label?q={{"filters":[{{"name":"label_scheme_id","op":"eq","val":"%i"}},{{"or":[{{"name":"name","op":"ilike","val":"%%{search_term}%%"}},{{"name":"vocab_elements","op":"any","val":{{"name":"name","op":"ilike","val":"%%{search_term}%%"}}}}]}}]}}&page={page}&results_per_page=100' % label_scheme_id
        self.match_fuzzy_name_url = None  # optional: doesn't currently exist
        self.match_vernacular_url = domain_url + '/api/label?q={{"filters":[{{"name":"label_scheme_id","op":"eq","val":"%i"}},{{"name":"vocab_elements","op":"any","val":{{"name":"other_names","op":"any","val":{{"name":"name","op":"ilike","val":"%%{search_term}%%"}}}}}}]}}&page={page}&results_per_page=100' % label_scheme_id

        # URLs for getting records
        self.get_element_url = domain_url + '/api/label?q={{"filters":[{{"name":"label_scheme_id","op":"eq","val":"%i"}},{{"name":"origin_code","op":"eq","val":"{key}"}}],"single":true}}' % label_scheme_id  # required
        self.get_root_url = domain_url + '/api/label?q={{"filters":[{{"name":"label_scheme_id","op":"eq","val":"%i"}},{{"name":"parent_id","op":"is_null"}}]}}&page={page}&results_per_page=100' % label_scheme_id
        self.get_children_url = domain_url + '/api/label?q={{"filters":[{{"name":"label_scheme_id","op":"eq","val":"%i"}},{{"name":"parent","op":"has","val":{{"name":"origin_code","op":"eq","val":"{key}"}}}}]}}&page={page}&results_per_page=100' % label_scheme_id
        self.get_reference_url = domain_url + '/api/label?q={{"filters":[{{"name":"label_scheme_id","op":"eq","val":"%i"}},{{"name":"key","op":"eq","val":"{key}"}}],"single":true}}&template=json.html' % label_scheme_id  # &noheader=true"   # without header
        self._get_vocab_elements_url = domain_url + '/api/vocab_element?q={{"filters":[{{"name":"labels","op":"any","val":{{"name":"id","op":"eq","val":"{id}"}}}},{{"name":"vocab_registry_key","op":"neq","val":"%s"}}]}}&results_per_page=100' % key
        # self.lineage_fields = ['kingdom', 'phylum', 'class', 'order', 'family']

    def get_match_results(self, url, *args, **kwargs):
        result = self.get_json(url)
        objects = [self.result_to_dict(r) for r in result.get("objects")]
        return objects

    def get_extended_element_attributes(self, result, data):
        super().get_extended_element_attributes(result, data)
        vocab_elements = self.get_json(self._get_vocab_elements_url.format(**data))
        result['other_names'] = []
        if not result['mapped_registries']:
            result['mapped_registries'] = []
        for v in vocab_elements.get("objects"):
            result['other_names'] += [dict(name=n.get('name')) for n in v.get('other_names')]
            result['mapped_registries'].append(dict(registry_key=v.get("vocab_registry", {}).get("key"), key=v.get("key")))
        return result

    def result_to_dict(self, result):
        return self.result2dict(name=result.get("name"), key=result.get("origin_code"),
                           parent_key=result.get('parent_origin_code'), origin_updated_at=result.get("updated_at") or result.get("created_at"),
                           lineage=result.get('lineage'), other_names=result.get('other_names'),
                                mapped_registries=result.get('mapped_registries'))


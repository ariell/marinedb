import requests
import inspect
from dateutil.parser import parse

from modules.flask_restless import ProcessingException
# from pydoc import locate

vocab_registries = {}


def register_vocab_registry(cls, key=None, *args, **kwargs):
    global vocab_registries
    print("Registering vocab_registry: {}".format(key))
    instance = cls(key=key, *args, **kwargs)
    if key is None:
        key = instance.registry_key
    vocab_registries[key.lower()] = instance


def get_vocab_registry(key, cls, *args, **kwargs):
    global vocab_registries
    key = key.lower()   # assert that it is lowercase
    # if silent is not True:
    #     assert key in vocab_registries, "Unknown vocab_registry key. Each registry needs a supported plugin. " \
    #                                     "Currently supported registries are: {}".format(vocab_registries.keys())
    if key not in vocab_registries:
        # cls = getattr(class_modules, module)
        # cls = locate("vocab_registries.plugins.{}".format(module))
        register_vocab_registry(cls, key=key, *args, **kwargs)
    return vocab_registries.get(key, None)


def is_supported_registry(key):
    global vocab_registries
    return key in vocab_registries


def parse_bool(val):
    if isinstance(val, str): return val.lower() == "true"
    return val


def nest_vocab_elements_on_lineage(results):
    nested_results = dict({"elements": [], "children": {}})
    num_results = len(results)
    for r in results:
        node = nested_results
        levels = [i.get('name') for i in r.get("lineage")]
        reg_key = r.get("vocab_registry", {}).get("name", None)
        if reg_key:
            levels = [reg_key] + levels
        print(levels)
        for l in levels:
            if l not in node.get("children"):
                node["children"][l] = {"elements": [], "children": {}}
            node = node["children"][l]
        node["elements"].append(r)
    return dict(objects=nested_results, num_results=num_results)


class Registry:
    registry_key = None
    get_element_url = "https://repo.url/api/{key}"
    match_name_url = "https://repo.url/api/exact_name?s={search_term}&page={page}"
    match_synonym_url = None
    match_vernacular_url = None
    match_fuzzy_name_url = None
    get_children_url = "https://repo.url/api/{key}/children?page={page}"
    get_root_url = None
    get_reference_url = "https://repo.url/ui/{key}"
    lineage_fields = ['kingdom', 'phylum', 'class', 'order', 'family', 'genus']
    root_node_key = None

    def __init__(self, key=None, domain_url=None, *args, **kwargs):
        if key is None:
            self.registry_key = self.__class__.__name__.lower()
        else:
            self.registry_key = key

    def result2dict(self, name: str = None, key: str = None, origin_updated_at: str = None, other_names=None, lineage=None,
                    parent_key: str = None, mapped_registries=None):
        """

        :param name: the accepted name of the concept / element
        :param key: the native registry key for that concept / element (eg: CAAB code / AphiaID)
        :param parent_key: the native registry key of the parent element
        :param origin_updated_at: (optional) the datetime the record was last updated
        :param other_names: (optional) other names / synonyms / vernacular names that can be used to refer to the concept / element
        :param lineage: (optional) the classification lineage, i.e. 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species'
        :param mapped_registries: (optional) a list of key-value pairs for mapping between other registries
        :return:
        """
        if mapped_registries is None:
            mapped_registries = []
        if lineage is None:
            lineage = {}
        if other_names is None:
            other_names = []
        try:
            if isinstance(origin_updated_at, str):
                origin_updated_at = parse(origin_updated_at)
        except Exception as e:
            origin_updated_at = None
        return dict(name=name, key=key, parent_key=parent_key, origin_updated_at=origin_updated_at, lineage=lineage,
                    other_names=other_names, mapped_registries=mapped_registries, vocab_registry_key=self.registry_key)

    @staticmethod
    def registry_mapping(registry_key, key, identifier=None):
        return dict(registry_key=registry_key, key=key, identifier=identifier)

    def get_element(self, key, include_extended_attributes=True, **kw):
        assert isinstance(self.get_element_url, str), "No 'get_element_url' defined for this registry"
        url = self.get_element_url.format(key=key)
        print(f"{inspect.currentframe().f_code.co_name.upper()}, ", end="")
        data = self.get_json(url, silent=False)
        result = self.result_to_dict(data)
        #result["vocab_registry_key"] = self.registry_key
        if include_extended_attributes:
            self.get_extended_element_attributes(result, data)
        return result

    def get_all_elements(self, key, **kw):
        elements = []
        primary_element = self.get_element(key, include_extended_attributes=True)
        elements.append(primary_element)
        for e in primary_element.get("mapped_registries"):
            if e.get("key"):
                try:
                    r = get_vocab_registry(e.get("registry"), silent=True)
                    if r is not None:
                        elements.append(r.get_element(key=e.get("key")))
                except Exception as ex:
                    print("Error getting mapped_registry element: {} ({}: {})".format(e, ex.__class__.__name__, ex))
        return elements

    def get_extended_element_attributes(self, result, data):
        result['data'] = data
        # if 'lineage' in result:
        #     result['description'] = " > ".join(["{name} ({order})".format(**i) for i in result.get("lineage")])
        return result

    def match_name(self, search_term, page=1, **url_params):
        if not isinstance(self.match_name_url, str):
            return []
        url = self.match_name_url.format(search_term=search_term, page=page, **url_params)

        return self.get_hierarchical_match_results(url)

    def match_synonym(self, search_term, page=1, **url_params):
        if not isinstance(self.match_synonym_url, str):
            return []
        url = self.match_synonym_url.format(search_term=search_term, page=page, **url_params)
        return self.get_hierarchical_match_results(url)

    def match_fuzzy_name(self, search_term, page=1, **url_params):
        if not isinstance(self.match_fuzzy_name_url, str):
            return []
        url = self.match_fuzzy_name_url.format(search_term=search_term, page=page, **url_params)
        return self.get_hierarchical_match_results(url)

    def match_vernacular(self, search_term, page=1, **url_params):
        if not isinstance(self.match_vernacular_url, str):
            return []
        url = self.match_vernacular_url.format(search_term=search_term, page=page, **url_params)
        return self.get_hierarchical_match_results(url)

    def get_children(self, key=None, page=1, **url_params):
        if not isinstance(self.get_children_url, str):
            return []
        if not key:
            url = self.get_root_url.format(page=page, **url_params)
        else:
            url = self.get_children_url.format(key=key, page=page, **url_params)
        return self.get_match_results(url)

    def get_reference(self, key, as_url=True, **url_params):
        assert isinstance(self.get_reference_url, str), "No 'get_reference_url' defined for this registry"
        url = self.get_reference_url.format(key=key, **url_params)
        if as_url:
            return url
        else:
            return requests.get(url).content

    def get_json(self, url, silent=True):
        print(f"GET: {url} ... ", end="")
        r = requests.get(url)
        print(f" {r.reason}:{r.status_code} ({r.elapsed.total_seconds()} s)")
        if r.ok and r.status_code != 204:
            return r.json()
        else:
            if silent:
                return []
            else:
                raise ProcessingException(description="No result(s) found", code=404)

    def get_other_name(self, field=None, result=None, name=None, vernacular_code=None):
        info = f"common:{vernacular_code}" if vernacular_code is not None else "synonym"
        if name is None:
            name = result.get(field)
        return dict(name="{} ({})".format(name, info))

    def get_lineage(self, result):
        return [dict(rank=f, name=result.get(f)) for f in self.lineage_fields if result.get(f)]

    def match(self, search_term, page=1, match_names=False, match_synonyms=False, match_vernaculars=False, match_fuzzy_names=False, **kw):
        objects = {}
        if parse_bool(match_names):
            objects['matched_names'] = self.match_name(search_term, page=page)
        if parse_bool(match_synonyms):
            objects['matched_synonyms'] = self.match_synonym(search_term, page=page)
        if parse_bool(match_vernaculars):
            objects['matched_vernaculars'] = self.match_vernacular(search_term, page=page)
        if parse_bool(match_fuzzy_names):
            objects['matched_fuzzy_names'] = self.match_fuzzy_name(search_term, page=page)
        return objects

    def get_hierarchical_match_results(self, *args, **kwargs):
        print(f"{inspect.stack()[1][3].upper()}, ", end="")
        results = self.get_match_results(*args, **kwargs)
        return nest_vocab_elements_on_lineage(results)

    def get_match_results(self, url, *args, **kwargs):
        raise NotImplementedError("`{}` method has not been implemented for `{}`".format(
            inspect.currentframe().f_code.co_name, self.__class__.__name__))

    def result_to_dict(self, result):
        raise NotImplementedError("`{}` method has not been implemented for `{}`".format(
            inspect.currentframe().f_code.co_name, self.__class__.__name__))

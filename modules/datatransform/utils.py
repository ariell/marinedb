import numpy as np

data_transform_modules = {}


def register_data_transformer_module(module, module_key=None):
    global data_transform_modules
    if module_key is None:
        module_key = module.__name__
    #print("Registering module {}: {}".format(module_key, module.__name__))
    data_transform_modules[module_key] = module


def get_data_transform_modules(module_key=None):
    global data_transform_modules
    if module_key is not None:
        return data_transform_modules.get(module_key, None) or \
               _raise_dt_exception("Module '{}' does not exist or has not been registered in the DataTransformer API".format(module_key))
    return data_transform_modules


def _raise_dt_exception(msg):
    raise DataTransformerException(msg)


class DataTransformerException(Exception):
    pass

def nullifnan(value):
    """Convert NaN to None/null. Causes JSON rendering issues - needs to be fixed in import scripts."""
    return value if isinstance(value, str) or value is None or not np.isnan(value) else None

def get_nested_object_values(obj, fields):
    value = dict()
    for k, v in fields.items():
        # if not hasattr(obj, k):
        #     return None
        # else:
        o = getattr(obj, k, None)
        if o is None:
            # value[k] = None
            pass
        elif isinstance(v, dict):
            value[k] = get_nested_object_values(o, v)
        else:
            value[k] = o() if callable(o) else o
    return value


def build_nested_fields(flattened_fields, nest_delimiter="."):
    nested_fields = dict()
    for c in flattened_fields:
        field_list = c.split(nest_delimiter)
        d = nested_fields
        for part in field_list[:-1]:
            if part not in d:
                d[part] = dict()
            d = d[part]
        d[field_list[-1]] = None
    return nested_fields

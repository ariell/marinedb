import mimetypes
import os
from uuid import UUID

import pandas as pd
import numpy as np
from datetime import datetime, date
from flask import make_response, jsonify
from numpy import nan
from werkzeug.utils import secure_filename
# import json
import simplejson as json

# from .models import register_data_transformer_module
from modules.flask_restless import ProcessingException
from modules.flask_restless.views import render_data_template
from .utils import register_data_transformer_module, build_nested_fields, get_nested_object_values




class FlaskHelper:
    """
    FlaskHelper
    """
    def __init__(self):
        """
        __init__
        """
        pass

    def make_csv_response(self, data, filename=None, metadata=None, mimetype=None):
        """
        make_csv_response
        :param data:
        :param filename:
        :param metadata:
        :param mimetype:
        :return:
        """
        resp = make_response(data)
        if filename is not None:
            if not filename.endswith('csv'):
                filename = "{}.csv".format(filename)
            resp.headers["Content-Disposition"] = "attachment; filename={}".format(secure_filename(filename))
            resp.headers["Content-Type"] = mimetype or "text/csv"
        else:
            resp.headers["Content-Type"] = mimetype or "application/json"

        if metadata is not None:
            resp.headers["X-Content-Metadata"] = json.dumps(metadata)
        return resp

    def make_json_response(self, data, filename=None, metadata=None, mimetype=None):
        """
        make_json_response
        :param data:
        :param filename:
        :param metadata:
        :param mimetype:
        :return:
        """
        if isinstance(data, list):
            data = dict(objects=data, num_results=len(data))
        if isinstance(data, dict) and metadata is not None:
            data['metadata'] = metadata
        resp = make_response(json_dumps(data))
        # resp = jsonify(dict_data)
        if filename is not None:
            resp.headers["Content-Disposition"] = "attachment; filename={}.json".format(secure_filename(filename))
            resp.headers["Content-Type"] = mimetype or "text/csv"
        else:
            resp.headers["Content-Type"] = mimetype or "application/json"

        return resp

    def make_template_response(self, data, template, filename=None, metadata=None, mimetype=None):
        """
        make_template_response
        :param data:
        :param template:
        :param filename:
        :param metadata:
        :param mimetype:
        :return:
        """
        resp = make_response(render_data_template(data, template=template, json_handler=json_serial, set_mimetype=False))
        if mimetype is not None:
            resp.headers["Content-Type"] = mimetype
        if filename is not None:
            resp.headers["Content-Disposition"] = "attachment; filename={}-{}".format(secure_filename(filename),os.path.basename(template))
            if mimetype is None: resp.headers["Content-Type"] = mimetypes.guess_type(filename)[0]
        if metadata is not None:
            resp.headers["X-Content-Metadata"] = json.dumps(metadata)
        return resp

    def make_response_from_dataframe(self, data, metadata=None, output_format="json", filename=None, mimetype=None):
        """
        make_response_from_dataframe
        :param data:
        :param metadata:
        :param output_format:
        :param filename:
        :param mimetype:
        :return:
        """
        if output_format is None:
            output_format = os.path.splitext(filename)[1]
        if output_format.lower().endswith("csv"):
            #data = data.to_csv(index=None)  # NOTE: setting index to None breaks export of columns if aggregated/grouped
            data = data.to_csv()
            return self.make_csv_response(data, filename, metadata, mimetype=mimetype)
        elif output_format.lower().endswith("json"):
            data = data.to_dict(orient="records")
            return self.make_json_response(data, filename, metadata, mimetype=mimetype)
        else:
            raise ProcessingException("Unrecognised output format: '{}'".format(output_format))


class PandasHelper:
    """
    PandasHelper
    """
    def __init__(self):
        """
        __init__
        """
        pass

    def get_columns(self, df, exclude=None, intersection=None):
        """
        get_columns
        :param df:
        :param exclude:
        :param intersection:
        :return:
        """
        cols = list(set(df.columns) - set(exclude or []))
        return list(set(cols).intersection(set(intersection))) if intersection is not None else cols

    def pivot_table(self, df, columns=None, values=None, columns_prefix=None, **kwargs):
        """
        pivot_table
        :param df:
        :param columns:
        :param values:
        :param columns_prefix:
        :param kwargs:
        :return:
        """
        idx_cols = self.get_columns(df, exclude=[columns, values])
        df = df.pivot_table(index=idx_cols, columns=columns, values=values, **kwargs).reset_index()
        # df = df.pivot(index=idx_cols, columns=columns, values=values).reset_index()
        if columns_prefix is not None and not df.empty:
            col_mapper = {c: columns_prefix+c for c in self.get_columns(df, exclude=idx_cols)}
            df.rename(columns=col_mapper, inplace=True)

        return df

    def count_unstack(self, df, columns=None):
        """
        count_unstack
        :param df:
        :param columns:
        :return:
        """
        columns = self.get_columns(df, intersection=columns)                    # use interection to avoid inconsequential missing key error
        return df.groupby(df.columns.tolist()).size().unstack(columns)

    def apply_multicol(self, df, columns=None, columns_func=None, **kwargs):
        """
        apply_multicol
        :param df:
        :param columns:
        :param columns_func:
        :param kwargs:
        :return:
        """
        if len(df) > 0:
            df[columns] = df.apply(lambda row: pd.Series(columns_func(row)), **kwargs).reindex()
        #raise Exception
        return df

    def groupby_concat(self, df, column=None, delimeter=",", **kwargs):
        """
        groupby_concat
        :param df:
        :param column:
        :param delimeter:
        :param kwargs:
        :return:
        """
        group_cols = self.get_columns(df, exclude=[column])
        df = df.groupby(group_cols)[column].apply(lambda x: delimeter.join(x)).reset_index()
        return df

    def groupby_count(self, df, ascending=False, **kwargs):
        """
        groupby_count
        :param ascending:
        :param df:
        :param kwargs:
        :return:
        """
        df = df.groupby(df.columns.tolist()).size().to_frame('count').reset_index()
        if ascending is not None:
            df = df.sort_values(['count'], ascending=ascending)
        return df

    def move_column(self, df, column=None, position="end"):
        """
        move_column
        :param df:
        :param column:
        :param position:
        :return:
        """
        cols = df.columns.tolist()
        c = cols.pop(cols.index(column))
        if position=="end":
            cols.append(c)
        elif isinstance(position, int):
            cols.insert(position, c)
        return df.ix[:, cols]

    def column_operation(self, df, column=None, operation=None, args=None, kwargs=None):
        """
        column_operation
        :param df:
        :param column:
        :param operation:
        :param args:
        :param kwargs:
        :return:
        """
        if kwargs is None:
            kwargs = {}
        if args is None:
            args = []
        op = df[column]  # get column
        for m in operation.split("."):  # get list of column ops (delimited by ".")
            op = getattr(op, m)  # get column operation
        df[column] = op(*args, **kwargs)
        return df

    def format_column(self, df, column=None, fstring=""):
        """

        @param df:
        @param column:
        @param newcol:
        @param fstring:
        @return:
        """
        df[column] = [fstring.format(**r) for i, r in df.iterrows()]
        return df

    def concat_columns(self, df, newcol=None, columns=None, sep=""):
        """

        """
        df[newcol] = df[columns].apply(lambda x: sep.join(map(str, x)), axis=1)
        return df

    def query_to_dataframe(self, query, **kwargs):
        """
        query_to_dataframe
        :param query:
        :param kwargs:
        :return:
        """
        kwargs.update({
            "sql": query.statement,
            "con": query.session.bind
        })
        return pd.read_sql(**kwargs)

    # Selected pandas modules for reading data. For documentation, see pandas docs
    # exposed here, rather than through pandas directly to avoid functions that write to disk and pose security threats
    def read_table(self, *args, **kwargs):
        """
        read_table
        :param args:
        :param kwargs:
        :return:
        """
        return pd.read_table(*args, **kwargs)

    def read_csv(self, *args, parse_timestamps=None, **kwargs):
        """
        read_csv
        :param args:
        :param kwargs:
        :return:
        """
        if parse_timestamps is not None:
            kwargs.update(dict(parse_dates=parse_timestamps, date_parser=lambda x: pd.to_datetime(x, unit="s")))
        return pd.read_csv(*args, **kwargs)
    def read_fwf(self, *args, **kwargs):
        """
        read_fwf
        :param args:
        :param kwargs:
        :return:
        """
        return pd.read_fwf(*args, **kwargs)
    def read_excel(self, *args, **kwargs):
        """
        read_excel
        :param args:
        :param kwargs:
        :return:
        """
        return pd.read_excel(*args, **kwargs)
    def read_json(self, *args, **kwargs):
        """
        read_json
        :param args:
        :param kwargs:
        :return:
        """
        return pd.read_json(*args, **kwargs)
    def json_normalize(self, *args, **kwargs):
        """
        json_normalize
        :param args:
        :param kwargs:
        :return:
        """
        return pd.json_normalize(*args, **kwargs)
    def read_html(self, *args, **kwargs):
        """
        read_html
        :param args:
        :param kwargs:
        :return:
        """
        return pd.read_html(*args, **kwargs)
    def read_gbq(self, *args, **kwargs):
        """
        read_gbq
        :param args:
        :param kwargs:
        :return:
        """
        return pd.read_gbq(*args, **kwargs)
    def from_dict(self, *args, **kwargs):
        """
        from_dict
        :param args:
        :param kwargs:
        :return:
        """
        return pd.DataFrame.from_dict(*args, **kwargs)
    def merge(self, *args, **kwargs):
        """
        merge
        :param args:
        :param kwargs:
        :return:
        """
        return pd.merge(*args, **kwargs)


class DataHelper:
    def unflatten_dicts(self, dictlist, nest_delimiter="."):
        """
        unflatten_dicts
        :param dictlist:
        :param nest_delimiter:
        :return: list of dicts (nested records)
        """
        objects = []
        for dict_flat in dictlist:
            dict_nested = dict()
            for key, value in dict_flat.items():
                parts = key.split(nest_delimiter)
                d = dict_nested
                for part in parts[:-1]:
                    if part not in d:
                        d[part] = dict()
                    d = d[part]
                d[parts[-1]] = value
            objects.append(dict_nested)
        return objects

    def flatten_dicts(self, dictlist, nest_delimiter="."):
        """
        flatten_dicts
        :param dictlist:
        :param nest_delimiter:
        :return: list of dicts (flattened records)
        """
        return pd.json_normalize(dictlist, sep=nest_delimiter).to_dict(orient="records")

    def query_to_dicts(self, query, include_columns=None, nest_delimiter="."):
        """
        query_to_dicts
        :param query:
        :param include_columns:
        :param nest_delimiter:
        :return: list of dicts (nested records)
        """
        object_structure = build_nested_fields(include_columns, nest_delimiter=nest_delimiter)
        objects = []
        for r in query.all():
            obj = get_nested_object_values(r, object_structure)
            objects.append(obj)
        return objects


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    elif isinstance(obj, UUID):
        return str(obj)
    elif pd.isna(obj):
        return None
    raise TypeError ("Type {} not serializable".format(type(obj).__name__))


def json_dumps(obj, indent=2, default=json_serial, **kw):
    return json.dumps(obj, indent=indent, default=default, ignore_nan=True, **kw)



# Register helper modules
register_data_transformer_module(PandasHelper(), module_key="pandas")
register_data_transformer_module(FlaskHelper(), module_key="flask")
register_data_transformer_module(DataHelper(), module_key="data")

"""
DataTransformer models
"""
#from .helpers import pd, FlaskHelper, PandasHelper
import hashlib
import json
from io import BytesIO
from json import JSONEncoder
from time import time

from flask import send_file, request, session
from pandas import DataFrame

from modules.datatransform.utils import DataTransformerException, register_data_transformer_module
from modules.flask_restless.search import search
from .utils import get_data_transform_modules


class DataTransformer:
    """

    """
    def __init__(self, operations=None, modules=None, data=None):
        """

        :param operations:
        :param modules:
        :param data:
        """
        self.operations = []
        self.data = data

        # initialise operation sequence
        if isinstance(operations, list):
            self.operations = operations

        # add additional modules if appropriate
        if isinstance(modules, dict):
            for k,m in modules.items():
                register_data_transformer_module(m, k)


    def add_module(self, module_name, module):
        """

        :param module_name:
        :param module:
        :return:
        """
        # TODO: check valid args
        # TODO: do dynamic import for different module? May be risky - need to think about it more
        # self.modules[module_name] = module
        register_data_transformer_module(module, module_key=module_name)

    def set_data(self, data):
        """

        :param data:
        :return:
        """
        self.data = data

    def set_operations(self, operations):
        """

        :param operations:
        :return:
        """
        self.operations = operations

    def add_operation(self, method=None, module=None, **kwargs):
        """

        :param method:
        :param module:
        :param kwargs:
        :return:
        """
        assert method is not None, "No method specified for operation"
        self.operations.append(dict(method=method, module=module, kwargs=kwargs))

    def insert_operation(self, index=0, method=None, module=None, **kwargs):
        """

        :param index:
        :param method:
        :param module:
        :param kwargs:
        :return:
        """
        # TODO: check valid args
        self.operations.insert(index, dict(method=method, module=module, kwargs=kwargs))

    def transform_data(self, data, method, module=None, **kwargs_dict):
        """

        :param data:
        :param method:
        :param module:
        :param kwargs_dict:
        :return:
        """
        tic = time()
        try:
            fn = data if module is None else get_data_transform_modules(module)
            for m in method.split("."):  # [:-1]:        # iterate through package modules
                fn = getattr(fn, m)
            # fn = getattr(fn, module_methods[-1])      # get method
            if module is None:
                self.data = fn(**kwargs_dict)
            else:
                self.data = fn(data, **kwargs_dict)
            print(f"DataTransformer: completed {module}>{method} in {time()-tic} s")
            return self.data
        except Exception as e:
            print(f"DataTransformer: ERROR! {module}>{method} in {time()-tic} s")
            raise DataTransformerException("{}: {} in operation '{}'".format(type(e).__name__, e, dict(method=method, module=module, kwargs=kwargs_dict)))

    def run_all(self, data=None):
        """

        :param data:
        :return:
        """
        op = {}
        try:
            if data is None: data = self.data
            for op in self.operations:
                method = op["method"]
                module = op.get("module", None)
                kwargs_dict = op.get("kwargs", {})
                data = self.transform_data(data, method, module, **kwargs_dict)
            return data
        except DataTransformerException as e:
            raise e
        except Exception as e:
            raise DataTransformerException("{}: {} in operation '{}'".format(type(e).__name__, e, op))

    def get_filename(self, filename_pattern, metadata=None):

        class SafeEncoder(JSONEncoder):
            def default(self, o):
                return "DATA"

        h = hashlib.blake2b(digest_size=10)  # get 10bit (20char) hex code
        h.update(json.dumps(self.operations, cls=SafeEncoder).encode("UTF-8"))  # hash the operations to ensure unique, repeatable file name for each op sequence
        return filename_pattern.format(metadata=metadata, hash=h.hexdigest()) if filename_pattern else None


def get_data_transformer(data, operations=None, run_operations=True):
    """

    :param data:
    :param operations:
    :return:
    """
    f = json.loads(request.args.get('f', '{}') or "{}")
    fileops = (operations if isinstance(operations, list) else []) + f.get("operations", [])
    dt = DataTransformer(data=data)
    if fileops:
        dt.set_operations(fileops)
        if run_operations is True:
            dt.run_all()
    return dt


def render_data_transformer(dt, filename_pattern=None, metadata=None, default_disposition="attachment"):
    """

    :param dt:
    :param filename_pattern:
    :param metadata:
    :param default_disposition:
    :return:
    """
    as_attachment = request.args.get("disposition", default_disposition) == "attachment"
    # get filename from pattern, metadata and operations if attachment, otherwise None
    attachment_filename = filename = dt.get_filename(filename_pattern, metadata=metadata) if as_attachment else None
    mimetype = request.args.get("mimetype", None)
    template = request.args.get("template", None) or request.headers.get("X-template", None)
    if template is not None:
        return dt.transform_data(dt.data, module='flask', method='make_template_response', filename=attachment_filename, template=template, mimetype=mimetype, metadata=metadata)
    elif isinstance(dt.data, DataFrame):
        return dt.transform_data(dt.data, module='flask', method='make_response_from_dataframe', filename=attachment_filename, mimetype=mimetype, metadata=metadata)
    elif isinstance(dt.data, (list, dict)):
        return dt.transform_data(dt.data, module='flask', method='make_json_response', filename=attachment_filename, mimetype=mimetype, metadata=metadata)
    elif filename is not None:
        return send_file(dt.data, attachment_filename=attachment_filename, mimetype=mimetype, as_attachment=as_attachment)
    elif isinstance(dt.data, BytesIO):
        return send_file(dt.data, as_attachment=False)
    return dt.data


def export_transformed_query(query, allowed_columns=None, filename_pattern=None, metadata=None, extra_operations=None, limit=None):
    """
        Querystring parameters are:

        ```?q={...}&f={...}&disposition=...&template=...&include_columns=[...]```

        Where `q` is a JSON search query (see [Making API search queries](#api_query)) on the collection model,
        where `f` is a JSON dictionary containing a sequence of operations for transforming the data
        (see [Data transformation API](#api_data_transform) for more info),
        `disposition` defines the download type which can be `attachment` (default, triggers a download) or `inline` (displays in
        the browser),
        `template` is optional depending on the output of the steps in `f` and can be used to define the export format,
        `include_columns` is a JSON list of column fields for the exported model.
        Nested fields for related models delimited with a `.`
        If `include_columns` is omitted, all allowed columns will be returned.

        Metadata for the [annotation_set](#annotation_set) matching `id` is also returned in the response. Depending on the output
        format, this could be as part of the response body in a `metadata` field or in the response headers with the key
        `X-Content-Metadata`.
    """
    assert isinstance(allowed_columns, list), \
        "`allowed_columns` not defined for an exporter endpoint. Must be a list of allowed fields."
    q = json.loads(request.args.get("q", "{}") or "{}")
    include_columns = (json.loads(request.args.get("include_columns", "[]") or "[]")
                       + request.args.getlist('c')) or allowed_columns
    include_columns = list(set(allowed_columns) & set(include_columns))

    # get the query
    operations = [dict(module="data", method="query_to_dicts", kwargs=dict(include_columns=include_columns))]
    # TODO: consider alternative to ._entity_zero() restricted method - find a public way to do this - this may be fragile...?
    collection_class = query._entity_zero().class_    # get class of model for query (assumes model query)
    query = search(session, collection_class, q, query=query)

    if isinstance(limit, int):
        query = query.limit(limit)

    # create data transformer
    dt = get_data_transformer(query, operations=operations, run_operations=False)
    if extra_operations:
        for op in extra_operations:
            dt.add_operation(**op)
    dt.run_all()

    # render result of data transformer
    result = render_data_transformer(dt, filename_pattern=filename_pattern, metadata=metadata)
    return result
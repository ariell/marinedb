from marinedb.compat import StringIO

from datetime import datetime
from pandas import read_csv, DataFrame, read_excel, merge
import jsondate as jsond


class FileMunge:
    def __init__(self, filepath_or_buffer, dbinstance=None):
        self.filedata = filepath_or_buffer
        self.dbinstance = dbinstance

    def loadcsv(self, columns=None, delimiter="\t", usecols=None, skiprows=None, comment="#"):
        if columns is None:
            self.filedata = read_csv(self.filedata, delimiter=delimiter, parse_dates=False, index_col=None,
                                     usecols=usecols, skiprows=skiprows, comment=comment)
        else:
            self.filedata = read_csv(self.filedata, delimiter=delimiter, parse_dates=False, index_col=None,
                                     usecols=usecols, skiprows=skiprows, header=None, names=columns, comment=comment)
        return self.filedata

    def loadjson(self, usefield=None):
        self.filedata = DataFrame.from_dict(jsond.loads(self.filedata)) if usefield is None else DataFrame.from_dict(
            jsond.loads(self.filedata)[usefield])
        return self.filedata

    def loadexcel(self):
        self.filedata = read_excel(StringIO(self.filedata))
        return self.filedata

    def aggregate(self, groupby=None, col=None, newcol=None, agg=None):
        if newcol is None:
            newcol = col
        self.filedata = self.filedata.groupby(groupby, as_index=False)[col].agg({newcol: agg})
        return self.filedata

    def drop(self, labels=None):
        self.filedata.drop(labels, axis=1, inplace=True)
        return self.filedata

    def rename(self, columns=None):
        self.filedata.columns = [columns[c] if c in columns else c for c in self.filedata.columns]
        return self.filedata

    def formatstr(self, col=None, newcol=None, format=None):
        if newcol is None:
            newcol = col
        self.filedata[newcol] = [format.format(i) for i in self.filedata[col]]
        return self.filedata

    def splitstr(self, col=None, newcol=None, sep="\t"):
        if newcol is None:
            newcol = col
        self.filedata[newcol] = [i.split(sep) for i in self.filedata[col]]
        return self.filedata

    def joincols(self, newcol=None, columns=None, sep="\t"):
        self.filedata[newcol] = self.filedata[columns].apply(lambda x: sep.join(map(str, x)), axis=1)
        return self.filedata

    def reformat(self, col=None, newcol=None, how=None, format=None):
        if newcol is None:
            newcol = col
        self.filedata[newcol] = [self._reformat_value(i, how, format=format) for i in self.filedata[col]]
        return self.filedata

    def df_to_dict(self, columns=None):
        if columns is None:  # if no columns are mapped, then just use existing column headings
            columns = dict(list(zip(self.filedata.columns, self.filedata.columns)))
        dictlist = []
        for index, row in self.filedata.iterrows():
            dictel = {}
            for k in columns:
                if isinstance(columns[k], dict):
                    if columns[k]["ref"] == "inherit":  # inherit value from field in this Model
                        dictel[k] = getattr(self.dbinstance, columns[k]['col'])
                    elif columns[k]["ref"] == "latlon":
                        tude = row[columns[k]['col']].strip()
                        multiplier = -1 if tude[-1] in ['S', 'W'] else 1
                        dictel[k] = multiplier * sum(float(x) / 60 ** n for n, x in enumerate(tude[:-1].split('-')))
                    elif columns[k]["ref"] == "literal":  # get value from literal
                        dictel[k] = columns[k]["value"]
                    elif columns[k]["ref"] == "struct":  # reformat from nested structure
                        dictel[k] = {i: row[columns[k]["format"][i]] for i in columns[k]["format"]}
                    elif columns[k]["ref"] == "structlist":  # reformat from nested structure
                        dictel[k] = {i: row[columns[k]["format"][i]] for i in columns[k]["format"]}
                    else:  # convert to datetime
                        dictel[k] = self._reformat_value(row[columns[k]['col']], columns[k]["ref"],
                                                         format=columns[k].get('format', None))
                else:
                    dictel[k] = row[columns[k]]
            dictlist.append(dictel)
        self.filedata = dictlist
        return self.filedata

    def dict_to_db(self, table=None, method="dict_to_db"):
        self.filedata = getattr(self._get_class_by_tablename(table), method)(self.filedata)
        return self.filedata

    def dict_to_json(self):
        self.filedata = jsond.dumps(self.filedata, indent=4, separators=(',', ': '))
        return self.filedata

    def df_to_csv(self, delimiter="\t"):
        self.filedata = self.filedata.to_csv(None, sep=str(delimiter), index=False, encoding='utf-8')
        return self.filedata

    def run_op(self, operation, *args, **kwargs):
        opfnc = getattr(self, operation)
        return opfnc(*args, **kwargs)

    @staticmethod
    def _reformat_value(val, how, format=None):
        # print (val, how, format)
        if how == "latlon":
            tude = val.strip()
            multiplier = -1 if tude[-1] in ['S', 'W'] else 1
            # print (multiplier * sum(float(x) / 60 ** n for n, x in enumerate(tude[:-1].split('-'))))
            return multiplier * sum(float(x) / 60 ** n for n, x in enumerate(tude[:-1].split('-')))
        elif how == "datetime":
            if format == "timestamp":
                return datetime.fromtimestamp(val)
            else:
                return datetime.strptime(val, format)

    def _get_class_by_tablename(self, tablename):
        """Return class reference mapped to table.

        :param tablename: String with name of table.
        :return: Class reference or None.
        """
        for c in list(self.dbinstance._decl_class_registry.values()):
            if hasattr(c, '__tablename__') and c.__tablename__ == tablename:
                return c


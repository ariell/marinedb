import inspect
import random
from time import sleep
import numpy as np

class AnnotationPointSampler:

    def __init__(self):
        pass

    def random(self, n: int = 10, **kwargs):
        """Random distribution of 'n' points"""
        xys = [{"x": random.random(), "y": random.random()} for i in range(int(n))]
        # sleep(10)
        return xys

    def radialdisk(self, n: int=10, r=50, **kwargs):
        """https://mathworld.wolfram.com/DiskPointPicking.html"""
        xys = []
        radius = float(r)/100
        for i in range(int(n)):
            r = random.random() * radius**2
            theta = random.random() * 2 * np.pi
            xys.append({"x": np.sqrt(r)*np.cos(theta)+.5, "y": np.sqrt(r)*np.sin(theta)+.5})
        return xys

    def poissondisk(self, n: int = 10, min_dist: float = 0, **kwargs):
        """Poisson Disk sampling. Random distribution of 'n' points with a minimum distance of 'min_dist' between each point."""
        # https://github.com/Compizfox/MDBrushGenerators/blob/master/PoissonDiskGenerator.py
        return []

    def grid(self, nX:int=5, nY:int=4, **kwargs):
        """Uniform grid of 'nX' x 'nY' points"""
        stepX = 1.0 / float(nX)
        stepY = 1.0 / float(nY)
        return [{"x": x, "y": y} for x in np.arange(stepX / 2, 1, stepX) for y in np.arange(stepY / 2, 1, stepY)]

    def quincunx(self, **kwargs):
        """Quincunx 5-point pattern arranged in a cross"""
        return [{"x": 1.0 / 6, "y": 1.0 / 6}, {"x": 5.0 / 6, "y": 1.0 / 6},
                                {"x": 3.0 / 6, "y": 3.0 / 6},
                {"x": 1.0 / 6, "y": 5.0 / 6}, {"x": 5.0 / 6, "y": 5.0 / 6}]

    def pointclick(self, **kwargs):
        """No points generated. All points created by annotator."""
        return []

    @classmethod
    def get_points(cls, method, margin_left=0, margin_right=0, margin_top=0, margin_bottom=0, *args, **kw):
        xys = []
        sampler = cls()
        if isinstance(method, str) and hasattr(sampler, method):
            fnc = getattr(sampler, method)
            xys = fnc(*args, **kw)
        if margin_left > 0 or margin_right > 0 or margin_top > 0 or margin_bottom > 0:
            w = (1.0 - (margin_right+margin_left)/100.0)
            h = (1.0 - (margin_top + margin_bottom)/100.0)
            xys = [dict(x=max(min(p.get('x')*w + margin_left/100, 1), 0), y=max(min(p.get('y')*h + margin_top/100, 1), 0))
                   for p in xys]
        return xys

    @classmethod
    def get_args(cls, method):
        sig = inspect.signature(getattr(cls, method))
        return [dict(name=p.name, type=p.annotation, default=p.default)
                for p in sig.parameters.values() if p.annotation != inspect._empty]

    @classmethod
    def get_methods(cls):
        return [dict(name=k, description=v.__doc__, parameters=cls.get_args(k)) for k, v in cls.__dict__.items()
                if not isinstance(v, classmethod) and not k.startswith("_")]


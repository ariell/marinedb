#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
# import sys
# import subprocess

# from flask import url_for, redirect, request
from datetime import datetime
from time import time
from flask_script import Manager, Shell, Server
from flask_migrate import MigrateCommand
# from werkzeug.serving import run_with_reloader

from marinedb.app import create_app, build_api
from marinedb.geodata.utils import deployments2dataframe
from marinedb.user.models import User
from marinedb.settings import DevConfig, ProdConfig
from marinedb.database import db
# from gevent.pywsgi import WSGIServer
# from gevent import monkey

if os.environ.get("MDB_ENV") == 'prod':
    app = create_app(ProdConfig)
else:
    app = create_app(DevConfig)

HERE = os.path.abspath(os.path.dirname(__file__))
TEST_PATH = os.path.join(HERE, 'tests')

manager = Manager(app)


def _make_context():
    """Return context dict for a shell session so you can access
    app, db, and the User model by default.
    """
    return {'app': app, 'db': db, 'User': User}


@manager.command
def test():
    """Run the tests."""
    import pytest
    exit_code = pytest.main([TEST_PATH, '--verbose'])
    return exit_code


@manager.command
def api_doc():
    """Build API documentation"""
    build_api(app, db, build_docs=True)

@manager.command
def wiki_toc():
    """Rebuild wiki TOC dynamically based on file structure"""
    from marinedb.public.views import get_wiki_toc
    get_wiki_toc(force_refresh=True)

@manager.command
def create_report(report_type, output_file=None, include_extras=False):
    """

    example:    python manage.py create_report deployment
    result:     generates a report in a CSV file in the 'reports/' directory that includes a bunch of info about deployments

    example:    python manage.py create_report deployment --include_extras
    result:     as above, but also includes ECOREGION info looked up in 3rd party API

    :param report_type:     must be one of 'deployment', ...
    :param output_file:     OPTIONAL, file to output results. If blank, one will be auto-generated
    :param include_extras:  OPTIONAL, bool switch whether or not to include extra fields
    :return:
    """
    if output_file is None:
        if not os.path.isdir("reports"): os.makedirs("reports")
        output_file = "reports/report-{}-{}.csv".format(report_type, datetime.now().strftime("%Y%m%d-%H%M%S"))
    include_extras = include_extras is not False
    tic = time()
    print("Generating '{}' report ({}, include_extras={})".format(report_type, output_file, include_extras))
    if report_type == "deployment":
        df=deployments2dataframe(include_extras=include_extras)
    # elif report_type == "annotation":
    #     pass   # generate an annotation report
    else:
        raise ValueError("Invalid report_type: `{}`. Must be one of: [deployment, ...]".format(report_type))
    result_count = len(df.index)
    df.to_csv(output_file)
    print("\rDone processing {} report, {} records in {:.2f}s...".format(report_type, result_count, time()-tic))




# @manager.command
# @run_with_reloader   # reload if change detected (useful for debug)
# def server():
#     """Start development server"""
#     # need to patch sockets to make requests async
#     http_server = WSGIServer(('', 5000), app)
#     http_server.serve_forever()

manager.add_command('server', Server(host="0.0.0.0", port=5000, threaded=True))

manager.add_command('shell', Shell(make_context=_make_context))
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    # monkey.patch_all()
    manager.run()

# MarineDB

An online API-based platform for managing, exploring & annotating georeferenced images, video & large-scale mosaics. Here is a video demo of the annotation workflow (as at AUG2016): https://youtu.be/8VFqsIXkDvY

## Quickstart / Development environment

For Ubuntu install postgresql, git and pip.

```
#!bash
sudo apt-get install postgresql git python-pip python-psycopg2 libpq-dev python-dev
sudo apt install postgis  #postgresql-12-postgis-3
```

For OSX, make sure you have brew installed, along with pip, then run:

```
#!bash
brew install postgres
brew install -s postgis
ln -sfv /usr/local/opt/postgresql/*.plist ~/Library/LaunchAgents/  # Add to Launch agents
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist  # Start postgres and launch on startup
# sudo launchctl load -w /System/Library/LaunchDaemons/org.apache.httpd.plist
```

Next, set your app's secret key as an environment variable. For example, example add the following to ``.bashrc`` or ``.bash_profile``.

```
#!bash
export MDB_SECRET='something-really-secret'
export MDB_PROJECT_NAME='MarineDB+'          # if you want to call it something else
```

Then run the following commands to bootstrap your environment.

```
#!bash
git clone https://ariell@bitbucket.org/ariell/marinedb.git
cd marinedb
pip install virtualenv
virtualenv env
source env/bin/activate
pip install -r requirements/dev.txt       # set up dependencies
```

If you run into issues with the pip installs, try running ```pip install --upgrade pip``` and then try install requirements again.

Next, if using postgres, you need to create the database:

```
#!bash
sudo su postgres
psql template1
ALTER USER postgres with encrypted password 'something-really-secret';
create EXTENSION if not EXISTS "uuid-ossp";  # add uuid extension
```

```
CREATE EXTENSION postgis;
```

Hit ```CTL+D``` to leave psql console, and then create db and restart service:

```
#!bash
createdb marinedb
sudo systemctl restart postgresql.service
```

Once you have installed your DBMS, run the following to create your app's database tables and perform the initial migration:

```
#!bash
python manage.py db init
python manage.py db migrate
python manage.py db upgrade
python manage.py server
```

You should now be able to view the site through the testing server in your local browser at: http://localhost:5000.


## Deployment on Ubuntu ##

Install postgresql, git, pip and apache:

```
#!bash
sudo apt-get install postgresql git python-pip python-psycopg2 libpq-dev python-dev
sudo apt-get install apache2 libapache2-mod-wsgi python-dev
```

Setup site:

```
#!bash
sudo a2enmod wsgi    # enable mod_wsgi
sudo nano /etc/apache2/sites-available/marinedb.conf
```

Paste the following:

```
<VirtualHost *:80>
    ServerName marinedb.org
    ServerAdmin ariell@greybits.com.au
    #WSGIDaemonProcess marinedb user=local_username threads=15
    WSGIScriptAlias / /var/www/marinedb/marinedb.wsgi
    <Directory /var/www/marinedb/marinedb/>
            Require all granted
    </Directory>
    Alias /static /var/www/marinedb/marinedb/static
    <Directory /var/www/marinedb/marinedb/static/>
            Require all granted
    </Directory>
    ErrorLog ${APACHE_LOG_DIR}/error.log
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Get site files and set up permissions:

```
#!bash
cd /var/www/
sudo mkdir marinedb                       # set up directory
sudo usermod -a -G www-data ubuntu        # add user to www-data group (ubuntu=local user)
sudo chown ubuntu:www-data marinedb/      # configure directory permissions
git clone https://ariell@bitbucket.org/ariell/marinedb.git    # get code
```

Add extra config settings for local install:

```
#!bash
nano /var/www/marinedb/marinedb/settings.cfg
```

Paste the following:

```
#!python
PROJECT_NAME = 'SQUIDLE+'
SECRET_KEY = 'highly-secure'
SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:something-really-secret@localhost/marinedb'
MEDIA_UPLOAD_DIR = "/home/ubuntu/DATA/media/"
MEDIA_THM_CACHE_DIR = "/home/ubuntu/DATA/thumbnails/"
CACHE_DIR = "/home/ubuntu/marinedb/_cache"
```


**NOTE: IF YOU WANT TO MOVE THE DB LOCATION SEE BELOW.**

Setup the database. If using postgres, create the database using:

```
#!bash
sudo su postgres
psql template1
ALTER USER postgres with encrypted password 'something-really-secret';
```

Hit ```CTL+D``` to leave psql console, and then create db and restart service:

```
#!bash
createdb marinedb
sudo service postgresql restart
```

Set up site environment and enable site in apache:

```
#!bash
cd marinedb
pip install virtualenv
virtualenv env
source env/bin/activate
pip install -r requirements/dev.txt       # set up dependencies
sudo a2ensite marinedb                    # enable site
sudo service apache2 reload               # reload apache
```

Setup permissions for certain files:
```
#!bash
sudo chgrp -R www-data /var/www/marinedb
sudo chmod -R g+w /var/www/marinedb/marinedb/static/
```

Spoof link to fix bug with fontawesome:
```
cd /var/www/marinedb/marinedb/static/
ln -s libs/font-awesome-4.3.0/fonts .
```


### Move Postgres Database storage location
Check current location
```
sudo -u postgres psql
postgres=# SHOW data_directory;
```

Then move it:
```
#!bash
sudo systemctl stop postgresql # stop service
sudo systemctl status postgresql  # check it has been stopped
sudo rsync -av /var/lib/postgresql /path/to/new/location # change locations as required
sudo mv /var/lib/postgresql/9.5/main /var/lib/postgresql/9.5/main.bak  # make backup so we know it's not being used
```

Pointing to the New Data Location
```
#!bash
sudo nano /etc/postgresql/9.5/main/postgresql.conf
```

Change location in config file:
```
data_directory = '/path/to/new/location/postgresql/9.5/main'
```

Restart postgres:
```
sudo systemctl start postgresql
sudo systemctl status postgresql  # confirm that it is working
```


## Shell ##

To open the interactive shell, run:

```
#!bash
python manage.py shell
```

By default, you will have access to ``app``, ``db``, and the ``User`` model.


## Running Tests (NOT WORKING)  ##

To run all tests, run:

```
#!bash
python manage.py test
```

## Miscellaneous ##

### Editing handlebars templates ###

In order to recompile all handlebars templates contained in api_libs, make sure the handlebars compiler is installed and then run:

```
#!bash
ls -d static/api_libs/*/templates | xargs -I {} sh -c "handlebars {}> {}/templates.js"
```

This needs to be done whenever the api_libs handlebars templates are edited.


### Migrations ###

Whenever a database migration needs to be made. Run the following commmands:

```
#!bash
python manage.py db migrate
```

This will generate a new migration script. Then run:

```
#!bash
python manage.py db upgrade
```

To apply the migration.

For a full migration command reference, run ``python manage.py db --help``.

Note: in order to detect migrations for more subtle changes like column type modifications, you need to edit "migrations/env.py"

Change:

```
#!python
context.configure(
    connection=connection,
    target_metadata=target_metadata
    )
```

To:

```
#!python
context.configure(
    connection=connection,
    target_metadata=target_metadata,
    compare_type=True
    )
```

In order to make the use of Alembic easier, a few helpers are provided in geoalchemy2.alembic_helpers. These helpers
can be used in the `env.py` file used by Alembic, like in the following example:

```#!python
from geoalchemy2.alembic_helpers import include_object
from geoalchemy2.alembic_helpers import render_item

def run_migrations_offline():
    context.configure(
        ...,
        render_item=render_item,
        include_object=include_object,
    )
    
def run_migrations_online():
    context.configure(  
        ...,
        render_item=render_item,
        include_object=include_object,
    )
```
After running the alembic command, the migration script will be properly generated and should not need to be
manually edited.


### Adding reverse proxy:

This can be useful to expose media files from a local/privately visible that the system is referencing (if needed)

Enable modules:
```
#!bash
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod proxy_balancer
sudo a2enmod lbmethod_byrequests
```
Where ``mod_proxy`` is the main proxy module Apache module for redirecting connections; it allows Apache to act as a gateway to the underlying application servers.
``mod_proxy_http`` adds support for proxying HTTP connections.
``mod_proxy_balancer`` and ``mod_lbmethod_byrequests`` add load balancing features for multiple backend servers.


The add the ``ProxyPass`` and ``ProxyPassReverse`` parameters to the ``/etc/apache2/sites-available/marinedb.conf`` file:

```
<VirtualHost *:80>
    #ProxyPreserveHost On
    ProxyPass /ovdmdata/ http://10.23.9.127/
    ProxyPassReverse /ovdmdata/ http://10.23.9.127/
</VirtualHost>
```

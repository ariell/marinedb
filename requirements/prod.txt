# Everything needed in production

# Management script
Flask-Script


# Flask
Flask==0.10.1
MarkupSafe==0.23
Werkzeug==0.16.0
Jinja2==2.11.3
itsdangerous==0.24

# Documentation
markdown

# Database
#psycopg2-binary
psycopg2    # seems this is needed for python >= 3.9
alembic==1.4.2
Flask-SQLAlchemy==2.0
#SQLAlchemy==1.2.0
SQLAlchemy==1.3.20
pandas
xlrd  # optional dependency for pandas to read excel
marshmallow-sqlalchemy  # serialization
flask_marshmallow  # serialization
geoalchemy2
shapely  # for geoalchemy2
sqlalchemy_continuum  # database versioning

# Validation
savalidation

# API
# Flask-Restless==0.17.0  # this is a local module now

# Utils
jsondate   # replacement for json that handles dates properly
flask-compress     # compressed responses
requests

# Migrations
Flask-Migrate==1.3.1

# Forms
Flask-WTF==0.11
WTForms==2.0.2

# Deployment
gunicorn>=19.1.1

# Assets
Flask-Assets==0.10
cssmin>=0.2.0
jsmin>=2.0.11

# Auth
Flask-Login==0.2.11
Flask-Bcrypt==0.6.2    # NB: newer flask-bcrypt causes encoding bug for password hash

# Caching
Flask-Cache>=0.13.1

# Debug toolbar
Flask-DebugToolbar==0.9.2

# Image processing
scikit-image

# humanise displays
Flask-Humanize

# Alternative server options
#gevent==20.5.2

# Emails
flask_mail

# App performance monitoring
#flask_monitoringdashboard

# Mod-wsgi for deployment (referenced in the apache site config)
mod_wsgi

mimerender

simplejson

timezonefinder  # offline timezone lookup

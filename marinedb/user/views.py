# -*- coding: utf-8 -*-
from flask import Blueprint, render_template, request, flash, jsonify, redirect, url_for
from flask_login import login_user, login_required, logout_user, current_user
from marinedb.extensions import login_manager
from marinedb.user.models import User, Group, UserGroupAssociation
from marinedb.database import db
import json



blueprint = Blueprint("user", __name__, url_prefix='/users', static_folder="../static", template_folder="templates")


@blueprint.route("/")
@login_required
def members():
    return render_template("users/members.html")


@blueprint.route("/signup")
def signup():
    msg = {"msg": request.args.get('msg'), "status": request.args.get('status')} if request.args.get('msg') else None

    if current_user.is_authenticated(): # if user is logged in, show
        action = "/api/users/{}".format(current_user.id)
        method = "PATCH"
    else:
        action = "/api/users"
        method = "POST"
    return render_template("users/forms/usersignup.html", action=action, method=method, msg=msg)


# @blueprint.route("/api_token")
# def api_token():
#     return render_template("users/forms/templates/models/users/api_token.html")

# @blueprint.route("/login", methods=["GET", "POST"])
# def login():
#     if request.method == 'POST':
#         data = request.get_json(force=True)
#         username = data["username"]
#         password = data["password"]
#         user = User.query.filter_by(username=username).first()
#         if not user:
#             resp = jsonify(validation_errors={"username": "Invalid username"})
#             resp.status_code = 401
#         elif not user.check_password(password):
#             resp = jsonify(message="Invalid username or password", validation_errors={"username":"", "password":""})
#             resp.status_code = 401
#         elif not user.active:
#             resp = jsonify(message="Your account is not activated")
#             resp.status_code = 401
#         else:
#             login_user(user)
#             flash("You have been logged in successfully!", "success")
#             resp = jsonify({"message": "success"})
#             resp.status_code = 200
#
#         return resp
#     else:
#         msg = {"msg": request.args.get('msg'), "status": request.args.get('status')} if request.args.get('msg') else None
#         return render_template("users/forms/usersignin.html", msg=msg)


# @blueprint.route('/usergroup', defaults={'group_id': None, 'user_id': None, 'method': None}, methods=["POST", "DELETE"])
# @login_required
# def manageusergroup(group_id, user_id, method):
#
#     method = request.method if method == None else method
#
#     if method == "DELETE":  # delete
#         data = request.form
#         group_id = int(data['group_id']) if group_id == None else group_id
#         user_id = int(data['user_id']) if user_id == None else user_id
#         group = Group.query.filter_by(id=group_id).one()
#         data, msg = group.removeuser(user_id)
#     elif method == "POST":  # add
#         data = request.get_json()
#         group_id = int(data['group_id']) if group_id == None else group_id
#         user_id = int(data['user_id']) if user_id == None else user_id
#         group = Group.query.filter_by(id=group_id).one()
#         data, msg = group.adduser(user_id)
#
#     # if response contains a message with an error in it, return an error response and the error message
#     if "error" in msg:
#         return jsonify({'message': msg['error']}), 400, {'ContentType': 'application/json'}
#     else:
#         return jsonify(data)


# @blueprint.route('/logout')
# @login_required
# def logout():
#     logout_user()
#     flash('You are logged out.', 'info')
#     return redirect(url_for('public.home'))
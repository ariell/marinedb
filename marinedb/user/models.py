# -*- coding: utf-8 -*-
import datetime as dt

from flask_login import UserMixin, current_user
from flask import request, jsonify
from flask.views import MethodView
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.serializer import loads, dumps

from marinedb.extensions import bcrypt
from marinedb.database import (
    Column,
    db,
    Model,
    ReferenceCol,
    relationship,
    backref,
    SurrogatePK,
    UniqueConstraint,
)
#from flask_sqlalchemy import before_models_committed  # models_committed
# from sqlalchemy import event
from savalidation import ValidationMixin, ValidationError
import savalidation.validators as val
from savalidation.helpers import before_flush
from modules.flask_restless import ProcessingException
import sys
import hashlib
import random

# class CustomValidationError(Exception):
#     def __init__(self, errors):
#         self.errors = errors
#
from modules.flask_restless.mixins import APIModel


def get_current_user():
    return current_user.id if current_user.is_authenticated() else None


class Role(APIModel, SurrogatePK, Model):
    __tablename__ = 'roles'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), unique=True, nullable=False)

    def __init__(self, name, **kwargs):
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<Role({name})>'.format(name=self.name)


class User(APIModel, UserMixin, SurrogatePK, Model):

    __tablename__ = 'users'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    username = Column(db.String(80), unique=True, nullable=False)
    email = Column(db.String(80), unique=True, nullable=False)
    password = Column(db.String(128), nullable=False) #: The hashed password
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    last_login = Column(db.DateTime, default=db.func.now())
    first_name = Column(db.String(30), nullable=False)
    last_name = Column(db.String(30), nullable=False)
    active = Column(db.Boolean(), default=True)
    is_admin = Column(db.Boolean(), default=False)
    info = Column(db.Text, nullable=True)
    role_id = ReferenceCol('roles', nullable=True)
    role = relationship('Role')
    api_token = Column(db.String(80), unique=True, nullable=True)

    affiliations_usergroups = relationship(
        "UserGroupAssociation",
        # primaryjoin="and_(Group.id==UserGroupAssociation.group_id, User.id==UserGroupAssociation.user_id, Group.grouptype=='affiliation')",
        primaryjoin="and_(Group.id==UserGroupAssociation.group_id, User.id==UserGroupAssociation.user_id, Group.group_type_id == GroupType.id, GroupType.name=='affiliation')",
        cascade="all, delete-orphan"
    )
    affiliations = association_proxy('affiliations_usergroups', 'group')

    def __init__(self, username, email, password=None, **kwargs):
        db.Model.__init__(self, username=username, email=email, **kwargs)
        if password is not None:
            self.set_password(password)

    def __repr__(self):
        return '<User({username!r})>'.format(username=self.username)

    def set_password(self, password):
        self.password = str(bcrypt.generate_password_hash(password))

    def check_password(self, value):
        return bcrypt.check_password_hash(str(self.password), value)

    def _set_api_token(self):
        self.api_token = hashlib.sha224("{}{}".format(self.email,str(random.getrandbits(256))).encode('utf-8')).hexdigest()
        db.session.commit()

    def full_name(self):
        return "{0} {1}".format(self.first_name.encode('utf-8').decode('utf-8','ignore'), self.last_name.encode('utf-8').decode('utf-8','ignore'))

    def affiliations_names(self):
        return " / ".join(["{} ({})".format(i.name, i.groupinfo) for i in self.affiliations])


class UserGroupAssociation(APIModel, Model):
    __tablename__ = 'usergroups'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    user_id = Column(db.Integer, db.ForeignKey('users.id'), primary_key=True, index=True)
    group_id = Column(db.Integer, db.ForeignKey('groups.id'), primary_key=True, index=True)
    created_at = Column(db.DateTime, default=dt.datetime.utcnow)
    approved = Column(db.Boolean(), default=False)
    can_edit = Column(db.Boolean(), default=False)
    can_add_members = Column(db.Boolean(), default=False)
    can_share_data = Column(db.Boolean(), default=False)
    has_agreed = Column(db.Boolean(), default=False)
    added_by_user_id = Column(db.Integer, db.ForeignKey('users.id'), nullable=True)
    user = relationship("User", foreign_keys=[user_id], backref=backref('usergroups', lazy="dynamic"))
    added_by_user = relationship("User", foreign_keys=[added_by_user_id])
    group = relationship("Group", backref=backref('usergroups', cascade="all, delete-orphan", lazy="dynamic"))

    def __init__(self, user_id, group_id, **kwargs):
        db.Model.__init__(self, user_id=user_id, group_id=group_id, **kwargs)


class GroupType(APIModel, SurrogatePK, Model):
    __tablename__ = 'group_types'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(128), nullable=False)


class Group(APIModel, SurrogatePK, Model):
    __tablename__ = 'groups'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(128), nullable=False, doc="Name of Group")
    is_public = Column(db.Boolean(), default=False, doc="Whether or not contents are publicly viewable")
    is_restricted = Column(db.Boolean(), default=True, doc="Whether or not members can see other member's datasets")
    requires_approval = Column(db.Boolean(), default=True, doc="Whether or not custodian approval is required")
    requires_agreement = Column(db.Boolean(), default=False,
                                doc="Whether or not members need to agree to Data Usage Agreement (DUA)")
    description = Column(db.Text, nullable=True, doc="Extra information about the Group")
    dua_link = Column(db.Text, nullable=True,
                      doc="Data Usage Agreement (DUA) link. Must be an HTTP accessible, embeddable document")
    user_id = ReferenceCol('users', nullable=True, doc="`id` of group custodian [user](#users)")
    user = relationship("User", doc="Related [user](#users) for group custodian")
    group_type_id = ReferenceCol('group_types', nullable=False, doc="`id` of related [group_type](#group_type)")
    group_type = relationship("GroupType", doc="related [group_type](#group_type) object")
    # grouptype = Column(db.String(64), nullable=False)
    groupinfo = Column(db.Text, nullable=True, doc="Shorter description / tagline of Group")
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow, doc="Datetime of creation")

    # Enforce uniqueness constraint on user and name
    __table_args__ = (UniqueConstraint('name', 'user_id', name='_group_name_uc'),)

    # users = association_proxy('usergroups', 'user')
    members = relationship("User", secondary="usergroups", lazy='dynamic', secondaryjoin="UserGroupAssociation.user_id==User.id")

    def __init__(self, name, **kwargs):
        db.Model.__init__(self, name=name, **kwargs)

    def __repr__(self):
        return '<Group({name})>'.format(name=self.name)

    def member_count(self):
        return UserGroupAssociation.query.filter(UserGroupAssociation.group_id == self.id, UserGroupAssociation.approved.is_(True)).count()

    def pending_member_count(self):
        return UserGroupAssociation.query.filter(UserGroupAssociation.group_id == self.id, UserGroupAssociation.approved.is_(False)).count()

    def dataset_counts(self):
        """the number of different datasets that are shared in this group"""
        # TODO: automatically work out `shared_*` property backrefs and populate dict
        return dict(
            label_schemes=self.shared_label_schemes.count(),
            media_collections=self.shared_media_collections.count(),
            annotation_sets=self.shared_annotation_sets.count()
        )

    # def _check_request_user_permissions(self, user_id, request_user_id, action_msg="add"):
    #     if request_user_id is None:
    #         request_user_id = current_user.id
    #     if self.is_restricted and not self.user_is_owner(request_user_id):
    #         raise ProcessingException(description='Unauthorised. This group is restricted. '
    #                                               'Only the owner can %s members' % action_msg, code=401)
    #     if not self.is_restricted and not (self.user_is_member(request_user_id) or
    #                                        self.user_is_owner(request_user_id)) and user_id != request_user_id:
    #         raise ProcessingException(description='Unauthorised. Only members can %s members to this group.' % action_msg, code=401)
    #     return request_user_id

    # ADD user to group
    def add_update_user(self, user_id, approved=False, can_edit=False, has_agreed=False, can_add_members=False,
                        can_share_data=False, request_user_id=None, update_existing=False, commit=True):
        # Check request permissions: if restricted group, only owner can add
        # request_user_id = self._check_request_user_permissions(user_id, request_user_id, action_msg="edit" if update_existing else "add")
        if request_user_id is None:
            raise ProcessingException(description='request_user_id is not set', code=400)

        ug = UserGroupAssociation.query.filter(
            UserGroupAssociation.user_id == user_id, UserGroupAssociation.group_id == self.id).one_or_none()

        if user_id != request_user_id:
            # if the requesting user is not an edit member, ensure that they cannot assign edit permissions
            if not self.user_can_add_member(request_user_id):
                raise ProcessingException(description="Unauthorised. You do not have permission to add/edit members. "
                                                      "Contact the Custodian for permission.", code=401)
            # if requester is not the owner
            if not self.user_is_owner(request_user_id):
                # check requester is a member
                if not self.user_is_member(request_user_id):
                    raise ProcessingException(description='Unauthorised. Only members can add/edit group members.', code=401)

                # only allow members to edit other members that they added
                if update_existing and ug is not None and ug.added_by_user_id != request_user_id:
                    raise ProcessingException(description="Unauthorised. You can only edit members that you added.", code=401)

                # make sure that the requesting user is not assigning rights they don't have
                if can_edit is True and not self.user_is_edit_member(request_user_id):
                    raise ProcessingException(description="Unauthorised. You can not assign edit rights if you don't "
                                                          "have edit rights.", code=401)
                if can_share_data is True and not self.user_can_share_data(request_user_id):
                    raise ProcessingException(description="Unauthorised. You can not assign data sharing rights if you "
                                                          "don't have data sharing rights.", code=401)

            # if group requires custodian approval, check requester is owner, otherwise set to false
            if self.requires_approval and not self.user_is_owner(request_user_id):
                approved = False

            # if requester is not themself, don't set has_agreed
            has_agreed = ug.has_agreed if ug is not None else False
        elif not self.user_is_owner(request_user_id):  # the requester is changing their own membership
            if ug is not None:   # if updating, check if changes are kosher
                if (can_edit is True and ug.can_edit is False) or \
                        (can_share_data is True and ug.can_share_data is False) or \
                        (can_add_members is True and ug.can_add_members is False):
                    approved = False   # user is trying to elevate their rights so revoke approval
                else:
                    approved = ug.approved
            else:
                approved = False


        try:
            # if user is in group and not updating, make sure it doesn't exist before creating
            data = dict(
                user_id=user_id, added_by_user_id=request_user_id, group_id=self.id, can_add_members=can_add_members,
                can_edit=can_edit, approved=approved, has_agreed=has_agreed, can_share_data=can_share_data)
            data = {k: v for k, v in data.items() if v is not None}  # filter out null values
            if not update_existing:
                if ug is not None:
                    raise ProcessingException(description='Member is already in group. Cannot re-add.', code=409)
                ug = UserGroupAssociation.create(commit=commit, **data)
            # if user is in group and updating, update existing, otherwise raise error
            elif update_existing:
                if ug is None:
                    ProcessingException(description='Member not in group. Cannot update their membership.', code=409)
                ug = ug.update(commit=commit, **data)
            return ug
        except Exception as e:
            db.session.rollback()
            raise ProcessingException(description="Error adding user to group: {}.".format(e), code=500)

    # REMOVE user from group
    def remove_user(self, user_id, request_user_id=None):
        # request_user_id = self._check_request_user_permissions(user_id, request_user_id, action_msg="remove")
        if request_user_id is None:
            raise ProcessingException(description='request_user_id is not set', code=400)
        ug = UserGroupAssociation.query.filter(
            UserGroupAssociation.user_id == user_id, UserGroupAssociation.group_id == self.id).one()
        if user_id != request_user_id:    # don't raise error if user is removing themselves
            if not self.user_is_owner(request_user_id):  # let owners do whatever they want
                # only allow members to remove people they added if they still have rights
                if not self.user_can_add_member(request_user_id) or ug.added_by_user_id != request_user_id:
                    raise ProcessingException(description="Unauthorised. You don't have permission to "
                                                          "remove this member", code=401)
        if ug:
            try:
                ug.delete()
            except Exception as e:
                print(e)
                db.session.rollback()
                raise ProcessingException(description="Error removing user from group: {}.".format(e), code=500)
        return None

    # check if user is a member
    def user_is_member(self, user_id):
        return db.session.query(self.members.filter(UserGroupAssociation.user_id == user_id,
                                UserGroupAssociation.approved.is_(True)).exists()).scalar()

    def user_is_pending_approval(self, user_id):
        return db.session.query(self.members.filter(UserGroupAssociation.user_id == user_id,
                                UserGroupAssociation.approved.is_(False)).exists()).scalar()

    def user_is_edit_member(self, user_id):
        return db.session.query(self.members.filter(UserGroupAssociation.user_id == user_id,
                                UserGroupAssociation.can_edit.is_(True),
                                UserGroupAssociation.approved.is_(True)).exists()).scalar()

    def user_can_add_member(self, user_id):
        return self.user_is_owner(user_id) or db.session.query(self.members.filter(
            UserGroupAssociation.user_id == user_id, UserGroupAssociation.can_add_members.is_(True),
            UserGroupAssociation.approved.is_(True)).exists()).scalar()

    def user_can_share_data(self, user_id):
        return self.user_is_owner(user_id) or db.session.query(self.members.filter(
            UserGroupAssociation.user_id == user_id, UserGroupAssociation.can_share_data.is_(True),
            UserGroupAssociation.approved.is_(True)).exists()).scalar()

    # check if user is owner
    def user_is_owner(self, user_id):
        return user_id == self.user_id

    def user_has_agreed(self, user_id):
        return db.session.query(self.members.filter(
            UserGroupAssociation.user_id == user_id, UserGroupAssociation.has_agreed.is_(True)).exists()).scalar()






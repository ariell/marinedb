# # Check is current user is in a given group using only ids as inputs
# def user_in_group (grpid, user_id=None):
#     # import here to remove circular dependence error on import
#     from marinedb.user.models import UserGroupAssociation
#
#     # if no user is supplied, check the current user
#     if user_id is None and current_user.is_authenticated():
#         user_id = current_user.id
#
#     if user_id:
#         # if we find a match, then the user is in the group
#         if UserGroupAssociation.query.filter_by(group_id=grpid, user_id=user_id).first():
#             return True
#
#     return False


# class InheritUserGroupMixin(object):
#     __table_args__ = {'extend_existing': True}
#     allow_anonymous_viewing = True
#     inherit_usergroup_column_name = None
#
#
#     @hybrid_property
#     def is_public(self):
#         #return getattr(self, self.inherit_usergroup_column_name).is_public
#         return self.media_collection.is_public
#
    # @is_public.expression
    # def is_public(cls):
    #     return getattr(cls, cls.inherit_usergroup_column_name).is_public
    #
    # # Add apifield for current user denoting whether or not they own the group
    # @hybrid_property
    # def current_user_is_owner(self):
    #     return getattr(self, self.inherit_usergroup_column_name).current_user_is_owner
    #
    # @current_user_is_owner.expression
    # def current_user_is_owner(cls):
    #     return getattr(cls, cls.inherit_usergroup_column_name).current_user_is_owner
    #
    # @hybrid_property
    # def current_user_can_view(self):
    #     return getattr(self, self.inherit_usergroup_column_name).current_user_can_view
    #     # return False
    #
    # @current_user_can_view.expression
    # def current_user_can_view(cls):
    #     return getattr(cls, cls.inherit_usergroup_column_name).current_user_can_view
    #
    # @hybrid_property
    # def current_user_can_edit(self):
    #     return getattr(self, self.inherit_usergroup_column_name).current_user_can_edit()
    #
    # @current_user_can_edit.expression
    # def current_user_can_edit(cls):
    #     return getattr(cls, cls.inherit_usergroup_column_name).current_user_can_edit



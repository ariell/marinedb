import json
import os.path
import sys
import uuid

from flask import request, jsonify, flash, redirect, url_for, render_template, current_app, session
from flask_login import current_user, login_user, logout_user
from flask_mail import Message
from itsdangerous import URLSafeTimedSerializer
from savalidation import ValidationMixin
from savalidation.helpers import before_flush
from sqlalchemy import inspect, or_
from sqlalchemy.ext.hybrid import hybrid_property

from marinedb.extensions import db
from modules.flask_restless.views import render_data_response
from .models import Group, UserGroupAssociation, User, Role, GroupType
from modules.flask_restless import ProcessingException
from modules.flask_restless.mixins import APIModelEndpoint
from modules.flask_restless.mixins import APIModel, add_to_api
import savalidation.validators as val
from datetime import datetime
from marinedb.extensions import mail


PASSWORD_SALT = "password-salt-1234567890987654321"


def send_email(subject, html, recipients, sender):
    msg = Message(subject, recipients=recipients, sender=sender)
    msg.html = html
    # If logdir is set in config, log HTML of each email sent. Otherwise, just
    if current_app.config.get("MAIL_LOG_TO_DIR") is not None and os.path.isdir(current_app.config.get("MAIL_LOG_TO_DIR")):
        with open(os.path.join(current_app.config.get("MAIL_LOG_TO_DIR"), str(uuid.uuid4())+".html"), 'w') as f:
            print("<h1>{}</h1><b>SENDER:</b> {}<br><b>RECIPIENT(S):</b> {}<br>{}<br><br>{}".format(subject, sender, recipients, datetime.now(), html), file=f)
    mail.send(msg)


def user_token_login(*args, **kw):
    auth_token = request.headers.get("X-auth-token") or request.headers.get("auth-token", None) or None  # keep both for backwards compatibility
    if auth_token is not None:  # if auth token provided, then log user in for session
        user = User.query.filter_by(api_token=auth_token).one_or_none()
        if user:
            login_user(user)
            current_app.logger.debug("User logged in with API Token, ID: {}".format(current_user.id))
        else:
            raise ProcessingException(description="Unrecognised auth-token...", code=401)
    # print("Current user: {}".format(current_user.username if current_user.is_authenticated() else None))


class GroupAPI(Group, ValidationMixin):
    """
    Group resource: user groups for managing shared resources and permissions.
    """
    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    api_include_columns = ['id', 'name', 'is_public', 'dua_link', 'user', 'user.id', 'user.first_name',
                           'user.last_name', 'user.username', 'is_restricted', 'group_type', 'groupinfo', 'created_at',
                           'current_user_is_edit_member','current_user_is_member', 'current_user_is_owner',
                           'current_user_can_share_data', 'current_user_can_add_member', 'current_user_has_agreed',
                           'current_user_is_pending_approval', 'requires_agreement', 'requires_approval']
    api_include_methods = ["member_count", "pending_member_count"]
    api_add_columns_single = ['description']
    api_add_methods_single = ['dataset_counts']
    api_max_results_per_page = 10000
    api_allow_functions = True
    api_allow_anonymous_post = True

    api_postprocessors = {
        # "GET_SINGLE": ['get_single_postprocessor'],
        "POST": ["post_postprocessor"]
    }
    api_preprocessors = {
        "DELETE": ['delete_preprocessor'],
        "POST": ["group_data_preprocessor"],
        "PATCH_SINGLE": ["group_data_preprocessor"]
    }

    val.validates_constraints()

    @before_flush
    def _validate(self):
        if GroupAPI.query.filter(  # if group exists and not updating itself
                GroupAPI.name == self.name, GroupAPI.user_id == self.user_id, GroupAPI.id != self.id).first():
            self.add_validation_error('name', 'You already own a group with that name. Please try a different name.')
        if self.requires_agreement and self.dua_link is None:
            self.add_validation_error('dua_link', 'You need to provide a DUA link for groups requiring agreement.')

    @classmethod
    def delete_preprocessor(cls, instance_id=None, **kw):
        # Next, check if the user is authorized to modify the specified
        # instance of the model.
        if not Group.get_by_id(instance_id).current_user_is_owner():
            raise ProcessingException(description='Unauthorised. Only the group owner can delete this group!', code=401)

    @classmethod
    def post_postprocessor(cls, result=None, instance=None, **kw):
        # automatically make group public and unrestricted if no user id / owner is supplied
        if instance.user_id is None:
            instance.update(is_public=True, is_restricted=False)

    @classmethod
    def group_data_preprocessor(cls, data=None, **kw):
        """Handle group_type_name parameter and secure against membership/dataset tampering"""
        if "group_type_name" in data:
            group_type_name = data.pop("group_type_name")
            group_type, created = GroupType.get_or_create(name=group_type_name)
            data["group_type_id"] = group_type.id

        # disable adding of members through this endpoint
        data.pop("usergroups", None)
        data.pop("members", None)

        # disable adding of datasets through this endpoint
        for key in data.keys():
            if key.startswith("added_"):
                data.pop(key, None)
        # print(data)

    # @classmethod
    # def get_single_postprocessor(cls, result=None, instance=None, **kw):
    #     includes = request.args.getlist("include")
    #     # instance = cls.get_by_id(result.get('id'))
    #
    #     if "members" in includes:
    #         result["members"] = []
    #         for u in instance.usergroups.order_by(db.desc(UserGroupAssociation.created_at)):
    #             membr = UserAPI.model_to_dict(u.user)
    #             membr["membership"] = dict(group_id=instance.id, added_at=u.created_at, approved=u.approved, can_edit=u.can_edit,
    #                                        current_user_is_owner=instance.current_user_is_owner, added_by_user_id=u.added_by_user_id)
    #             result["members"].append(membr)
    #         # result["members"] = [
    #         #     dict(added_at=u.created_at, current_user_is_owner=g.current_user_is_owner, approved=u.approved,
    #         #          can_edit=u.can_edit, added_by_user_id=u.added_by_user_id, **UserAPI.model_to_dict(u.user))
    #         #     for u in g.usergroups.order_by(db.desc(UserGroupAssociation.created_at))]
    #         if "nonmembers" in includes:
    #             nonmembers = UserAPI.query.filter(UserAPI.id != instance.user_id, ~UserAPI.id.in_([u.get('id') for u in result["members"]]))
    #             result["nonmembers"] = [u.instance_to_dict() for u in nonmembers]


    # Add apifield for current user denoting whether or not they own the group
    # def current_user_is_owner(self):
    #     return self.user_is_owner(current_user.id) if current_user.is_authenticated() else False
    #
    # # Add apifield for current user denoting whether or not they are members
    # def current_user_is_member(self):
    #     return self.user_is_member(current_user.id) if current_user.is_authenticated() else False

    @hybrid_property
    def current_user_is_owner(self):
        """

        :return:
        """
        return self.user_is_owner(current_user.id) \
            if current_user.is_authenticated() \
            else False

    @current_user_is_owner.expression
    def current_user_is_owner(cls):
        """
        q={"filters":[{"name":"current_user_is_owner","op":"eq","val":true}]}
        q={"order_by":[{"field":"current_user_is_owner","direction":"desc"}]}
        :return:
        """
        primary_key = inspect(cls).primary_key[0]
        if current_user.is_authenticated():
            subquery = db.session.query(primary_key).filter(cls.user_id == current_user.id)
            return primary_key.in_(subquery)
        else:
            return False

    @hybrid_property
    def current_user_is_pending_approval(self):
        """

        :return:
        """
        return self.current_user_is_owner or self.user_is_pending_approval(current_user.id) \
            if current_user.is_authenticated() \
            else False

    @current_user_is_pending_approval.expression
    def current_user_is_pending_approval(cls):
        """
        Return true if user is an APPROVED group member or the item owner or it is part of a public group

        q={"filters":[{"name":"current_user_can_view","op":"eq","val":true}]}
        q={"order_by":[{"field":"current_user_can_view","direction":"desc"}]}
        :return:
        """
        if current_user.is_authenticated():
            # subquery to check if current user is approved member or owner
            primary_key = inspect(cls).primary_key[0]
            subquery = db.session.query(primary_key).filter(
                UserGroupAssociation.approved.is_(False),
                UserGroupAssociation.group_id == Group.id, UserGroupAssociation.user_id == current_user.id)
            return db.not_(or_(cls.current_user_is_owner, primary_key.notin_(subquery)))
        else:
            return False


    @hybrid_property
    def current_user_is_member(self):
        """

        :return:
        """
        return self.current_user_is_owner or self.user_is_member(current_user.id) \
            if current_user.is_authenticated() \
            else False

    @current_user_is_member.expression
    def current_user_is_member(cls):
        """
        Return true if user is an APPROVED group member or the item owner or it is part of a public group

        q={"filters":[{"name":"current_user_can_view","op":"eq","val":true}]}
        q={"order_by":[{"field":"current_user_can_view","direction":"desc"}]}
        :return:
        """
        if current_user.is_authenticated():
            # subquery to check if current user is approved member or owner
            primary_key = inspect(cls).primary_key[0]
            subquery = db.session.query(primary_key).filter(
                UserGroupAssociation.group_id == Group.id, UserGroupAssociation.user_id == current_user.id,
                UserGroupAssociation.approved.is_(True))
            return primary_key.in_(subquery)
        else:
            return False

    @hybrid_property
    def current_user_is_edit_member(self):
        """

        :return:
        """
        return self.user_is_owner(current_user.id) or self.user_is_edit_member(current_user.id) \
            if current_user.is_authenticated() \
            else False

    @current_user_is_edit_member.expression
    def current_user_is_edit_member(cls):
        if current_user.is_authenticated():
            # subquery to check if current user is approved member or owner
            primary_key = inspect(cls).primary_key[0]
            subquery = db.session.query(primary_key).filter(
                UserGroupAssociation.group_id == Group.id, UserGroupAssociation.user_id == current_user.id,
                UserGroupAssociation.approved.is_(True), UserGroupAssociation.can_edit.is_(True))
            # print(subquery)
            return or_(cls.current_user_is_owner, primary_key.in_(subquery))
        else:
            return False

    @hybrid_property
    def current_user_can_share_data(self):
        """BOOL, whether current_user has permission to share"""
        return self.user_can_share_data(current_user.id) if current_user.is_authenticated() else False

    @current_user_can_share_data.expression
    def current_user_can_share_data(cls):
        if current_user.is_authenticated():
            # subquery to check if current user is approved member or owner
            primary_key = inspect(cls).primary_key[0]
            subquery = db.session.query(primary_key).filter(
                UserGroupAssociation.group_id == Group.id, UserGroupAssociation.user_id == current_user.id,
                UserGroupAssociation.approved.is_(True), UserGroupAssociation.can_share_data.is_(True))
            # print(subquery)
            return or_(cls.current_user_is_owner, primary_key.in_(subquery))
        else:
            return False

    @hybrid_property
    def current_user_can_add_member(self):
        """BOOL, whether current_user has permission to share"""
        return self.user_can_add_member(current_user.id) if current_user.is_authenticated() else False

    @current_user_can_add_member.expression
    def current_user_can_add_member(cls):
        if current_user.is_authenticated():
            # subquery to check if current user is approved member or owner
            primary_key = inspect(cls).primary_key[0]
            subquery = db.session.query(primary_key).filter(
                UserGroupAssociation.group_id == Group.id, UserGroupAssociation.user_id == current_user.id,
                UserGroupAssociation.approved.is_(True), UserGroupAssociation.can_add_members.is_(True))
            # print(subquery)
            return or_(cls.current_user_is_owner, primary_key.in_(subquery))
        else:
            return False

    @hybrid_property
    def current_user_has_agreed(self):
        """BOOL, whether current_user has permission to share"""
        return self.user_has_agreed(current_user.id) if current_user.is_authenticated() else False

    @current_user_has_agreed.expression
    def current_user_has_agreed(cls):
        if current_user.is_authenticated():
            # subquery to check if current user is approved member or owner
            primary_key = inspect(cls).primary_key[0]
            subquery = db.session.query(primary_key).filter(
                UserGroupAssociation.group_id == Group.id, UserGroupAssociation.user_id == current_user.id,
                UserGroupAssociation.has_agreed.is_(True))
            # print(subquery)
            return or_(cls.current_user_is_owner, primary_key.in_(subquery))
        else:
            return False


class UserGroupAssociationAPI(UserGroupAssociation, ValidationMixin):
    """
    User / Group Association resource.
    """
    api_build_restless_endpoints = True
    api_restless_methods = ['GET']
    # api_exclude_columns = []
    api_max_results_per_page = 1000
    api_allow_functions = True
    api_include_columns = ["created_at", "approved", "can_edit", "added_by_user", "added_by_user.id", "added_by_user.first_name",
                           "added_by_user.last_name", "user", "user.id", "user.username", "user.created_at",
                           "user.last_login", "user.first_name", "user.last_name", "user.active", "user.is_admin",
                           "group", "group.id", "group.name", "can_add_members", "can_share_data", "has_agreed",
                           "group.is_public", "group.is_restricted", "group.user_id", "group.requires_approval",
                           "group.requires_agreement", "group.group_type_id", "group.groupinfo", "group.created_at"]


class GroupTypeAPI(GroupType, ValidationMixin):
    """
    Group Type resource.
    """
    api_build_restless_endpoints = True
    api_restless_methods = ['GET']
    # api_exclude_columns = []
    api_max_results_per_page = 10000
    api_allow_functions = True


class UserAPI(User, ValidationMixin):
    """
    User resource: captures all user information which is linked to most objects and resources.
    """
    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'DELETE']
    api_include_columns = ['id', 'user_created_at', 'last_login', 'first_name', 'last_name', 'username', 'active', 'info',
                           'role', 'affiliations', 'affiliations.name', 'affiliations.groupinfo', 'affiliations.id']
    api_include_methods = ['affiliations_names']
    api_max_results_per_page = 10000
    api_allow_functions = True
    api_allow_anonymous_post = True

    api_custom_endpoints = [
        APIModelEndpoint("manage_api_token", endpoint='{tablename}/api_token', methods=['GET', 'PATCH', 'DELETE']),
        APIModelEndpoint("user_to_group", endpoint='{tablename}/<int:user_id>/group/<int:group_id>', methods=['POST', 'DELETE', 'PATCH']),
        APIModelEndpoint("login", endpoint='{tablename}/login', methods=['POST']),
        APIModelEndpoint("logout", endpoint='{tablename}/login', methods=['DELETE']),
        APIModelEndpoint("current_user_info", endpoint='{tablename}/login', methods=['GET']),
        APIModelEndpoint("create_update_user", endpoint='{tablename}', methods=['POST']),
        APIModelEndpoint("create_update_user", endpoint='{tablename}/<int:user_id>', methods=["PATCH"]),
        APIModelEndpoint("reset_password", endpoint='{tablename}/reset_password', defaults={"token": None}, methods=['POST', 'GET']),
        APIModelEndpoint("reset_password", endpoint='{tablename}/reset_password/<token>', methods=['POST', 'GET']),
        APIModelEndpoint("email_user", endpoint='{tablename}/<int:user_id>/email', methods=["POST"])
    ]

    api_postprocessors = {
        "GET_SINGLE": ['get_single_postprocessor']
    }

    api_preprocessors = {
        "DELETE_SINGLE": ['assert_current_user_is_admin'],
        "DELETE_MANY": ['assert_current_user_is_admin']
    }

    # validations
    val.validates_constraints()
    val.validates_email('email')

    @before_flush
    def _check_unique(self):
        if UserAPI.query.filter(UserAPI.username == self.username, UserAPI.id != self.id).first():
            self.add_validation_error('username', 'Username is already in use')
        if UserAPI.query.filter(UserAPI.email == self.email, UserAPI.id != self.id).first():
            self.add_validation_error('email', 'Email address is already in use')

    # @classmethod
    # def patch_single_preprocessor(cls, instance_id=None, data=None, **kw):
    #     if current_user.id != int(instance_id):
    #         raise ProcessingException(description='Not authorised!', code=401)
    #
    #     print(data)
    #     # if we are patching affiliations, get rid of all the old ones and make room for the new ones
    #     if "affiliations_usergroups" in data:
    #         UserGroupAssociation.query.filter(
    #             UserGroupAssociation.user_id == instance_id,
    #             UserGroupAssociation.group.has(Group.grouptype == 'affiliation')
    #         ).delete(synchronize_session='fetch')

    @classmethod
    def get_single_postprocessor(cls, result=None, instance=None, **kw):
        # instance = cls.get_by_id(result.get("id"))
        # result["affiliations"] = [dict(id=a.id, name=a.name) for a in u.affiliations]
        result["affiliation_ids"] = [a.id for a in instance.affiliations]
        if (current_user.is_authenticated() and current_user.is_admin) \
                or (current_user.is_authenticated() and current_user.id == instance.id):
            result["email"] = instance.email

    @classmethod
    def assert_security_checks(cls, data={}):
        if "is_admin" in data and not (current_user.is_authenticated() and current_user.is_admin):
            raise ProcessingException(description="You are not authorised to make admin accounts.", code=401)
        if "api_token" in data:
            raise ProcessingException(description="You cannot update an API token like this.", code=401)

    @classmethod
    def create_update_user(cls, user_id=None):
        """Update user information"""
        data = request.get_json()
        affiliation_ids = data.pop('affiliation_ids', [])
        if not affiliation_ids or len(affiliation_ids) <= 0:
            return jsonify(message="You need to select an affiliation", validation_errors={"affiliation_ids": "Please select an affiliation"}), 400
        cls.assert_security_checks(data=data)
        if user_id is not None:
            cls.assert_user_login()
            if current_user.id != user_id and not current_user.is_admin:
                raise ProcessingException(description="You are not authorised to edit this user account", code=401)
            user = cls.get_by_id(user_id)
            user.update(commit=False, **data)
        else:
            user = cls.create(commit=False, **data)

        # set affiliations
        UserGroupAssociation.query.filter(
            UserGroupAssociation.user_id == user.id,
            UserGroupAssociation.group.has(Group.group_type.has(name='affiliation'))
        ).delete(synchronize_session=False)
        if affiliation_ids:
            for i in affiliation_ids:
                UserGroupAssociation.create(user_id=user.id, group_id=i, commit=False)

        db.session.commit()
        return jsonify(user.instance_to_dict())

    # @classmethod
    # def update_user(cls, user_id):
    #     """Update user information"""
    #     cls.assert_user_login()
    #     if current_user.id != user_id and not current_user.is_admin:
    #         raise ProcessingException(description="You are not authorised to edit this user account", code=401)
    #     user = cls.get_by_id(user_id)
    #     data = request.get_json()
    #     cls.assert_security_checks(data=data)
    #     affiliation_ids = data.pop('affiliation_ids', [])
    #     user.update(commit=False, **data)
    #     user.set_affiliations(affiliation_ids)
    #     db.session.commit()
    #     return jsonify(user.instance_to_dict())
    #
    # @classmethod
    # def create_user(cls):
    #     """Create new user account"""
    #     data = request.get_json()
    #     cls.assert_security_checks(data=data)
    #     affiliation_ids = data.pop('affiliation_ids', [])
    #     user = cls.create(commit=False, **data)
    #     user.set_affiliations(affiliation_ids)
    #     db.session.commit()
    #     return jsonify(user.instance_to_dict())

    def set_affiliations(self, affiliation_ids):
        UserGroupAssociation.query.filter(
            UserGroupAssociation.user_id == self.id, UserGroupAssociation.group.has(Group.group_type.has(name='affiliation'))
        ).delete(synchronize_session=False)
        if affiliation_ids:
            for i in affiliation_ids:
                UserGroupAssociation.create(user_id=self.id, group_id=i, commit=False)

    @classmethod
    def login(cls):
        """
        Log in. Requires form parameters `username` and `password`.
        """
        data = request.get_json(force=True)
        user = User.query.filter(db.or_(cls.username == data.get('username'), cls.email == data.get('username'))).first()
        if not user:
            return jsonify(message="Username/email not found", validation_errors={"username": "Invalid username/email"}), 401
        elif current_user.is_authenticated() and current_user.is_admin:   # admin user can switch user without password
            session["current_admin_user_id"] = current_user.id
            login_user(user)
            return jsonify(message="ADMIN: Currently logged in as '{}'.".format(user.full_name()), api_token=user.api_token)
        elif not user.check_password(data.get('password')):
            return jsonify(message="Invalid username/email or password", validation_errors={"username": "", "password": ""}), 401
        elif not user.active:
            return jsonify(message="Your account is not activated. Please click the activation link in the activation email."), 401
        else:
            login_user(user)
            user.update(last_login=datetime.utcnow())
            flash("You have been logged in successfully!", "success")
            return jsonify(message="You have been logged in successfully!", api_token=user.api_token)

    @classmethod
    def logout(cls):
        """
        Log out the current session
        """
        if session.get("current_admin_user_id", None) is not None:
            login_user(User.get_by_id(session.pop("current_admin_user_id")))  # remove session variable
            msg = 'ADMIN: You have switched back to your admin account.'
        else:
            logout_user()
            msg = 'You are logged out.'
        flash(msg, 'info')
        return jsonify(message=msg)

    @classmethod
    def current_user_info(cls):
        """Get information about current logged in user"""
        cls.assert_user_login()
        u = cls.get_by_id(current_user.id)
        return render_data_response(u.instance_to_dict())


    @classmethod
    def user_to_group(cls, user_id, group_id):
        """Manage group membership"""
        group = Group.get_by_id(group_id)
        data = request.get_json(force=True, silent=True)
        if request.method == "PATCH":
            ug = group.add_update_user(update_existing=True, user_id=user_id, request_user_id=current_user.id, **data)
        elif request.method == "POST":
            ug = group.add_update_user(user_id=user_id, request_user_id=current_user.id, **data)
        elif request.method == "DELETE":
            group.remove_user(user_id=user_id, request_user_id=current_user.id)
            return jsonify({})
        else:
            raise ProcessingException(description='Method not allowed', code=405)
        # PATCH OR POST
        # if not ug.approved:
        #     print("\n\nTODO: SEND AN APPROVAL EMAILS TO THE CUSTODIAN\n\n")
        return jsonify(user_id=user_id, group_id=group_id, **data)

    @classmethod
    def manage_api_token(cls):
        """Manage API token"""
        cls.assert_user_login()
        if request.method == 'GET':
            return jsonify({"api_token": current_user.api_token})
        elif request.method == "PATCH":
            current_user._set_api_token()
            return jsonify({"api_token": current_user.api_token})
        elif request.method == "DELETE":
            current_user.api_token = None
            db.session.commit()
            return jsonify({"api_token": current_user.api_token})
        else:
            raise ProcessingException(description='Method not allowed', code=405)

    @classmethod
    def reset_password(cls, token=None):
        if token is not None:
            try:
                password_reset_serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
                email = password_reset_serializer.loads(token, salt=PASSWORD_SALT, max_age=3600)
            except Exception as e:
                if request.method == 'POST':
                    return jsonify(message="The password reset link is invalid or has expired."), 401
                flash("The password reset link is invalid or has expired.", "error")
                return redirect(url_for("public.home"))
            if request.method == 'POST':
                data = request.get_json(force=True)
                password = data.get("password", None)
                try:
                    user = User.query.filter_by(email=email).one()
                except Exception as e:
                    return jsonify(message="Email not found", validation_errors={}), 401
                if password:
                    user.set_password(password)
                    db.session.commit()
                    flash("Your password has been updated! You can now log in with your new password!", "success")
                    return jsonify(status="success", message=""), 200
            return render_template("user/password_reset_with_token.html", token=token)
        elif request.method == 'POST':
            data = request.get_json(force=True)
            email = data.get("email")
            try:
                user = cls.query.filter(cls.email == email).one()
            except Exception as e:
                return jsonify(message="Email not found"), 401
            user._send_password_reset_email()
            flash("Email sent! Please check your email for a password reset link.", "success")
            return jsonify(status="success", message=""), 200
        return render_template('models/users/password_reset_request.html')

    def _send_password_reset_email(self):
        reset_link = self._get_password_reset_link()
        html = render_template('models/users/email_password_reset.html', reset_link=reset_link, user=self)
        sender = (current_app.config['PROJECT_NAME'], "noreply@{}".format(request.host))
        subject = 'Your {} password reset request'.format(current_app.config['PROJECT_NAME'])
        send_email(subject, html, [self.email], sender)

    def _get_password_reset_link(self):
        password_reset_serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
        token = password_reset_serializer.dumps(self.email, salt=PASSWORD_SALT)
        return "{}api/users/reset_password/{}".format(request.host_url, token)

    @classmethod
    def email_user(cls, user_id):
        """
        Send an email to the user. Requires json post body containing `message` and optionally `subject`. If no
        `subject`, default subject will be "User message from PROJECT_NAME"
        """
        cls.assert_user_login()
        user = cls.get_by_id(user_id)
        data = request.get_json()
        subject = data.get("subject", "{} sent you a message from {}".format(current_user.first_name, current_app.config.get('PROJECT_NAME')))
        html = render_template('models/users/email_user.html', message=data.get("message"))
        send_email(subject, html, [user.email], (current_user.full_name(), current_user.email))
        return jsonify(message="Email has been sent to {}.".format(user.full_name()))


class RoleAPI(Role, ValidationMixin):
    """
    Not implemented yet. Not in use.
    """
    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    api_include_columns = []
    api_max_results_per_page = 10000
    api_allow_functions = True

    api_preprocessors = {
        "POST": ['assert_current_user_is_admin'],
        "PATCH_SINGLE": ['assert_current_user_is_admin'],
        "PATCH_MANY": ['assert_current_user_is_admin'],
        "DELETE_SINGLE": ['assert_current_user_is_admin']
    }


# def register_api(apimanager, app):
#     GroupAPI.register_api(apimanager, app)
#     UserGroupAssociationAPI.register_api(apimanager, app)
#     UserAPI.register_api(apimanager, app)
#     RoleAPI.register_api(apimanager, app)

add_to_api(GroupAPI)
add_to_api(UserGroupAssociationAPI)
add_to_api(UserAPI)
add_to_api(RoleAPI)
add_to_api(GroupTypeAPI)



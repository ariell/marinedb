# -*- coding: utf-8 -*-
from flask import Blueprint, jsonify, render_template, request, redirect, url_for, render_template, flash, make_response, send_file, current_app
from flask_login import login_required, current_user
import os
#import glob
#from uuid import uuid4
from jinja2 import TemplateNotFound
from sqlalchemy import exc as sqlalchemy_exc
from sqlalchemy.orm.exc import NoResultFound
# from marinedb.geodata.models import Campaign, CampaignFile, db

# from zlib import compress, decompress
# from struct import pack
import zipfile
from io import BytesIO
import time
import sys
# from marinedb.geodata.utils import getCampaignFiles

from pandas import DataFrame
import numpy as np


blueprint = Blueprint("annotations", __name__, url_prefix='/annotations', static_folder="../static", template_folder="templates")


@blueprint.route("/tags/", methods=['GET', 'POST'])
@login_required
def tags():
    return "tags"


# @blueprint.route("/models/<resource>", defaults={'mode': 'get', 'id': None})
# @blueprint.route("/models/<resource>/<mode>", defaults={'id': None})
# @blueprint.route("/models/<resource>/<mode>/<id>")
# # @login_required
# def model_templates(resource, mode, id):
#     params = dict(list(request.args.items()))
#     query_string = request.query_string
#     try:
#         return render_template("model_templates/"+resource+"/"+mode+".html", id=id, params=params, query_string=query_string, resource=resource, mode=mode)
#     except TemplateNotFound as e:
#         return render_template("model_templates/example_model/"+mode+".html", id=id, params=params, query_string=query_string, resource=resource, mode=mode)


@blueprint.route("/munge_file")
def munge_file():
    f = request.args.get('f') if request.args.get('f') else ""
    return render_template("munge_file.html", f=f)



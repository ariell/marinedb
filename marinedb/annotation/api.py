import json

# from savalidation.helpers import before_flush
# from sqlalchemy import or_, and_

# from marinedb.database import get_model_by_table_name
from savalidation import ValidationMixin

from marinedb.extensions import db
# from marinedb.geodata.api import AnnotationSetAPI, AnnotationLabelAPI
from marinedb.geodata.models import AnnotationLabel, AnnotationSet
# from marinedb.user.api import UserAPI
from modules.datatransform.models import export_transformed_query
from modules.flask_restless.search import search
from modules.flask_restless.usergroups import APIUserGroupMixin
from modules.flask_restless.views import render_data_response
# from modules.vocab_registries.search import get_vocab_registry
from modules.vocab_registries.search import is_supported_registry, nest_vocab_elements_on_lineage
# from modules.vocab_registries import plugins as vocab_registry_plugins
from .models import Tag, LabelInfo, LabelSchemeFile, Label, LabelAssociation, LabelScheme, TagType, VocabRegistry, \
    VocabElement, VocabElementNames, LabelVocabElementAssociation

from flask import request, jsonify, current_app  #, send_file
from modules.flask_restless import ProcessingException
from modules.flask_restless.mixins import APIModelEndpoint, docstring_parameter
from flask_login import current_user

from modules.flask_restless.mixins import APIModel, add_to_api  # , api_custom_endpoint

from modules.flask_restless.helpers import to_dict
# from modules.flask_restless.usergroups import APIUserGroupMixin
import savalidation.validators as val


class TagAPI(Tag, ValidationMixin):
    """
    Tag resource. Suggested tags can linked to class labels [label](#label) and tags can be assigned to annotations [annotation](#annotation)
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    # api_postprocessors = {"GET_SINGLE": ['get_single_postprocessor'],
    #                       "GET_MANY": ['get_many_postprocessor']}
    api_preprocessors = {"POST": ["post_preprocessor"]}
    # api_exclude_columns = ['user', 'labels', 'label_tags', 'annotation_tags']
    api_include_columns = ["user_id", "name", "type_id", "synonym_tag_id", "type", "id", "is_synonym", "type",
                           "synonyms", "synonyms.name", "synonyms.id", "synonyms.user_id"]

    # validations
    val.validates_constraints()

    # @classmethod
    # def get_many_postprocessor(cls, result=None, **kw):
    #     # TODO: is there a more efficient way to do this?
    #     for tag in result['objects']:
    #         TagAPI.get_single_postprocessor(result=tag)
    #
    # @classmethod
    # def get_single_postprocessor(cls, result=None, **kw):
    #     # Add tags to taggroup lists for single get
    #     if result['synonyms_tag_synonym_id'] is not None:
    #         taggroup = TagSynonym.query.filter_by(id=result['synonyms_tag_synonym_id']).first()
    #         result['synonym_tags'] = [{'name': i.name, 'id': i.id} for i in taggroup.synonym_tags]
    #     else:
    #         result['synonym_tags'] = []
    #     del result['synonyms_tag_synonym_id']

    @classmethod
    def post_preprocessor(cls, data, **kw):
        cls.assert_user_login()
        if "user_id" not in data:
            data['user_id'] = current_user.id    # set to current user
        if "name" in data:
            data['name'] = data['name'].title().replace(",", ".")  # convert to title case and remove commas


class LabelInfoAPI(LabelInfo, ValidationMixin):
    """
    Label Info resource. Attaches name-value information to [label](#label) objects.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']

    # validations
    val.validates_constraints()


# class TagSynonymAPI(TagSynonym, APIModel):
#     api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
#

class LabelSchemeFileAPI(LabelSchemeFile, ValidationMixin):
    """
    Label Scheme File resource. Files that can be used for batch update / creation of [label](#label) objects attached
    to [label_scheme](#label_scheme) objects.
    """
    api_restless_methods = ['GET', 'DELETE', 'PATCH', 'POST']
    # api_exclude_columns = ['rawfiledata']
    api_include_columns = ["description", "iscompressed", "label_scheme", "file_url", "label_scheme_id", "id", "name", "last_save"]

    # api_custom_endpoints = [
    #     APIModelEndpoint("get_filedata", endpoint="{tablename}/<int:fid>/data", methods=["GET"]),
    #     APIModelEndpoint("post_file", endpoint="{tablename}/save", methods=["POST"])
    # ]
    api_custom_endpoints = [
        APIModelEndpoint("get_data", endpoint="{tablename}/<int:fid>/data", methods=["GET"]),
        APIModelEndpoint("set_data", endpoint="{tablename}/data", methods=["POST"])
    ]

    # validations
    val.validates_constraints()


class LabelAPI(Label, ValidationMixin):
    """
    Label resource. This defines the list of labels contained in a [label_scheme](#label_scheme) and the labels to be applied to
    [annotation](#annotation) objects. Each label can also have suggested [tag](#tag) objects linked to it.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']

    # api_postprocessors = {"GET_SINGLE": ['get_single_postprocessor']}

    #api_allow_nonowner_delete = True  # avoid default owner test, since labels can be deleted by label_scheme editors

    api_preprocessors = {
        "POST": ["validate_preprocessor"],
        "PATCH_SINGLE": ["validate_preprocessor"]
    #     'DELETE_SINGLE': ['delete_auth_preprocessor'],
    }

    api_include_columns = ["user_id", "name", "color", "parent_id", "label_scheme_id", "id", "uuid", "is_child",
                           "is_parent", "created_at", "updated_at", "is_approved", "origin_code", "parent_origin_code"]
    api_include_methods = ["name_path", "lineage"]

    api_add_columns_single = ["info", "info.name", "info.value", "info.id", "tags", "tags.id", "tags.name",
                              "label_scheme", "label_scheme.id", "label_scheme.name", "label_scheme.user_id",
                              "parent", "parent.id", "parent.name",
                              "user", "user.id", "user.username", "user.last_login", "user.first_name",
                              "user.last_name", "user.active", "user.info", "user.role"]
                              # , "vocab_elements", "vocab_elements.id", "vocab_elements.name", "vocab_elements.key",
                              # "vocab_elements.lineage", "vocab_elements.vocab_registry_key"]

    api_add_methods_single = ["annotation_count", "descendant_count", "exemplar_annotation_sets",
                              "label_scheme_custodian", "current_user_can_edit", "current_user_is_custodian"]

    # validations
    val.validates_constraints()

    api_custom_endpoints = [
        APIModelEndpoint("api_edit_tag", endpoint='{tablename}/<int:id>/tag/<int:tag_id>', methods=['DELETE', 'POST']),
        APIModelEndpoint("get_exemplars", endpoint='{tablename}/<int:id>/exemplars', methods=['GET']),
        APIModelEndpoint("get_exemplars", endpoint='{tablename}/<int:id>/exemplars/<int:annotation_set_id>', methods=['GET'])
    ]

    def exemplar_annotation_sets(self):
        # return any exemplar annotation_sets containing the label
        return [dict(id=a.id, name=a.name) for a in AnnotationSet.query.filter(AnnotationSet.annotations.any(AnnotationLabel.label_id==self.id), AnnotationSet.is_exemplar==True)]

    @classmethod
    def post_auth_preprocessor(cls, data, *args, **kw):
        label_scheme_id = (int(data.get("label_scheme_id", 0)) or None) if isinstance(data, dict) else None
        if label_scheme_id is not None:
            label_scheme = LabelSchemeAPI.get_by_id(label_scheme_id)
            if not label_scheme.current_user_is_member and not label_scheme.current_user_can_edit:
                raise ProcessingException(description="Unauthorised. You don't have edit permission for this label scheme. "
                                                      "You can request edit permission from the custodian.", code=401)
        else:
            raise ProcessingException(description="No label_scheme_id found.", code=400)

        # cls._validate_preprocessor(data=data)

    @classmethod
    def patch_auth_preprocessor(cls, instance_id=None, instance=None, data=None, **kw):
        """
        checks that the current user has permission to edit this class label.
        """
        if instance is None and instance_id is not None:
            instance = cls.get_by_id(instance_id)
        if instance is not None:
            if not instance.current_user_can_edit:
                raise ProcessingException(description="Unauthorised. You are not authorised to edit this class label", code=401)
        else:
            raise ProcessingException(description="No instance_id found.", code=400)

        # cls._validate_preprocessor(instance_id=instance_id, instance=instance, data=data)

    @classmethod
    def delete_auth_preprocessor(cls, instance_id=None, *args, **kw):
        """
        checks that the class label does not have any children or annotations which may be orphaned by delete.
        """
        instance = cls.get_by_id(instance_id)
        cls.patch_auth_preprocessor(instance=instance)
        annotation_count = instance.annotations.count()
        descendant_count = instance.children.count()
        if annotation_count > 0:            # check for orphaned annotations
            raise ProcessingException(description="You cannot delete a class label that has been used in annotations. "
                                                  "This class label has been used in {} annotation(s). Please re-annotate "
                                                  "or batch change existing annotations before deleting.".format(annotation_count))
        if descendant_count > 0:            # check for orphaned child nodes / descendants
            raise ProcessingException(description="You cannot delete a class label that has child nodes. This class "
                                                  "label has {} direct descendant(s). Please update child nodes before"
                                                  " deleting.".format(descendant_count))

    # def current_user_has_role(self, role):
    #     return self.user_has_role(role, current_user, label=self)


    @classmethod
    def get_exemplars(cls, id, annotation_set_id=None):
        """
        Get list of exemplar images for the label_scheme with the ID matching `id`. Optionally you can specify an exemplar
        `annontation_set_id` if there are more than one for this scheme. If not `annotation_set_id` is specified.
        All will be returned.
        :param id:
        :param annotation_set_id:
        :return:
        """
        # instance = cls.get_by_id(id)
        # exemplar_list = instance.exemplars
        exemplar_list = AnnotationLabel.query.filter(AnnotationLabel.label_id==id, AnnotationLabel.annotation_set.has(AnnotationSet.is_exemplar==True))
        if annotation_set_id is not None:
            exemplar_list = exemplar_list.filter_by(annotation_set_id=annotation_set_id)
        results = [dict(id=a.id, label_id=a.label_id, color=a.color(),
                        point=dict(id=a.point.id, media_id=a.point.media_id, updated_at=a.point.updated_at))
                   for a in exemplar_list]
        return render_data_response(cls.paginated_results(results))
        # "label_id","id","color","point","point.media_id","point.id"

    @classmethod
    def validate_preprocessor(cls, instance_id=None, data=None, instance=None, **kw):
        """
        if `parent_id` is set, checks that nominated parent class label is valid, i.e.: that the `parent_id` exists,
        is from the same annotation scheme (`label_scheme_id`) and is not a circular reference to itself (witch can break
        things).
        """
        parent_id = int(data.get("parent_id")) if data.get('parent_id') else None
        is_approved = data.get("is_approved", None)
        if instance is None:
            instance = cls.get_by_id(instance_id) if instance_id is not None else None
        label_scheme_id = int(data.get("label_scheme_id")) if data.get('label_scheme_id') else instance.label_scheme_id
        label_scheme = LabelSchemeAPI.get_by_id(label_scheme_id)

        # validate setting of is_approved flag
        if is_approved is not None:
            # if existing instance, check label_scheme permission
            if instance is not None:
                if is_approved != instance.is_approved and not instance.current_user_is_custodian:
                    raise ProcessingException(description="Only custodians of this Annotation Scheme are permitted to "
                                                          "change the approval of labels.", code=401)
            # if new instance, check label_scheme permission from label_scheme_id
            elif is_approved is True and not label_scheme.current_user_can_edit:
                    raise ProcessingException(description="Only custodians of this Annotation Scheme are permitted to "
                                                          "approve labels.", code=401)
        # validate parent
        if parent_id is not None:
            parent_instance = cls.get_by_id(parent_id)
            if not parent_instance:
                raise ProcessingException(description="Selected parent label does not exist.")
            if instance is not None and instance.id == parent_id:
                raise ProcessingException(description="Circular reference error. A class cannot be it's own parent.")
            if parent_instance.label_scheme_id not in label_scheme.parent_label_scheme_ids():
                raise ProcessingException(description="Parent label needs to be from the same label scheme.")

    @classmethod
    def api_edit_tag(cls, id, tag_id):
        cls.patch_auth_preprocessor(instance_id=id)
        instance = cls.get_by_id(id)
        if request.method == "DELETE":
            instance.remove_tag(tag_id)
        elif request.method == "POST":
            instance.add_tag(tag_id)
        return jsonify(instance.instance_to_dict())

    # @classmethod
    # def get_single_postprocessor(cls, result=None, instance=None, **kw):
    #     """
    #     adds additional computed fields to single request including: `info`, `tags`, `label_scheme`, `parent`, `name_path`,
    #     `current_user_can_edit`, `annotation_count`, `descendant_count`, `user`, `current_user_is_custodian`
    #     """
    #     # instance = cls.get_by_id(result.get("id"))
    #     result["info"] = [dict(name=i.name, value=i.value, id=i.id) for i in instance.info]
    #     result['tags'] = [dict(name=i.name, id=i.id) for i in instance.tags]
    #     result['label_scheme'] = dict(id=instance.label_scheme.id, name=instance.label_scheme.name, user=dict(
    #         first_name=instance.label_scheme.user.first_name, last_name=instance.label_scheme.user.last_name,
    #         username=instance.label_scheme.user.username, id=instance.label_scheme.user.id))
    #     result['parent'] = dict(id=instance.parent.id, name=instance.parent.name) if instance.parent_id else None
    #     result['name_path'] = instance.name_path
    #     result["current_user_can_edit"] = instance.current_user_can_edit
    #     result["current_user_is_custodian"] = instance.current_user_is_custodian
    #     result["annotation_count"] = instance.annotations.count()
    #     result["descendant_count"] = instance.children.count()
    #     #result["exemplar_annotation_set_id"] = tg.label_scheme.exemplar_annotation_set_id
    #     result["exemplar_annotation_sets"] = [dict(id=a.id, name=a.name) for a in instance.label_scheme.exemplar_annotation_sets]
    #     result["user"] = dict(UserAPI.model_to_dict(instance.user, is_single=False))
    #     # result['lineage'] = tg.lineage()

    # @staticmethod
    # def user_has_role(role, user, label_scheme_id=None, label=None):
    #     if label_scheme_id is None:
    #         label_scheme_id = label.label_scheme_id
    #     label_scheme = LabelSchemeAPI.get_by_id(label_scheme_id)
    #     if role == "editor":
    #         label_is_approved = getattr(label, "is_approved", False)
    #         current_user_is_creator = getattr(label, "user_id", None) == user.id \
    #             if user.is_authenticated() else False
    #         return label_scheme.current_user_can_edit \
    #             or (label_scheme.current_user_is_member and not label_is_approved and current_user_is_creator)
    #     elif role == "custodian":
    #         return label_scheme.current_user_can_edit
    #     return False


class LabelAssociationAPI(LabelAssociation, ValidationMixin):
    """
    Label tag associations. Links suggested [tag](#tag) objects to [label](#label) objects. Provides convenience for quickly
    assigning tags to certain labels
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']

    # validations
    val.validates_constraints()


class LabelSchemeAPI(LabelScheme, ValidationMixin):
    """
    Label Scheme resource. This resource is for managing and define label schemes otherwise known as (annotation scheme,
    taxonomic hierarchy, category lists, label lists, classification scheme)
    """
    # __tablename__ = LabelScheme.__tablename__

    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH', 'PUT']
    # api_exclude_columns = ['tag_files.rawfiledata', 'labels', 'annotation_sets']
    api_max_results_per_page = 10000

    #auth_preprocessors = APIUserGroupMixin.get_preprocessors()
    #api_preprocessors = APIModel.append_processor(auth_preprocessors, "DELETE_SINGLE", 'delete_preprocessor')
    api_preprocessors = {"DELETE_SINGLE": ['delete_preprocessor']}
    # api_preprocessors = APIModel.append_processor(api_preprocessors, "PATCH_SINGLE", 'patch_single_preprocessor')

    # api_postprocessors = {"GET_SINGLE": ['get_single_postprocessor']}

    api_include_columns = ["user_id", "description", "is_hierarchy", "id", "name", "user", "user.username",
                           "user.first_name", "user.last_name", "user.id", "label_scheme_files", "label_scheme_files.description",
                           "label_scheme_files.file_url", "label_scheme_files.id", "label_scheme_files.name",
                           'current_user_can_view', 'current_user_is_owner', 'current_user_is_member',
                           'current_user_can_edit', 'is_public', 'is_child', 'parent_label_scheme',
                           'parent_label_scheme.id', 'parent_label_scheme.name', "created_at", "updated_at"]

    api_include_methods = ["usergroup_count", 'annotation_set_count', "exemplar_annotation_set_count"]  #, "exemplar_annotation_sets"]

    api_add_columns_single = []
    api_add_methods_single = ["parent_label_scheme_ids"]


    # Optional for restless (or custom method)
    # api_validation_exceptions = [ValidationError]
    # api_include_methods = None
    # api_include_columns = None
    # api_results_per_page = 10
    # api_allow_patch_many = False
    # api_allow_functions = False  # eg: GET /api/eval/person?q={"functions": [{"name": "count", "field": "id"}]}
    # api_postprocessors = None
    # api_post_form_preprocessor = None

    api_custom_endpoints = APIUserGroupMixin.build_api_endpoints() + [
        # APIModelEndpoint("get_nested", endpoint='{tablename}/<int:id>/nested', methods=["GET"]),
        APIModelEndpoint("api_reset_label_parents", endpoint='{tablename}/<int:label_scheme_id>/reset_label_parents', methods=["PATCH"]),
        APIModelEndpoint("export_labels", endpoint='{tablename}/<int:label_scheme_id>/export', methods=["GET"]),
        APIModelEndpoint("get_labels", endpoint='{tablename}/<int:label_scheme_id>/labels', methods=["GET"]),
        APIModelEndpoint("get_labels", endpoint='{tablename}/<int:label_scheme_id>/labels/<int:label_parent_id>', methods=["GET"])
    ]

    # validations
    val.validates_constraints()

    # def __init__(self):
    #     print(self.__class__)
    #     self.add_api_endpoint("get_nested", endpoint='{tablename}/<int:id>/nested', methods=["GET"])

    # def exemplar_annotation_sets(self):
    #     query = self.annotation_sets.filter_by(is_exemplar=True)
    #     # query = AnnotationSet.query.filter(AnnotationSet.label_scheme_id.in_(self.get_parent_label_scheme_ids()), AnnotationSet.is_exemplar==True)
    #     return [dict(id=a.id, media_collection_id=a.media_collection_id, name=a.name, current_user_can_edit=a.current_user_can_edit)
    #             for a in query]


    @staticmethod
    def patch_single_preprocessor(instance_id=None, data=None, **kw):
        # Check if changing structure, if so, preprocess things
        if "_run_method" in data:
            ts = LabelScheme.query.filter_by(id=instance_id).first()
            getattr(ts, data['_run_method'])()
            del data['_run_method']

    @classmethod
    def delete_preprocessor(cls, instance_id=None, **kw):
        """
        checks that the scheme does not contain annotation sets or children.
        """
        # print("\n\nRUNNING DELETE PREPROCESSOR FOR ID={}\n\n".format(instance_id))
        instance = cls.get_by_id(instance_id)
        annotation_set_count = instance.annotation_sets.count()
        if annotation_set_count > 0:  # check for orphaned annotations
            raise ProcessingException(description="You cannot delete a scheme that has been used in an annotation_set. "
                                                  "This scheme has been used in {} annotation_set(s). Please re-annotate "
                                                  "or batch change existing annotations before deleting.".format(annotation_set_count))
        descendant_count = instance.children.count()
        if descendant_count > 0:  # check for orphaned child nodes / descendants
            raise ProcessingException(description="You cannot delete a scheme that has been extended in other schemes. "
                                                  "This scheme has {} direct descendant(s). Please edit or remove child"
                                                  " schemes before deleting.".format(descendant_count))

    # @property
    # def current_user_is_member(self):
    #     return self.current_user_is_member

    # @property
    # def current_user_can_edit(self):
    #     return self.current_user_can_edit

    # @classmethod
    # def get_single_postprocessor(cls, result=None, instance=None, **kw):
    #     """
    #     adds additional computed fields to single request including: `info`, `tags`, `label_scheme`, `parent`, `lineage_names`,
    #     `current_user_can_edit`, `annotation_count`, `descendant_count`, `user`, `current_user_is_custodian`
    #     """
    #     # instance = cls.get_by_id(result.get("id"))
    #     result["exemplar_annotation_sets"] = [a.instance_to_dict(is_single=False) for a in instance.exemplar_annotation_sets]
    #     # result["parent_label_scheme"] = dict(id=instance.parent_label_scheme.id, name=instance.parent_label_scheme.name) if instance.is_child else None

    @classmethod
    @docstring_parameter(export_transformed_query.__doc__)
    def export_labels(cls, label_scheme_id):
        """
        Export [labels](#label) collection from [label_scheme](#label_scheme) matching `id`.
        Search queries are executed on the [label](#label) model.

        %s

        Allowed fields include:

        `id`, `parent_id`, `uuid`, `name`, `color`, `user.id`, `user.full_name`, `label_scheme.id`,
        `label_scheme.name`, `lineage_names`, `is_approved`, `tags`, `vocab_registry`,
        `created_at`, `updated_at`, `origin_code`, `parent_origin_code`, `is_mapped`

        See [label](#label) model columns, relations and attributes for more information on each of the included fields.

        **TRANSLATING TO OTHER LABEL_SCHEMES**

        This endpoint also supports the translation of labels from a source to a target label_scheme.
        another label_scheme using the semantic translation framework. In order to do this, you need to specify the
        additional `translate` url query parameter:

        ```
        &translate={"target_label_scheme_id":..., "vocab_registry_keys": ..., "mapping_override": ...}
        ```
        Where `target_label_scheme_id` [required] is an INT with the `id` of the target label scheme,
        `vocab_registry_keys` [optional] is a list containing the priority order of keys for `vocab_registries` for
        which to perform the semantic translation and `mapping_override` defines a set of key-value pairs containing
        `source label.id : target label.id` for which to override the translation.
        Note: for extended schemes, labels are translated through `tree_traversal` and no semantic translation is
        required.

        NOTE: you also need to add the `translated.*` columns to obtain the translation output. These columns are
        ignored if no `translate` query parameter is supplied.

        **EXAMPLES**

        ```
        # Example1: get all columns as an HTML table in the browser (including all labels from base scheme)
        /api/label_scheme/7/export?template=data.html&disposition=inline

        # Example2: get all columns as a downloaded CSV file (including all labels from base scheme)
        /api/label_scheme/7/export?template=data.csv&disposition=inline

        # Example3: get all columns as an HTML table in the browser, but only for labels defined in the extended scheme (not the base scheme)
        /api/label_scheme/7/export?template=data.html&disposition=inline&q={"filters":[{"name":"label_scheme_id","op":"eq","val":7}]}

        # Example4: get translation lookup between schemes, and only return specific columns
        /api/label_scheme/8/export?disposition=inline&translate={"vocab_registry_keys":["worms","caab","catami"],"target_label_scheme_id":"7"}
          &include_columns=["label_scheme.name","lineage_names","name","id","translated.id","translated.name","translated.lineage_names",
          "translated.translation_info","translated.label_scheme.name"]

        # MORE TO COME... Need something else or an example added here, please ask...
        ```
        """
        allowed_columns = [
            "id", "parent_id", "uuid", "name", "color", "user.id", "user.full_name", "label_scheme.id",
            "label_scheme.name", "lineage_names", "is_approved", "tag_names", "vocab_registry",
            "created_at", "updated_at", "origin_code", "parent_origin_code", "is_mapped",
            "translated.id", "translated.name", "translated.lineage_names", "translated.uuid",
            "translated.translation_info", "translated.label_scheme.id", "translated.label_scheme.name"
        ]
        # , "vocab_elements", "tags", "info", "is_child"]
        cls.get_single_auth_preprocessor(instance_id=label_scheme_id)  # check authentication
        instance = cls.get_by_id(label_scheme_id)
        filename_pattern = "labels-u{metadata[user][id]}-{metadata[name]}-{metadata[id]}-{hash}"

        extra_ops = []
        labels = LabelAPI.query.filter(LabelAPI.label_scheme_id.in_(instance.parent_label_scheme_ids()))

        return export_transformed_query(labels, allowed_columns=allowed_columns,
                                        filename_pattern=filename_pattern, metadata=instance.instance_to_dict(),
                                        extra_operations=extra_ops)

    @classmethod
    def get_label_with_parents(cls, label, label_columns, label_methods=None, child_tg_dict=None):
        tg_dict = to_dict(label, include=label_columns, include_methods=label_methods)
        if child_tg_dict is not None:
            tg_dict["children"] = [child_tg_dict]
        if label.parent:
            tg_dict = cls.get_label_with_parents(label.parent, label_columns, label_methods=label_methods, child_tg_dict=tg_dict)
        return tg_dict

    @classmethod
    def merge_label_parents(cls, label_list, label):
        for tg in label_list:
            if tg.get("id", None) == label.get("id", None):
                child_tg = label.get("children", None)
                if child_tg is not None:
                    if "children" not in tg:
                        tg["children"] = []
                    cls.merge_label_parents(tg["children"], child_tg[0])
                return   # bail out - match found
        label_list.append(label)

    def label_count(self):
        # return LabelAPI.query.filter((or_(*self.get_label_filter()))).count()
        return LabelAPI.query.filter(LabelAPI.label_scheme_id.in_(self.parent_label_scheme_ids())).count()

    @classmethod
    def get_labels(cls, label_scheme_id, label_parent_id=None):
        label_columns = ["color", "id", "name", "parent_id", "label_scheme_id"]
        if True:  # TODO: set is_mapped option dynamically
            label_columns.append("is_mapped")
        label_methods = None
        max_labels_non_lazy = current_app.config.get('LABEL_SCHEME_LAZY_LOAD_THRESH', 1000)
        max_labels_returned = current_app.config.get('LABEL_SCHEME_MAX_SEARCH_RESULTS', 1000)
        scheme = LabelSchemeAPI.get_by_id(label_scheme_id)
        data = scheme.instance_to_dict(is_single=False, include_methods=['label_count', 'parent_label_scheme_ids'],
                                       include_columns=["description", "id", "is_hierarchy", "is_public", "name",
                                                        "user_id", "current_user_can_edit"])
        data["is_lazyloaded"] = data["label_count"] > max_labels_non_lazy

        # Check if search query is set
        q = json.loads(request.args.get("q") or "{}")
        has_search_terms = bool(q.get("filters", None))

        # Filter for this annotation scheme and all parent schemes
        # labels = search(db.session, LabelAPI, q).filter((or_(*scheme.get_label_filter())))
        labels = search(db.session, LabelAPI, q).filter(LabelAPI.label_scheme_id.in_(scheme.parent_label_scheme_ids()))

        # Filter for parent_id if supplied
        if label_parent_id is not None:
            labels = labels.filter(LabelAPI.parent_id == label_parent_id)
        # Filter for lazy loaded scheme to return only root nodes if no search is done and no parent ID is defined
        elif not has_search_terms and data["is_lazyloaded"]:
            labels = labels.filter(Label.is_child.is_(False))

        # Check we're not trying to send too much data
        if labels.count() > max_labels_returned:
            raise ProcessingException("Too many search results. Returned {} of {} records. Please narrow down your search.".format(labels.count(), data["label_count"]))

        # If flat list or parent_id is defined, return flat list
        if not data['is_hierarchy'] or label_parent_id is not None:
            data["labels"] = [to_dict(i, include=label_columns, include_methods=label_methods) for i in labels.all()]
        # If hierarchy, present children under parents in structure
        else:
            # If search, traverse up tree to show parents for matched search results
            if has_search_terms:
                data["labels"] = []  # if search term is less than three characters, return empty
                for tg in labels.all():
                    new_label = cls.get_label_with_parents(tg, label_columns, label_methods=label_methods)
                    cls.merge_label_parents(data["labels"], new_label)
            # Present classes in parent child structure. Building it in this way may seem inelegant, but it is much
            # faster than nested queries to DB loading children, which would be very slow.
            else:
                # get all labels and build index
                labels_lookup = {i.id: to_dict(i, include=label_columns, include_methods=label_methods) for i in labels.all()}
                # labels_lookup = {i['id']: i for i in data['labels']}  # build index of labels
                for i, tg in labels_lookup.items():  # loop through labels
                    tg['is_child'] = False  #
                    if tg['parent_id'] in labels_lookup:
                        if 'children' not in labels_lookup[tg['parent_id']]:
                            labels_lookup[tg['parent_id']]['children'] = []
                        labels_lookup[tg['parent_id']]['children'].append(tg)
                        tg['is_child'] = True
                data['labels'] = [labels_lookup[k] for k in labels_lookup if not labels_lookup[k]['is_child']]
                labels_lookup = None  # free memory (may have been deemed to leak somehow?)

        return render_data_response(data)

    @classmethod
    def api_reset_label_parents(cls, label_scheme_id):
        """
        Reset [labels](#label) parents for `label_scheme` with `id` matching `label_scheme_id`.
        Uses `origin_code` and `parent_origin_code` to match up [labels](#label). Accepts optional query string argument
        `replace_existing=true`, which will reset parent_ids for labels that are already set. By default this is set to `false`
        """
        replace_existing = request.args.get('replace_existing') == "true"
        label_scheme = cls.get_by_id(label_scheme_id)
        if not label_scheme.current_user_can_edit:
            raise ProcessingException("You do not have permission to edit this label_scheme.")
        return render_data_response(label_scheme.reset_label_parents(replace_existing=replace_existing))

    # # @api_custom_endpoint('{tablename}/<int:id>/nested', 'marinedb.annotation.api.LabelSchemeAPI', methods=["GET"])
    # @classmethod
    # def get_nested(cls, id):
    #     label_columns = ["color", "id", "name", "parent_id"]
    #     max_labels_non_lazy = 1000
    #
    #     scheme = LabelSchemeAPI.get_by_id(id)
    #     # data = to_dict(scheme, include=["description", "id", "is_hierarchy", "is_public", "name", "user_id", "current_user_can_edit"])
    #     data = scheme.instance_to_dict(run_get_single_processors=False, include_methods=False,
    #                                    include_columns=["description", "id", "is_hierarchy", "is_public", "name", "user_id", "current_user_can_edit"], )
    #     labels = Label.query.filter((or_(*scheme.get_label_filter())))
    #     data["n_labels"] = labels.count()
    #
    #     data["is_lazyloaded"] = data["n_labels"] > max_labels_non_lazy
    #
    #     search_term = request.args.get("search", None) if request.args.get("search", None) else None  # get search_term (if supplied)
    #     if isinstance(search_term, str):
    #         data["labels"] = []  # if search term is less than three characters, return empty
    #         if len(search_term) >= 3:        # only do search if more than 3 characters
    #             for i in labels.filter(Label.name.ilike("%" + search_term.replace(" ", "%") + "%")).all():
    #                 new_label = cls.get_label_with_parents(i, label_columns)
    #                 cls.merge_label_parents(data["labels"], new_label)
    #                 #data["labels"].append(cls.get_label_with_parents(i, label_columns))
    #     else:
    #         # # using DB lazy loading is too slow
    #         # def add_children(children, el):
    #         #     if len(children) > 0:
    #         #         el["children"] = []
    #         #         for c in children:
    #         #             c_dict = to_dict(c, include=["color", "id", "name", "parent_id", "uuid"])
    #         #             add_children(c.children, c_dict)
    #         #             el["children"].append(c_dict)
    #         #
    #         # data["labels"] = []
    #         # if data["is_lazyloaded"] is False:
    #         #     for i in labels.filter(Label.parent_id == None).all():
    #         #         tg = to_dict(i, include=["color", "id", "name", "parent_id", "uuid"])
    #         #         add_children(i.children, tg)
    #         #         data["labels"].append(tg)
    #
    #         # Add filter if lazyloading to only pull out top level
    #         if data["is_lazyloaded"]:
    #             data["labels"] = [to_dict(i, include=label_columns)
    #                                   for i in labels.filter(Label.is_child.is_(False)).all()]
    #         elif data['is_hierarchy']:
    #             data["labels"] = [to_dict(i, include=label_columns) for i in labels.all()]  # get all labels
    #             labels_lookup = {i['id']: i for i in data['labels']}        # build index of labels
    #             for i in data['labels']:                                        # loop through labels
    #                 i['is_child'] = False                                           #
    #                 if i['parent_id'] in labels_lookup:
    #                     if 'children' not in labels_lookup[i['parent_id']]:
    #                         labels_lookup[i['parent_id']]['children'] = []
    #                     labels_lookup[i['parent_id']]['children'].append(i)
    #                     i['is_child'] = True
    #             data['labels'] = [labels_lookup[k]
    #                                   for k in labels_lookup if not labels_lookup[k]['is_child']]
    #             labels_lookup = None  # free memory (may have been deemed to leak somehow?)
    #
    #         else:
    #             data["labels"] = [to_dict(i, include=label_columns) for i in labels.all()]
    #
    #     return jsonify(data)


    # api_prefix = "/api"  # this gets set/updated in register_api as well
    #
    # val.validates_constraints()
    #
    #
    # @classmethod
    # def register_api(cls, apimanager):
    #     cls.api_prefix = apimanager.APINAME_FORMAT.format("/")  # update class field from API definition
    #
    #     def get_single_postprocessor(result=None, **kw):
    #         # # TODO: is there a more efficient way to do this? Perhaps a custo serializer
    #         # # Add tags and info to taggroup lists for single get
    #         # def add_tags_to_result(result):
    #         #     for tg in result['labels']:
    #         #         taggroup = Label.query.filter_by(id=tg['id']).first()
    #         #         tg['tags'] = dict([(i.tag.name, i.tag.id) for i in taggroup.label_tags])
    #         #         # tg['info'] = dict([(i.name, i.value) for i in taggroup.info])
    #
    #         #add_tags_to_result(result)
    #         # Return hierarchical list of values
    #         if result['is_hierarchy']:
    #             labels_lookup = {i['id']: i for i in result['labels']}
    #             for i in result['labels']:
    #                 i['is_child'] = False
    #                 if i['parent_id'] in labels_lookup:
    #                     if 'children' not in labels_lookup[i['parent_id']]:
    #                         labels_lookup[i['parent_id']]['children'] = []
    #                     labels_lookup[i['parent_id']]['children'].append(i)
    #                     i['is_child'] = True
    #
    #             result['labels'] = [labels_lookup[k] for k in labels_lookup if not labels_lookup[k]['is_child']]
    #         labels_lookup = None  # free memory (seemed to leak somehow)
    #
    #     def get_many_postprocessor(result=None, **kw):
    #         for scheme in result['objects']:
    #             scheme['num_labels'] = len(scheme['labels'])
    #             del scheme['labels']
    #
    #     def patch_single_preprocessor(instance_id=None, data=None, **kw):
    #         # Check if changing structure, if so, preprocess things
    #         if "_run_method" in data:
    #             ts = LabelScheme.query.filter_by(id=instance_id).first()
    #             getattr(ts, data['_run_method'])()
    #             del data['_run_method']
    #
    #     def api_check_auth_preprocessor(instance_id=None, **kw):
    #         user_id = LabelScheme.query.filter_by(id=instance_id).one().user_id
    #         if user_id != current_user.id:
    #             raise ProcessingException(description="You are not authorised to edit this dataset", code=401)
    #
    #     # TODO: probably do not need two, just keep tree version?
    #     apimanager.create_api(cls, methods=['GET', 'POST', 'DELETE', 'PATCH', 'PUT'],
    #                           exclude_columns=['tag_files.rawfiledata', 'labels', 'annotation_sets'],
    #                           preprocessors={'PATCH_SINGLE': [patch_single_preprocessor, api_check_auth_preprocessor],
    #                                          'DELETE':[api_check_auth_preprocessor]},
    #                           validation_exceptions=[ValidationError],
    #                           #include_methods=['label_list_url'],
    #                           max_results_per_page=10000)
    #     apimanager.create_api(cls, collection_name='{}-tree'.format(cls.__tablename__), methods=['GET'],
    #                           postprocessors={'GET_SINGLE': [get_single_postprocessor],
    #                                           'GET_MANY': [get_many_postprocessor],
    #                                           'GET_SINGLE': [get_single_postprocessor]},
    #                           preprocessors={'PATCH_SINGLE': [api_check_auth_preprocessor],
    #                                          'DELETE':[api_check_auth_preprocessor]},
    #                           exclude_columns=['tag_files.rawfiledata', 'annotation_sets'],
    #                           validation_exceptions=[ValidationError],
    #                           max_results_per_page=10000)


class TagTypeAPI(TagType, ValidationMixin):
    """
    Tag Type resource. A modifier denoting the type for a [tag](#tag) resource.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']

    # validations
    val.validates_constraints()


class VocabElementNamesAPI(VocabElementNames, ValidationMixin):
    """
    VocabElement Names resource.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']

    # validations
    val.validates_constraints()


class VocabRegistryAPI(VocabRegistry, ValidationMixin):
    """
    Vacab Registry resource: defines a mapping to a vocab registry module and handles interactions with them.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    val.validates_constraints()
    api_include_columns = ["key", "name", "description", "created_at", "updated_at", "user", "user.id",
                           "user.first_name", "user.last_name", "vocab_element_key_name"]
    api_include_methods = ['registry_urls']

    api_preprocessors = {
        "POST": ['assert_current_user_is_admin'],
        "PATCH_SINGLE": ['assert_current_user_is_admin'],
        "PATCH_MANY": ['assert_current_user_is_admin'],
        "DELETE_SINGLE": ['assert_current_user_is_admin']
    }

    api_custom_endpoints = [
        APIModelEndpoint("match", endpoint='{tablename}/<key>/match', methods=["GET"]),
        APIModelEndpoint("get_element", endpoint='{tablename}/<key>/get_element', methods=["GET"]),
        APIModelEndpoint("get_all_elements", endpoint='{tablename}/<key>/get_all_elements', methods=["GET"]),
        APIModelEndpoint("get_children", endpoint='{tablename}/<key>/get_children', methods=["GET"]),
    ]

    def registry_urls(self):
        # registry = get_vocab_registry(self.key, self.module, **self.module_kwargs)
        if self.registry_plugin is not None:
            return dict(
                match_name_url=self.registry_plugin.match_name_url,
                match_synonym_url=self.registry_plugin.match_synonym_url,
                match_vernacular_url=self.registry_plugin.match_vernacular_url,
                match_fuzzy_name_url=self.registry_plugin.match_fuzzy_name_url,
                get_children_url=self.registry_plugin.get_children_url,
                get_reference_url=self.registry_plugin.get_reference_url
            )
        else:
            return dict()

    @classmethod
    def match(cls, key):
        # registry = get_vocab_registry(key)
        instance = cls.query.get(key)
        # registry = get_vocab_registry(instance.key, instance.module, **instance.module_kwargs)
        return render_data_response(instance.registry_plugin.match(**request.args))

    @classmethod
    def get_element(cls, key):
        # registry = get_vocab_registry(key)
        instance = cls.query.get(key)
        # registry = get_vocab_registry(instance.key, instance.module, **instance.module_kwargs)
        data = instance.registry_plugin.get_element(**request.args)
        data["vocab_registry"] = dict(name=instance.name, key=instance.key, description=instance.description,
                                      vocab_element_key_name=instance.vocab_element_key_name)
        return render_data_response(data)

    @classmethod
    def get_all_elements(cls, key):
        # registry = get_vocab_registry(key)
        instance = cls.query.get(key)
        # registry = get_vocab_registry(instance.key, instance.module, **instance.module_kwargs)
        result = instance.registry_plugin.get_all_elements(**request.args)
        return render_data_response(dict(objects=result, num_results=len(result)))

    @classmethod
    def get_children(cls, key):
        # registry = get_vocab_registry(key)
        instance = cls.query.get(key)
        # registry = get_vocab_registry(instance.key, instance.module, **instance.module_kwargs)
        return render_data_response(instance.registry_plugin.get_children(**request.args))


class VocabElementAPI(VocabElement, ValidationMixin):
    """
    Vocab Element resource: locally cached version of element from [vocab_registry](#vocab_registry)
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']

    api_include_columns = ["id", "name", "key", "parent_key", "vocab_registry", "vocab_registry.key",
                           "vocab_registry.vocab_element_key_name", "vocab_registry.name", "vocab_registry.description",
                           "created_at", "updated_at", "user", "user.id", "lineage", "user.first_name",
                           "mapped_registries",
                           "user.last_name", "other_names", "other_names.id", "other_names.name", "origin_updated_at"]

    api_include_methods = ["reference_url", "reference_url_parent"]

    api_add_columns_single = ["data", "description"]
    api_add_methods_single = ["mapped_labels"]

    api_custom_endpoints = [
        APIModelEndpoint("get_by_key", endpoint='{tablename}/<registry_key>/<element_key>', methods=["GET"]),
        APIModelEndpoint("get_create_by_key", endpoint='{tablename}/<registry_key>/<element_key>', methods=["POST"]),
        APIModelEndpoint("update_by_key", endpoint='{tablename}/<registry_key>/<element_key>', methods=["PATCH"]),
        APIModelEndpoint("link_label_by_key", endpoint='{tablename}/<registry_key>/<element_key>/label/<label_id>', methods=["POST"]),
        APIModelEndpoint("unlink_label_by_key", endpoint='{tablename}/<registry_key>/<element_key>/label/<label_id>', methods=["DELETE"]),
        APIModelEndpoint("get_nested", endpoint='{tablename}/nested', methods=["GET"]),
    ]

    def reference_url(self):
        return self._get_reference_url(self.vocab_registry_key, self.key)

    def reference_url_parent(self):
        return self._get_reference_url(self.vocab_registry_key, self.parent_key)

    @staticmethod
    def _get_reference_url(registry_key, element_key):
        registry = VocabRegistryAPI.query.get(registry_key)
        return registry.registry_plugin.get_reference(element_key)

    def mapped_labels(self):
        return [dict(name=l.name, name_path=l.name_path, id=l.id, current_user_can_edit=l.current_user_can_edit,
                     label_scheme=dict(
                         name=l.label_scheme.name, id=l.label_scheme.id,
                         children=[dict(name=c.name, id=c.id) for c in l.label_scheme.children]
                     )) for l in self.labels]

    @classmethod
    def link_label_by_key(cls, registry_key, element_key, label_id, _results=None):
        registry_key = str(registry_key).strip()
        element_key = str(element_key).strip()
        label_instance = LabelAPI.get_by_id(label_id)
        if not label_instance.current_user_can_edit:
            raise ProcessingException(description="Unauthorised. Only a scheme custodian / editor can link vocab_elements. "
                                                  "Please contact the custodian, or apply for edit permission.", code=401)
        if _results is None:
            _results = dict(nelements_linked=0, nelements_imported=0, nelements=0, element_ids=[], label_id=label_id)
        instance, elem_created = cls.get_create_by_key(registry_key, element_key, render_response=False)
        assoc_inst, assoc_created = LabelVocabElementAssociation.get_or_create(
            label_id=label_instance.id, vocab_element_id=instance.id, user_id=current_user.id)

        # record results
        _results["nelements"] += 1
        _results["element_ids"].append(instance.id)
        if elem_created: _results["nelements_imported"] += 1
        if assoc_created: _results["nelements_linked"] += 1

        if isinstance(instance.mapped_registries, list):
            for i in instance.mapped_registries:
                # if is_supported_registry(i.get("registry_key")) is True:
                if VocabRegistryAPI.query.filter_by(key=i.get("registry_key")).count() > 0:
                    cls.link_label_by_key(i.get("registry_key"), i.get("key"), label_id, _results=_results)
        return render_data_response(_results)

    @classmethod
    def unlink_label_by_key(cls, registry_key, element_key, label_id):
        label_instance = LabelAPI.get_by_id(label_id)
        if not label_instance.current_user_can_edit:
            raise ProcessingException(description="Unauthorised. Only a scheme custodian / editor can unlink vocab_elements. "
                                                  "Please contact the custodian, or apply for edit permission.", code=401)
        instance = cls.query.filter(cls.key == element_key, cls.vocab_registry_key == registry_key).one()
        LabelVocabElementAssociation.query.filter_by(label_id=label_instance.id, vocab_element_id=instance.id).one().delete()
        return render_data_response(dict(message="Vocab Element has bee unlinked from Label"))

    @classmethod
    def update_by_key(cls, registry_key, element_key):
        cls.assert_user_login()
        instance = cls.query.filter(cls.key == element_key, cls.vocab_registry_key == registry_key).one_or_none()
        if not instance:
            raise ProcessingException("No vocab_element found matching element_key={}, registry_key={}.".format(element_key, registry_key), 404)
        else:
            registry = VocabRegistryAPI.query.get(registry_key)
            data = registry.registry_plugin.get_element(key=element_key, **request.args)
            instance = instance.update(**cls._deserialise_relatives(data))
            # instance = instance.update(**data)
        return render_data_response(instance.instance_to_dict(is_single=True))
        # return cls.get_create_update_by_key(registry_key, element_key, update=True, query_registry=False, create=False)

    @classmethod
    def get_create_by_key(cls, registry_key, element_key, render_response=True):
        cls.assert_user_login()
        instance = cls.query.filter(cls.key == element_key, cls.vocab_registry_key == registry_key).one_or_none()
        created = False
        if not instance:
            registry = VocabRegistryAPI.query.get(registry_key)
            data = registry.registry_plugin.get_element(key=element_key, **request.args)
            # Double check key in case registry smartly map superseded keys to other ones. Avoid reimporting.
            instance = cls.query.filter(cls.key==str(data.get('key')), cls.vocab_registry_key == registry_key).one_or_none()
            if not instance:
                instance = cls.create(user_id=current_user.id, **data)
                created = True
        if render_response:
            return render_data_response(instance.instance_to_dict(is_single=True))
        else:
            return instance, created
        # return cls.get_create_update_by_key(registry_key, element_key, create=True, query_registry=False, update=False)

    # @classmethod
    # def get_by_key(cls, registry_key, element_key):
    #     instance = cls.query.filter(cls.key == element_key, cls.vocab_registry_key == registry_key).one_or_none()
    #     if not instance:
    #         registry = get_vocab_registry(registry_key)
    #         data = registry.get_element(key=element_key, **request.args)
    #         i = VocabRegistryAPI.query.get(registry_key)
    #         data["vocab_registry"] = dict(name=i.name, key=i.key, description=i.description, vocab_element_key_name=i.vocab_element_key_name)
    #         data["reference_url"] = cls._get_reference_url(registry_key, element_key)
    #         data["reference_url_parent"] = cls._get_reference_url(registry_key, data.get("parent_key"))
    #         return render_data_response(data)
    #     return render_data_response(instance.instance_to_dict(is_single=True))
    #     # return cls.get_create_update_by_key(registry_key, element_key, create=False, query_registry=True, update=False)

    # @classmethod
    # def get_create_update_by_key(cls, registry_key, element_key, update=False, create=False, query_registry=False):
    #     """Get / create / update"""
    #     registry = get_vocab_registry(registry_key)
    #     instance = cls.query.filter(cls.key == element_key, cls.vocab_registry_key == registry_key).one_or_none()
    #     if instance:
    #         if update:
    #             data = registry.get_element(key=element_key, **request.args)
    #             instance = instance.update(**cls._deserialise_relatives(data))
    #     elif create:
    #         data = registry.get_element(key=element_key, **request.args)
    #         instance = cls.create(**cls._deserialise_relatives(data))
    #     elif query_registry:
    #         registry = get_vocab_registry(registry_key)
    #         data = registry.get_element(key=element_key, **request.args)
    #         i = VocabRegistryAPI.query.get(registry_key)
    #         data["vocab_registry"] = dict(name=i.name, key=i.key, description=i.description, vocab_element_key_name=i.vocab_element_key_name)
    #         data["reference_url"] = cls._get_reference_url(registry_key, element_key)
    #         data["reference_url_parent"] = cls._get_reference_url(registry_key, data.get("parent_key"))
    #         return render_data_response(data)
    #     else:
    #         raise ProcessingException("No results found.", code=404)
    #     return render_data_response(instance.instance_to_dict(is_single=True))

    @classmethod
    def get_by_key(cls, registry_key, element_key):
        instance = cls.query.filter(cls.key == element_key, cls.vocab_registry_key == registry_key).one_or_none()
        if instance:    # key has already been imported, so return imported element
            data = instance.instance_to_dict(is_single=True)
        else:           # key has not been imported, so present from registry
            # registry = VocabRegistryAPI.query.get(registry_key)
            registry = VocabRegistry.query.filter_by(key=registry_key).one_or_none()
            assert registry is not None, "Unknown vocab_registry key '{}'".format(registry_key)
            data = registry.registry_plugin.get_element(key=element_key, **request.args)
            i = VocabRegistryAPI.query.get(registry_key)
            data["vocab_registry"] = dict(name=i.name, key=i.key, description=i.description,
                                          vocab_element_key_name=i.vocab_element_key_name)
            data["reference_url"] = cls._get_reference_url(registry_key, element_key)
            data["reference_url_parent"] = cls._get_reference_url(registry_key, data.get("parent_key"))
        return render_data_response(data)

    @classmethod
    def get_nested(cls):
        results = cls.get_search_results(paginate_results=False)
        return render_data_response(nest_vocab_elements_on_lineage(results))

    # def patch_pre

# class AnnotationAPI(Annotation):
#     @classmethod
#     def register_api(self, apimanager):
#         apimanager.create_api(self, methods=['GET', 'POST', 'DELETE', 'PATCH'], max_results_per_page=-1)
#
#
# class TagAnnotationAssociationAPI(TagAnnotationAssociation):
#     @classmethod
#     def register_api(self, apimanager):
#         apimanager.create_api(self, methods=['GET', 'POST', 'DELETE', 'PATCH'], max_results_per_page=-1)


# def register_api(apimanager, app):
#     TagAPI.register_api(apimanager, app)
#     TagTypeAPI.register_api(apimanager, app)
#     LabelAPI.register_api(apimanager, app)
#     TagSynonymAPI.register_api(apimanager, app)
#     LabelSchemeAPI().register_api(apimanager, app)
#     LabelSchemeFileAPI.register_api(apimanager, app)
#     LabelAssociationAPI.register_api(apimanager, app)
#     LabelInfoAPI.register_api(apimanager, app)
#     # AnnotationAPI.register_api(apimanager, app)
#     # TagAnnotationAssociationAPI.register_api(apimanager, app)

add_to_api(TagAPI)
add_to_api(TagTypeAPI)
add_to_api(LabelAPI)
# add_to_api(TagSynonymAPI)
add_to_api(LabelSchemeAPI)
add_to_api(LabelSchemeFileAPI)
add_to_api(LabelAssociationAPI)
add_to_api(LabelInfoAPI)
add_to_api(VocabRegistryAPI)
add_to_api(VocabElementAPI)
add_to_api(VocabElementNamesAPI)
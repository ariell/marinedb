from pandas import read_csv, merge
from itertools import chain
import json


filename = "/Users/ariell/Google Drive/MyBiz/Greybits Projects/SOI/Data/AnnotationSchemes/catami-caab-codes_1.4.csv"
usecols = [0, 1, 2, 3]
delimiter = ","
skiprows = 1
index_col = None

# Load CSV file
df = read_csv(filename, parse_dates=False, index_col=index_col, delimiter=delimiter, usecols=usecols, skip_blank_lines=True, skiprows=skiprows)

# SPLIT STRING (new fileop)
col = 'CATAMI_DISPLAY_NAME'
newcol = 'tags'
sep = ": "
df[newcol] = [i.split(sep) for i in df[col]]

# # Get unique list of tags
# tags = set(chain(*df['tags']))

# APPEND STRING (new fileop)
col = 'SPECIES_CODE'
newcol = 'link'
format = "http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode={}"
df[newcol] = [format.format(i) for i in df[col]]


# DROP (existing file op)
labels = ['CATAMI_PARENT_ID', 'CPC_CODES']
df.drop(labels, axis=1, inplace=True)

# # RENAME (existing file op)
# columns = {
#     'SPECIES_CODE': 'caab_code',
#     'CATAMI_DISPLAY_NAME': 'name'
# }
# df.columns = [columns[c] if c in columns else c for c in df.columns]

# TO DICT (new fileop)
taggroups = df.to_dict(orient='records')


qa = {
    "operation": "to_dict",
    "columns": {
        "name": "CATAMI_DISPLAY_NAME",
        "tags": "tags",
        "user_id": {
            "ref": "inherit",
            "col": "user_id"
        },
        "label_scheme_id": {
            "ref": "inherit",
            "col": "label_scheme_id"
        },
        "info": {
            "ref": "json",
            "format": "[{'name':'caab_code', 'description':{}},{'name':'link', 'description':{}}]",
            "cols": ["SPECIES_CODE", "link"]
        }
    }
}


test = [
    {
        "operation": "to_dict",
            "columns": {
            "name": "OpCode",
            "platform": "Info8Value",
            "info": {
                "ref": "struct",
                "format": "[{{'name':'lat', 'description':{}}},{{'name':'lon', 'description':{}}}]",
                "cols": ["Info6Value", "Info7Value"]
            },
            "timestamp": {
                "ref": "datetime",
                "col": "Info12Value",
                "format": "%Y %m %d %H:%M:%S"
            },
            "campaign_id": {
                "ref": "inherit",
                "col": "campaign_id"
            },
            "user_id": {
                "ref": "inherit",
                "col": "user_id"
            }
        }
    },
    {
        "operation": "dict_to_json"
    }
]



# TODO: add new fileop for saving new file as well


###############
# TESTING!!!!!!
###############

from marinedb.annotation.models import LabelScheme, Label, TagType, LabelInfo

testscheme = [{'caab_code': 82001015,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=82001015',
  'tags': ['Substrate',
   'Unconsolidated (soft)',
   'Sand / mud (<2mm)',
   'Fine sand (no shell fragments)']},
 {'caab_code': 82001016,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=82001016',
  'tags': ['Substrate',
   'Unconsolidated (soft)',
   'Sand / mud (<2mm)',
   'Mud / silt (<64um)']},
 {'caab_code': 80600901,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=80600901',
  'tags': ['Worms']},
 {'caab_code': 36110000,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=36110000',
  'tags': ['Worms', 'Acorn worms']},
 {'caab_code': 17020000,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=17020000',
  'tags': ['Worms', 'Echiura']},
 {'caab_code': 13000000,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=13000000',
  'tags': ['Worms', 'Flatworms']},
 {'caab_code': 15400000,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=15400000',
  'tags': ['Worms', 'Penisworms']},
 {'caab_code': 22000000,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=22000000',
  'tags': ['Worms', 'Polychaetes']},
 {'caab_code': 22000902,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=22000902',
  'tags': ['Worms', 'Polychaetes', 'Other polychaetes']},
 {'caab_code': 22000901,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=22000901',
  'tags': ['Worms', 'Polychaetes', 'Tube worms']},
 {'caab_code': 17000000,
  'link': 'http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=17000000',
  'tags': ['Worms', 'Sipuncula']}]

newscheme=[]

for i in testscheme:
    newscheme.append({'tags':i['tags'], 'info':[{'name':'caab_code', 'description':i['caab_code']},{'name':'link','description':i['link']}]})









# # MERGE  TODO: doesn't work!
# df2 = df.copy(deep=True)
# df_merged = merge(df, df2, how='outer', on=['CATAMI_PARENT_ID'])


# [{"operation":"load","usecols":[0,1,2,3],"delimiter":",","skiprows":1},{"operation":"splitstr","col":"CATAMI_DISPLAY_NAME","sep":":%20","newcol":"tags"},{"operation":"formatstr","col":"SPECIES_CODE","format":"http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode={}","newcol":"link"},{"operation":"to_dict","columns":{"name":"CATAMI_DISPLAY_NAME","tags":"tags","info":{"ref":"struct","format":{"code":"SPECIES_CODE","link":"link","code_parent":"CATAMI_PARENT_ID","code_short":"CPC_CODES"}},"label_scheme_id":{"ref":"inherit","col":"label_scheme_id"}}},{"operation":"dict_to_db","table":"label"},{"operation":"dict_to_json"}]


# [
#     {
#         "operation": "load",
#         "usecols": [0, 1, 2, 3],
#         "delimiter": ",",
#         "skiprows": 1
#     },
#     {
#         "operation": "splitstr",
#         "col": "CATAMI_DISPLAY_NAME",
#         "sep": ":%20",
#         "newcol": "tags"
#     },
#     {
#         "operation": "formatstr",
#         "col": "SPECIES_CODE",
#         "format": "http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode={}",
#         "newcol": "link"
#     },
#     {
#         "operation": "to_dict",
#          "columns": {
# 			"name": "CATAMI_DISPLAY_NAME",
#             "tags": "tags",
# 			"info": {
# 				"ref": "struct",
# 				"format": {"code":"SPECIES_CODE","link":"link","code_parent":"CATAMI_PARENT_ID","code_short":"CPC_CODES"}
# 			},
# 			"label_scheme_id": {
# 				"ref": "inherit",
# 				"col": "label_scheme_id"
# 			}
#         }
#     },
#     {
#         "operation": "dict_to_db",
#         "table":"label"
#     },
#     {
#         "operation": "dict_to_json"
#     }
# ]


[{"SPECIES_CODE": 35000000, "link": "http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=35000000",
  "CATAMI_DISPLAY_NAME": "Ascidians", "tags": ["Ascidians"]},
 {"SPECIES_CODE": 35000904, "link": "http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=35000904",
  "CATAMI_DISPLAY_NAME": "Ascidians: Stalked", "tags": ["Ascidians", "Stalked"]},
 {"SPECIES_CODE": 22000901, "link": "http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=22000901",
  "CATAMI_DISPLAY_NAME": "Worms: Polychaetes: Tube worms", "tags": ["Worms", "Polychaetes", "Tube worms"]},
 {"SPECIES_CODE": 17000000, "link": "http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode=17000000",
  "CATAMI_DISPLAY_NAME": "Worms: Sipuncula", "tags": ["Worms", "Sipuncula"]}]
# -*- coding: utf-8 -*-

import datetime as dt

# from sqlalchemy.ext.compiler import compiles
# from sqlalchemy.sql.elements import ColumnClause
import json

from marinedb.database import (
    Column,
    db,
    Model,
    ReferenceCol,
    relationship,
    backref,
    SurrogatePK,
    UniqueConstraint,
    DBFileMixin,
    Index,
    JSONDataFieldMixin,
    UUIDMixin,
    get_model_by_table_name)

from sqlalchemy.ext.associationproxy import association_proxy
from flask_login import current_user, current_app
import random
import time

from sqlalchemy.ext.hybrid import hybrid_property

from savalidation.helpers import before_flush

from marinedb.user.models import get_current_user
from modules.flask_restless.mixins import APIModel
from modules.flask_restless.search import search
from modules.flask_restless.usergroups import APIUserGroupMixin
from flask import request, g

from modules.vocab_registries import get_vocab_registry
from modules.vocab_registries import plugins as vocab_registry_plugins


def get_random_hex_color():
    return "#%06x" % random.randint(0, 0xFFFFFF)


class Tag(APIModel, SurrogatePK, Model):
    __tablename__ = 'tag'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False)
    user_id = ReferenceCol('users', nullable=False)
    user = relationship('User')
    type_id = ReferenceCol('tag_type', nullable=True)
    type = relationship('TagType', backref=backref('tags'))
    description = Column(db.Text, nullable=True)
    # synonyms_tag_synonym_id = ReferenceCol('tag_synonym', nullable=True)

    synonym_tag_id = ReferenceCol('tag', nullable=True, cascade=True)
    synonym_tag = relationship('Tag', remote_side='Tag.id', backref=backref('synonyms', passive_deletes=True))

    __table_args__ = (UniqueConstraint('type_id', 'name', name='_tag_name_uc'),)

    # association proxy to convert backref label_tags
    labels = association_proxy('label_tags', 'label')

    @hybrid_property
    def is_synonym(self):
        return self.synonym_tag_id is not None

    @is_synonym.expression
    def is_synonym(cls):
        return cls.synonym_tag_id.isnot(None)

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        # self.mungedfile = None

    def __repr__(self):
        return '<Tag({name})>'.format(name=self.name)


class TagType(APIModel, SurrogatePK, Model):
    __tablename__ = 'tag_type'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), unique=True, nullable=False)
    description = Column(db.Text, nullable=True)

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        # self.mungedfile = None

    def __repr__(self):
        return '<TagType({name})>'.format(name=self.name)


class LabelSchemeFile(APIModel, DBFileMixin, Model):
    __tablename__ = 'label_scheme_file'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    # Inherits from DBFileMixin:
    #  - name
    #  - description
    #  - rawfiledata
    #  - iscompressed
    label_scheme_id = ReferenceCol('label_scheme', nullable=False)
    label_scheme = relationship('LabelScheme', backref=backref("label_scheme_files", cascade="all, delete-orphan"))

    def __repr__(self):
        return '<LabelSchemeFile({name})>'.format(name=self.name)


class LabelScheme(APIUserGroupMixin, APIModel, SurrogatePK, Model):
    __tablename__ = 'label_scheme'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), unique=False, nullable=False)
    description = Column(db.Text, nullable=True)
    user_id = ReferenceCol('users', nullable=False)
    user = relationship('User')
    is_hierarchy = Column(db.Boolean(), default=False)
    created_at = Column(db.DateTime, default=dt.datetime.utcnow, doc="Creation time (UTC)")
    updated_at = Column(db.TIMESTAMP, default=dt.datetime.utcnow, onupdate=dt.datetime.utcnow, doc="Updated time (UTC)")
    parent_label_scheme_id = ReferenceCol('label_scheme', nullable=True, cascade=False)
    parent_label_scheme = relationship('LabelScheme', remote_side='LabelScheme.id', backref=backref('children', lazy="dynamic", passive_deletes=False))
    annotation_sets = relationship("AnnotationSet", back_populates="label_scheme", passive_deletes=True, lazy='dynamic')
    # exemplar_annotation_sets = relationship(
    #     "AnnotationSetAPI",
    #     primaryjoin="and_(or_(AnnotationSetAPI.label_scheme_id==LabelScheme.id,AnnotationSetAPI.label_scheme_id==LabelScheme.parent_label_scheme_id), AnnotationSetAPI.is_exemplar==True)",
    #     lazy="dynamic"
    # )

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        # self.mungedfile = None

    def __repr__(self):
        return '<LabelScheme({name})>'.format(name=self.name)

    @hybrid_property
    def is_child(self):
        """BOOLEAN, whether or not this class label has a parent class label"""
        return self.parent_label_scheme_id is not None

    @is_child.expression
    def is_child(cls):
        return cls.parent_label_scheme_id.isnot(None)

    def annotation_set_count(self):
        return self.annotation_sets.filter_by(is_exemplar=False).count()

    def exemplar_annotation_set_count(self):
        return self.annotation_sets.filter_by(is_exemplar=True).count()

    # def get_label_filter(self, label_filter=None):
    #     """
    #     Return label_filter as a tuple of filter arguments recursively for parents if an extended scheme
    #     :param label_filter: append to filter recursively
    #     :return:
    #     """
    #     if label_filter is None: label_filter = []
    #     label_filter.append(Label.label_scheme_id == self.id)
    #     if self.is_child:
    #         label_filter = self.parent_label_scheme.get_label_filter(label_filter=label_filter)
    #     return tuple(label_filter)

    def parent_label_scheme_ids(self, _label_scheme_ids=None):
        if _label_scheme_ids is None: _label_scheme_ids = []
        _label_scheme_ids.append(self.id)
        if self.parent_label_scheme_id is not None:
            _label_scheme_ids = self.parent_label_scheme.parent_label_scheme_ids(_label_scheme_ids=_label_scheme_ids)
        return _label_scheme_ids

    def reset_label_parents(self, replace_existing=False):
        counts = dict(parent_labels_set=0, parent_labels_cleared=0)
        for l in self.labels:
            if self.is_hierarchy:
                parent_origin_code = l.parent_origin_code
                if ((l.parent_id and replace_existing) or l.parent_id is None) and parent_origin_code is not None:
                    parent_label = Label.query.filter(Label.label_scheme_id.in_(self.parent_label_scheme_ids()),
                                                      Label.origin_code == parent_origin_code).one_or_none()  # throw error if more than one
                    l.parent_id = parent_label.id if parent_label else None
                elif ((l.parent_id and replace_existing) or l.parent_id is not None) and parent_origin_code is None:
                    l.parent_id = None
            else:
                l.parent_id = None
            if l.parent_id is None:
                counts['parent_labels_cleared'] += 1
            else:
                counts['parent_labels_set'] += 1

        db.session.commit()
        return counts

    # def set_parent_labels(self):
    #     #TODO: log message properly in a way that gets passed to API
    #     t0 = time.time()
    #     count = {'success': 0, 'error': 0}
    #     taggroups = [{'name':i.name, 'id':i.id, 'info': {j.name:j.value for j in i.info}, 'obj':i} for i in self.labels]
    #     taggroups_code = {i['info']['code']: i for i in taggroups if "code" in i['info']}
    #     print("Built label index in {} s...".format(time.time()-t0))
    #     for i in taggroups:
    #         if 'code_parent' in i['info']:
    #             if i['info']['code_parent'] in taggroups_code:
    #                 i['obj'].parent_id = taggroups_code[i['info']['code_parent']]['id']
    #                 count['success'] += 1
    #             else:
    #                 print("WARNING: Cannot find parent_code: '{}' for '{}' (id={})".format(i['obj'].parent_id, i['name'], i['id']))
    #                 count['error'] += 1
    #         else:
    #             print ("ERROR: No 'code_parent' field in {} (id={})".format(i['name'], i['id']))
    #             count['error'] += 1
    #     print ("Set {} parent_ids with {} errors in {} s".format(count['success'], count['error'], time.time()-t0))
    #
    #     self.is_hierarchy = count['success'] > 0
    #
    #     db.session.commit()
    #     print("Completed import in {} s...".format(time.time() - t0))
    #
    # def set_child_tags(self, parent=None):
    #     if parent is None:
    #         t0 = time.time()
    #         for parent in Label.query.filter_by(parent_id=None, label_scheme_id=self.id).all():
    #             self.set_child_tags(parent)
    #         print ("Set child tags in {} s".format(time.time()-t0))
    #         db.session.commit()
    #     else:
    #         for c in parent.children:
    #             for pt in parent.tags:
    #                 LabelAssociation.get_or_create(commit=False, tag_id=pt.id, label_id=c.id)
    #             self.set_child_tags(c)


# # Handle recursive path search
# class NamePathColumn(ColumnClause):
#     pass
#
# @compiles(NamePathColumn)
# def compile_name_path_column(element, compiler, **kwargs):
#     return 'label.name_path'  ## something missing?


class Label(APIModel, SurrogatePK, Model, UUIDMixin):
    __tablename__ = 'label'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(160), unique=False, nullable=False, doc="The name of this class label")
    color = Column(db.String(10), unique=False, default=get_random_hex_color, doc="The colour of this class label (hex code, including #)")
    user_id = ReferenceCol('users', nullable=False, doc="The `id` of the user who owns or created this class label")
    user = relationship('User', doc="The user who owns or created this class label")
    label_scheme_id = ReferenceCol('label_scheme', nullable=False, doc="The `id` of the annotation scheme ([label_scheme](#label_scheme)) containing this class label")
    label_scheme = relationship('LabelScheme', backref=backref("labels", cascade="all, delete-orphan", lazy="dynamic"), doc="The annotation scheme containing this class label")
    parent_id = ReferenceCol('label', nullable=True, doc="`id` of the parent class label")
    parent = relationship('Label', remote_side='Label.id', backref=backref("children", lazy="dynamic"), doc="The parent class label (linked observation)")
    is_approved = Column(db.Boolean(), default=False, doc="Whether this class label has been approved by the custodian")
    created_at = Column(db.DateTime, default=dt.datetime.utcnow, doc="Creation time (UTC)")
    updated_at = Column(db.TIMESTAMP, default=dt.datetime.utcnow, onupdate=dt.datetime.utcnow, doc="Updated time (UTC)")
    origin_code = Column(db.String(80), unique=False, nullable=True, index=True)
    parent_origin_code = Column(db.String(80), unique=False, nullable=True)
    vocab_elements = relationship("VocabElement", secondary="label_vocab_element", lazy="dynamic", enable_typechecks=False,
                                  doc="A list of `vocab_elements` linked to this `label`")

    tags = association_proxy('label_tags', 'tag')
    # tags = relationship("Tag", secondary="label_tags", lazy="dynamic", enable_typechecks=False)

    # exemplars = relationship(
    #     "AnnotationLabelAPI",
    #     primaryjoin="and_(or_(AnnotationSetAPI.label_scheme_id==LabelScheme.id,AnnotationSetAPI.label_scheme_id==LabelScheme.parent_label_scheme_id), AnnotationSetAPI.is_exemplar==True, AnnotationLabel.annotation_set_id==AnnotationSet.id, AnnotationLabel.label_id==Label.id)",
    #     lazy="dynamic"
    # )


    # Enforce uniqueness constraint on type and name
    # __table_args__ = (UniqueConstraint('label_scheme_id', 'name', 'parent_id', name='_taggroup_name_uc'),)

    # Enforce unique contraint on label_scheme and origin_code (if exists) to prevent reimport on file ops
    # If doesn't exist, prevent duplicate with parent_id and name
    __table_args__ = (
        Index(
            "_label_origin_uc",
            "label_scheme_id",
            "origin_code",
            unique=True,
            postgresql_where=origin_code.isnot(None)
        ),
        Index(
            "_label_parent_uc",
            "label_scheme_id",
            "name",
            "parent_id",
            unique=True,
            postgresql_where=origin_code.is_(None)
        ),
    )

    # association proxy to convert backref label_tags
    # tags = association_proxy('label_tags', 'tag', creator=lambda tag: LabelAssociation(tag=tag))

    def __init__(self, info=None, **kwargs):
        # Set info fields
        kwargs["info"] = []
        if isinstance(info, dict):     # convert key-value pairs to list of dicts
            info = [dict(name=k, value=v) for k,v in info.items()]
        if not isinstance(info, list): # if not list (of dicts) just
            info = []
        for i in info:
            kwargs["info"].append(LabelInfo(**i))

        # # Set parent
        # parent_origin_code = kwargs.get("parent_origin_code", None)
        # if "parent_id" not in kwargs and parent_origin_code is not None:
        #     label_scheme_id = kwargs.get('label_scheme_id') if 'label_scheme_id' in kwargs else self.label_scheme.id
        #     scheme = LabelScheme.get_by_id(label_scheme_id)
        #     # Match parent_origin_code within this scheme and all inherited parent schemes
        #     # parent_label = Label.query.filter(db.or_(*scheme.get_label_filter()), Label.origin_code==parent_origin_code).one()  # throw error if more than one
        #     parent_label = Label.query.filter(Label.label_scheme_id.in_(scheme.parent_label_scheme_ids()), Label.origin_code == parent_origin_code).one()  # throw error if more than one
        #     kwargs["parent_id"] = parent_label.id if parent_label else None

        # User info
        if "user_id" not in kwargs:
            if current_user.is_authenticated():
                kwargs["user_id"] = current_user.id
            else:
                ValueError("user_id not set, and user not logged in")

        db.Model.__init__(self, **kwargs)

        self.translation_info = None
        # self.mungedfile = None

    def __repr__(self):
        return '<Label({name})>'.format(name=self.name)

    @property
    def vocab_registry(self):
        """Dictionary representation of `vocab_elements` by registry key (conveniently formatted for exporting schemes)"""
        result = {}
        for v in self.vocab_elements:
            if v.vocab_registry_key not in result:
                result[v.vocab_registry_key] = []
            result[v.vocab_registry_key].append(v.key)
        return {k: ",".join(v) for k,v in result.items()}


    def _get_translation_parameters(self):
        t = json.loads(request.args.get("translate", '{}') or '{}')
        _translation_cache = getattr(g, "_translation_cache", None)
        if _translation_cache is None:
            _translation_cache = g._translation_cache = dict()
        _translation_registry_keys = getattr(g, "_translation_registry_keys", None)
        if _translation_registry_keys is None:
            _translation_registry_keys = g._translation_registry_keys = t.get('vocab_registry_keys', current_app.config["TRANSLATION_REGISTRY_KEY_ORDER"])
        _target_label_scheme = getattr(g, "_target_label_scheme", None)
        if _target_label_scheme is None:
            target_label_scheme_id = t.get('target_label_scheme_id', None)
            if target_label_scheme_id is not None:
                _target_label_scheme = g._target_label_scheme = LabelScheme.get_by_id(target_label_scheme_id)

        return _translation_cache, _translation_registry_keys, _target_label_scheme


    @property
    def translated(self):
        """Special API property returns [label](#label) object in target label scheme.
        Requires `translate` query parameter containing `target_label_scheme_id` and optional `vocab_registry_keys`
        parameters. See [Label Translation](#label_translation) for more information."""
        # TODO: this should not be in model (because it is processing the request arg - should be in api.py,
        #  but relation doesn't have api properties)
        _cache, vocab_registry_keys, target_label_scheme = self._get_translation_parameters()
        if target_label_scheme:
            return self.translate_label(target_label_scheme, vocab_registry_keys=vocab_registry_keys, _cache=_cache)
        else:
            return None

    def translate_label(self, target_label_scheme, vocab_registry_keys=None, mapping_override=None, _match_type=None, _cache=None):
        # Per request cache to avoid doing same label lookup more than once
        if isinstance(_cache, dict) and self.id in _cache:
            return _cache[self.id]
        else:
            label = None
            if _match_type is None:
                _match_type = "direct"

            if mapping_override is None:
                mapping_override = {}

            # 0) override: check if direct mapping is defined
            if self.id in mapping_override:
                label = Label.get_by_id(mapping_override[str(self.id)])   # use str of ID for key
                label.set_translation_info("mapping_override.{}".format(_match_type))

            # 1) tree_traversal: if source label is in the target scheme, we have a match
            elif self.label_scheme_id in target_label_scheme.parent_label_scheme_ids():
                label = self
                label.set_translation_info("tree_traversal.{}".format(_match_type))

            # 2) tree_traversal: if target is base of source, move up tree (extended schemes)
            elif target_label_scheme.id in self.label_scheme.parent_label_scheme_ids():
                if self.parent_id is not None:
                    label = self.parent.translate_label(target_label_scheme, _match_type="relative", _cache=_cache)

            # 3) semantic translation using mapped vocab_elements
            else:
                # iterate through registry keys in order
                target_label_scheme_filter = Label.label_scheme_id.in_(target_label_scheme.parent_label_scheme_ids())
                for vocab_registry_key in vocab_registry_keys:
                    for ve in self.vocab_elements.filter_by(vocab_registry_key=vocab_registry_key):
                        if ve:
                            # filter matched labels by target scheme
                            label = ve.labels.filter(target_label_scheme_filter).first()
                            if label is not None:
                                # Match found, set translation info
                                label.set_translation_info("semantic.{}.{}.{}".format(_match_type, vocab_registry_key, ve.key))
                                return label
                # No match found, travers up tree and try with parent label
                if label is None and self.parent_id is not None:
                    label = self.parent.translate_label(
                        target_label_scheme, vocab_registry_keys=vocab_registry_keys, mapping_override=None,
                        _match_type="relative", _cache=_cache)
            _cache[self.id] = label
            return label

    def set_translation_info(self, match_type):
        self.translation_info = match_type

    @property
    def current_user_can_edit(self):
        label_scheme = self.label_scheme  # LabelSchemeAPI.get_by_id(self.label_scheme_id)
        return label_scheme.current_user_can_edit or \
               (label_scheme.current_user_is_member and not self.is_approved and self.user_id == current_user.id)

    @property
    def current_user_is_custodian(self):
        # return LabelSchemeAPI.get_by_id(self.label_scheme_id).current_user_is_custodian
        return self.label_scheme.current_user_can_edit

    @hybrid_property
    def is_child(self):
        """BOOLEAN, whether or not this class label has a parent class label"""
        return self.parent_id is not None

    @is_child.expression
    def is_child(cls):
        return cls.parent_id.isnot(None)

    # @hybrid_property
    # def is_parent(self):
    #     """BOOLEAN, whether or not this class label has children / descendants"""
    #     return self.children.count() > 0
    #
    # @is_parent.expression
    # def is_parent(cls):
    #     return db.true(cls.children.count() > 0)

    @hybrid_property
    def is_mapped(self):
        """BOOLEAN, whether this label has one or more vocab_element mappings"""
        return db.session.query(self.label_vocab_element.exists()).scalar()

    @is_mapped.expression
    def is_mapped(cls):
        return db.session.query(cls.id).filter(LabelVocabElementAssociation.label_id==cls.id).exists()

    @property
    def name_path(self):
        return self.lineage_names()

    def annotation_count(self):
        """Number of annotations that have been assigned with this label"""
        return self.annotations.count()

    def descendant_count(self):
        """Number of direct children labels nested under this label"""
        return self.children.count()

    # def exemplar_annotation_sets(self):
    #     """A list of exemplar annotation sets from the parent [label_scheme](#label_scheme) object"""
    #     return [dict(id=a.id, name=a.name) for a in self.label_scheme._exemplar_annotation_set_query().all()]

    def label_scheme_custodian(self):
        return dict( first_name=self.label_scheme.user.first_name, last_name=self.label_scheme.user.last_name,
                     username=self.label_scheme.user.username, id=self.label_scheme.user.id)

    # def tags(self):
    #     """List of tags associated with this """
    #     return [dict(name=i.tag.name, id=i.tag.id) for i in self.label_tags]

    # @name_path.expression
    # def name_path(cls):
    #     return NamePathColumn(cls)

    def lineage_names(self, children_names=None):
        lineage_names = "{} > {}".format(self.name, children_names) if children_names is not None else self.name
        return self.parent.lineage_names(children_names=lineage_names) if self.parent_id is not None else lineage_names

    def lineage(self, children=[]):
        lineage = [dict(name=self.name, id=self.id)]+children
        return self.parent.lineage(children=lineage) if self.parent_id is not None else lineage

    def remove_tag(self, tag_id):
        ta = LabelAssociation.query.filter(LabelAssociation.tag_id == tag_id, LabelAssociation.label_id == self.id).one()
        ta.delete()

    def add_tag(self, tag_id):
        return LabelAssociation.get_or_create(tag_id=tag_id, label_id=self.id)

    @property
    def tag_names(self):
        """Comma-delimited names of applied tags. Can also be used to set tags using comma-delimited tags when creating annotaions"""
        return ",".join([t.name for t in self.tags])

    @tag_names.setter
    def tag_names(self, value):
        if isinstance(value, str):
            for t in value.split(','):
                tag, is_cached = self.request_cache("_annotation_tags_cache", t)
                if not is_cached:
                    tag = Tag.query.filter(Tag.name.ilike(t)).first()
                    _, is_cached = self.request_cache("_annotation_tags_cache", t, tag)
                if tag:
                    # TagAnnotationAssociation.create(annotation_id=self.id, tag_id=tag.id, commit=False)
                    self.tags.append(tag)

    # @classmethod
    # # example:
    # # http://localhost:5000/api/label_scheme_file_get/12?queryparams=[{"operation":"load","usecols":[0,1,2,3],"delimiter":",","skiprows":1},{"operation":"splitstr","col":"CATAMI_DISPLAY_NAME","sep":":%20","newcol":"tags"},{"operation":"formatstr","col":"SPECIES_CODE","format":"http://www.marine.csiro.au/caabsearch/caab_search.caab_report?spcode={}","newcol":"link"},{"operation":"to_dict","columns":{"name":"CATAMI_DISPLAY_NAME","tags":"tags","info":{"ref":"struct","format":{"code":"SPECIES_CODE","link":"link","code_parent":"CATAMI_PARENT_ID","code_short":"CPC_CODES"}},"label_scheme_id":{"ref":"inherit","col":"label_scheme_id"}}},{"operation": "dict_to_db","table":"label"},{"operation":"dict_to_json"}]
    # def dict_to_db(cls, dictlist):
    #     user_id = current_user.id
    #     stats = {'label_sent': len(dictlist), 'label_added': 0, 'tags_new': 0, 'tags_reused': 0, 'info_added': 0}
    #     for i in dictlist:
    #         # if color provided, use it otherwise get a random color
    #         color = i['color'] if 'color' in i else "#%06x" % random.randint(0, 0xFFFFFF)
    #         taggroup = Label.create(commit=False, name=i['name'], user_id=user_id, label_scheme_id=i['label_scheme_id'], color=color)  #, parent_id)
    #         stats['label_added'] += 1
    #         if 'tags' in i:
    #             for t in i['tags']:
    #                 tag, created = Tag.get_or_create(commit=True, name=t.strip().lower(), user_id=user_id)  #, type_id,synonyms_label_id,is_public)
    #                 #taggroup.tags.append(tag)
    #                 tgassoc = LabelAssociation.create(commit=False, label_id=taggroup.id, tag_id=tag.id)
    #                 stats['tags_new'] += created is True        # add if created
    #                 stats['tags_reused'] += created is False    # add if reused
    #         if 'info' in i:
    #             for f in i['info']:
    #                 taggroup.info.append(LabelInfo.create(commit=False, name=f, value=i['info'][f]))
    #                 stats['info_added'] += 1
    #
    #     db.session.commit()
    #     if taggroup.label_scheme.is_hierarchy:
    #         taggroup.label_scheme.set_parent_labels()
    #         #taggroup.label_scheme.set_child_tags()
    #     return stats


class LabelAssociation(APIModel, Model):
    __tablename__ = 'label_tags'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    label_id = ReferenceCol('label', primary_key=True)
    tag_id = ReferenceCol('tag', primary_key=True)
    assoc_created_at = Column(db.DateTime, default=dt.datetime.utcnow)
    label = relationship("Label", backref=backref("label_tags", cascade="all, delete-orphan"))
    tag = relationship("Tag", backref=backref("label_tags", cascade="all, delete-orphan"))


class LabelInfo(APIModel, SurrogatePK, Model):
    __tablename__ = 'label_info'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), unique=False, nullable=False)
    value = Column(db.Text, nullable=True)
    label_id = ReferenceCol('label', nullable=True)
    label = relationship('Label', backref=backref("info", cascade="all, delete-orphan", lazy="dynamic"))

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)
        # self.mungedfile = None

    def __repr__(self):
        return '<LabelInfo({name})>'.format(name=self.name)


class VocabRegistry(APIModel, Model):
    __tablename__ = "vocab_registry"
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    key = Column(db.String(96), unique=True, nullable=False, primary_key=True)
    name = Column(db.String(128), unique=True, nullable=False)
    module = Column(db.String(96), nullable=False)
    module_kwargs = Column(db.JSON, nullable=True)
    vocab_element_key_name = Column(db.String(96), nullable=True)
    description = Column(db.Text, nullable=True)
    user_id = ReferenceCol('users', nullable=False, doc="The `id` of the user who created and owns this record")
    user = relationship('User', doc="The user who owns or created and owns this record")
    created_at = Column(db.DateTime, default=dt.datetime.utcnow)
    updated_at = Column(db.TIMESTAMP, default=dt.datetime.utcnow, onupdate=dt.datetime.utcnow, doc="Updated time (UTC)")

    @property
    def registry_plugin(self):
        module = getattr(vocab_registry_plugins, self.module)
        return get_vocab_registry(self.key, module, **self.module_kwargs)


class VocabElement(APIModel, SurrogatePK, Model):
    __tablename__ = "vocab_element"
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(128), unique=False, nullable=False)
    key = Column(db.String(96), nullable=False)
    lineage = Column(db.JSON, nullable=True)
    data = Column(db.JSON, nullable=True)
    mapped_registries = Column(db.JSON, nullable=True)
    description = Column(db.Text, nullable=True)
    parent_key = Column(db.String(96), nullable=True)
    vocab_registry_key = ReferenceCol('vocab_registry', pk_name="key", nullable=False,
                                      doc="The `id` of the `vocab_registry`")
    vocab_registry = relationship('VocabRegistry', doc="The `vocab_registry` that contains this `vocab_element`",
                                  backref=backref("vocab_elements", lazy="dynamic", cascade="all, delete-orphan"))
    user_id = ReferenceCol('users', nullable=False, doc="The `id` of the user who created and owns this record")
    user = relationship('User', doc="The user who owns or created and owns this record")
    created_at = Column(db.DateTime, default=dt.datetime.utcnow)
    updated_at = Column(db.TIMESTAMP, default=dt.datetime.utcnow, onupdate=dt.datetime.utcnow,
                        doc="Updated time (UTC)")
    origin_updated_at = Column(db.DateTime)
    labels = relationship("Label", secondary="label_vocab_element", lazy="dynamic", enable_typechecks=False,
                          doc="A list of `labels` linked to this `vocab_element`")

    __table_args__ = (UniqueConstraint('key', 'vocab_registry_key', name='_vocab_element_registry_uc'),)

    def __init__(self, **kwargs):
        kwargs = self._deserialise_relatives(kwargs)
        db.Model.__init__(self, **kwargs)

    # @before_flush
    # def preprocess(self):
    #     self.other_names = [(VocabElementNames(**i) if isinstance(i,dict) else i) for i in self.other_names]

    @staticmethod
    def _deserialise_relatives(data):
        data["other_names"] = [VocabElementNames(**i) for i in data.get("other_names", [])]
        return data


class VocabElementNames(APIModel, SurrogatePK, Model):
    __tablename__ = "vocab_element_names"
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(128), unique=False, nullable=False)
    vocab_element_id = ReferenceCol('vocab_element')
    vocab_element = relationship("VocabElement", enable_typechecks=False, backref=backref("other_names", cascade="all, delete-orphan"))

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)


class LabelVocabElementAssociation(Model):
    __tablename__ = 'label_vocab_element'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    label_id = ReferenceCol('label', primary_key=True)
    vocab_element_id = ReferenceCol('vocab_element', primary_key=True)
    created_at = Column(db.DateTime, default=dt.datetime.utcnow)
    label = relationship("Label", backref=backref("label_vocab_element", cascade="all, delete-orphan", lazy="dynamic"))
    vocab_element = relationship("VocabElement", backref=backref("label_vocab_element", cascade="all, delete-orphan", lazy="dynamic"))
    user_id = ReferenceCol('users', nullable=False, default=get_current_user, doc="The `id` of the user who associated the `label` to the `vocab_element`")
    user = relationship('User', doc="The user who associated the `label` to the `vocab_element`")

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)

    # @before_flush
    # def preprocess(self):
    #     if not self.user_id:
    #         self.user_id = current_user.id



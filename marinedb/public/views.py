# -*- coding: utf-8 -*-
'''Public section, including homepage and signup.'''
# import calendar
import os
import re
import subprocess
# import traceback
# import zipfile
# from datetime import datetime, timedelta
import glob
import socket
# from io import BytesIO
from shutil import make_archive, rmtree
# from statistics import mean, median

# import math
# from dateutil.parser import parse
# from dateutil.relativedelta import relativedelta
from flask import (Blueprint, request, render_template, flash, url_for,
                   redirect, session, jsonify, current_app, send_file, Response)
from flask_login import login_user, login_required, logout_user, current_user
import requests
# from requests import ConnectionError

from marinedb.extensions import login_manager
from marinedb.geodata.models import Campaign, Deployment, Platform, Media, AnnotationLabel, AnnotationSet, \
    AnnotationPoint
from marinedb.user.models import User, Group, GroupType
# from marinedb.utils import flash_errors
# from marinedb.database import db
import json
# from marinedb.extensions import apimanager
# import urllib.request, urllib.error, urllib.parse
# import sqlalchemy as sa

# Markdown plugins
import markdown
import markdown.extensions.fenced_code
import markdown.extensions.codehilite
# from pygments.formatters import HtmlFormatter

from modules.flask_restless import ProcessingException
from modules.flask_restless.views import render_data_response

# import folium


blueprint = Blueprint('public', __name__, static_folder="../static", template_folder="templates")


@login_manager.user_loader
def load_user(id):
    return User.get_by_id(int(id))


@blueprint.route("/")
def home():
    return render_template("home.html")

# TODO: delete snippet_with_nav route and snippetview-with_nav.html template
@blueprint.route("/content/<path:snippet>")
def snippet_with_nav(snippet):
    return render_template("snippetview-with_nav.html", snippet=snippet, query_string=request.query_string)

@blueprint.route("/about/")
def about():
    # return render_template("snippetview-with_nav.html", snippet="snippet-about.html")
    return render_template("about.html")

@blueprint.route("/iframe/<path:snippet>")
def snippet_in_iframe(snippet):
    api_token = request.args.get("api_token", False)
    if api_token and not current_user.is_authenticated():
        user = User.query.filter_by(api_token=api_token)
        if user.count():
            login_user(user.one())
    return render_template("snippetview-in_iframe.html", snippet=snippet, query_string=request.query_string)


@blueprint.route("/ui/<path:f>")
def template(f):
    args = request.args.to_dict()
    return render_template(f, args=args, data={})

@blueprint.route("/maintenance")
def maintenance_mode():
    return render_template("maintenance_mode.html")

@blueprint.route("/api/help/<path:collection>")
@blueprint.route("/api/help", defaults={"collection": "*"})
def api_help(collection="*"):
    doc_dir = current_app.config.get('API_DOC_DIR')
    doc_files = glob.glob("{}/{}.json".format(doc_dir, collection))
    doc_data = []
    for f in sorted(doc_files):
        with open(f) as json_file:
            doc_data.append(json.load(json_file))

    return render_data_response(data=dict(num_results=len(doc_data), objects=doc_data))


@blueprint.route("/dbtemplate.zip")
def zip_dbtemplate():
    assert current_user.is_authenticated() and current_user.is_admin, "Only admin can do this... "
    dir_path = current_app.config.get('DB_BKP_TEMPLATE_PATH')
    zip_path = f"{dir_path}.zip"
    if request.args.get('zip') == 'true' or not os.path.isfile(zip_path):
        # dbuser = current_app.config.get("DB_BKP_USER")
        # f = os.path.basename(dir_path)
        # b = os.path.dirname(dir_path)
        # cmd = 'bash {root}/scripts/database/backup.sh -f {f} -b {b} -u {usr} -t'.format(
        #     root=current_app.config.get('PROJECT_ROOT'), f=f, b=b, usr=dbuser)
        # # return cmd
        # if os.path.isdir(dir_path):
        #     rmtree(dir_path)
        # subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE).wait()
        make_archive(dir_path, 'zip', dir_path)
    return send_file(zip_path, attachment_filename="dbtemplate.zip", as_attachment=True)


# @blueprint.route("/lastbackup.zip")
# def zip_lastbackup():
#     if not (current_user.is_authenticated() and current_user.is_admin):
#         raise ProcessingException("Only admin can do this... ", code=401)
#     dir_path = current_app.config.get('DB_BKP_PATH')
#     if request.args.get('zip') != 'false':
#         out = os.popen("ls -dt {}/20* | head -n 1".format(dir_path)).read()
#         make_archive(os.path.join(dir_path, 'latest'), 'zip', str(out).strip())
#     return send_file(os.path.join(dir_path, 'latest.zip'), attachment_filename="dblatest.zip", as_attachment=True)


@blueprint.route("/wiki_page/<path:document>", defaults={"page_only": True})
@blueprint.route("/wiki/<path:document>")
@blueprint.route("/wiki")
def wiki_doc(document=None, page_only=False):
    doc_dir = current_app.config.get('WIKI_DOC_DIR')

    if not os.path.isdir(doc_dir):
        raise ProcessingException("Wiki directory does not appear to be set up on this instance. Contact administrator", code=404)

    if document is not None:
        f = os.path.join(doc_dir, document)
        fbase, fext = os.path.splitext(f)

        if fext != ".md" and fext != "" and os.path.isfile(f):
            return send_file(f)

        if page_only is True:
            # Check if we have a markdown file
            md_f = "{}.md".format(fbase)
            if os.path.isfile(md_f):
                with open(md_f, encoding="utf-8") as data:
                    md = data.read()
                    info = wiki_get_doc_info(md_f, include_git=True)
                    # Convert all relative links to have correct prefix (ignore links starting with "http", "/" or "#")
                    # This is necessary when wiki links are embedded in other pages with differing paths
                    md = re.sub(r'\[(.*?)\]\(((?!http)(?!\/)(?!#).*?)\)', r'[\1]('+info.get('location')+r'/\2)', md)
                    content = markdown.markdown(md, extensions=["fenced_code", "codehilite", "tables", "toc"])
                return render_template("partials/wiki_content.html", content=content, **info)
            else:
                raise ProcessingException("Wiki document {} does not exist".format(f), code=404)
                # return render_template("markdown.html", data=data.read())

    # If nothing returned a result above, show wiki root / toc
    toc = get_wiki_toc()
    return render_template("wiki.html", toc=toc, document=document)


def get_wiki_toc(force_refresh=False):
    doc_dir = current_app.config.get('WIKI_DOC_DIR')
    if not os.path.isdir(doc_dir):
        raise ProcessingException("Wiki directory does not appear to be set up on this instance. Contact administrator", code=404)
    toc_file = os.path.join(doc_dir,"toc.json")
    if not os.path.isfile(toc_file) or force_refresh:
        toc = dict()
        for f in sorted(glob.glob("{}/*/*.md".format(doc_dir))):
            sec_info = wiki_get_doc_info(f)
            if sec_info['section'] not in toc: toc[sec_info['section']] = []
            toc[sec_info['section']].append(sec_info)
        toc = [dict(name=k, documents=v) for k, v in toc.items()]
        with open(toc_file, "w") as outfile:
            print("Writing new TOC file for wiki...")
            json.dump(toc, outfile, indent=2)
        return toc
    else:
        with open(toc_file, 'r') as openfile:
            return json.load(openfile)


def wiki_get_doc_info(filepath, include_git=False, wiki_root="/wiki"):
    parts = filepath.split(os.path.sep)
    sec = parts[-2]
    doc = os.path.splitext(parts[-1])[0]
    location = wiki_root+'/' + sec if sec != os.path.basename(wiki_root) else wiki_root

    file_info = dict(name=doc.replace("_", " ").capitalize(), section=sec.replace("_", " ").capitalize(),
                     document=os.path.join(parts[-2], doc), filepath=filepath, location=location)
    if include_git:
        try:
            cmd = ['git', '-C', current_app.config.get('WIKI_DOC_DIR'), 'log', '-1',
                   '--pretty={"updated_by":"%cn","updated_at":"%cI","created_by":"%an","created_at":"%aI"}',
                   filepath]
            git_info = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode('utf-8')
            file_info.update(json.loads(git_info or "{}"))
        except: pass

    return file_info


@blueprint.route("/api/stats")
def system_stats():
    accepted_includes = ["userdata", "datasets"]
    include = request.args.getlist("include") or accepted_includes

    # TODO: add filtering fields (from and to dates)

    result = {}
    if "userdata" in include:
        result["userdata"] = {
            "groups": Group.query.quick_count(),
            "users": User.query.quick_count(),
            "annotations": AnnotationLabel.query.filter(AnnotationLabel.label_id.isnot(None)).quick_count(),
            "annotation_sets": AnnotationSet.query.quick_count()
        }
    if "datasets" in include:
        result["datasets"] = {
            "campaigns": Campaign.query.quick_count(),
            "deployments": Deployment.query.quick_count(),
            "platforms": Platform.query.quick_count(),
            "media": Media.query.quick_count()
        }

    if not bool(result):
        raise ProcessingException("Unknown stat(s) '{}'. Must be one of {}".format(include, accepted_includes))

    return render_data_response(data=result)



# @blueprint.route("/api/stats/tally_annotations/<tally_by>")
# def tally_annotations(tally_by):
#     today = datetime.today()
#     last_month = today.replace(day=1) - timedelta(days=1)
#     _, eom = calendar.monthrange(last_month.year, last_month.month)
#     after = parse(request.args.get("after")) if request.args.get("after") else today.replace(day=1) - relativedelta(months=12)
#     before = parse(request.args.get("before")) if request.args.get("before") else datetime(last_month.year, last_month.month, eom)
#
#     if tally_by == "month":
#         q = db.session.query(
#             db.func.count(AnnotationLabel.id).label('count'), db.extract('year', AnnotationLabel.created_at).label('year'),
#             db.extract('month', AnnotationLabel.created_at).label('month')
#         ).filter(
#             AnnotationLabel.label_id.isnot(None), AnnotationLabel.created_at >= after, AnnotationLabel.created_at <= before
#         ).group_by(
#             db.extract('year', AnnotationLabel.created_at), db.extract('month', AnnotationLabel.created_at)
#         ).order_by(db.asc("year")).order_by(db.asc("month"))
#     elif tally_by == "user":
#         q = db.session.query(
#             db.func.count(AnnotationLabel.id).label('count'), User.first_name, User.last_name, User.id.label("user_id")
#         ).filter(
#             AnnotationLabel.label_id.isnot(None), AnnotationLabel.created_at >= after,
#             AnnotationLabel.created_at <= before, AnnotationLabel.user_id == User.id
#         ).group_by(User.first_name, User.last_name, User.id).order_by(db.desc("count"))
#     elif tally_by == "affiliation":
#         q = db.session.query(
#             db.func.count(AnnotationLabel.id).label('count'), Group.name.label("group_name"),
#             Group.groupinfo.label("group_info"), Group.id.label("group_id")
#         ).filter(
#             Group.group_type.has(GroupType.name == "affiliation"), AnnotationLabel.label_id.isnot(None),
#             AnnotationLabel.created_at >= after, AnnotationLabel.created_at <= before, Group.members.any(User.id==AnnotationLabel.user_id)
#         ).group_by(Group.name, Group.groupinfo, Group.id).order_by(db.desc("count"))
#     elif tally_by == "platform":
#         q = db.session.query(
#             db.func.count(AnnotationLabel.id).label('count'), Platform.name.label("platform_name"),
#             Platform.key.label("platform_key"), Platform.id.label("platform_id")
#         ).filter(
#             AnnotationLabel.label_id.isnot(None), AnnotationLabel.created_at >= after, AnnotationLabel.created_at <= before,
#             AnnotationLabel.point_id==AnnotationPoint.id, AnnotationPoint.media_id==Media.id, Media.deployment_id==Deployment.id,  Deployment.platform_id==Platform.id
#         ).group_by(Platform.name, Platform.id, Platform.key).order_by(db.desc("count"))
#     elif tally_by == "deployment":
#         q = db.session.query(
#             db.func.count(AnnotationLabel.id).label('count'), Deployment.name.label("deployment_name"),
#             Deployment.key.label("deployment_key"), Deployment.id.label("deployment_id"), Deployment.platform_id.label("platform_id")
#         ).filter(
#             AnnotationLabel.label_id.isnot(None), AnnotationLabel.created_at >= after, AnnotationLabel.created_at <= before,
#             AnnotationLabel.point_id==AnnotationPoint.id, AnnotationPoint.media_id==Media.id, Media.deployment_id==Deployment.id
#         ).group_by(Deployment.name, Deployment.key, Deployment.id, Deployment.platform_id).order_by(db.desc("count"))
#     elif tally_by == "campaign":
#         q = db.session.query(
#             db.func.count(AnnotationLabel.id).label('count'), Campaign.name.label("campaign_name"),
#             Campaign.key.label("campaign_key"), Campaign.id.label("campaign_id")
#         ).filter(
#             AnnotationLabel.label_id.isnot(None), AnnotationLabel.created_at >= after, AnnotationLabel.created_at <= before,
#             AnnotationLabel.point_id==AnnotationPoint.id, AnnotationPoint.media_id==Media.id, Media.deployment_id==Deployment.id, Deployment.campaign_id==Campaign.id
#         ).group_by(Campaign.name, Campaign.key, Campaign.id).order_by(db.desc("count"))
#     else:
#         raise ProcessingException("Unknown bin method: '{}'. Must be one of: 'month', 'user', 'affiliation'".format(tally_by))
#
#     # paginate results
#     num_results = q.count()
#     results_per_page = min(5000, int(request.args.get('results_per_page', 500)))
#     total_pages = int(math.ceil(float(num_results) / float(results_per_page)))
#     page_num = min(total_pages, int(request.args.get('page', 1)))  # get the page number (first page is page 1)
#     start = (page_num - 1) * results_per_page
#     end = min(num_results, start + results_per_page)
#     q = q[start:end]
#
#     # convert to dicts
#     objects = [u._asdict() for u in q]
#     counts = [i.get('count', 0) for i in objects]
#     stats = dict(mean=mean(counts), median=median(counts), min=min(counts), max=max(counts), sum=sum(counts))
#     result = dict(
#         page=page_num, objects=objects, total_pages=total_pages, num_results=num_results, metadata=dict(
#             page_stats=stats, num_bins=len(objects), bin_by=tally_by, before=before.isoformat(),
#             after=after.isoformat(), today=today.isoformat()))
#     return render_data_response(data=result)


@blueprint.route("/userissue", methods=["GET", "POST"])
@login_required
def userissue():
    TOKEN ='42415df1015628969ec15ca66b231e59'
    PROJECT_ID = 1425822
    api_url = 'https://www.pivotaltracker.com/services/v5/projects/{}/stories'.format(PROJECT_ID)
    headers = {'content-type': 'application/json', 'X-TrackerToken': TOKEN}
    user_label = "user_feedback"

    if request.method == 'POST':
        story_type = request.form.get("story_type")  # "bug"  #"bug" | "feature"
        name = request.form.get("name")  # "Issue name"
        description = request.form.get("description")  # "Issue description user info\n\nUser info: Ariell Friedman (ariell@greybits.com.au)\nURL: kjsdhkjhf\nBrowser info: kjsdhf\nSession info: ksjdfh"

        user = current_user.first_name+" "+current_user.last_name+" ("+current_user.email+")"
        user_agent = request.headers.get('User-Agent')
        user_url = request.headers.get("Referer")  #request.path + request.query_string

        description_info = "{}\n\nUSER: {}\nURL: {}\nAGENT: {}".format(description, user, user_url, user_agent)
        data = {"name": name, "description": description_info, "labels": [user_label, current_user.email], "story_type": story_type}

        response = requests.post(api_url, data=json.dumps(data), headers=headers)

        return response.text
    elif request.method == "GET":
        if request.args.get("filter"):
            # filter format:
            #  - filter=label:"user_feedback"  'label:"{}"'.format(user_label)
            print((request.args.get("filter")))
            response = requests.get(api_url, headers=headers, params={"filter": request.args.get("filter")})
            return response.text
        else:
            return render_template("userissue-form.html", user_label=user_label)
        # return render_template("userissue-form.html")


@blueprint.route("/proxy/<string:protocol>/<path:url>", methods=["GET", "POST", "PATCH", "DELETE"])
def _proxy(protocol, url, *args, **kwargs):
    url = "{}://{}?{}".format(protocol, url, request.query_string.decode('utf-8'))
    # print(url)
    # return url
    # print(request.environ)
    try:
        payload = dict(
            method=request.method,
            url=url,
            data=request.get_data(),
            cookies=request.cookies,
            allow_redirects=False
        )
        resp = requests.request(**payload)

        excluded_headers = {'content-encoding', 'content-length', 'transfer-encoding', 'connection'}
        headers = [(name, value) for (name, value) in list(resp.raw.headers.items())
                   if name.lower() not in excluded_headers]
        headers.append(("Set-Cookie", "HttpOnly;Secure;SameSite=Strict"))

        response = Response(resp.content, resp.status_code, headers)
        return response
    except (Exception, socket.error, IOError) as e:
        return jsonify(message="{}: {}".format(type(e).__name__, e)), 500



# @blueprint.route("/wip/map")
# def wip_map():
#     start_coords = (46.9540700, 142.7360300)
#     folium_map = folium.Map(location=start_coords, zoom_start=14)
#     return folium_map._repr_html_()
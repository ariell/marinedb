<a id="api_pagination"></a><h3>Pagination</h3>
<pre><code class="hljs bash">/api/resource?results_per_page=...&page=...</code></pre>
<div class="well well-sm">
<p>
    Responses to most <code>GET</code> requests (with the exception of the export endpoints) are paginated by default, with at most ten objects per page. To request a specific page,
    add a <code>page=N</code> query parameter to the request URL, where <code>N</code> is a positive integer (the first page is page one). If no
    page query parameter is specified, the first page will be returned.
</p>
<p>
    In order to specify the number of results per page, add the query parameter <code>results_per_page=N</code> where <code>N</code> is a
    positive integer. If <code>results_per_page</code> is greater than the maximum number of results per page as configured by
    the server, then the query parameter will be ignored.
</p>
<p>
    In addition to the <code>"objects"</code> list, the response JSON object will have a <code>"page"</code> key with the current
    page number, a <code>"num_pages"</code> key with total number of pages into which the set of matching instances is
    divided, and a <code>"num_results"</code> key with total number of instances which match the requested search.
</p>
</div>

<a id="api_query_format"></a><h3>Query format:</h3>
<pre><code class="hljs bash">/api/resource?q={"filters":[...],"limit":...,"offset":...,"order_by":[...],"group_by":[...],"single":...}</code></pre>
<div class="well well-sm">
<p>The query parameter <code>q</code> must be a JSON string. It can have the following mappings, all of which are optional:</p>
<dl>
    <dt>
        <code>filters</code>
    </dt>
    <dd>
        <p class="first">A list of <code>&lt;filterobjects&gt;</code> of one of the following forms:</p>
        <pre><code class="hljs bash">{"name": &lt;fieldname&gt;, "op": &lt;operatorname&gt;, "val": &lt;argument&gt;}</code></pre>
        <p>Where <code>&lt;operatorname&gt;</code> is one of the strings described below, <code>&lt;argument&gt;</code> is a value to be
            used as the second argument to the given operator and <code>&lt;fieldname&gt;</code> is the name of the field
        of the model to which to apply the operator, which can be either a <code>MODEL COLUMN</code>,
            <code>RELATED MODEL</code>, <code>ASSOCIATION PROXY</code> or  <code>HYBRID ATTRIBUTE</code> for a resource.</p>
        <pre><code class="hljs bash">{"name": &lt;fieldname&gt;, "op": &lt;operatorname&gt;, "field": &lt;fieldname&gt;}</code></pre>
        <p>Where the first <code>&lt;fieldname&gt;</code> and <code>&lt;operatorname&gt;</code> are as above and the
            second <code>&lt;fieldname&gt;</code> is the field of the model that should be used as the
        second argument to the operator. </p>

        <p>The first <code>&lt;fieldname&gt;</code> may specify a field on a related model, if it is
        a string of the form <code>&lt;relationname&gt;__&lt;fieldname&gt;</code>. Alternatively if the field name is the name of a
        relation and the operator is <code>"has"</code> or <code>"any"</code>, the <code>"val"</code> argument can be a
        dictionary with the arguments representing another, nested filter to be applied as the argument for
        <code>"has"</code> or <code>"any"</code>.</p>

        <p>The <code>&lt;operatorname&gt;</code> strings recognized by the API include:</p>
        <table class="table table-striped table-sm">
            <tr>
                <td><code>==</code>, <code>eq</code>, <code>equals</code>, <code>equals_to</code></td>
                <td>"equal to" operator, where <code>val</code> can be numeric or string</td>
            </tr>
            <tr>
                <td><code>!=</code>, <code>neq</code>, <code>does_not_equal</code>, <code>not_equal_to</code></td>
                <td>"not equal to" operator, where <code>val</code> can be numeric or string</td>
            </tr>
            <tr>
                <td><code>&gt;</code>, <code>gt</code>, <code>&lt;</code>, <code>lt</code></td>
                <td>"less/greater than" operator, where <code>val</code> is typically numeric</td>
            </tr>
            <tr>
                <td><code>&gt;=</code>, <code>ge</code>, <code>gte</code>, <code>geq</code>, <code>&lt;=</code>, <code>le</code>, <code>lte</code>, <code>leq</code></td>
                <td>"less/greater than or equal to" operator, where <code>val</code> is typically numeric</td>
            </tr>
            <tr>
                <td><code>in</code>, <code>not_in</code></td>
                <td>"is in" operator, where <code>val</code> is typically a list of values</td>
            </tr>
            <tr>
                <td><code>is_null</code>, <code>is_not_null</code></td>
                <td>"is null" check, where there is no <code>val</code> param</td>
            </tr>
            <tr>
                <td><code>like</code>, <code>ilike</code>:</td>
                <td>"like" (case sensitive) or "ilike" (case insensitive) comparison, where <code>val</code> is a string.
                Wildcards can be included as a <code>%</code> symbol (or <code>%25</code> url-encoded).</td>
            </tr>
            <tr>
                <td><code>has</code></td>
                <td>for nesting operators on a relation that is a single object (i.e.: <code>MANYTOONE</code>)</td>
            </tr>
            <tr>
                <td><code>any</code></td>
                <td>for nesting operators on a relation that is a list of objects  (i.e.: <code>ONETOMANY/MANYTOMANY</code>)</td>
            </tr>
        </table>

        <p>For geometry columns, there are also some spatial operators for <code>&lt;operatorname&gt;</code>, which include:</p>
        <table class="table table-striped table-sm">
            <tr>
                <td><code>geo_at</code></td>
                <td>Geometry / Geography type <code>&lt;fieldname&gt;</code> is close to a nominated <code>lat/lon</code>.
                    The <code>val</code> takes the form: <code>{"lat":...,"lon":...,"dist":...}</code> where <code>dist</code> is optional and defaults to <code>0.00005</code>.
                    For Geometry types <code>dist</code> is in srid units (eg: degrees), for Geography types it is meters.
                    Hint: assume Geometry types most of the time.
                </td>
            </tr>
            <tr>
                <td><code>geo_in_bbox</code></td>
                <td>Geometry / Geography type <code>&lt;fieldname&gt;</code> is within a bounding box.
                    The <code>val</code> takes the form: <code>[p1,p2]</code>,
                    which is a list containing two elements being two diagonal points defining the bounding box, eg:
                    <code>[bottom-left, top-right]</code>. <code>p1,p2</code> are position objects of the form,
                    <code>{"lat":...,"lon":...}</code>
                </td>
            </tr>
            <tr>
                <td><code>geo_in_poly</code></td>
                <td>Geometry / Geography type <code>&lt;fieldname&gt;</code> is within a polygon.
                    The <code>val</code> is a polygon list of points of the form: <code>[p1,p2,p3,...,pn,p1]</code>,
                    where <code>p1-pn</code>
                    are position objects of the form <code>{"lat":..., "lon":...}</code>. Note the polygon is closed,
                    being that it starts and ends wth <code>p1</code>.
                </td>
            </tr>
        </table>


        <p>Filters can also exclude results, for example:</p>
        <pre><code class="hljs bash">{"not": &lt;filterobject&gt;}</code></pre>

        <p>Filter objects can also be arbitrary Boolean formulas, for example:</p>
        <pre><code class="hljs bash">{"or": [&lt;filterobject&gt;, {"and": [&lt;filterobject&gt;, ...]}, ...]}</code></pre>

        <p>Filter objects can also be defined on Hybrid Attributes of a model. <code>hybrid property</code> filter objects are
            defined in the same way as column fields, but to filter by a <code>hybrid method</code> with one or more
            input arguments (<code>&lt;arg1&gt;,&lt;arg2&gt;,...&lt;argN&gt;</code>), the object can be defined as:</p>
        <pre><code class="hljs bash">{"name": {"method":&lt;method_name&gt;,"args":[&lt;arg1&gt;,&lt;arg2&gt;,...&lt;argN&gt;]}, "op": &lt;operatorname&gt;, "val": &lt;argument&gt;}</code></pre>

        <p>The returned list of matching instances will include only those instances
        that satisfy all of the given filters.</p>
        <p>
            If a filter is poorly formatted (for example, op is set to <code>'=='</code> but val is not set), the server
            responds with 400 Bad Request.
        </p>
    </dd>
    
    
    
    <dt><code>limit</code></dt>
    <dd>A positive integer which specifies the maximum number of objects to return.</dd>

    <dt><code>offset</code></dt>
    <dd>A positive integer which specifies the offset into the result set of the
    returned list of instances.</dd>

    <dt><code>order_by</code></dt>
    <dd><p>A list of objects of the form:</p>
    <pre><code class="hljs bash">{"field": &lt;fieldname&gt;, "direction": &lt;directionname&gt;}</code></pre>
    <p>where <code>&lt;fieldname&gt;</code> is a string corresponding to the name of a field of the
    requested model and <code>&lt;directionname&gt;</code> is either <code>"asc"</code> for ascending
    order or <code>"desc"</code> for descending order.</p>
    <p class="last"><code>&lt;fieldname&gt;</code> may alternately specify a field on a related model, if it is
    a string of the form <code>&lt;relationname&gt;__&lt;fieldname&gt;</code>.</p>
        <p>Ordering can also be defined on Hybrid Attributes of a model. <code>hybrid property</code> order_by objects are
            defined in the same way as column fields, but to filter by a <code>hybrid method</code> with one or more
            input arguments (<code>&lt;arg1&gt;,&lt;arg2&gt;,...&lt;argN&gt;</code>), the object can be defined as:</p>
    <pre><code class="hljs bash">{"field": {"method":&lt;method_name&gt;,"args":[&lt;arg1&gt;,&lt;arg2&gt;,...&lt;argN&gt;]}, "direction": &lt;directionname&gt;}</code></pre>
    </dd>

    <dt><code>group_by</code></dt>
    <dd><p>A list of objects of the form:</p>
    <pre><code class="hljs bash">{"field": &lt;fieldname&gt;}</code></pre>
    <p>where <code>&lt;fieldname&gt;</code> is a string corresponding to the name of a field of the
    requested model.</p>
    <p><code>&lt;fieldname&gt;</code> may alternately specify a field on a related model, if it is
    a string of the form <code>&lt;relationname&gt;__&lt;fieldname&gt;</code>.</p>
    </dd>

    <dt><code>single</code></dt>
    <dd>A Boolean representing whether a single result is expected as a result of the
    search. If this is <code>true</code> and either no results or multiple results meet
    the criteria of the search, the server responds with an error message.</dd>
</dl>

</div>


<a id="api_data_transform"></a><h3>Data transformation and batch operations</h3>
<pre><code class="hljs bash">/api/resource?f={...}&save={...}</code></pre>
<div class="well well-sm">
<p>
    Some endpoints support data transformation and manipulation of the output using the data transformation API.
    This allows users to define a sequence of operations that can be used to transform the output of the response
    in order to (i) export data in a desired format or (ii) perform batch import operations.

    <div class="well well-sm"><i class="fa fa-exclamation-triangle text-warning"></i> DOCUMENTATION FOR THIS IS STILL COMING... WATCH THIS SPACE...</div>
</p>
</div>

{#<a id="query_operators"></a><h3>Operators</h3>#}
{#<div class="well well-sm">#}
{#<p>The operator strings recognized by the API incude:</p>#}
{#<ul>#}
{#    <li><code>==</code>, <code>eq</code>, <code>equals</code>, <code>equals_to</code></li>#}
{#    <li><code>!=</code>, <code>neq</code>, <code>does_not_equal</code>, <code>not_equal_to</code></li>#}
{#    <li><code>&gt;</code>, <code>gt</code>, <code>&lt;</code>, <code>lt</code></li>#}
{#    <li><code>&gt;=</code>, <code>ge</code>, <code>gte</code>, <code>geq</code>, <code>&lt;=</code>, <code>le</code>, <code>lte</code>, <code>leq</code></li>#}
{#    <li><code>in</code>, <code>not_in</code></li>#}
{#    <li><code>is_null</code>, <code>is_not_null</code></li>#}
{#    <li><code>like</code></li>#}
{#    <li><code>has</code></li>#}
{#    <li><code>any</code></li>#}
{#</ul>#}
{#<p>These correspond to SQLAlchemy column operators as defined <a target="_blank" href="http://docs.sqlalchemy.org/en/latest/core/expression_api.html#sqlalchemy.sql.operators.ColumnOperators">here</a>.</p>#}

{#<h4>Using has and any</h4>#}
{#<p>#}
{#    Use the <code>has</code> and <code>any</code> operators to search for instances by fields on related instances.#}
{#    In general, use the <code>any</code> operator if the relation is a list of objects and use the <code>has</code>#}
{#    operator if the relation is a single object. For example, you can search for all User instances that have a related#}
{#    Annotations with a certain ID number by using the <code>any</code> operator. For another example, you can search for#}
{#    all Annotation instances that have an owner with a certain name by using the <code>has</code> operator.#}
{##}
{#    For more information, see the SQLAlchemy documentation.#}
{#</p>#}
{#</div>#}

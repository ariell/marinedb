# -*- coding: utf-8 -*-
"""Python 2/3 compatibility module."""


import sys

PY2 = int(sys.version[0]) > 3

if PY2:
    from StringIO import StringIO
    text_type = str
    binary_type = str
    string_types = (str, str)
    unicode = unicode
    basestring = basestring
else:
    from io import StringIO
    text_type = str
    binary_type = bytes
    string_types = (str,)
    unicode = str
    basestring = (str, bytes)
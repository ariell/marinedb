/**
 *
 * Dependencies:
 *      jQuery
 *      Handlebars (for template)
 *      Bootstrap (weak dependence for layout)
 *
 */
function LabelScheme($container, options) {

    var _this = this;

    // options
    _this.options = $.extend({
        taginfo_template: 'models/label/detail.html',
        //label_scheme_template: 'model_templates/label_scheme/get_tree.html',
        on_label_click: function(tgdata) {_this.label_info_modal(tgdata.id)},
        annotation_set_id: null,
        show_label_actions: true
    }, options);

    // defaults
    _this.$container = $container;
    _this.label_scheme_id = null;
    _this.parent_label_scheme_ids = [];
    _this.$list_container = $("<div id='label-list'></div>");
    _this.$quick_label_list_container = $("<div id='label-list'></div>");
    _this.$quick_label_container = $("<div class='quick-label-list'>");
    // _this.options.taginfo_template = 'models/label/detail.html';
    // _this.options.label_scheme_template = 'model_templates/label_scheme/get_tree.html';
    // _this.quick_label_template = 'models/annotation/quick_label.html'
    _this.$search = $('<input type="text" class="form-control search" value="" placeholder="Search / TAB for quick lists"  title="Use keyboard to start typing and then select using pointer or UP/DOWN keyboard arrows. Hit ALT key for quick lists">').prop("disabled", true);
    _this.$search_opt_vocab_elements = $('<li><label> <input type="checkbox" name="search_vocab_element"> Mapped names (vocab_elements)</label></li>');
    _this.$search_opt_other_names = $('<li><label> <input type="checkbox" name="search_other_names"> Common names (vocab_elements)</label></li>');
    _this.$search_opt_mappings = $('<li><label> <input type="checkbox" name="search_show_mappings"> Synonyms (linked labels)</label></li>');
    _this.$search_opt_hide_unmapped = $('<li><label> <input type="checkbox" name="search_hide_unmapped"> Hide unmapped labels</label></li>');
    _this.xjr = null;
    _this.tooltip_xhr = null;
    //_this.$settings_container = $("<div class='annotation_scheme-setting'>Settings...</div>").hide();

    _this.url = {
        // label_scheme_tree_single: "/api/label_scheme/{0}/nested?search={1}",
        // label_single: "/api/label/{0}",
        // label_scheme_tree_lazy: '/api/label?q={"filters":[{"name":"label_scheme_id","op":"eq","val":{0}},{"name":"parent_id","op":"eq","val":{1}}]}&results_per_page=1000',
        // quick_labels: '/api/annotation_set/{0}/labels/{1}?results_per_page={2}'
        label_scheme_tree_single: '/api/label_scheme/{0}/labels?q={1}',
        label_single: "/api/label/{0}",
        label_scheme_tree_lazy: '/api/label_scheme/{0}/labels/{1}?q={"order_by":[{"field":"name","direction":"asc"}]}',
        quick_labels: '/api/annotation_set/{0}/annotations/{1}?results_per_page={2}'
    };

    _this.init_list_level = -1;
    //_this.$popover_container = "body";
    _this.is_lazyloaded = false;
    // _this.annotation_set_id = null;
    _this.current_user_can_edit = false;

    var $search_container = _this.$list_container;  // initialise search container
    var $search_list_items = [];

    // default classes for bulleted list of tags
    var b_class_expanded = "fa-minus-square";
    var b_class_collapsed = "fa-plus-square";
    var b_class_nochildren = "fa-square";


    this.search_opt = {
        other_names: function(){return _this.$search_opt_other_names.find("input[type='checkbox']").is(":checked")},
        vocab_elements: function(){return _this.$search_opt_vocab_elements.find("input[type='checkbox']").is(":checked")},
        mappings: function(){return _this.$search_opt_mappings.find("input[type='checkbox']").is(":checked")},
        hide_unmapped: function(){return _this.$search_opt_hide_unmapped.find("input[type='checkbox']").is(":checked")}
    };


    /**
     *
     * @param $container
     * @param taginfo_template
     * @returns {LabelScheme}
     * @param label_scheme_template
     * @param on_label_click
     */
    this.initList = function (options) {
        // Set defualts
        $.extend(_this.options, options);

        // Construct list elements
        var $search_container = $("<div class='search-controls'>");
        // var $settings = $('<a href="javascript:void(0)" class="input-group-addon"><i class="fa fa-info-circle"></i></a>')
        //     .click(function(){_this.$settings_container.toggle()});

        var $quick_label_btn = $('<button type="button" class="btn btn-default" title="Open quick lists"><i class="fa fa-sort-amount-desc"></i></button>');
        var $search_opt_btn = $('<span class="input-group-btn"></span>').append(
            $('<button type="button" class="btn btn-default" title="Click for search options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></button>').tooltip({container: $search_container, placement:"right"}),
            $('<ul class="dropdown-menu">').append(
                $("<li><small><i class='fa fa-search'></i> SEARCH / MATCH ON:</small></li>"),
                _this.$search_opt_vocab_elements, _this.$search_opt_other_names, _this.$search_opt_mappings,  //_this.$search_opt_show_mappings_count,
                $("<li><small><i class='fa fa-filter'></i> FILTERS:</small></li>"),
                _this.$search_opt_hide_unmapped
            )
        );
        //var $search_addon = $('<span class="input-group-addon"><i class="fa fa-search"></i></span>');
        var $search_group = $("<div class='input-group input-group-sm'>");
        $search_group.append($search_opt_btn, _this.$search, $('<span class="input-group-btn">').append($quick_label_btn));

        // $search_container.append($('<span class="input-group-addon"><i class="fa fa-search"></i></span>'),_this.$search,$settings);
        //_this.$container.append(_this.$settings_container, $search_container, _this.$list_container);
        $search_container.append($search_group);
        _this.$container.append($search_container, _this.$list_container, _this.$quick_label_container);

        // Enable search
        _this.$search.on('keyup keydown input', handle_search_keys);

        $search_opt_btn.find("input[type='checkbox']").on("change", function(){
            _this.refreshList();
        })

        // quick label properties
        var $quick_label_refresh = $("<button class='btn btn-link btn-xs' title='Reload list'><i class='fa fa-refresh'></i></button>").click(function(){
            quick_label_list(true);
        });
        $quick_label_btn.click(function(){quick_label_list()});
        // var $quick_label_ops = $('<div class="btn-group" data-toggle="buttons">').append(
        //     $('<label class="btn btn-default btn-xs active"><input type="radio" name="list_type" value="recent" checked><i class="fa fa-clock-o"></i> RECENT</label>'),
        //     $('<label class="btn btn-default btn-xs"><input type="radio" name="list_type" value="frequent"><i class="fa fa-sort-amount-desc"></i> FREQUENT</label>'),
        //     $("<select name='results_per_page' title='Number to show'><option value='5'>5</option>" +
        //         "<option value='10'>10</option><option value='20' selected>20</option><option value='50'>50</option></select>")
        // );
        var $quick_label_list_type = $('<select class="pull-right" name="list_type">').append(
            $('<option value="label_updated" selected>RECENT</option>'),
            $('<option value="label_frequency">FREQUENT</option>')
        );
        var $quick_label_nresults = $("<select class='pull-right' name='results_per_page' title='Number to show'>").append(
            $("<option value='5'>5</option>"), $("<option value='10'>10</option>"),
            $("<option value='20' selected>20</option>"), $("<option value='50'>50</option>"));
        _this.$quick_label_container.hide().append(
            $quick_label_list_type,
            $quick_label_nresults,
            $quick_label_refresh,
            $("<div class='ul-container'>").append(_this.$quick_label_list_container)
        );
        $quick_label_list_type.add($quick_label_nresults).change(function () {
            quick_label_list(true);
        });


        return _this;
    };


    function quick_label_list(show) {
        if (_this.options.annotation_set_id !== null) {
            if (typeof show === "undefined") show = null;
            if ((_this.$quick_label_container.is(":visible") && show === null) || show === false) {
                _this.$quick_label_container.hide();
                build_search_list(_this.$list_container);
            }
            else if ((!_this.$quick_label_container.is(":visible") && show == null) || show === true) {
                _this.$quick_label_container.show();
                var list_type = _this.$quick_label_container.find("[name='list_type']").val();
                var results_per_page = _this.$quick_label_container.find("[name='results_per_page']").val();
                $.getJSON(_this.url.quick_labels.format(_this.options.annotation_set_id, list_type, results_per_page), function (data) {
                    _this.addList(data.objects, _this.$quick_label_list_container, 0);
                    build_search_list(_this.$quick_label_list_container);
                });
                _this.$search.select().focus();
            }
        }
    }

    function build_search_list($active_container) {
        if (typeof $active_container !== "undefined") $search_container = $active_container;
        $search_list_items = $search_container.find("li.label-li");
        //console.debug("build_search_list: "+$search_list_items.length);
    }

    function handle_search_keys(e) {
        console.debug("*** KEY: ",e.type,e.which);
        var keyCode = e.which || e.keyCode;
        var $tag_link_selected;
        if (e.type==="keyup") {
            if (keyCode === 40 || keyCode === 38) {   // Select when Down or Up key pressed
                e.stopImmediatePropagation();
                //console.debug("handle_search_keys: "+$search_list_items.length);
                var $taglist_visible = $search_list_items.filter(":visible");
                $tag_link_selected = $taglist_visible.filter('li.label-li.selected');
                // inc_by and start index for down arrow
                var start_idx = (keyCode === 38) ? $taglist_visible.length - 1 : 0;
                var inc_by = (keyCode === 38) ? -1 : +1;

                if ($tag_link_selected.length > 0) {
                    var idx = $taglist_visible.index($tag_link_selected);
                    $tag_link_selected.removeClass("selected");
                    if ($taglist_visible.length > idx && keyCode === 40 || idx > 0 && keyCode === 38)
                        $($taglist_visible[idx + inc_by]).addClass("selected");
                    // else
                    //     $($taglist_visible[start_idx]).addClass("selected");
                }
                else {
                    $($taglist_visible[start_idx]).addClass("selected");
                }
                e.preventDefault();
            }
            else if (keyCode === 27 && _this.$quick_label_container.is(":visible")) {
                // hide quick label list if visible
                quick_label_list(false);
                e.preventDefault();
                e.stopImmediatePropagation();
            }
        }
        else if (e.type === "keydown") {
            if (keyCode === 9) {  // TAB key
                // show quick label list
                quick_label_list();
                e.preventDefault();
                e.stopImmediatePropagation();
            }
            else if (keyCode === 13) {    // Select on enter
                $tag_link_selected = $search_list_items.filter('li.label-li.selected:visible');
                $tag_link_selected.find('> a.label-link').trigger("click"); // click selected label
                $tag_link_selected.removeClass("selected");
                e.preventDefault();
                e.stopImmediatePropagation();
            }
            else if (keyCode === 32) {   // on space, check if selected and expandable
                var $expandable_node = $search_list_items.filter('li.label-li.selected:visible').find("> i");
                if ($expandable_node.length > 0) {
                    $expandable_node.trigger("click");
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }
            }
        }
        if (!(e.ctrlKey || e.metaKey || e.shiftKey)) { // if not a metakey (eg: ctrl or cmd) to avoid refresh with special ops
            // for all alphanumeric keystrokes that change input or delete (46) or backspace (8), do search
            if (e.type==="input" || (e.type==='keyup' && (keyCode===46 || keyCode===8))) {
                var val = _this.$search.val().toLowerCase().trim();
                if (_this.is_lazyloaded && !_this.$quick_label_container.is(":visible")) {  // if quick tag container is not visible
                    _this.refreshList(_this.label_scheme_id, null, val);
                }
                else {
                    $search_list_items.hide();
                    val = val.split(/[ ,]+/);  // split keywords by white-space
                    $search_list_items.each(function (i, li) {
                        var text = $(li).removeClass("selected").data("label_search").toLowerCase();
                        var show = true;
                        for (var j in val)
                            show = show && (text.indexOf(val[j]) >= 0);  // check all keywords are in text
                        if (show)
                            $(li).show().parents("li.label-li").show();
                    });
                }
            }
        }
    }

    /**
     *
     * @param label_scheme_id
     * @param callback
     * @param search_term
     * @param clear_search
     */
    this.refreshList = function (label_scheme_id, callback, search_term, clear_search) {
        if (clear_search === true) _this.$search.val("");
        if (typeof label_scheme_id !== 'undefined') _this.label_scheme_id = label_scheme_id;
        var search_query = {order_by:[{field:"name",direction:"asc"}], filters:[]};
        if (_this.search_opt.hide_unmapped())
           search_query.filters.push({name:"is_mapped",op:"eq",val:true});
        if (typeof  search_term === 'undefined') search_term = _this.$search.val().toLowerCase().trim();
        if (search_term)
            $.each(search_term.trim().split(/[ ,]+/), function(i, s){
                s = encodeURIComponent("%"+s+"%");
                // search_query.filters.push({name:"name",op:"ilike",val:s});
                var filts_or = [{name:"name",op:"ilike",val:s}]
                if (_this.search_opt.other_names())
                    filts_or.push({name:"vocab_elements", op:"any", val:{name:"other_names", op:"any", val:{name:"name",op:"ilike",val:s}}})
                if (_this.search_opt.vocab_elements())
                    filts_or.push({name:"vocab_elements", op:"any", val:{name:"name", op:"ilike", val:s}})
                if (_this.search_opt.mappings())
                    filts_or.push({name:"vocab_elements", op:"any", val:{name:"labels", op:"any", val:{name:"name",op:"ilike",val:s}}})
                search_query.filters.push({or: filts_or});
                /*
                search_query.filters.push({or:[
                    {name:"name",op:"ilike",val:s},
                    {name:"vocab_elements", op:"any", val:{name:"name", op:"ilike", val:s}},
                    {name:"vocab_elements", op:"any", val:{name:"name", op:"any", val:{name:"other_names", op:"has", val:{name:"name",op:"ilike",val:s}}}}
                ]});

                 */
            });
        /*
        var search_query = (search_term)
            //? JSON.stringify({filters:[{name:"name",op:"ilike",val:encodeURIComponent("%"+search_term.trim().replace(" ","%")+"%")}]})
            ? JSON.stringify({filters:search_term.trim().split(/[ ,]+/).map(function(s){
                return {name:"name",op:"ilike",val:encodeURIComponent("%"+s+"%")}
            })})
            : "";
        */
        _this.$list_container.html("<i style='margin: 20px' class='fa fa-circle-o-notch fa-spin fa-2x fa-fw'></i>");
        if (_this.xjr) _this.xjr.abort();             // abort previous request if still running
        _this.xjr = ajax.get(_this.url.label_scheme_tree_single.format(_this.label_scheme_id, JSON.stringify(search_query)), {
            success: function (data) {
                // console.log(data);
                _this.parent_label_scheme_ids = data.parent_label_scheme_ids;
                _this.is_lazyloaded = data.is_lazyloaded;   // set lazyload propery
                _this.current_user_can_edit = data.current_user_can_edit;   // whether or not current user can edit the scheme
                //_this.$settings_container.html(data);
                _this.$list_container.html("");

                if(data.labels.length>0) {
                    _this.addList(data.labels, _this.$list_container, 0);
                }
                else {
                    var empty_msg = (_this.is_lazyloaded)
                        ? "This scheme contains "+data.label_count+" entries. Your search did not return any results. Please refine your search terms."
                        : "This scheme does not appear to have any labels imported...";
                    _this.$list_container.html($("<div class='alert'><i class='fa fa-exclamation-triangle'></i> </div>").append(empty_msg));
                }

                // Refresh search
                build_search_list(_this.$list_container);
                //_this.$search.val(""); // clear val on refresh

                _this.$search.prop("disabled", false);

                if (typeof callback === "function") callback(data);
            },
            fail_notify: function(msg, ajax_params) {
                var $alert = $("<div class='alert'><i class='fa fa-exclamation-triangle'></i> </div>").append(
                    msg + "<div style='font-size: xx-small'><b>" + ajax_params.type + "</b>: " + ajax_params.url + "</div>"
                );
                _this.$list_container.html($alert);
            }
        });
    };

    /**
     *
     * @param list
     * @param $container
     * @param level
     */
    this.addList = function (list, $container, level) {
        var ul_display=(level <= _this.init_list_level || _this.init_list_level<0) ? "block" : "none";
        var init_b_class_children =(level < _this.init_list_level || _this.init_list_level<0 ) ? b_class_expanded : b_class_collapsed;

        var $ul = ($container.find(">ul.label-ul").length > 0)
            ? $container.find(">ul.label-ul").html("")                                          // clear if exists
            : $("<ul class='label-ul' style='display:"+ul_display+"'>").appendTo($container);   // create if doesn't

        $(list).each(function (i, el) {
            var label_search = el.name;
            if (!_this.is_lazyloaded) {
                if (el.hasOwnProperty('tags')) label_search += " " + Object.keys(el.tags).join(" ");
                if ($container.data().hasOwnProperty("label_search")) label_search += " " + $container.data("label_search");
            }
            var $listctl = $("<i class='fa "+b_class_nochildren+" link' style='color: " + el.color + "'></i>");
            var $labellink = $("<a href='javascript:void(0)' class='label-link'>" + el.name + "</a>");
            var $is_inherited = (el.label_scheme_id !== _this.label_scheme_id)
                ? $(" <i class='fa fa-clone fa-fw label-icon' title='Cloned label<br>from base scheme'>")
                : $("<span>");
            var $count = $("<span>");
            var $infolink = $("<span>");

            var $is_mapped;
            if (el.hasOwnProperty('is_mapped'))
                $is_mapped = (!el.is_mapped)
                    ? $("<a href='#' class='label-icon' title='NO VOCAB_ELEMENT MAPPINGS! Click to manage.'><i class='fa fa-exclamation-triangle fa-fw text-danger'></i></a>")
                    : $("<a href='#' class='label-icon' title='Has vocab_element mappings. Click to manage.'><i class='fa fa-check fa-fw'></i></a>");
            else
                $is_mapped = $("<span>");

            if (_this.options.show_label_actions) {
                $infolink = $("<a href='#' class='label-icon' title='Click for more<br>info / properties'><i class='fa fa-info fa-fw'></i></a>");
                if (el.hasOwnProperty('count'))
                    $count = $("<b class='pull-right'>[ # " + el.count + " ]&nbsp;</b>");

                // Callback
                $infolink.on("click", function () {
                    _this.label_info_modal(el.id);
                });


                $is_mapped.click(function (e) {
                    dataset_modal.label.manage_mappings(el.id)
                });
            }

            var $tooltip = $("<span><i class='fa fa-spinner fa-spin'></i></span>");
            $labellink.on("click", function () {
                _this.options.on_label_click(el);
            }).tooltip(
                {html:true, delay:{show:500, hide:0}, trigger:'focus hover', placement:'bottom', title:$tooltip}
            ).on('show.bs.tooltip', function(){
                if (_this.tooltip_xhr) _this.tooltip_xhr.abort();
                _this.tooltip_xhr = $.getJSON('/api/label/'+el.id, function (data){
                    var title = data.label_scheme.name+" | "+data.name_path +
                        ((data.annotation_count) ? " (global uses: "+data.annotation_count+")" : "");
                    $tooltip.html(title);
                });
            });

            var $li = $("<li class='label-li' data-label_search='" + label_search + "'></li>").append($count, $listctl, $labellink, $is_inherited, $is_mapped, $infolink);
            $li.find('.label-icon').tooltip({delay: {show: 200, hide: 0}, container: _this.$list_container, placement: "auto", html: true})
            $ul.append($li);


            // expand / load children classes
            if (_this.is_lazyloaded) {
                // if lazy loading, attempt to expand and load classes on request
                $listctl.removeClass(b_class_nochildren).addClass(b_class_collapsed).on("click", function () {
                    if (!$li.data("loaded")) {
                        $.getJSON(_this.url.label_scheme_tree_lazy.format(_this.label_scheme_id, el.id), function(data){
                            console.debug(data);
                            if (data.labels.length > 0) {
                                _this.addList(data.labels, $li, level + 1);
                                $listctl.removeClass(b_class_collapsed).removeClass(b_class_nochildren).addClass(b_class_expanded);
                                // Refresh search
                                build_search_list();
                            }
                            else {
                                $listctl.removeClass(b_class_collapsed).addClass(b_class_nochildren)
                            }
                            $li.data("loaded", true);
                        }).fail(function(ajxqr, msg, err){
                            console.log(ajxqr, msg, err);
                        });
                    }
                })
            }
            if (el.hasOwnProperty('children')) {
                // Nest lists if element has children
                _this.addList(el.children, $li, level + 1);
                $listctl.removeClass(b_class_nochildren).addClass(init_b_class_children);
            }

            $listctl.on("click", function () {
                var $ul_children = $li.children("ul.label-ul");
                if ($ul_children.is(":visible") && ($li.data("loaded") || !_this.is_lazyloaded)) {
                    $listctl.removeClass(b_class_expanded).addClass(b_class_collapsed);
                    $ul_children.hide();
                }
                else if ($ul_children.length > 0) {
                    $listctl.removeClass(b_class_collapsed).addClass(b_class_expanded);
                    $ul_children.show();
                }
            }).attr("title", "show/hide child nodes");
        });
    };

    this.label_info_modal = function(id) {
        open_modal("/api/label/"+id+"?template="+_this.options.taginfo_template, {
            modal_class:"modal-lg",
            hide_open_modals: true,
            backdrop: true,  // close on click outside
            onhidden:function () {
                if ($(this).data('modified')===true)
                    _this.refreshList();
            }
        });
    };

    /*
    this.edit_label = function(id, options) {
        var url = "/api/label/"+id+ "?template=models/label/edit.html";
        var opts = $.extend({
            modal_id: 'edit-label-modal',
            modal_class: 'modal-md',
            hide_open_modals: true,
            oncompleted: function(data){
                _this.refreshList();
            }
        }, options);
        return open_modal(url, opts);
    };
    */

//    this.editList = function() {
//        var $edit_links = _this.$list_container.find(".label-edit");
//        if (_this.$list_container.data("edit")){
//            $edit_links.hide();
//            _this.$list_container.data('edit', false);
//        }
//        else {
//            $edit_links.show();
//            _this.$list_container.data('edit', true);
//        }
//    };

    /**
     *
     * @returns {string}
     */
    this.getLabelInfo = function (id, tmplt) {
        //console.log(id, tmplt, _this.url.label_single.format(id));
        var content = "Loading...";
        $.ajax({
            url: _this.url.label_single.format(id)+"?template="+tmplt,
            async: false,
            dataType: "html",
            success: function (data) {
                content = data;
                //var template = tmplt;
                //content = template(data);
            }
        });
        return content;
    };

    function getLabelInfo_callback() {
        return _this.getLabelInfo($(this).children('a.label-link').data('info').id, _this.options.taginfo_template);
    }

    /**
     *
     */
    if (!String.prototype.format) {
      String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
          return typeof args[number] !== 'undefined'
            ? args[number]
            : match
          ;
        });
      };
    }
}
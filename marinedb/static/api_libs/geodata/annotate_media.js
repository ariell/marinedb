function MediaObject($container, $tools, $info, $labelcontainer, $thumbnail_container, $setting) {
    var _this = this;


    // properties
    this.$media_container = $container;
    this.$tools_container = $tools;
    this.$info_container = $info;
    this.$medialabel_container = $labelcontainer;
    this.$thumbnail_container = $thumbnail_container;
    this.$settings_container = $setting;
    this.$media = null;
    this.$frame_polygon_container = null;
    this.$annotation_grid = null;
    this.media_data = null;
    this.annotation_xhr = null;

    this.user_id = null;
    this.annotation_set_id = null;
    this.media_id = null;

    // annotation options
    this.auto_select_mode = "off";
    this.show_grid = null;          // unitialised
    this.shift_pressed = false;
    // this.num_supplementary_annotations = 0;

    this.template_point_popover = "models/point/get_popover.html";  //Handlebars.templates['annotation-popover'];
    this.template_media_info = "models/media/detail_single.html";  //Handlebars.templates['media-annotation-info'];

    this.media_list = {
        results_per_page: 50,
        page: null,
        current_page: null,
        total_pages: null,
        num_results: null,
        q: "",
        next: function(){
            var page = Math.min(_this.media_list.total_pages, _this.media_list.current_page+1);
            _this.refresh_thumbnails(page);
            _this.$thumbnail_container.scrollLeft(0);
        },
        prev: function(){
            var page = Math.max(1, _this.media_list.current_page-1);
            _this.refresh_thumbnails(page);
            _this.$thumbnail_container.scrollLeft(0);
        }
    };

    this.annotation_set_props = {
        allow_add: false,
        allow_wholeframe: false
    };

    // module constants
    var point_class = "annotation-point",
        media_label_class = "annotation-point",
        point_class_selected = "selected",
        point_class_unlabeled = "unlabeled",
        point_class_suggestion = "suggestion",
        select_bbox_class = "drawnBox",
        point_poly_class = 'point-poly',
        frame_poly_class = 'frame-poly';

    var $main_container = _this.$media_container.parents(".media-gui-container");

    /**
     *
     * @param src
     * @param annotations
     * @param callback
     * @returns {null|*}
     */
    this.load_image = function(src, annotations, callback) {
        if (typeof annotations === 'undefined') annotations = [];
        var $loading_spinner = $('<i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw" style="position:absolute;right:10px;top:10px;"></i>');
        _this.$media = $('<canvas id="media-object"></canvas>');
        // _this.$frame_polygon_container = $('<svg id="media-object-frame-polygon-container"></svg>').css({top: 0, left: 0, position:'absolute', width:0, height:0});
        _this.$frame_polygon_container = $('<svg id="media-object-frame-polygon-container" class="' + frame_poly_class + '"></svg>');
        if (_this.media_data !== null) _this.media_data.src = ''; // abort any current loading
        _this.media_data = new Image();
        _this.media_data.src = src;
        _this.$media_container.append($loading_spinner);
        _this.media_data.onload = function() {
            var cxt = _this.$media[0].getContext("2d");
            var match_height = (_this.$media_container.width()/_this.$media_container.height() > _this.media_data.width/_this.media_data.height);
            var width = (match_height) ? _this.media_data.width*(_this.$media_container.height()/_this.media_data.height) : _this.$media_container.width();
            var height = (match_height) ? _this.$media_container.height() : _this.media_data.height*(_this.$media_container.width()/_this.media_data.width);

            _this.$media[0].width=width;
            _this.$media[0].height=height;
            cxt.drawImage(_this.media_data,0,0,_this.media_data.width,_this.media_data.height,0,0,width,height);

            _this.$media_container.html(_this.$media);
            // clear the whole-frame annotation list
            _this.$medialabel_container.html("");
            // _this.num_supplementary_annotations = 0;

            // append full frame polygon container, if relevant
            _this.$media_container.append(_this.$frame_polygon_container);

            // create annotations
            $(annotations).each(function(i, ap) {
                var $point = _this.create_point(ap, ap.x * width, ap.y * height);
                _this.add_annotation($point);
                if (ap.data.hasOwnProperty("polygon")) {
                    var points = _this.point_polygon.transform_points(ap.data.polygon, _this.xy2frame, $point.data('poly_offset'));
                    var $container = ($point.data('annotation_type') === 'point') ? $point : _this.$frame_polygon_container;
                    _this.point_polygon.set($container, {points: points, offset: $point.data('poly_offset')}, false);
                }
            });

            // load image tools
            _this.load_media_tools();

            // auto select if activated
            if (_this.auto_select_mode!=="off") _this.auto_select_points();

            if (typeof callback === "function") {callback();}

            // remove any open popovers to prevent orphaned objects
            // TODO: this feels like a hack, is there a better way?
            $main_container.find(".popover.in").remove();

            // // show notification for suggested annotations
            // if (_this.num_supplementary_annotations > 0) {
            //     notify.info("MAGICAL SUGGESTIONS: " + _this.num_supplementary_annotations + " annotations have suggested " +
            //         "labels. Click to see suggestions.", 7, "notify-suggested-labels");
            // }
        };


        // function to get xyt on click
        this.mouse2xyt = function(mouse){
            var xy = this.frame2xy(mouse.x, mouse.y);
            return {x: xy.x, y: xy.y, t: null};
        };

        this.mouse2frame = function(e) {
            var media_offset = _this.$media.offset();
            return {
                x: e.pageX - media_offset.left,
                y: e.pageY - media_offset.top
            }
        };

        this.xy2frame = function(x, y, offset) {
            offset = $.extend({x:0,y:0}, offset);
            return {
                x: x * _this.$media.width() + offset.x,
                y: y * _this.$media.height() + offset.y
            }
        };

        this.frame2xy = function(x, y, offset) {
            offset = $.extend({x:0,y:0}, offset);
            return {
                x: (parseFloat(x)+offset.x) / _this.$media.width(),
                y: (parseFloat(y)+offset.y) / _this.$media.height()
            }
        };

        this.point2frame = function($point) {
            return this.xy2frame($point.data("info").x, $point.data("info").y);
            /*
            var p = $point.offset();
            var m = _this.$media.offset();
            return {
                x: p.left - m.left,
                y: p.top - m.left,
                t: null
            }
            */
        };

        // function to add annotation
        this.add_annotation = function($point) {
            if ($point.data("annotation_type") === "point")
                _this.$media_container.append($point);
            else if ($point.data("annotation_type") === "frame")
                _this.$medialabel_container.append($point);
        };

        // function to load media tools and handle events
        this.load_media_tools = function() {
            var background_position = {top:0, left:0};
            var init_zoom = _this.$tools_container.data().hasOwnProperty("ctlSliderZoom") ?
                _this.$tools_container.data("ctlSliderZoom") : 3;
            var init_brightness = _this.$tools_container.data().hasOwnProperty("ctlSliderBrightness") ?
                _this.$tools_container.data("ctlSliderBrightness") : 100;
            var init_contrast = _this.$tools_container.data().hasOwnProperty("ctlSliderContrast") ?
                _this.$tools_container.data("ctlSliderContrast") : 100;

            var $zoom_point = $('<svg class="'+point_class+'"><circle cx="12" cy="12" r="7" /></svg>');
            var $controls = $("<div class='zoomctl'><a href='#' class='zoomtools-toggle'><i class='fa fa-sliders'></i></a></div>");
            var $zoom_box = $("<div class='zoombox'></div>").append($controls,$zoom_point);
            var $ctl_tools = $("<div class='zoomtools'></div>");
            var $zoom_slider = $("<input type='range' min='1' max='11' value='"+init_zoom+"' step=0.1>");
            var $brightness_slider = $("<input type='range' min='0' max='500' value='"+init_brightness+"' step=1>");
            var $contrast_slider = $("<input type='range' min='0' max='500' value='"+init_contrast+"' step=1>");
            var $zoom_ctl = $("<label class='media-tool'><i class='fa fa-search-plus'></i> Zoom (x <b class='ctl-val'>"+init_zoom+"</b>)</label>");
            var $brightness_ctl = $("<label class='media-tool'><i class='fa fa-sun-o'></i> Brightness (<b class='ctl-val'>"+init_brightness+"</b>%)</label>");
            var $contrast_ctl = $("<label class='media-tool'><i class='fa fa-adjust'></i> Contrast (<b class='ctl-val'>"+init_contrast+"</b>%)</label>");

            // Create Settings
            _this.$settings_container.html('<div class="help-block">Point selection:</div>');
            $zoom_ctl.append($zoom_slider);
            $brightness_ctl.append($brightness_slider);
            $contrast_ctl.append($contrast_slider);
            $ctl_tools.append($zoom_ctl, $brightness_ctl, $contrast_ctl);
            $controls.append($ctl_tools);
            $zoom_box.css('background-image','url("'+_this.media_data.src+'")');
            _this.$tools_container.html($zoom_box);
            //$zoom_point.css({top: $zoom_box.height() / 2.0, left: $zoom_box.width() / 2.0});

            if (_this.get_points_by_class(null).length > 0) {  // only allow image advancing points in annotation set
                _this.$settings_container.append(
                    $("<label> Auto-skip labeled <b>frames &amp; points</b></label>").prepend(
                        $('<input type="radio" name="point-selection-method">')
                            .change(function () {
                                if ($(this).is(":checked")) _this.auto_select_points("images")
                            }).prop("checked", _this.auto_select_mode === "images")
                    )
                );
            }
            else {
                _this.auto_select_points("off");  // if no points, switch off to prevent rampant behaviour
            }

            _this.$settings_container.append(
                $("<label> Auto-select unlabeled points in <b>current frame</b></label>").prepend(
                    $('<input type="radio" name="point-selection-method">')
                        .change(function(){if($(this).is(":checked")) _this.auto_select_points("points")})
                        .prop("checked", _this.auto_select_mode === "points")
                ),
                $("<label> Manually select points and frames</label>").prepend(
                    $('<input type="radio" name="point-selection-method">')
                        .change(function(){if($(this).is(":checked")) _this.auto_select_points("off")})
                        .prop("checked", _this.auto_select_mode === "off")
                )//,
                //$('<label><input type="checkbox" checked  disabled> Allow multi-point selection</label>')
            );


            // ADD GRID TOOL
            if (_this.annotation_set_props.hasOwnProperty('params')) {
                if (_this.annotation_set_props.params.hasOwnProperty('grid')) {
                    if (_this.show_grid === null)  // initialise showing of grid
                        _this.show_grid = _this.annotation_set_props.params.grid === "show";
                    if (_this.annotation_set_props['type'] === 'quincunx')
                        _this.create_grid(2, 2);
                    else if (_this.annotation_set_props.params.hasOwnProperty('nX') && _this.annotation_set_props.params.hasOwnProperty('nY'))
                        _this.create_grid(_this.annotation_set_props.params['nX'] - 1, _this.annotation_set_props.params['nY'] - 1);

                    _this.$settings_container.append($('<label> Show grid lines</label>').prepend(
                        $('<input type="checkbox">').change(function () {
                            if ($(this).is(":checked")) _this.$annotation_grid.fadeIn(300);
                            else _this.$annotation_grid.fadeOut(500);
                            _this.show_grid = $(this).is(":checked");
                        }).prop("checked", _this.show_grid)
                    ));
                }
            }


            // If annotation set allows dynamic editing of annotations, add annotations to be added
            // Append new whole-frame annotation button
            if (_this.annotation_set_props.allow_add){
                var $medialabel_btn = $('<a class="ctl-btn pull-right" href="javascript:void(0)" ><i class="fa fa-plus-circle"></i></a>')
                    .click(_this.new_frame_annotation)
                    .tooltip({title:"Add new whole-frame annotation", placement:"top"});
                _this.$medialabel_container.append($medialabel_btn);
            }


            // Media TOOLS
            $zoom_slider.on("change", function(){
                $zoom_box.css('background-size', parseInt($(this).val()*_this.$media.width())+'px '+parseInt($(this).val()*_this.$media.height())+'px');
                $(this).siblings(".ctl-val").html($(this).val());
                _this.$tools_container.data("ctlSliderZoom", $(this).val());
            }).trigger("change");
            $brightness_slider.on("change", function(){
                var css_params = {
                    'filter': 'contrast(' + $contrast_slider.val() + '%) brightness(' + $brightness_slider.val() + '%)',
                    '-webkit-filter': 'contrast(' + $contrast_slider.val() + '%) brightness(' + $brightness_slider.val() + '%)',
                    '-moz-filter':'contrast(' + $contrast_slider.val() + '%) brightness(' + $brightness_slider.val() + '%)'
                };
                $(_this.$media[0]).css(css_params);
                $zoom_box.css(css_params);
                $(this).siblings(".ctl-val").html($(this).val());
                _this.$tools_container.data("ctlSliderBrightness", $(this).val());
            }).trigger("change");
            $contrast_slider.on("change", function(){
                var css_params = {
                    'filter': 'contrast(' + $contrast_slider.val() + '%) brightness(' + $brightness_slider.val() + '%)',
                    '-webkit-filter': 'contrast(' + $contrast_slider.val() + '%) brightness(' + $brightness_slider.val() + '%)',
                    '-moz-filter':'contrast(' + $contrast_slider.val() + '%) brightness(' + $brightness_slider.val() + '%)'
                };
                //_this.$media[0].setAttribute('style', 'filter:contrast(' + $contrast_slider.val() + '%) brightness(' + $brightness_slider.val() + '%); -webkit-filter:contrast(' + $contrast_slider.val() + '%) brightness(' + $brightness_slider.val() + '%); -moz-filter:contrast(' + $contrast_slider.val() + '%) brightness(' + $brightness_slider.val() + '%);');
                $(_this.$media[0]).css(css_params);
                $zoom_box.css(css_params);
                $(this).siblings(".ctl-val").html($(this).val());
                _this.$tools_container.data("ctlSliderContrast", $(this).val());
            }).trigger("change");

            var zoom2point = function($pt, zoom_scale) {
                $zoom_box.addClass("blink-point");
                var offsetX = $pt.offset().left - _this.$media.offset().left,
                    offsetY = $pt.offset().top - _this.$media.offset().top;
                return {
                    left: ($zoom_box.width()) / 2.0 - (offsetX + $pt.width()/2.0) * zoom_scale,
                    top: ($zoom_box.height()) / 2.0 - (offsetY + $pt.height()/2.0) * zoom_scale
                };
            };

            _this.zoom_handler = function(mouse, e){
                if ($zoom_box.is(":visible")) {
                    var imgscale = $zoom_slider.val();
                    // if over image, move zoom window
                    if ($(e.target).is(_this.$media)) {
                        $zoom_box.removeClass("blink-point");
                        background_position = {
                            left: $zoom_box.width() / 2.0 - mouse.x * imgscale,
                            top: $zoom_box.height() / 2.0 - mouse.y * imgscale
                        };
                    }
                    //if over point snap zoom window
                    else if (e.target instanceof SVGElement) {
                        if (e.target.tagName.toLowerCase() === "circle")  // if circle, use SVG parent
                            background_position = zoom2point($(e.target).parent("svg"), imgscale);
                        else if (e.target.tagName.toLowerCase() === "svg")  // use SVG element
                            background_position = zoom2point($(e.target), imgscale);
                    }
                    // if not over image, snap to first selected point
                    else {
                        var $points_list = _this.get_points_by_class(point_class_selected);
                        if ($points_list.length>0) background_position = zoom2point($($points_list[0]), imgscale);
                    }
                    $zoom_box.css('background-position', background_position.left + 'px '+background_position.top + 'px');
                }
            };

            // Handle container mouse events
            _this.$media_container.on('mousedown mouseup mousemove dblclick mouseout', _this.mouse_handler);

            // handle keyboard events
            _this.$media_container.parents('.modal').on("keydown keyup", _this.keyboard_handler);

        };


        this.point_polygon = {
            /**
             * Attach or modify bounding box associated with a point
             * @param $container
             * @param poly_params
             * @param is_active
             * @param $polygon_container (optional, if left out, will default to $point)
             */

            set: function ($container, poly_params, is_active) {
                if (typeof is_active === "undefined") is_active = true;
                var params = $.extend({
                    points: [],
                    offset: {width:12, height: 12},
                    width: null,
                    height: null,
                    temp_point: null
                }, poly_params);
                var $polygon = $container.find("polygon."+point_poly_class);
                if ($polygon.length === 0) {   // if no bbox found, create it
                    var poly = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
                    $polygon = $(poly).attr('class',point_poly_class).appendTo($container);
                }
                //var cx = parseInt($point.find("circle").attr("cx"));
                //var cy = parseInt($point.find("circle").attr("cy"));
                if (params.points.length <= 0) { // if points not found, use width/height to generate points
                    params.points = this.bbox2points(params.width, params.height, params.offset.x, params.offset.y);
                }
                else {              // if points found, calculate bounds for width/height
                    var bbox = this.points2bbox(params.points, params.offset.x, params.offset.y);
                    params.width = bbox.width;
                    params.height = bbox.height;
                }
                if (params.points.length > 0) {
                    var draw_points = params.points.slice();  // clone the points before showing
                    if (params.temp_point !== null) draw_points.push(params.temp_point);  // add a temp point if requested
                    $polygon.attr({x: params.offset.x, y: params.offset.y, points: this.points2str(draw_points)});
                    if (params.width * params.height < this._min_area)
                        $polygon.css('stroke', 'red');      // show bbox as red if too small (will be deleted)
                    else
                        $polygon.css('stroke', '');         // reset default color if valid bbox
                }
                $polygon.data(params).data("is_active", is_active);
                return $polygon;
            },
            /**
             * Save bounding box for a point if bigger than minimum size, otherwise remove it
             * @param $point
             */
            save:  function($point, remove_last) {
                var $polygon = $point.find("polygon."+point_poly_class);
                var poly_offset = $point.data('poly_offset');
                this.drawing_bbox($point, false);
                this.drawing_polygon($point, false);
                //console.debug($polygon.data());
                if ($polygon.length > 0 && $polygon.data("is_active")) {
                    var ptinfo = $point.data('info');
                    //console.debug(ptinfo);
                    var orig_poly = ptinfo.data.hasOwnProperty("polygon") ? ptinfo.data.polygon : null; // whether or not it already has a polygon
                    if (remove_last === true) $polygon.data("points").pop(); // remove last point (i.e. from double click)
                    var scaled_points = this.transform_points($polygon.data("points"), _this.frame2xy, {x: -poly_offset.x, y: -poly_offset.y});

                    var data = $.extend(ptinfo.data, {polygon: scaled_points});
                    // if only two verticies, convert into a rectangle
                    //console.debug(data.polygon);
                    if (data.polygon.length === 2){
                        //console.debug("Converting into rectangle...")
                        var p1 = data.polygon[0],
                            p2 = data.polygon[1];
                        data.polygon = [p1, [p1[0], p2[1]], p2, [p2[0], p1[1]]];
                        this.set($point, {points: _this.point_polygon.transform_points(data.polygon, _this.xy2frame, poly_offset)});
                        this._update_data($point, $polygon, data);
                    }
                    // if small, remove
                    else if ($polygon.data("width") * $polygon.data("height") < this._min_area) {
                        if (orig_poly !== null) {   // if existing poly/bbox, confirm delete
                            var _that = this;
                            notify.confirm("Do you want to remove the polygon or bounding box?", {
                                ok: function () {
                                    delete data.polygon;
                                    _that._update_data($point, $polygon, data);
                                },
                                cancel: function () {
                                    data.polygon = orig_poly;
                                    _that.set($point, {points: _this.point_polygon.transform_points(data.polygon, _this.xy2frame, poly_offset)});
                                    _that._update_data($point, $polygon, data);
                                }
                            });
                        }
                        else {
                            delete data.polygon;
                            this._update_data($point, $polygon, data);
                        }
                    }
                    else {
                        this._update_data($point, $polygon, data);
                    }

                }
            },
            _update_data: function($point, $polygon, data) {
                var ptinfo = $point.data("info");
                //console.log($point.data());
                ajax.patch("/api/point/" + ptinfo.id, JSON.stringify({data:data}), {
                    success: function (pdata) {
                        $polygon.data("is_active", false);
                        _this.set_point_state($point, 'deselect');
                        if(!data.hasOwnProperty("polygon"))
                            $polygon.remove();  // if it is not found in the response, remove the DOM element
                    }
                });
            },
            add_point: function($point, mouse, save){
                var $polygon = $point.find("polygon."+point_poly_class);
                var points = $polygon.data('points') || [];
                var p = _this.point2frame($point);
                var vertex = [mouse.x+12 - p.x, mouse.y+12 - p.y];
                if (save) {
                    points.push(vertex);
                    this.set($point, {points: points});
                }
                else if (points.length > 0) {
                    this.set($point, {points: points, temp_point: vertex});
                }
            },
            drawing_bbox: function($point, is_drawing) {
                return this._drawing_something("_drawing_point_bbox", $point, is_drawing);
            },
            drawing_polygon: function ($point, start_drawing) {
                if (start_drawing===true) {  // if initialising, empty polygon points
                    var $polygon = $point.find("polygon."+point_poly_class);
                    $polygon.remove();
                    //_this.$media.css({cursor: 'crosshair'});
                    _this.$media_container.addClass('drawing-polygon');
                    notify.warning(
                        "Click to start drawing polygon points. Double-click to end.<br><br><b>TIP:</b> to draw " +
                        "a rectangle, click to create a vertex, draw a diagonal and double-click to expand.",
                        -1, "notify-point-polygon-info");
                }
                else if (start_drawing===false) {
                    //_this.$media.css({cursor: ''});
                    _this.$media_container.removeClass('drawing-polygon');
                    notify.hide("notify-point-polygon-info");
                }
                return this._drawing_something("_drawing_point_polygon", $point, start_drawing);
            },
            _drawing_something: function (key, $point, is_drawing) {
                if ($point != null && $point.length>0) {
                    if (typeof is_drawing !== 'undefined') {
                        $point.data(key, is_drawing);
                        console.log("Set "+key+" is_drawing: ", is_drawing);
                    }
                    return $point.data(key) || false;
                }
                else {
                    return false;
                }
            },
            transform_points: function(points, fnc, offset){
                return points.map(function(p){
                    var xy = fnc(p[0], p[1], offset);
                    return [xy.x, xy.y];
                });
            },
            points2str: function (points) {
                var points_str = "";
                $.each(points, function(i, p){points_str += p[0]+","+p[1]+" "});
                return points_str;
            },
            points2bbox: function(points, x, y) {
                return {
                    width: Math.max.apply(null, points.map(function (e) { return e[0]})) -
                        Math.min.apply(null, points.map(function (e) { return e[0]})),
                    height: Math.max.apply(null, points.map(function (e) { return e[1]})) -
                        Math.min.apply(null, points.map(function (e) { return e[1]}))
                }
            },
            bbox2points: function(width, height, x, y) {
                // if shift is pressed, draw a dodecagon
                if (_this.shift_pressed)  {
                    var points = [];
                    //var h = Math.min(width, height)/2
                    var theta = 0.5235987756;  // 2 * pi / 12 (dodecagon)
                    for (var n = 0; n < 12; n++) {
                        points.push([width/2*Math.sin(n*theta) + x, height/2*Math.cos(n*theta) + y]);
                    }
                    return points;
                }
                // otherwise, draw a rectangular bounding box
                else {
                    return [[(x - width / 2), (y + height / 2)],
                        [(x - width / 2), (y - height / 2)],
                        [(x + width / 2), (y - height / 2)],
                        [(x + width / 2), (y + height / 2)]];
                }
            },
            _min_area:400
            //_min_height:20
        };


        return _this.$media;
    };


    this.keyboard_handler = function(e) {
        var KEY_RIGHT_ARROW = 39,
            KEY_LEFT_ARROW = 37,
            KEY_A = 65,
            KEY_R = 82;
        if (e.type==="keydown") {
            if (!$(e.target).is(":input") || $(e.target).val()==='') {
                e.stopImmediatePropagation();                   // prevent multiple firings of keyboard event
                if (e.which === KEY_LEFT_ARROW) {               // left
                    _this.prev();
                    e.preventDefault();
                }
                else if (e.which === KEY_RIGHT_ARROW) {         // right
                    _this.next();
                    e.preventDefault();
                }
            }
            if (e.ctrlKey || e.metaKey) {  // if holding down CTRL or CMD
                if (e.keyCode === KEY_A) {      // select all unlabeled points
                    var $unlabeled_points = _this.get_points_by_class(point_class_unlabeled);
                    $($unlabeled_points).each(function (i, p) {
                        _this.set_point_state($(p), "select")
                    });
                    e.preventDefault();
                }
                else if (e.keyCode === KEY_R) { // refresh image display
                    _this.reload();
                    e.preventDefault();
                }
            }
            if (e.shiftKey) {
                _this.shift_pressed = true;
            }
        }
        else if (e.type === "keyup") {
            _this.shift_pressed = false;
        }
        console.debug("*** KEY NOT CONFIGURED: e.type= "+e.type+", e.which=" + e.which);
    };

    this.mouse_handler = function(e){
        e.preventDefault();
        //e.stopPropagation();
        e.stopImmediatePropagation();
        var mouse = _this.mouse2frame(e);
        var $clicked_point = _this.$media_container.data("last_clicked_point") || null;
        var $select_bbox = _this.$media_container.find("."+select_bbox_class);

        if ( e.type === 'mousedown') { // if mouse down (with left/right click)
            if (e.target instanceof SVGElement && e.target.tagName.toLowerCase() === "circle") {
                $clicked_point = $(e.target).parent("svg.annotation-point");
                _this.$media_container.data("last_clicked_point", $clicked_point);
            }
            if (e.which === 1) {      // capture only left click
                if (_this.move_point_annotation.is_moving($clicked_point)) {
                    // do nothing, but stop other things
                }
                else if (_this.point_polygon.drawing_polygon($clicked_point)) {
                    // do nothing, but stop other things
                }
                else if (e.target instanceof SVGElement && e.target.tagName.toLowerCase() === "circle") {  // point bbxox
                    _this.point_polygon.drawing_bbox($clicked_point, true);
                }
                else if ($(e.target).is(_this.$media)) {  // draw selection bbox
                    _this.point_selection_bbox.set($select_bbox, mouse, true);
                }
            }
            //_this.$media_container.focus(); // fix focus in some situations where popups / modals reset to wrong elements
        }
        else if ( e.type === 'mousemove') {
            if (_this.move_point_annotation.is_moving($clicked_point)) {
                _this.move_point_annotation.move($clicked_point, mouse.x, mouse.y);
            }
            else if (_this.point_polygon.drawing_polygon($clicked_point)) {
                if ($(e.target).is(_this.$media))
                    _this.point_polygon.add_point($clicked_point, mouse, false);  // don't save point
            }
            else if (_this.point_polygon.drawing_bbox($clicked_point)) {  // point bbxox
                var xyt = _this.point2frame($clicked_point);
                _this.point_polygon.set($clicked_point, {
                    width: Math.abs(mouse.x - xyt.x) * 2, height: Math.abs(mouse.y - xyt.y) * 2,
                    offset: $clicked_point.data('poly_offset')
                });
            }
            else if ($select_bbox.length > 0) { // draw selection bbox
                _this.point_selection_bbox.set($select_bbox, mouse);
            }
        }
        else if ( e.type === 'mouseup') {
            if (_this.move_point_annotation.is_moving($clicked_point)) {
                _this.move_point_annotation.save($clicked_point, mouse.x, mouse.y);
            }
            else if (_this.point_polygon.drawing_polygon($clicked_point)) {
                if ($(e.target).is(_this.$media))
                    _this.point_polygon.add_point($clicked_point, mouse, true);  // add point
            }
            else if (_this.point_polygon.drawing_bbox($clicked_point)) {  // point bbxox
                _this.point_polygon.save($clicked_point);
            }
            else if ($select_bbox.length > 0) { // draw selection bbox
                _this.point_selection_bbox.select_points($select_bbox);
            }
        }
        else if (e.type === "mouseout") {

        }
        else if ( e.type === 'dblclick' ) {
            if (_this.point_polygon.drawing_polygon($clicked_point)) {  // save polygon on dblclick
                if ($(e.target).is(_this.$media))
                    _this.point_polygon.save($clicked_point, true);
            }
            else if ($(e.target).is(_this.$media) && _this.annotation_set_props.allow_add) {
                _this.new_point_annotation(_this.mouse2xyt(mouse));  // add point on double-click if target is $media
            }
        }

        // Handle zoom box updates
        if (e.type==="mousemove") {
            _this.zoom_handler(mouse, e);
        }
    };

    this.point_selection_bbox = {
        set: function($select_bbox, mouse, create) {
            if (typeof create === "undefined") create = false;
            if (create && $select_bbox.length <= 0)
                $('<div class="'+select_bbox_class+'"></div>').data({ "bbox_x": mouse.x, "bbox_y": mouse.y }).appendTo(_this.$media_container);
            var xleft = ( $select_bbox.data('bbox_x') > mouse.x );  // true: moving left, false: moving right
            var yup = ( $select_bbox.data('bbox_y') > mouse.y );  // true: moving up, false: moving down
            $select_bbox.css({
                'right': (xleft) ? _this.$media_container.width()-$select_bbox.data('bbox_x') : 'auto',
                'left': (xleft) ? 'auto' : $select_bbox.data('bbox_x'),
                'bottom':(yup) ? _this.$media_container.height()-$select_bbox.data('bbox_y') : 'auto',
                'top': (yup) ? 'auto' : $select_bbox.data('bbox_y'),
                'width': Math.abs($select_bbox.data('bbox_x')-mouse.x),
                'height': Math.abs($select_bbox.data('bbox_y')-mouse.y)
            });
        },
        select_points: function($select_bbox) {
            var bb_offset = $select_bbox.offset();
            var bb = {  left: bb_offset.left, right: bb_offset.left + $select_bbox.width(),
                        top: bb_offset.top, bottom: bb_offset.top + $select_bbox.height() };
            _this.$media_container.find('.annotation-point').each(function() {
                var p = _this.point2frame($(this));
                if (p.x < bb.right && p.x > bb.left && p.y < bb.bottom && p.y > bb.top)
                    _this.set_point_state($(this));
            });
            $select_bbox.remove();
        }
        // is_drawing: false
    };


    this.load_media_info = function(id) {
        var url = "/api/media/"+id+"?template="+_this.template_media_info;
        load_content.html(url, _this.$info_container);
        // _this.$info_container.load(url, function(responseTxt, statusTxt, xhr){
        //     if (statusTxt === "error")
        //         _this._handle_ajax_fail(xhr, statusTxt, responseTxt);
        // });
    };

    this.load_media = function(media_id, annotation_set_id, user_id, callback, media_callback) {
        _this.user_id = user_id;
        _this.annotation_set_id = annotation_set_id;
        _this.media_id = media_id;

        // Get annotation data
        if (_this.annotation_xhr) _this.annotation_xhr.abort();

        _this.annotation_xhr = ajax.get('/api/media/'+media_id+'/annotations/'+annotation_set_id+"?_="+Date.now(), {
            success: function(data) {
                //console.log(data);
                var annotations = data.annotations;
                _this.annotation_set_props = data.annotation_set.data;

                var $media;
                if (data.media.type === "image") {
                    var media_path = data.media.path_best;  //.updateURIParam("_", Date.now());
                    $media = _this.load_image(media_path, annotations, media_callback);
                }
                else if (data.media.type === "mosaic") {
                    $media = $("<div class='alert alert-info'>MOSAIC media viewer not implemented yet</div>");
                }
                else if (data.media.type === "youtube") {
                    $media = $("<div class='alert alert-info'>YouTube VIDEO media viewer not implemented yet</div>");
                }
                else if (data.media.type === "video") {
                    $media = $("<div class='alert alert-info'>VIDEO media viewer not implemented yet</div>");
                }
                else {
                    $media = $("<div class='alert alert-danger'>Unrecognised media type!</div>");
                }

                _this.load_media_info(data.media.id);
                if (typeof callback === "function") {callback(data);}
                notify.hide("notify-media-annotation-load-abort");  // hide abort message on load
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.debug(jqXHR, textStatus, errorThrown);
                var msg = ajax._parse_error(jqXHR, textStatus, errorThrown);
                if (msg === 'abort')
                    notify.error("Annotation loading was aborted. Probably scrolling too fast?", 5000, "notify-media-annotation-load-abort");
                else
                    notify.error(msg, 7000, "notify-media-annotation-load-error");
            }
        });

        /*
        _this.annotation_xhr = $.getJSON('/api/media/'+media_id+'/annotations/'+annotation_set_id+"?_="+Date.now(), function(data) {
            //console.log(data);
            var annotations = data.annotations;
            _this.annotation_set_props = data.annotation_set.data;

            // clear the whole-frame annotation list (TODO: this should be somewhere else)
            _this.$medialabel_container.html("");

            var $media;
            if (data.media.type === "image") {
                var media_path = data.media.path_best;  //.updateURIParam("_", Date.now());
                $media = _this.load_image(media_path, annotations, media_callback);
            }
            else if (data.media.type === "mosaic") {
                $media = $("<div class='alert alert-info'>MOSAIC media viewer not implemented yet</div>");
            }
            else if (data.media.type === "youtube") {
                $media = $("<div class='alert alert-info'>YouTube VIDEO media viewer not implemented yet</div>");
            }
            else if (data.media.type === "video") {
                $media = $("<div class='alert alert-info'>VIDEO media viewer not implemented yet</div>");
            }
            else {
                $media = $("<div class='alert alert-danger'>Unrecognised media type!</div>");
            }

            // If annotation set allows dynamic editing of annotations, add annotations to be added
            // TODO: this should be somewhere else
            if (_this.annotation_set_props.allow_add){
                $media.dblclick(function(e) {
                    var xyt = _this.mouse2xyt(e);  // media-specific method to get xyt
                    _this.new_point_annotation(xyt);
                });

                // Append new whole-frame annotation button
                var $medialabel_btn = $('<a class="ctl-btn pull-right" href="javascript:void(0)" ><i class="fa fa-plus-circle"></i></a>')
                    .click(_this.new_frame_annotation)
                    .tooltip({title:"Add new whole-frame annotation", placement:"top"});
                _this.$medialabel_container.append($medialabel_btn);
            }

            _this.load_media_info(data.media.id);
            if (typeof callback === "function") {callback(data);}
        }).fail(_this._handle_ajax_fail);
        */
    };


    this.new_frame_annotation = function() {
        var annotation = {
            x: null,
            y: null,
            t: null,
            media_id: _this.media_id,
            annotation_set_id: _this.annotation_set_id,
            data: {user_created: true},
            user_id: _this.user_id
            // annotations: [
            //     {user_id: _this.user_id, annotation_set_id: _this.annotation_set_id}
            // ]
        };
        _this.new_point(annotation);
    };

    this.new_point_annotation = function(xyt) {
        var annotation = {
            x: xyt.x,
            y: xyt.y,
            t: xyt.t,
            media_id: _this.media_id,
            annotation_set_id: _this.annotation_set_id,
            data: {user_created: true},
            user_id: _this.user_id
            // annotations: [
            //     {user_id: _this.user_id, annotation_set_id: _this.annotation_set_id}
            // ]
        };
        _this.new_point(annotation);
    };

    this.move_point_annotation = {
        start: function($point) {
            if ($point.data("annotation_type") === "frame") {
                var info = $.extend({}, $point.data('info'), {x:0.5, y:0.5});
                $point.remove();
                $point = _this.create_point(info, {x:0.5, y:0.5});
                _this.$media_container.append($point).data("last_clicked_point", $point);
                notify.info("Convert whole-frame annotation to point annotation. Set a new location by clicking on " +
                    "the frame.", -1, "notify-move-annotation-point");
            }
            else {
                notify.info("Click on frame to set new point location.", -1, "notify-move-annotation-point");
            }
            return $point.data("_is_moving", true);
        },
        move: function($point, x, y) {
            $point.css({left: x, top: y});
        },
        save: function ($point, x, y) {
            if (x < _this.$media.width() && y < _this.$media.height()) {
                $point.data("_is_moving", false);
                var xy = _this.frame2xy(x, y);
                ajax.patch("/api/point/" + $point.data("info").id, JSON.stringify(xy), {
                    success: function(data) {
                        $point.data("info", $.extend($point.data("info"), xy));  // update xy pos in point data
                        _this.set_point_state($point, 'deselect');
                    }
                });
                notify.hide("notify-move-annotation-point");
            }
        },
        is_moving: function($point) {
            if ($point !== null && $point.length > 0)
                return $point.data("_is_moving") || false;
            return false;
        }
    };


    /**
     *
     * @returns {*}
     */
    this.get_points_by_class = function(get_class){
        get_class = (get_class == null) ? "" : "."+get_class;
        var $selected_points = _this.$media_container.find("svg."+point_class+get_class);
        $.merge($selected_points, _this.$medialabel_container.find("svg."+media_label_class+get_class));
        return $selected_points;
    };


    /**
     *
     * @param patch_data
     */
    this.set_label_selected = function(patch_data) {
        var $points_list = _this.get_points_by_class(point_class_selected);
        var patch_filter = {"filters":[{"name":"id","op":"in","val":_this.get_selected_annotation_ids($points_list)}]};
        _this.patch_annotation_label(
            patch_filter,
            patch_data,
            function(data) {
                //console.log(data);
                $points_list.each(function(i,point){
                    _this.point_popover_hide($(point));
                    _this.update_point_data($(point), {annotations:data.annotations}, "auto");
                });
            }
        );
    };


    this.get_selected_annotation_ids = function($points_list) {
        // get array of selected label ids to set from selected points
        var annotation_point_ids = $.map($points_list, function(p, i) {
            // look at annotations and set the first one that has not yet been labeled to active for annotation
            if (!$(p).data("info").hasOwnProperty("annotations")) $(p).data("info").annotations=[];  // ensure it has empty array
            var is_labeled = $(p).data("info").annotations.length > 0;
            if (is_labeled) {
                for (var j in $(p).data("info").annotations) {
                    // return first unlabeled label
                    if ($(p).data("info").annotations[j].label_id == null) return $(p).data("info").annotations[j].id;
                }
                return $(p).data("info").annotations[0].id;  // default to first label
            }
            return null;
        });

        return annotation_point_ids;
    }


    /**
     *
     * @param $point
     * @param select: optional, whether to select or deselect (select|deselect|toggle|auto).
     * @returns {string}
     */
    this.set_point_state =  function($point, select) {
        var point_info = $point.data('info');
        var annotation_type = $point.data("annotation_type");
        var fill_color = "#555";
        var needs_review = false;
        var icon_text = "";
        // console.log("SET_POINT_STATE", point_info);

        // if set to auto, but not auto labelling, deselect
        //if (select === "auto" && _this.auto_select_mode==="off") select = "deselect";

        // if select status unspecified or set to toggle, then toggle
        //else if
        if (typeof select === "undefined" || select === "toggle")
            select = ($point.attr("class").indexOf(point_class_selected) <= 0) ? "select" : "deselect";

        // check if point was originally selected
        var was_selected = $point.attr("class").indexOf(point_class_selected)>=0;

        // check if all labels have been assigned, if not show as unlabeled
        //console.log(point_info);
        if (!point_info.hasOwnProperty("annotations")) point_info.annotations=[];  // ensure it has empty array
        var is_labeled = point_info.annotations.length>0;
        for (var i = 0; i < point_info.annotations.length; i++) {
            if (point_info.annotations[i].label_id == null) {
                is_labeled = false;
                //fill_color = point_info.annotations[i]['color'];
            }
            else if (point_info.annotations[i].needs_review) {
                needs_review=true;
            }
        }


        if (needs_review) icon_text += '<tspan class="point-icon-flash">&#xf024;</tspan> ';
        if (point_info.hasOwnProperty('supplementary_annotations'))
            if (point_info.supplementary_annotations.length > 0) {
                fill_color = point_info.supplementary_annotations[0].color;
                icon_text += (point_info.supplementary_annotations[0].label_id === point_info.annotations[0].label_id)
                    ? '&#xf00c; ' : '&#xf00d;';  //f069: asterisk, f17b: android, f0d0: magic, f078: down chevron, f00c: check
            }


        // set fill colour if labeled
        if (is_labeled) { // set fill colour if labeled to colour of first label
            fill_color = point_info.annotations[0].color;
        }
        $point.find("circle,rect").css({"fill": fill_color});


        // Add/clear point icon symbols
        $point.find("text.point-icon").html(icon_text);


        var this_point_class = (annotation_type === "point") ? point_class : media_label_class;
        if (!is_labeled) this_point_class += " "+point_class_unlabeled;
        if (point_info.hasOwnProperty('supplementary_annotations')) {
            if (point_info.supplementary_annotations.length > 0) {
                this_point_class += " " + point_class_suggestion;
            }
        }
        if (select === "select") {
            this_point_class += " " + point_class_selected;

            //TODO: this is a hack - remove it!!!! Should not reference something not handled/created in this module
            $("#tag-scheme-container input.search").focus().select();
            // endhack
        }


        $point.attr("class", this_point_class);

        if (select==="auto") {
            // if auto select, and was labeled, but still not complete, keep selected, but flash so that user knows label was applied
            if (was_selected && !is_labeled){   // && _this.auto_select_mode!=="off") {
                setTimeout(function() {
                    _this.set_point_state($point, "select");
                },150);
            }
            else {
                // if auto select next point is on, then select next point
                _this.auto_select_points();
            }
        }


        return this_point_class;
    };


    /**
     *
     * @param $point
     * @param point_info
     * @param point_state
     */
    this.update_point_data = function($point, point_info, point_state) {
        // if unspecified, deselect by default
        if (typeof point_state === "undefined") point_state = "deselect";

        //console.log("update_point_data", $point.data("info"), point_info);

        if ($point.data().hasOwnProperty("info")) {
            // Check if delete is passed in
            if (point_info.hasOwnProperty('delete')) {
                 for (var i in $point.data("info").annotations){
                    if ($point.data("info").annotations[i].id === point_info.delete) {
                        $point.data("info").annotations.splice(i, 1);
                        break;
                    }
                }
            }
            // check if labels exist, and then update annotations
            if (point_info.hasOwnProperty('annotations')) {
                for (var i in point_info.annotations) {
                    var label_found = false;
                    for (var j in $point.data("info").annotations) {
                        if ($point.data("info").annotations[j].id == point_info.annotations[i].id) {
                            $.extend($point.data("info").annotations[j], point_info.annotations[i]);
                            label_found = true;
                            break;
                        }
                    }
                    // if does not exist, create new
                    if (!label_found) $point.data("info").annotations.push(point_info.annotations[i]);
                }
            }
        }
        else {
            $point.data("info", point_info);  // init first time
        }

        //console.log("updates_point_data", $point.data("info"));

        //var info = $point.data("info");
        //var stroke_width = (info['label_id']) ? 2 : 4;

        // update point state
        _this.set_point_state($point, point_state);

    }

    /**
     *
     * @param mode (optional)  off|images|points
     */
    this.auto_select_points = function (mode) {
        // if on_or_off is passed in, update setting, otherwise just ignore
        if (typeof mode !== "undefined") _this.auto_select_mode = mode;

        // auto advance points to next point if no points are selected.

        if (_this.auto_select_mode!=="off") {
            if (_this.get_points_by_class(point_class_selected).length <= 0) {
                var $unlabeled_points = _this.get_points_by_class(point_class_unlabeled);
                if ($unlabeled_points.length > 0) {
                    _this.set_point_state($unlabeled_points.first(), "select");
                    _this.$media_container.trigger("mousemove");        // trigger mousemove to update zoom
                }
                else if (_this.auto_select_mode === "images")   // if no points left, auto advance to next media item
                    _this.next();
            }
        }
    };

    /**
     *
     * @param tmplt
     * @param $point
     * @param edit
     * @returns {string}
     */
    this.point_popover_show = function ($point, tmplt, edit) {
        var point_info = $point.data("info");
        //var _this = this;
        edit = (typeof edit !=='undefined') ? edit : false;
        var mode = (_this.shift_pressed && edit) ? "exemplars" : ((edit) ? "edit" : "view")


        $point.data("editing", edit);
        $point.data("popover-show", true);   // Set flag to show popover. This is required because ajax call takes time
                                             // and needs to check status after loaded

        if (!$point.data.hasOwnProperty("popover-content")) {  // initialise popover content container
            var $content = $("<div></div>");
            var $popover_close= $('<a class="popover-close" href="javascript:void(0)"><i class="fa fa-times"></i></a>')
                                    .click(function () {_this.point_popover_hide($point)});
            $point.data("popover-content", $content);
            $point.popover({
                content: function () {return $('<div>').append($popover_close, $point.data("popover-content"));},
                html: true,
                trigger: "manual",
                container: $main_container,
                placement: ($point.data("annotation_type") === "frame") ? "auto left" : "auto right"
            });
        }
        var $popover_content = $point.data("popover-content");

        //var label_ids = $.map(point_info.annotations ,function(lbl,i){return lbl.id});
        //$.getJSON('/api/annotation?q={"filters":[{"name":"id","op":"in","val":['+label_ids+']}]}', function (data) {
        if (!_this.move_point_annotation.is_moving($point)) {
            var url = '/api/point/' + point_info.id +
                "?template=" + tmplt + "&current_annotation_set_id=" + _this.annotation_set_id +
                "&mode=" + mode + "&allow_edit_points=" + _this.annotation_set_props.allow_add + "&_=" + Date.now();

            load_content.html(url, $popover_content, function (data) {
                $popover_content.find("a.label-labellink").click(function () {
                    _this.set_label_selected({label_id: $(this).data("label_id")});
                });
                // $popover_content.find("a.popover-close").click(function () {
                //     _this.point_popover_hide($point);
                // });
                if (mode === "edit") {  // Add edit functions
                    $popover_content.find("a.annotation-edit-tag-del").click(function () {
                        notify.error("REMOVE TAG: Not implemented yet! TagID: " + $(this).data("tag_id") + ", Lbl ID: " + $(this).data("annotation_id"));
                        _this.point_popover_hide($point);
                    });
                    $popover_content.find("a.annotation-edit-tag-add").click(function () {
                        //notify.error("ADD TAG: Not implemented yet! Lbl ID: " + $(this).data("annotation_id"));
                        _this.point_popover_hide($point);
                        open_modal("/api/annotation/" + $(this).data("annotation_id") + "?template=models/annotation/add_tags.html", {
                            modal_class: "modal-md modal-dark",
                            onhidden: function () {
                                _this.point_popover_show($point, _this.template_point_popover);
                                _this.set_point_state($point, "deselect");
                            },
                            backdrop: true
                        });
                    });
                    $popover_content.find("a.annotation-edit-lbl-rst").click(function () {
                        _this.patch_annotation_label($(this).data("annotation_id"),
                            {comment: "", label_id: false, tags: [], likelihood: 1, needs_review: false},   // clear label
                            function (data) {
                                _this.update_point_data($point, {
                                    annotations: [{
                                        color: data.color,
                                        id: data.id,
                                        label_id: data.label_id
                                    }]
                                }, "deselect");
                                _this.point_popover_hide($point);
                                //_this.point_popover_show($point, _this.template_point_popover);
                            }
                        );
                    });
                    $popover_content.find("a.annotation-edit-lbl-lnk").click(function () {
                        $(this).addClass('disabled').html("<i class='fa fa-circle-o-notch fa-spin'>");
                        var url = '/api/annotation/'+$(this).data("annotation_id")+
                            '?template=models/annotation/link_observations.html';
                        open_modal(url, {
                            modal_class: "modal-lg modal-dark",
                            onshown: function () {_this.point_popover_hide($point)},
                            onhidden: function () {_this.set_point_state($point, "deselect")},
                            backdrop: true
                        });
                    });

                    $popover_content.find("a.annotation-edit-lbl-del").click(function () {  // delete label
                        var del_annotation_id = $(this).data("annotation_id");
                        ajax.delete("/api/annotation/" + del_annotation_id, {
                            success: function () {
                                //TODO: update point to remove label from info
                                _this.update_point_data($point, {'delete': del_annotation_id}, "deselect");
                                _this.point_popover_hide($point);
                            }
                        }, "Are you sure you want to delete this annotation label? This will not delete the point or the other labels.");
                    });
                    $popover_content.find("select[name='tags']").ajax_select('/api/tag', {
                        results_per_page:100,
                        dropdown_parent: $popover_content,
                        template_option: '<div class="title">[[name]]</div><div class="description">[[description]]</div>',
                        build_query: function(search_term) {
                            var q = {filters: [], order_by: [{field:"name",direction:"asc"}]}
                            if (search_term) {
                                $(search_term.split(/(\s+)/)).each(function(i,s){
                                    if (s.trim().length)
                                        q.filters.push({or:[{name:"name",op:"ilike",val:'%'+s+'%'},{name:"description",op:"ilike",val:'%'+s+'%'}]});
                                });
                            }
                            return q;
                        }
                    });
                    $popover_content.find("form.annotation-edit-comment").on('submit', function (e) {
                        e.preventDefault();
                        var data = $(this).serializeObject();
                        var payload = {comment: data.comment, likelihood: parseFloat(data.likelihood),
                            tags: $.isArray(data.tags) ? data.tags : [data.tags], needs_review: data.needs_review};
                        _this.patch_annotation_label(parseInt(data.annotation_id), payload, function (data) {
                            //_this.point_popover_hide($point);
                            _this.point_popover_show($point, _this.template_point_popover);
                            // update point info and deselect
                            _this.update_point_data($point, {annotations:[data]}, "deselect");
                        });
                    });

                    $popover_content.find("button.annotation-edit-lbl-add").click(function () {
                        _this.new_annotation_label(
                            {
                                annotation_set_id: _this.annotation_set_id,
                                point_id: point_info.id,
                                user_id: _this.user_id
                            },
                            function (data) {
                                _this.update_point_data($point, {
                                    annotations: [{
                                        color: data.color,
                                        id: data.id,
                                        label_id: data.label_id
                                    }]
                                }, "select");
                                //point_info.annotations.push({color:data.color, id: data.id, label_id: data.label_id});
                                _this.point_popover_hide($point);
                            }
                        );
                    });
                    $popover_content.find("button.annotation-edit-pt-polygon").click(function () {
                        _this.point_popover_hide($point);
                        _this.point_polygon.drawing_polygon($point, true);
                    });
                    $popover_content.find("button.annotation-edit-pt-move").click(function () {
                        _this.point_popover_hide($point);
                        _this.move_point_annotation.start($point);
                    });
                    $popover_content.find("button.annotation-edit-pt-del").click(function () {
                        ajax.delete("/api/point/" + point_info.id, {
                            success: function () {
                                _this.point_popover_hide($point);
                                $point.remove();
                            }
                        }, "Are you sure you want to delete this annotation? All associated labels, tags, comments " +
                            "and metadata will also be deleted.");
                    });
                }
                else if (mode==="exemplars") {
                    var $exemplar_container = $popover_content.find(".label-exemplar-container");
                    $exemplar_container.each(function(){
                        load_content.paginated_html("/api/label/"+$(this).data("label_id")+
                            "/exemplars?results_per_page=4&include_link=false&top_pagination=true" +
                            "&template=models/annotation/list_thumbnails.html", $(this), function(){
                            $(this).find('img').magnify({delay:5000});
                        });
                    })
                }
                if ($point.data("popover-show") || $point.data("editing"))
                    $point.popover('show');
            }).fail(function () {
                $point.popover('show');   // show error message if failed request
            });
        }
    };

    this.point_popover_hide = function($point) {
        $point.data("popover-show", false);
        $point.data("editing", false);
        $point.popover('hide');
        _this.$media_container.focus();
    };

    this.patch_annotation_label = function(id_or_filter, data, success) {
        var url;
        if (typeof id_or_filter === 'number')   // if id, single patch
            url = '/api/annotation/'+id_or_filter;
        else if (typeof id_or_filter === 'object')  // if filter, patch many
            url = '/api/annotation?q='+JSON.stringify(id_or_filter);

        ajax.patch(url, JSON.stringify(data), {success:success});
        /*
        $.ajax({
            url : url,
            data : JSON.stringify(data),
            dataType: 'json',
            type : 'PATCH',
            contentType : 'application/json',
            xhr: function() {
                return window.XMLHttpRequest == null ||
                new window.XMLHttpRequest().addEventListener == null ?
                    new window.ActiveXObject("Microsoft.XMLHTTP") : $.ajaxSettings.xhr();
            },
            success: success
        }).fail(_this._handle_ajax_fail);
        */
    };

    this.new_annotation_label = function(data, success) {
        ajax.post('/api/annotation', JSON.stringify(data), {success:success});
        /*
        var url = '/api/annotation';
        $.ajax({
            url : url,
            data : JSON.stringify(data),
            dataType: 'json',
            type : 'POST',
            contentType : 'application/json',
            success: success
        }).fail(_this._handle_ajax_fail);
        */
    };


    this.new_point = function(point) {
        //console.log(annotation);
        ajax.post("/api/point", JSON.stringify(point), {
            success: function(data){
                var $point = _this.create_point(data, data.x * _this.$media.width(), data.y * _this.$media.height());
                _this.add_annotation($point);
                _this.set_point_state($point, "select");
            }
        });

        /*
        $.ajax({
            //url: "/api/annotation",
            url: "/api/point",
            data : JSON.stringify(point),
            dataType: 'json',
            type : 'POST',
            contentType : 'application/json',
            success: function(data){
                var $point = _this.create_point(data, data.x * _this.$media.width(), data.y * _this.$media.height());
                _this.add_annotation($point);
                _this.set_point_state($point, "select");
            }
            // TODO: add error handling
        }).fail(_this._handle_ajax_fail);
        */
    };

    /**
     *
     * @param point_info
     * @param x
     * @param y
     * @returns {*|jQuery}
     */
    this.create_point = function(point_info, x, y) {
        var $point;
        if (point_info.x == null || point_info.y == null) {     // Whole-Frame annotation
            $point = $('<svg id="point-' + point_info.id + '" class="' + media_label_class + '">' +
                '<rect height="18" width="18" x="2" y="2" rx="4" ry="4" style="fill:white"/>' +
                '<text class="point-icon" x="11" y="16"></text></svg>')
                .data({annotation_type: "frame", poly_offset: {x:0,y:0}});
        }
        else {    // Point annotation
            $point = $('<svg id="point-' + point_info.id + '" class="' + point_class + '">' +
                '<circle style="fill:white;" cx="12" cy="12" r="7" />' +
                '<text class="point-icon" x="12" y="4"></text></svg>')
                .css({top: y, left: x})
                .data({annotation_type: "point", poly_offset: {x:12,y:12}});
        }

        $point.click(function () {
            _this.set_point_state($point);
            _this.point_popover_show($point, _this.template_point_popover);
        }).on("dblclick contextmenu", function (e) {    // Double click / right click
            e.preventDefault();
            $.each(_this.get_points_by_class(point_class_selected), function (i, p) {
                if ($(p).attr("id") !== $point.attr("id")) {  // if selected point is not current point
                    _this.set_point_state($(p), "deselect");  // unselect all selected points
                    _this.point_popover_hide($(p));        // hide popover if open
                }
            });
            _this.set_point_state($point, "select");     // select this point
            _this.point_popover_show($point, _this.template_point_popover, true);
            return false;
        }).hover(function (e) {   // mouse in
            if ($point.data("editing") !== true) {
                _this.point_popover_show($point, _this.template_point_popover);
            }
        }, function (e) {       // mouse out
            if ($point.data("editing") === true)  // if editing popover, don't close
                return;
            if ($point.attr("class").indexOf(point_class_selected) >= 0) {  // if point is selected
                if (!point_info.hasOwnProperty("annotations"))
                    point_info.annotations=[];  // ensure it has empty array
                if (point_info.hasOwnProperty('supplementary_annotations')) {        // if has suggested label property
                    if (point_info.supplementary_annotations.length > 0)             // if has suggested labels
                        return;  // leave popover open
                }
                else if (point_info.annotations.length > 1)           // if point has more than one label, leave open to select
                    return;  // leave popover open
            }
            //$(this).popover("hide");
            _this.point_popover_hide($(this));
        });

        _this.update_point_data($point, point_info, "deselect");
        //console.log($point, point_info);

        // if (point_info.hasOwnProperty('supplementary_annotations') && point_info.supplementary_annotations.length > 0) {
        //     _this.num_supplementary_annotations++;
        // }

        return $point;
    };

    this.create_grid = function(nX, nY) {
        var y_spacer = _this.$media.height()/(nY+1);
        var x_spacer = _this.$media.width()/(nX+1);
        var grid = '<svg id="media-annotation-grid" height="'+_this.$media.height()+'" width="'+_this.$media.width()+'">';

        // need to create as string - for some reason jquery does not play nice with SVGs
        for (var x = 1; x<=nX; x++) {
            grid += '<line x1="'+x*x_spacer+'" y1="0" x2="'+x*x_spacer+'" y2="'+_this.$media.height()+'" style="stroke:rgba(255,255,255,0.5);stroke-width:2" />';
        }
        for (var y=1; y<=nY; y++) {
            grid += '<line x1="0" y1="'+y*y_spacer+'" x2="'+_this.$media.width()+'" y2="'+y*y_spacer+'" style="stroke:rgba(255,255,255,0.5);stroke-width:2" />';
        }
        grid += '</svg>';

        // Add grid
        _this.$annotation_grid = $(grid).css({position: 'absolute', top:0, 'left':0, 'pointer-events': 'none'});
        if (!_this.show_grid) _this.$annotation_grid.css("display", "none");  // show/hide
        _this.$media_container.append(_this.$annotation_grid);

    };


    this.close_window = function(el) {
        var isInIframe = (window.location !== window.parent.location);
        if ($(el).parents('.modal.in').length > 0)
            $(el).parents('.modal.in').modal('hide');
        else if (isInIframe)
            parent.$('#modal-iframe').parents('.modal.in').modal('hide');
        else
            close();
    };

    this.next = function() {
        if (_this.media_list.page !== _this.media_list.current_page) {  // check we're on the correct page in the list
            _this.refresh_thumbnails(_this.media_list.page, {selector: "next"});  // if not, refresh and select
        }
        else {
            var $item = _this.$thumbnail_container.find("a.active").next('a.thm');
            if ($item.length > 0) {
                $item.trigger('click');
            }
            else if (_this.media_list.total_pages > _this.media_list.page) {  // advance to the next page and select if necessary
                _this.media_list.page = Math.min(_this.media_list.total_pages, _this.media_list.page + 1);
                _this.refresh_thumbnails(_this.media_list.page, {selector: 'a.thm:first'});
            }
        }
    };

    this.prev = function() {
        if (_this.media_list.page !== _this.media_list.current_page) {
            _this.refresh_thumbnails(_this.media_list.page, {selector: "prev"});
        }
        else {
            var $item = _this.$thumbnail_container.find("a.active").prev('a.thm');
            if ($item.length > 0)
                $item.trigger('click');
            else if (_this.media_list.page > 1) {
                _this.media_list.page = Math.max(1, _this.media_list.page - 1);
                _this.refresh_thumbnails(_this.media_list.page, {selector: 'a.thm:last'});
            }
        }
    };

    this.reload = function() {
        _this.load_media( _this.media_id, _this.annotation_set_id, _this.user_id, null, null);
    };


    this.load_thumbnails = function(q, page_index_orig, page_orig, results_per_page_orig) {

        var index = parseFloat(page_index_orig) + (parseFloat(page_orig)-1)*parseFloat(results_per_page_orig);
        var page = Math.floor(Math.max(1, index-1)/_this.media_list.results_per_page)+1;
        console.debug('page_index_orig',page_index_orig, 'page_orig',page_orig, 'results_per_page_orig',results_per_page_orig,'index',index,'page',page, 'q', q);
        // var page_index = index % results_per_page;
        _this.media_list.page = page;
        _this.media_list.q = q;
        return _this.refresh_thumbnails(_this.media_list.page);
    };

    this.refresh_thumbnails = function(page, options) {
        var params = $.extend({selector:null, callback:null}, options);
        var url = '/api/annotation_set/'+_this.annotation_set_id+'/media?_='+Date.now();
        url = url.updateURIParam('q', _this.media_list.q).updateURIParam('page', page)
            .updateURIParam('results_per_page', _this.media_list.results_per_page);
        console.debug("*** REFRESHING THUMBS", url);

        //$.getJSON(url, function(data){
        return ajax.get(url, {success:function(data){
            if (data.objects.length > 0) {
                _this.$thumbnail_container.html("");
                $(data.objects).each(function(i, m){
                    _this.media_list.total_pages = data.total_pages;
                    _this.media_list.num_results = data.num_results;
                    _this.media_list.current_page = data.page;
                    var active = (parseInt(_this.media_id) === parseInt(m.id)) ? "active" : "";
                    var $thm = $("<a class='thm "+active+"' href='javascript:void(0)'><img src='"+m.path_best_thm+"'></a>").click(function(){
                        _this.load_media( m.id, _this.annotation_set_id, _this.user_id, null, null);
                        $(this).addClass('active').siblings('.active').removeClass('active');
                        _this.autoscroll_thumbnails();
                        _this.media_list.page = _this.media_list.current_page;
                    });
                    _this.$thumbnail_container.append($thm);
                });

                _this.$thumbnail_container.append($('<span class="thumbnail-container-info badge label-default"><i class="fa fa-info-circle"></i> Total: '+_this.media_list.num_results+' items. Showing page '+_this.media_list.current_page+'/'+_this.media_list.total_pages+'.</span>'));

                // auto select thumbnail
                if (params.selector === "next") _this.next();
                else if (params.selector === "prev") _this.prev();
                else if (params.selector !== null) _this.$thumbnail_container.find(params.selector).trigger('click');

                // add pagination links
                if (_this.media_list.current_page > 1)
                    _this.$thumbnail_container.prepend($('<a href="javascript:void(0)" class="nav-btn"><i class="fa fa-caret-left"></i></a>').click(function(){_this.media_list.prev()}));
                if (_this.media_list.current_page < _this.media_list.total_pages)
                    _this.$thumbnail_container.append($('<a href="javascript:void(0)" class="nav-btn"><i class="fa fa-caret-right"></i></a>').click(function(){_this.media_list.next()}));
            }
        }});
    };

    this.autoscroll_thumbnails = function(){
        var $container =  _this.$thumbnail_container;
        if ($container.is(":visible")) {
            var $target = $container.find("a.active");
            if ($target.length > 0)
                $container.scrollLeft($container.scrollLeft() + $target.position().left - $container.width() / 2 + $target.width() / 2);
        }
    };

    this.toggle_fullscreen = function(fullscreen) {
        if (typeof fullscreen === "undefined") fullscreen=null;
        //var doc = (window.location != window.parent.location) ? window.parent.document : document;   // for iframe - buggy
        var doc = document;

        if (fullscreen!==false &&
            ((doc.fullScreenElement && document.fullScreenElement !== null) ||
            (!doc.mozFullScreen && !document.webkitIsFullScreen))) {
            if (doc.documentElement.requestFullScreen)
                doc.documentElement.requestFullScreen();
            else if (doc.documentElement.mozRequestFullScreen)
                doc.documentElement.mozRequestFullScreen();
            else if (doc.documentElement.webkitRequestFullScreen)
                doc.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
        else if (fullscreen!==true) {
            if (doc.cancelFullScreen)
              doc.cancelFullScreen();
            else if (doc.mozCancelFullScreen)
              doc.mozCancelFullScreen();
            else if (doc.webkitCancelFullScreen)
              doc.webkitCancelFullScreen();
        }
    };

    /*
    this._handle_ajax_fail = function(jqXHR, textStatus, errorThrown){
        console.error(jqXHR, textStatus, errorThrown);
        var msg = (jqXHR.hasOwnProperty("responseJSON") && jqXHR.responseJSON && jqXHR.responseJSON.hasOwnProperty("message"))
            ? jqXHR.responseJSON.message
            : (jqXHR.responseText) ? jqXHR.responseText : "Unknown error occurred";
        notify.error(msg);
    };
    */



    return _this;
}
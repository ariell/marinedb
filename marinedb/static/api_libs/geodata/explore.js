function ExploreMap ($container, filters, options) {
    if(typeof options === "undefined") options = {};

    this.options = {
        url_campaign_map: "/api/campaign/map",
        url_campaign_popup: "/api/campaign/{id}?template=models/campaign/get_map_popup.html",
        url_deployment_map: "/api/deployment/map",
        url_deployment_popup: "/api/deployment/{id}?template=models/deployment/get_map_popup.html",
        url_deployment_images: '/api/media?q={"filters":[{"name":"deployment_id","op":"in","val":[{id}]}]}&' +
                               'template=models/media/get_many.html&results_per_page=100',
        url_campaign_images: '/api/media?q={"filters":[{"name":"deployment","op":"has","val":{"name":"campaign_id","op":"in","val":[{id}]}}]}&' +
                               'template=models/media/get_many.html&results_per_page=100',
        $isloading: $('<i id="progress" class="fa fa-circle-o-notch fa-spin"></i>'),
        update_filters: this.update_filters,
        panes: [
            //{name: "SQOverlay", zindex: 400},
            {name: "SQTiles", zindex: 300},
            {name: "BGTiles", zindex: 200}
        ]
    };

    $.extend(this.options, options);
    var _this = this;


    this.$container = $container;
    this.map = new L.map($container.attr("id"), {zoomControl:false, worldCopyJump:true});
    this.default_bounds = null;


    $(this.options.panes).each(function(i, el){
        _this.map.createPane(el.name);
        _this.map.getPane(el.name).style.zIndex = el.zindex;
    })
    this.map.createPane('markers');


    this.filters = filters;


    var layer_xhrs = {};   // object to store xhr requests

    this.marker_data = {
        campaign: {
            url: _this.options.url_campaign_map,
            popup: _this.options.url_campaign_popup,
            q: {filters:[]},
            last_url: null,
            build_filter: function(options){
                var layer = _this.marker_data.campaign;
                layer.q.filters = [];
                if (options.platform_ids.length > 0)
                    layer.q.filters.push({name:"deployments",op:"any",val:{name:"platform_id",op:"in",val:options.platform_ids.slice()}});
                    //layer.q.filters.push({"name":"platform_id","op":"in","val":options.platform_ids.slice()});
                return layer.q;
            },
            marker: function(el) {
                var title = el.deployment_count + " Deployments, "+el.total_annotation_count+" Annotations"; // ("+public_annotation_count+" public)" : "");
                var $icon = $('<div title="'+title+'" id="map-mkr-cmp-'+el.id+'" style="background-color:' + el.color + '"><span><b>' +
                    el.deployment_count +(el.total_annotation_count ? '<sup><i class="fa fa-tags"></i></sup>': '')+ '</b>'+'</span></div>'
                );
                var marker = new L.marker(el.latlon, {
                    fillColor: el.color,
                    count: el.deployment_count,
                    annotation_count: el.total_annotation_count,
                    public_annotation_count: el.public_annotation_count,
                    icon: L.divIcon({
                        html: $icon[0],
                        className: 'marker-cluster marker-campaign',
                        iconSize: [40, 40]
                    })
                });
                marker.bindTooltip(title, {permanent: false});
                return marker;
            },
            layer: new L.markerClusterGroup({
                attribution:"Squidle+",
                //pane: "SQOverlay",
                maxClusterRadius: 40,
                showCoverageOnHover: false,
                iconCreateFunction: function (cluster) {
                    // custom cluster color based on mode of colours of markers
                    var child_markers = cluster.getAllChildMarkers(),
                        deployment_count = child_markers.reduce((sum, val) => sum + val.options.count, 0),
                        annotation_count = child_markers.reduce((sum, val) => sum + val.options.annotation_count, 0);
                    var title = child_markers.length+" Campaigns, "+deployment_count + " Deployments, "+annotation_count+" Annotations"; // ("+public_annotation_count+" public)" : "");

                    //var $icon = $("<div title='"+title+"'>").tooltip({container:"body"});//,boundary:"window"});
                    var $icon = $("<div data-title='"+title+"'>");
                    var canvas = document.createElement('canvas');
                    canvas.width = 40;
                    canvas.height = 40;

                    var ctx = canvas.getContext('2d');
                    var centerX = canvas.width / 2;
                    var centerY = canvas.height / 2;
                    var radius = Math.min(centerX, centerY);
                    var startingAngle = 0;

                    child_markers.forEach(value => {
                        var count = (value.options.hasOwnProperty("count")) ? value.options.count : 0;
                        var sliceAngle = 2 * Math.PI * count/deployment_count;
                        ctx.beginPath();
                        ctx.moveTo(centerX, centerY);
                        ctx.arc(centerX, centerY, radius, startingAngle, startingAngle + sliceAngle);
                        ctx.fillStyle = value.options.fillColor;
                        ctx.fill();
                        startingAngle += sliceAngle;
                    });

                    $icon.html(canvas).append(
                        '<span style="position: absolute;left:0;top:0;right:0"><b>' + deployment_count +
                        (annotation_count?'<sup><i class="fa fa-tags"></i></sup>'+ '</b>':'')+ '</span>');
                    //$("body").prepend($icon);
                    //console.log($icon);

                    /*
                    var icon = '<div title="'+title+'" style="background-color:' + get_cluster_color(child_markers) +
                        '"><span><b>' + deployment_count +(annotation_count?'<i class="fa fa-tags text-danger"></i>'+ '</b>':'')+
                        '</span></div>';

                     */
                    var icon = new L.divIcon({
                        html: $icon[0],
                        className: 'marker-cluster marker-campaign marker-cluster-campaign',
                        iconSize: [50, 50] });
                    cluster.bindTooltip(title, {permanent: false});
                    return icon;
                }
            })
        },
        deployment: {
            url:_this.options.url_deployment_map,
            popup: _this.options.url_deployment_popup,
            q: {filters:[]},
            last_url: null,
            build_filter: function(options) {
                var layer = _this.marker_data.deployment;
                var filts = [];
                if (options.campaign_ids.length > 0)
                    filts.push({name:"campaign_id",op:"in",val:options.campaign_ids.slice()});
                if (options.deployment_keys.length > 0)
                    filts.push({name:"key",op:"in",val: options.deployment_keys.slice()});
                if (options.deployment_ids.length > 0)
                    filts.push({name:"id",op:"in",val: options.deployment_ids.slice()});
                layer.q.filters = [{name:"is_valid",op:"eq",val:true}, {or:filts}];
                //console.log(layer.q);
                return (filts.length <= 0) ? null : layer.q;
            },
            marker: function(el){
                var marker = new L.circleMarker(el.latlon, {
                    color: '#000',
                    fillColor: el.color,
                    fillOpacity: 0.7,
                    opacity: 0.9,
                    weight: 2,
                    fill: true,
                    radius: 7
                });
                return marker;
                // marker._path.parentElement.children[0].id = el.id;
                // console.log(marker);
                // return marker;
            },
            layer: new L.markerClusterGroup({
                attribution:"Squidle+",
                //pane: "SQOverlay",
                maxClusterRadius: 24,
                iconCreateFunction: function (cluster) {
                    return new L.divIcon({
                        html: '<div style="background-color:' + cluster.getAllChildMarkers()[0].options.fillColor + '"><span><b>' + cluster.getChildCount() + '</b></span></div>',
                        className: 'marker-cluster marker-deployment',
                        iconSize: new L.Point(24, 24) });
                }
            })
        }
    };


    this.wms_layer = {
        deployment_tracks: {
            //layer: new L.tileLayer.wms("/geoserver/squidle/wms", {
            //layer: new L.sqlayer("tileLayer.wmsInfo", "/geoserver/squidle/wms", {
            layer: new L.sqlayer("tileLayer.WMSDeploymentTracks", "/geoserver/squidle/wms", {
                "service": "WMS",
                "request": "GetMap",
                "layers": "squidle:deployment_tracks",
                "styles": "point-lines-w-colour",
                "format": "image/png",
                "transparent": true,
                "version": "1.1.1",
                "width": 256,
                "height": 256,
                "srs": "EPSG:4326",
                "minZoom": 3,
                "maxZoom": 20,
                "pane": "SQTiles"
            }),
            reload: function() {
                var cql_filter = "";
                if (_this.filters.platform_ids.length > 0)
                    cql_filter += "platform_id IN ("+_this.filters.platform_ids.join(",")+")";
                this.layer.setParams({cql_filter: cql_filter || "1=1"});
                // this.layer.bringToFront();
            }
        },
        pose_points: {
            layer: new L.sqlayer("tileLayer.wmsNoInfo","/geoserver/squidle/wms", {
                "service": "WMS",
                "request": "GetMap",
                "layers": "squidle:poses",
                "format": "image/png",
                "transparent": true,
                "version": "1.1.1",
                "width": 256,
                "height": 256,
                "srs": "EPSG:4326",
                "minZoom": 10,
                "maxZoom": 20,
                "cql_filter": "1=0",
                "zIndex": 10,
                "pane": "SQTiles"
            }),
            reload: function() {
                var cql_filter = [];
                if (_this.filters.deployment_ids.length > 0) cql_filter.push("deployment_id IN (" + _this.filters.deployment_ids.join(",") + ")");
                if (_this.filters.dep_max) cql_filter.push("dep < "+ _this.filters.dep_max);
                if (_this.filters.dep_min) cql_filter.push("dep > "+ _this.filters.dep_min);
                if (_this.filters.alt_max) cql_filter.push("alt < "+ _this.filters.alt_max);
                if (_this.filters.alt_min) cql_filter.push("alt > "+ _this.filters.alt_min);
                if (_this.filters.ts_max) cql_filter.push('timestamp < '+ _this.filters.ts_max+'');
                if (_this.filters.ts_min) cql_filter.push('timestamp > '+ _this.filters.ts_min+'');

                if (cql_filter.length > 0) {
                    // add any extra filters to help with performance
                    //if (_this.filters.campaign_ids.length > 0) cql_filter.push("campaign_id IN (" + _this.filters.campaign_ids.join(",") + ")");
                    //if (_this.filters.platform_ids.length > 0) cql_filter.push("platform_id IN (" + _this.filters.platform_ids.join(",") + ")");
                    //console.log(cql_filter, cql_filter.join(" and "));

                    this.layer.setParams({cql_filter: cql_filter.join(" and ")});
                    this.layer.bringToFront();
                    //if (!_this.map.hasLayer(this.layer))
                    //    _this.map.addLayer(this.layer);
                }
                else {
                    //_this.map.removeLayer(this.layer);
                    this.layer.setParams({cql_filter: "1=0"});
                }

            }
        }
    };

    // this.reload_wms = function() {
    //     this.wms_layer.deployment_tracks.layer.setParams({cql_filter: deployment_track_filter()});
    // };



    var baselayers = {
        // "ESRI Ocean": new L.tileLayer('https://services.arcgisonline.com/arcgis/rest/services/Ocean/World_Ocean_Base/MapServer/tile/{z}/{y}/{x}',
        //     {maxZoom: 18, maxNativeZoom: 10,attribution:"ESRI Oceans"}),
        "ESRI World Ocean Base": new L.tileLayer('https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}',
            {maxZoom: 20, maxNativeZoom: 19,attribution:"ESRI Oceans"}),
        "ESRI Imagery": new L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
            {maxZoom: 20, maxNativeZoom: 16,attribution:"ESRI Imagery"}),
        "Google Satellite": new L.tileLayer('https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}',
            {maxZoom: 20, maxNativeZoom: 16,attribution:"Google"}),
        "OpenStreetMap": new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            {maxZoom: 20, maxNativeZoom: 18,attribution:"OpenStreetMap"}),
        "Bright Earth": L.tileLayer.wms("https://maps.eatlas.org.au/maps/gwc/service/wms",
            {maxZoom: 20, service:'WMS',request:'GetMap',layers:'ea-be:World_Bright-Earth-e-Atlas-basemap',format:'image/jpeg',transparent:false,version:'1.1.1',srs:'EPSG:900913', attribution:"e-Atlas"}),
        // "Topography": L.tileLayer.wms('http://ows.mundialis.de/services/service?',
        //     {layers: 'TOPO-WMS'})
        // https://maps.eatlas.org.au/maps/gwc/service/wms?LAYERS=ea-be%3AWorld_Bright-Earth-e-Atlas-basemap&TRANSPARENT=FALSE&VERSION=1.1.1&SERVICE=WMS&REQUEST=GetMap&STYLES=&FORMAT=image%2Fjpeg&SRS=EPSG%3A900913&BBOX=-17532819.791584,-10018754.168608,-15028131.248736,-7514065.6257601&WIDTH=256&HEIGHT=256
        // "Google": new L.Google('ROADMAP'),
        // "Google Hybrid": new L.Google('HYBRID')
    };


    var overlays = {
        "Data": {
            "Campaigns": this.marker_data.campaign.layer,
            "Deployments": this.marker_data.deployment.layer,
            //"Deployment Track": new L.sqlayer("tileLayer.wmsInfo", "/nomapserver", {}),
            "Deployment tracks": this.wms_layer.deployment_tracks.layer,
            "Filtered Media selection": this.wms_layer.pose_points.layer
        },
        "Other layers": {
            "None": new L.tileLayer('', {"pane": "BGTiles"})
            // these are filled out in init and loaded from API
        }
    };





    this.init = function(){
        // add loader
        this.$container.append(_this.options.$isloading);

        // Initialise layers
        //_this.map.addLayer(baselayers["OpenStreetMap"])
        _this.map.addLayer(baselayers["Bright Earth"])
            .addLayer(overlays["Data"]["Campaigns"])
            .addLayer(overlays["Data"]["Deployments"])
            .addLayer(overlays["Data"]["Deployment tracks"])
            .addLayer(overlays["Data"]["Filtered Media selection"]);
            // .addLayer(overlays["Other layers"]["Australian Marine Parks - Reserves (2018, DoEE)"])
            //.addLayer(overlays["Data"]["DeploymentTrack"]);

        // get campaigns
        _this.reload_markers("campaign");

        // Add additional layers from API call
        $.getJSON('/api/maplayer?q={"order_by":[{"field":"id","direction":"asc"}]}', function(data){
            $(data.objects).each(function(i,layer){
                layer.properties.pane = (layer.hasOwnProperty('pane') && layer.pane !== null) ? layer.pane : "BGTiles";
                overlays["Other layers"][layer.name] = L.sqlayer(layer.layertype, layer.baseurl, layer.properties);
            });
        }).always(function(){
            // Add map controls
            _this.map.addControl(new L.control.groupedLayers(baselayers, overlays, {exclusiveGroups: ["Other layers"], groupCheckboxes:false, position: "topleft"}));
            _this.map.addControl(L.control.scale());  // scale controls
            //_this.map.addControl(new L.control.groupedLayers(baselayers, overlays, {exclusiveGroups: ["Other layers"], groupCheckboxes:false, position: "topleft"}));
            //_this.map.addControl(new L.control.layers(baselayers, overlays, {autoZIndex: false, position: "topleft"}));
            //_this.map.addControl(new L.control.layers(external, {}, {autoZIndex: false, position: "topleft"}));  // only allow single layer selected at a time
        });

        return _this;
    };


    this.reload_markers = function(layer_name, reset_bbox) {

        if (typeof reset_bbox === "undefined") reset_bbox = true;
        console.log("reload_markers", layer_name, reset_bbox);

        var layer = _this.marker_data[layer_name];
        var q = layer.build_filter(_this.filters);

        var url = layer.url+"?q="+JSON.stringify(q);
        if (q === null) {
            console.log("NOT RELOADING MAP LAYER '"+layer_name+"': null query");
            layer.layer.clearLayers();
            layer.last_url = null;
            return _this;
        }
        if (!_this.$container.is(":visible")){
            console.log("NOT RELOADING MAP LAYER '"+layer_name+"': container not visible");
            return _this;
        }
        else if (url === layer.last_url) {
            console.log("NOT RELOADING MAP LAYER '"+layer_name+"': query has not changed");
            return _this;
        }
        layer.last_url = url;
        console.log("RELOADING MAP LAYER '"+layer_name+"'");

        //_this.$progress_bar.css('width', "0%");
        _this.options.$isloading.show();

        layer.layer.clearLayers();
        if(layer_xhrs[layer_name]) layer_xhrs[layer_name].abort();  // cancel request if active
        layer_xhrs[layer_name] = ajax.get(url, {success: function (data) {
            // console.debug(data);
            var marker = null;
            $(data.objects).each(function (i, el) {
                marker = create_marker(el, layer);  //layer.marker(el, layer.popup);
                //console.debug(el, layer, marker);
                if (marker != null)
                    layer.layer.addLayer(marker);
            });
            if(data.objects.length ===1) marker.openPopup();  // if there is only one marker in the layer, open a popup
        }});
        /*
        .done(function(){
            _this.options.$isloading.hide(1000);
            console.log("Done loading!", layer_name, reset_bbox);
            if (reset_bbox !== false) {
                _this.map_reset_bounds(reset_bbox);
            }
        });
        */

        // check all xhr objects to reset view once complete
        //var layer_xhrs = $.map(_this.marker_data, function (el, key) {return el['xhr']});
        $.when.apply($, Object.values(layer_xhrs)).done(function(){
            console.log("Done loading!");
            _this.options.$isloading.hide(1000);
            if (reset_bbox != false) {
                console.log("RESETTING BBOX", layer_name, reset_bbox);
                _this.map_reset_bounds(reset_bbox);  // this seems to be out of scope and reset_bbox is always true?!
                //_this.map_reset_bounds(_this.marker_data[layer_name]._trigger_reset_bbox);
            }
        });

        return _this;
    };

    this.map_reset_size = function() {
        if (_this.$container.is(":visible"))
            _this.map.invalidateSize();

        return _this;
    };

    this.map_reset_bounds = function(bbox) {
        console.log("map_reset_bounds", bbox);
        if (typeof bbox === "undefined") bbox = true;
        var bounds = _this.default_bounds;      // default bounds
        if (bbox === false)                     // if bbox is explicitly false, then do not reset bounds
            return null;
        else if (bbox !== true)                 // if actual bbox is supplied, then use it
            bounds = bbox;
        else if (_this.marker_data.deployment.layer.getBounds().isValid())  // use deployments bbox if valid
            bounds = _this.marker_data.deployment.layer.getBounds();
        else if (_this.marker_data.campaign.layer.getBounds().isValid()) {  // use campaigns bbox if valid
            bounds = _this.marker_data.campaign.layer.getBounds();
            // todo: hacky having control set here - rather use a custom button on map to call reset view
            if (_this.default_bounds === null) {   // set default bounds to campaign bounds
                _this.default_bounds = bounds;
                _this.map.addControl(new L.Control.ZoomMin({minBounds:_this.default_bounds}));
            }
        }
        _this.map.fitBounds(bounds);
        return bounds;
        //else _this.map.setView([-23.126424, 134.469276], 4);
    };


    this.update_filters = function(field, value, opts) {
        console.log("Updating filter: "+field+": "+value);
        var was_removed = false;
        var options = {
            replace: false,     // true to clear and replace filter
            toggle: false       // true to clear if already added
        };
        $.extend(options, opts);

        var already_added = (_this.filters[field].indexOf(value) !== -1);

        if (already_added && options.toggle) {
            _this.filters[field] = _this.filters[field].filter(function (e) {return e !== value});
            was_removed = true;
        }
        else if (options.replace)
            _this.filters[field] = Array.isArray(value) ? value : [value];  // if replace, check if array otherwise create array
        else
            _this.filters[field].push(value);

        return was_removed;
    };

    this.deployment_toggle_select = function(deployment_id, reset_bbox) {
        console.log("deployment_toggle_select", deployment_id, reset_bbox);
        if (typeof reset_bbox === "undefined") reset_bbox = false;
        // if (_this.filters["deployment_ids"].indexOf(deployment_id) === -1) {    // if not found, add it
        //     _this.filters["deployment_ids"].push(deployment_id);
        // }
        // else {                                                      // if found, remove it, and always reload
        //     _this.filters["deployment_ids"] = _this.filters["deployment_ids"]   // if found, remove it
        //         .filter(function (e) {return e !== deployment_id});
        //     _this.reload_markers("deployment", reset_bbox);        // reload layer and only reset bounds if requested
        // }
        _this.options.update_filters("deployment_ids", deployment_id, {replace: false, toggle:true});
        _this.reload_markers("deployment", reset_bbox);        // reload layer and only reset bounds if requested
    };

    this.campaign_select = function(campaign_id, bbox) {
        // var removed = _this.update_filters("campaign_ids", campaign_id, {replace: true, toggle:true});
        // if (removed) _this.reload_markers("deployment");       // reload layer and reset to default extent
        // else _this.reload_markers("deployment", bbox);  // reload layer and reset bbox to campaign extent
        //var removed = _this.options.update_filters("campaign_ids", campaign_id, {replace: true, toggle:true});
        var removed = _this.options.update_filters("campaign_ids", campaign_id, {replace: false, toggle:true});
        _this.reload_markers("deployment", (removed && bbox !== true) ? false : bbox);
    };

    this.deployment_show_thumbs = function(deployment_id) {
        var url = _this.options.url_deployment_images.replace("{id}",deployment_id);
        open_modal(url, {modal_id:'media-modal-preview', modal_class:'modal-lg', load_func: load_content.paginated_html,
            onshown: function(){
                $(this).find(".media-thm")
                    .click(function() {
                        dataset_modal.media.view($(this).data('media_id'));
                    })
                    .hover(function() {console.log("Mouse entered: ", $(this));});
            }
        });
    };

    this.campaign_show_thumbs = function(campaign_id) {
        var url = _this.options.url_campaign_images.replace("{id}",campaign_id);
        open_modal(url, {modal_id:'media-modal-preview', modal_class:'modal-lg', load_func: load_content.paginated_html,
            onshown: function(){
                $(this).find(".media-thm")
                    .click(function() {
                        dataset_modal.media.view($(this).data('media_id'));
                    })
                    .hover(function() {console.log("Mouse entered: ", $(this));});
            }
        });
    };

    /*
    this.deployment_show_track = function(baseurl, properties, minzoom, latlon, layertype){
        var deployment_track_layer = overlays.Data["Deployment Track"];
        deployment_track_layer.setUrl(baseurl, true);           // update url, don't redraw
        deployment_track_layer.setParams(properties, true);     // update properties, don't redraw
        // TODO: redraw() doesn't seemt o work. See if it is possible to update layer without needing to add/remove it
        // overlays.Data.DeploymentTrack.redraw();
        if (_this.map.hasLayer(deployment_track_layer))
            _this.map.removeLayer(deployment_track_layer);
        _this.map.addLayer(deployment_track_layer);
        if (minzoom)
            _this.map.setView(latlon, minzoom);
    };
    */


    function update_popup(id, popup, url) {
        url = url.replace("{id}",id);
        popup.setContent("<i class='fa fa-window-minimize fa-spin'></i> loading...");
        $.get(url, function(html){
            var $html = $(html);
            $html.find(".map-action").click(function(el){map_action($(this).data())});
            popup.setContent($html[0]);
            popup.update();
        }, "html").fail(function(jqXHR, textStatus, errorThrown ) {
            popup.setContent("<h5 class='text-danger'><i class='fa fa-exclamation-triangle'></i> Error loading content.</h5>");
            console.log(jqXHR);
        });
    }

    function map_action(params) {
        if (params.action === "campaign_select") {
            _this.campaign_select(params.id, true);
        }
        else if (params.action === "deployment_toggle_select") {
            _this.deployment_toggle_select(params.id, false);
        }
        else if (params.action === "deployment_show_thumbs") {
            _this.deployment_show_thumbs(params.id);
        }
        else if (params.action === "campaign_show_thumbs") {
            _this.campaign_show_thumbs(params.id);
        }
        else if (params.action === "map_reset_bounds") {
            _this.map_reset_bounds(params.bbox);
        }
        /*
        else if (params.action === "deployment_show_track") {
            _this.deployment_show_track(params.layer.baseurl, params.layer.properties, params.layer.minzoom, params.latlon, params.layer.layertype);
        }
        */
    }

    function create_marker(el, layer_params){
        if (el.latlon == null || el.latlon.lat == null || el.latlon.lon == null) {
            console.debug("NO LATLON! ", el);
            return null;
        }
        var marker = layer_params.marker(el);
        var popup = L.popup();
        marker.bindPopup(popup);
        marker.on('popupopen', function(){
            update_popup(el.id, popup, layer_params.popup);
        });
        // marker.on('click', function (e) {
        //     this.openPopup();
        // });
        return marker;
    }

    /*
    function build_campaign_filter(options) {
        var layer = _this.marker_data.campaign;

        layer.q.filters = [];
        if (options.platform_ids.length > 0)
            layer.q.filters.push({name:"deployments",op:"any",val:{name:"platform_id",op:"in",val:options.platform_ids.slice()}});
            //layer.q.filters.push({"name":"platform_id","op":"in","val":options.platform_ids.slice()});

        return layer.q;
    }

    function build_deployment_filter(options) {
        var layer = _this.marker_data.deployment;

        var filts = [];
        if (options.campaign_ids.length > 0)
            filts.push({name:"campaign_id",op:"in",val:options.campaign_ids.slice()});
        if (options.deployment_keys.length > 0)
            filts.push({name:"key",op:"in",val: options.deployment_keys.slice()});
        if (options.deployment_ids.length > 0)
            filts.push({name:"id",op:"in",val: options.deployment_ids.slice()});

        layer.q.filters = [{name:"is_valid",op:"eq",val:true}, {or:filts}];

        console.log(layer.q);

        return (filts.length <= 0) ? null : layer.q;
    }

     */

    return _this;
}




function ExploreFilters ($container, options) {
    if (typeof options === "undefined") options = {};

    this.$container = $container;

    this.options = {
        template_campaign_list: "models/campaign-deployment/get_campaign_list.html",
        template_deployment_list: "models/campaign-deployment/get_deployment_list.html",
        search_delay: 200,
        search_min_chars: 2,
        $is_loading: $('<i class="fa fa-circle-o-notch fa-spin fa-2x"></i>'),
        on_update_filters: function(filters){},
        on_campaign_select: function(campaign_id){},
        on_deployment_select: function(deployment_id){},
        on_platform_select: function(deployment_id){},
        on_update_pose_filters: function(filters){}
    };

    this.api = {
        campaigns: {url: "/api/campaign-deployment/campaign"},
        deployment: {url: "/api/campaign-deployment/deployment"},
        campaign_deployment: {url: "/api/campaign-deployment"},
        platforms: {url: '/api/platform'},
        platform_ids: {url: '/api/platform?q={q}&results_per_page=1000', name:"id", op:"in"},
        campaign_ids: {url: '/api/campaign?q={q}&results_per_page=1000', name:"id", op:"in"},
        deployment_ids: {url: '/api/deployment?q={q}&results_per_page=1000', name:"id", op:"in"},
        deployment_keys: {url: '/api/deployment?q={q}&results_per_page=1000', name:"key", op:"in", store_filt: 'deployment_ids', store_field: 'id'}
    };

    this.elements = {
        $deployment_list_container: $container.find("#deployment-list-filter>.filter-list"),
        $deployment_search_input: $container.find('#deployment-list-filter .deployment-search'),
        $platform_select: $container.find("#platform-list-filter>select.filter-list"),
        $platform_filter_info: $container.find("#platform-list-filter>select.filter-info"),
        $campaign_filter_info: $container.find("#deployment-list-filter>.filter-info"),
        $deployment_filter_info: $container.find("#deployment-list-filter>.filter-info"),
        $alt_filter_info: $container.find("#alt-filter>select.filter-info"),
        $dep_filter_info: $container.find("#dep-filter>select.filter-info"),
        $ts_filter_info: $container.find("#timestamp-filter>select.filter-info"),
        $pose_filters: $container.find(".pose-filter")
    };

    this.filters = {
        platform_ids: [],
        campaign_ids: [],
        campaign_keys: [],
        deployment_ids: [],
        deployment_keys: [],
        media_collection_ids: [],
        dep_max: null,
        dep_min: null,
        alt_max: null,
        alt_min: null,
        ts_max: null,
        ts_min: null,
        // lat_max:null,
        // lat_min:null,
        // lon_max:null,
        // lon_min:null
    };


    // this.$deployment_list_container = $container.find(".filter-list");
    $.extend(this.options, options);

    var _this = this;
    // var $dpl_list_items = _this.elements.$deployment_list_container.find(".deployment-ul li");
    // var $dpl_list_uls = _this.elements.$deployment_list_container.find(".campaign-deployment-list");
    //var $deployment_search_input = _this.$container.find('.deployment-search');
    var ajax_xhr = null;
    var search_timeout = null;


    //this.filters = (_this.options.explore_map) ? _this.options.explore_map.filters : {};
    //_this.options.explore_map.filters = _this.filters;


    this.   init = function(init_filters) {
        _this.reload("campaign");

        // convert deployment_keys into deployment_ids
        if (init_filters.hasOwnProperty('deployment_keys')) {
            var q = {filters:[{name:'key',op:'in',val:init_filters.pop('deployment_keys')}]};
            if (!init_filters.hasOwnProperty('deployment_ids')) init_filters.deployment_ids = [];
            $.getJSON("/api/deployment/map?q="+JSON.stringify(q), function(data){
                $.each(data.objects, function(i, o){
                    init_filters.deployment_ids.push(o.id);
                    _this.deployment_select(o.id);
                });
            });
        }

        $.extend(_this.filters, init_filters);

        console.log(_this.filters);

        // show list if clicked on show element
        _this.$container.find('.deployment-show').on('click', function () {
            _this.show();
        });

        // hide list if clicked outside
        $(document).mouseup(function (e) {
            if (!_this.elements.$deployment_list_container.is(e.target) // if the target of the click isn't the container...
                    && !_this.$container.is(e.target)  // if target isn't the search box
                    && _this.elements.$deployment_list_container.has(e.target).length === 0) {  // ... nor a descendant of the container
                _this.elements.$deployment_list_container.hide();
            }
        });

        // handle search
        _this.elements.$deployment_search_input.keyup(function (e) {
            var val = _this.elements.$deployment_search_input.val().toLowerCase().trim();  // split keywords by space
            search_timeout = setTimeout(function(){
                if (val.length >= _this.options.search_min_chars && e.which !== 0)   // if it's greater than 3 chars and an actual character was triggered, do search
                    _this.reload("search", val);
                else if (val === "")                    // if search is cleared, reload campaigns
                    _this.reload("campaign");
            }, _this.options.search_delay);
        });

        // initialise platform select
        /*
        _this.elements.$platform_select.APISelect({
            url: _this.apiurl.platforms,
            itemname: {name: ['name'], info: []},
            selected: _this.filters.platform_ids,
            nonSelectedText: "Filter by Platform",
            labeltxt: "Platform",
            success: function(){ _this.elements.$platform_select.trigger("change")}
        }).change(function(){
            console.debug(_this.filters);
            var platform_ids =  _this.elements.$platform_select.val()
                ?  _this.elements.$platform_select.val()
                : [];
            _this.update_filters("platform_ids",platform_ids,{replace:true});
            _this.options.on_platform_select(platform_ids);
        });
        */
        _this.elements.$platform_select.ajax_select(_this.api.platforms.url, {
            template_option: '<div class="title">[[name]]</div><div class="description">[[description]]</div>',
            placeholder: "Filter by platform (showing all by default)",
            build_query: function(search_term) {
                var q = {filters: [], order_by: [{field:"name",direction:"asc"}]}
                if (search_term) {
                    $(search_term.split(/(\s+)/)).each(function(i,s){
                        if (s.trim().length)
                            q.filters.push({name:"name",op:"ilike",val:'%'+s+'%'});
                    });
                }
                return q;
            }
        }).change(function(){
            console.debug(_this.filters);
            var platform_ids =  _this.elements.$platform_select.val()
                ?  _this.elements.$platform_select.val()
                : [];
            _this.update_filters("platform_ids",platform_ids,{replace:true});
            _this.options.on_platform_select(platform_ids);
        });

        // pose filters
        _this.elements.$pose_filters.on("change", function(){
            var field = $(this).attr('name');
            var value = $(this).val();
            _this.update_filters(field, value, {replace: true});
            _this.options.on_update_pose_filters(field, value);
        });

        return _this;
    };

    this.build_urls = {
        search: function(search_term) {
            var q = {order_by:[{field:"name",direction:"asc"}], filters:[]};
            if (_this.filters.platform_ids.length > 0)
                q.filters.push({name:"platform_id",op:"in",val:_this.filters.platform_ids});
            return _this.api.campaign_deployment.url+'?q='+JSON.stringify(q)+'&s='+encodeURIComponent(search_term);
            // var q = {order_by:[{field:"name",direction:"asc"}], disjunction:true, filters:[
            //         {name:"name",op:"ilike",val:"%25"+search_term+"%25"},
            //         {name:"campaign",op:"has",val:{name:"name",op:"ilike",val:"%25"+search_term+"%25"}}]};
            // return _this.apiurl.campaign_deployment+'?q='+JSON.stringify(q);
        },
        campaign: function() {
            var q = {order_by:[{field:"name",direction:"asc"}],filters:[]};
            if (_this.filters.platform_ids.length > 0)
                q.filters.push({name:"deployments",op:"any",val:{name:"platform_id",op:"in",val:_this.filters.platform_ids}});
            return _this.api.campaigns.url+'?q='+JSON.stringify(q);
        },
        deployment: function (campaign_id) {
            var q = {order_by:[{field:"name",direction:"asc"}],filters:[{name:"campaign_id",op:"eq",val:campaign_id},{name:"is_valid",op:"eq",val:true}]};
            if (_this.filters.platform_ids.length > 0)
                q.filters.push({name:"platform_id",op:"in",val:_this.filters.platform_ids});
            return _this.api.deployment.url+'?q='+JSON.stringify(q);
        }
    };

    this.reload = function(resource, search_id) {
        if (typeof resource === "undefined") resource = "campaign";

        var url = _this.build_urls[resource](search_id)+"&template="+_this.options.template_campaign_list;
        _this.elements.$deployment_list_container.html(_this.options.$is_loading);
        if (ajax_xhr) ajax_xhr.abort();
        ajax_xhr = _this.load_list_content(url, _this.elements.$deployment_list_container);
    };

    this.update_filters = function(field, value, opts) {
        console.log("Updating filter: "+field+": "+value);
        var was_removed = false;
        var options = {
            replace: false,     // true to clear and replace filter
            toggle: false      // true to clear if already added
        };
        $.extend(options, opts);

        if (Array.isArray(_this.filters[field])) {
            var already_added = (_this.filters[field].indexOf(value) !== -1);

            if (already_added && options.toggle) {
                _this.filters[field] = _this.filters[field].filter(function (e) {
                    return e !== value
                });
                was_removed = true;
            }
            else if (options.replace)
                _this.filters[field] = Array.isArray(value) ? value : [value];  // if replace, check if array otherwise create array
            else if (!already_added)
                _this.filters[field].push(value);
        }
        else {
            if (options.toggle && _this.filters[field] === value)
                _this.filters[field] = null;
            else
                _this.filters[field] = value;
        }

        update_filter_info(field, (was_removed) ? null : value);

        _this.options.on_update_filters(_this.filters);

        return was_removed;
    };

    function update_filter_info(field, value) {
        console.log("update_filter_info field:"+field+" value:"+value);
        var $info = $container.find("#filtinfo-"+field).html("");
        var on_remove = function(){};
        if (field==="deployment_ids") {
            on_remove = function () {
                _this.deployment_select($(this).data("id"))
            };
            if (_this.filters.deployment_ids.length > 0)
                _this.elements.$pose_filters.prop("disabled", false);
            else
                _this.elements.$pose_filters.prop("disabled", true).val("").trigger("change");
        }
        else if(field==="campaign_ids")
            on_remove = function(){_this.campaign_deployments_map($(this).data("id"))};
        else if (field==="platform_ids") {
            on_remove = function(){
                /*
                var values = _this.elements.$platform_select.val().filter(function(e) { return e !== $(this).data("id") });
                _this.elements.$platform_select.val('').ajax_select_val(values);
                //_this.elements.$platform_select.multiselect('deselect', $(this).data("id"));
                _this.elements.$platform_select.trigger("change");
                // update_filter_info(field, value);
                */
            }
        }
        else {  // assume pose filter
            on_remove = function(){
                _this.elements.$pose_filters.filter("[name='"+field+"']").val("").trigger("change");
            };
        }

        if (_this.api.hasOwnProperty(field)) {
            var q = {filters:[{name: _this.api[field].name, op: _this.api[field].op, val: _this.filters[field]}]};
            get_filt_info(_this.api[field].url, q, $info, on_remove);
        }
        else {
            if (value)
                $info.append(create_filter_control(field+":"+value, value, on_remove));
        }

        function get_filt_info(url, q, $info, on_remove) {
            url = url.replace("{q}", JSON.stringify(q));
            $.getJSON(url, function(data){
                $(data.objects).each(function(i,el){
                    $info.append(create_filter_control(el.name, el.id, on_remove, el));
                });
            });
        }
    }

    function create_filter_control(title, id, on_remove, opts) {
        var options = {color: "#999", tooltip: null};
        $.extend(options, opts);
        //console.log(options);
        var $remv = $('<a href="javascript:void(0)" tabIndex="-1" data-id="'+id+'" class="clear-filter" title="Remove filter"><i class="fa fa-times"></i></a>');
        var $title = $('<span class="filter-info-text" title="'+title+'">'+title+'</span>');
        var $ctl = $('<span class="badge" style="background-color: '+options.color+'">').append($remv, $title);
        $remv.click(on_remove);
        return $ctl
    }


    this.campaign_deployments_show = function(campaign_id, toggle){
        if (typeof toggle === "undefined") toggle = true;
        var $dpl_list_parent = _this.elements.$deployment_list_container.find('#cmp-lst-'+campaign_id);
        var $dpl_toggle_icon = $dpl_list_parent.find("i.dpl-lst-toggle");

        var $dpl_list_container = $dpl_list_parent.find("ul.deployment-ul");
        if (!$dpl_list_container.is(":visible") || !toggle) {
            var url = _this.build_urls.deployment(campaign_id) + "&template=" + _this.options.template_deployment_list;
            _this.load_list_content(url, $dpl_list_container);
            $dpl_list_container.show();
            $dpl_toggle_icon.removeClass("fa-plus-square").addClass("fa-minus-square");
        }
        else {
            $dpl_list_container.hide();
            $dpl_toggle_icon.removeClass("fa-minus-square").addClass("fa-plus-square");
        }
    };

    this.campaign_deployments_map = function(campaign_id) {
        //_this.options.explore_map.campaign_select(campaign_id);
        _this.options.on_campaign_select(campaign_id);
        _this.campaign_deployments_show(campaign_id, false);
    };

    this.deployment_select = function(deployment_id) {
        //_this.options.explore_map.deployment_toggle_select(deployment_id, true);
        _this.options.on_deployment_select(deployment_id);
        _this.update_deployment_icon(deployment_id);
        console.log("SELECT DEPLOYMENT ID: "+deployment_id);
    };

    this.show = function() {
        _this.elements.$deployment_list_container.show();
        _this.update_deployments_select_status();
    };

    this.update_deployments_select_status = function() {
        console.log("UPDATE SELECT STATUS");
        _this.elements.$deployment_list_container.find(".dpl-select").removeClass("fa-check-square").addClass("fa-square");
        $(_this.filters.deployment_ids).each(function(i, deployment_id){
            _this.update_deployment_icon(deployment_id, true);
        });
    };



    this.load_list_content = function(url, $list_container) {
        return $.get(url+"&template="+_this.options.template_campaign_list, function(html){
            $list_container.html(html);
            $list_container.find("[rel='list_action']").click(function(e){
                list_action($(this).data());
                e.preventDefault();
            });
            _this.update_deployments_select_status();
        }, "html").fail(function( jqXHR, textStatus, errorThrown ){
            if (textStatus === "abort")
                $list_container.html(_this.options.$is_loading);
            else {
                $list_container.html(
                    '<span class="text-danger"><b><i class="fa fa-exclamation-triangle"></i> Unable to load list:</b> '
                    + jqXHR.responseText + '</span>'
                );
                console.log("Error loading content: "+url, jqXHR, textStatus, errorThrown);
            }
        });
    };

    this.update_deployment_icon = function(deployment_id, force_select) {
        if (typeof force_select === "undefined") force_select = false;
        var $dpl_container = _this.elements.$deployment_list_container.find("#cmp-lst-dpl-"+deployment_id);
        var $dpl_select_icon = $dpl_container.find("i.dpl-select");
        if (force_select || _this.filters.deployment_ids.indexOf(deployment_id) !== -1)
            $dpl_select_icon.removeClass("fa-square").addClass("fa-check-square");
        else
            $dpl_select_icon.removeClass("fa-check-square").addClass("fa-square");
    };

    function list_action(params) {
        if (params.action==="campaign_deployments_map") {
            _this.campaign_deployments_map(params.id);
        }
        else if (params.action==="campaign_deployments_show") {
            _this.campaign_deployments_show(params.id);
        }
        else if (params.action === "deployment_select") {
            _this.deployment_select(params.id);
        }
    }

    return _this;
}

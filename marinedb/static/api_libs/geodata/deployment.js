function DeploymentsMap ($container) {

    this.$container = $container;
    this.q = "";
    this.total_pages = 1;
    this.needs_refresh = true;

    this.$progress_bar = $("<div id='progress-bar'></div>");
    this.$loading = $("<div id='progress'></div>").append(this.$progress_bar);
    this.$container.append(this.$loading);

    var _this = this;

    var map = new L.map($container.attr("id"), {zoomControl:false, minZoom: 2});

    this.default_bounds = [[7.36246686553575, 187.03125000000003],[-48.10743118848039, 81.91406250000001]];

    this.map = map;
//    $container.height($container.parent().height());
//    $container.width($container.parent().width());

    var baselayers = {
        "ESRI Oceans": new L.tileLayer('https://services.arcgisonline.com/ArcGIS/rest/services/Ocean_Basemap/MapServer/tile/{z}/{y}/{x}',
            {maxZoom: 18, maxNativeZoom: 10,attribution:"ESRI Oceans"}),
        "ESRI Imagery": new L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
            {maxZoom: 18, maxNativeZoom: 18,attribution:"ESRI Imagery"}),
        "OpenStreetMap": new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            {maxZoom: 18, maxNativeZoom: 18,attribution:"OpenStreetMap"}),
        "eAtlas Bright Earth": L.tileLayer.wms("https://maps.eatlas.org.au/maps/gwc/service/wms",
            {service:'WMS',request:'GetMap',layers:'ea-be:World_Bright-Earth-e-Atlas-basemap',format:'image/jpeg',transparent:false,version:'1.1.1',srs:'EPSG:900913', attribution:"e-Atlas"}),
        "GEBCO": L.sqlayer("tileLayer.wmsInfo", "https://www.gebco.net/data_and_products/gebco_web_services/web_map_service/mapserv",
            {service:'WMS',request:'GetMap',layers:'gebco_latest',format:'image/png',transparent:true,version:'1.1.1',srs:'EPSG:4326', attribution:"GEBCO"})
        //"Google": new L.Google('ROADMAP'),
        //"Satallite": new L.Google('HYBRID')
    };
    var overlays = {};
    overlays["Data"] = {
        // Clustering
        // TODO: LOOK AT http://bl.ocks.org/gisminister/10001728
        //       - PIE chart cluster icons
        "Deployments": new L.markerClusterGroup({
            chunkedLoading: true,
            chunkInterval: 300,
            maxClusterRadius: 40,
            chunkProgress: function(processed, total, elapsed, layersArray){
                _this.$progress_bar.css('width', Math.round(processed/total*100) + '%');
            },
            iconCreateFunction: function (cluster) {
                // custom cluster color based on mode of colours of markers
                //console.log(cluster.getAllChildMarkers());
                //var cluster_color = "#cccccc"; //get_cluster_color(cluster.getAllChildMarkers());
                var cluster_color = get_cluster_color(cluster.getAllChildMarkers());
                return new L.DivIcon({ html: '<div style="background-color:' + cluster_color + '"><span><b>' + cluster.getChildCount() + '</b></span></div>', className: 'marker-cluster', iconSize: new L.Point(40, 40) });
            }
        })
        // No clustering
        // "Deployments": new L.featureGroup(),
    };

    overlays["Other layers"] = {
        // TODO: these should be added dynamically from database instance
        "None": new L.tileLayer(''),
        /*
        //http://services.ga.gov.au/gis/services/Bathymetry_Derivatives/MapServer/WMSServer?LAYERS=Hillshaded_Bathymetry_Topography&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image%2Fpng&SRS=EPSG%3A3857&BBOX=13775786.98375,-5009377.085,14401959.119375,-4383204.949375&WIDTH=256&HEIGHT=256
        "Australian Marine Parks": L.sqlayer("geoJSON.ajax", null, {
            style:{color:"black", "weight":1, fillColor:"green", fillOpacity:0.2},
            pane: map.createPane("geojson_pane"),
            url: "/static/json/final_commonwealth_reserves_network_proposal_2018-4dp.geojson",
            attribution:"GlobalArchive"
        }),
        */
        //https://geoserver.imas.utas.edu.au/geoserver/seamap/wms?service=WMS&request=GetMap&layers=SeamapAus_BOUNDARIES_AMP2018_ZONE_X_RES&styles=&format=image%2Fpng&transparent=true&version=1.1.1&FILTER=&width=256&height=256&srs=EPSG%3A4326&bbox=129.375,-33.75,135,-28.125
        "Australian Marine Parks - Reserves (2018, DoEE)": L.sqlayer("tileLayer.wmsInfo", "https://geoserver.imas.utas.edu.au/geoserver/seamap/wms",
            {service:'WMS',request:'GetMap',layers:'SeamapAus_BOUNDARIES_AMP2018_ZONE_X_RES',format:'image/png',transparent:true,version:'1.1.1',width:256,height:256,srs:'EPSG:4326',attribution:"Australian Government Department of the Environment and Energy"}),
        //https://geoserver.imas.utas.edu.au/geoserver/wms?service=WMS&request=GetMap&layers=seamap%3ASeamapAus_CAPAD_MAP&styles=&format=image%2Fpng&transparent=true&version=1.1.1&width=256&height=256&srs=EPSG%3A4326&bbox=140.625,-19.6875,143.4375,-16.875
        "Collaborative Australian Protected Areas Database (2016, DRAFT, DoEE)": L.sqlayer("tileLayer.wmsInfo", "https://geoserver.imas.utas.edu.au/geoserver/wms",
            {service:'WMS',request:'GetMap',layers:'seamap:SeamapAus_CAPAD_MAP',format:'image/png',transparent:true,version:'1.1.1',width:256,height:256,srs:'EPSG:4326',attribution:"Australian Government Department of the Environment and Energy"}),
        //https://thredds.imas.utas.edu.au/ncwms/wms?service=WMS&request=GetMap&layers=SeamapAus_Bathymetry_National_50m%2FDEPTH&styles=&format=image%2Fpng&transparent=true&version=1.1.1&height=256&width=256&srs=EPSG%3A4326&bbox=156.09375,-30.9375,157.5,-29.53125
        "Bathymetry (IMAS)": L.sqlayer("tileLayer.wmsInfo", "https://thredds.imas.utas.edu.au/ncwms/wms",
            {service:'WMS',request:'GetMap',layers:'SeamapAus_Bathymetry_National_50m/DEPTH',format:'image/png',transparent:true,version:'1.1.1',srs:'EPSG:4326',attribution:"IMAS"}),
        // https://www.cmar.csiro.au/geoserver/nerp/wms?service=WMS&request=GetMap&layers=nerp%3AALTT_31July2014&styles=&format=image%2Fpng&transparent=true&version=1.1.1&height=256&width=256&srs=EPSG%3A4326&bbox=-180,-67.5,-157.5,-45
        "Sea Surface Temperature (CSIRO, 1993-2013)": L.sqlayer("tileLayer.wmsInfo", "https://www.cmar.csiro.au/geoserver/nerp/wms",
            {service:'WMS',request:'GetMap',layers:'nerp:ALTT_31July2014',format:'image/png',transparent:true,version:'1.1.1',srs:'EPSG:4326',attribution:"CSIRO"}),
        // https://maps.marineparks.eatlas.org.au/maps/cmr/wms?service=WMS&request=GetMap&layers=cmr%3AAU_ALA_species_richness_2017&styles=&format=image%2Fpng&transparent=true&version=1.1.1&height=256&width=256&srs=EPSG%3A4326&bbox=-180,-67.5,-157.5,-45
        "Species Richness (ALA, 2017)": L.sqlayer("tileLayer.wmsInfo", "https://maps.marineparks.eatlas.org.au/maps/cmr/wms",
            {service:'WMS',request:'GetMap',layers:'cmr:AU_ALA_species_richness_2017',format:'image/png',transparent:true,version:'1.1.1',srs:'EPSG:4326',attribution:"eAtlas:ALA"}),
        //"Global Fisheries Landings (IMAS, 2014)": L.sqlayer("tileLayer.wmsInfo", "https://geoserver.imas.utas.edu.au/geoserver/wms",
        //    {service:'WMS',request:'GetMap',layers:'imas:Catch2014',format:'image/png',transparent:true,version:'1.1.1',attribution:"eAtlas:IMAS"}),
        //https://maps.marineparks.eatlas.org.au/maps/cmr/wms?service=WMS&request=GetMap&layers=cmr%3AAU_DOEE_KEF_2015&styles=&format=image%2Fpng&transparent=true&version=1.1.1&height=256&width=256&srs=EPSG%3A4326&bbox=112.5,-19.6875,115.3125,-16.875
        "Marine key ecological features (DoEE)": L.sqlayer("tileLayer.wmsInfo", "https://maps.marineparks.eatlas.org.au/maps/cmr/wms",
            {service:'WMS',request:'GetMap',layers:'cmr:AU_DOEE_KEF_2015',format:'image/png',transparent:true,version:'1.1.1',srs:'EPSG:4326',attribution:"Department of the Environment and Energy"}),
        //https://geoserver.imas.utas.edu.au/geoserver/wms?service=WMS&request=GetMap&layers=seamap%3AFINALPRODUCT_SeamapAus&styles=&format=image%2Fpng&transparent=true&version=1.1.1&height=256&width=256&srs=EPSG%3A4326&bbox=147.65625,-30.9375,149.0625,-29.53125
        "Benthic habitats (Seamap Aust.)": L.sqlayer("tileLayer.wmsInfo", "https://geoserver.imas.utas.edu.au/geoserver/wms",
            {service:'WMS',request:'GetMap',layers:'seamap:FINALPRODUCT_SeamapAus',format:'image/png',transparent:true,version:'1.1.1',srs:'EPSG:4326',attribution:"SeaMap Australia"}),
        //http://services.ga.gov.au/gis/services/Bathymetry_Derivatives/MapServer/WMSServer?LAYERS=Hillshaded_Bathymetry_Topography&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image%2Fpng&SRS=EPSG%3A3857&BBOX=13775786.98375,-5009377.085,14401959.119375,-4383204.949375&WIDTH=256&HEIGHT=256
        "Bathymetry shaded (Geoscience Aust.)": L.sqlayer("tileLayer.wmsInfo", "http://services.ga.gov.au/gis/services/Bathymetry_Derivatives/MapServer/WMSServer",
            {service:'WMS',request:'GetMap',layers:'Hillshaded_Bathymetry_Topography',format:'image/png',transparent:true,version:'1.1.1',srs:'EPSG:3857', attribution:"Geoscience Australia"}),
        //http://spatial.ala.org.au/geoserver/gwc/service/wms?LAYERS=ALA%3Ageo_feature&TRANSPARENT=TRUE&VERSION=1.1.1&SERVICE=WMS&REQUEST=GetMap&STYLES=&FORMAT=image%2Fpng&SRS=EPSG%3A900913&BBOX=14401959.12416,-4383204.9472001,15028131.259872,-3757032.8114881&WIDTH=256&HEIGHT=256
        "Geomorphic features (Geoscience Aust.)": L.sqlayer("tileLayer.wmsInfo", "http://spatial.ala.org.au/geoserver/gwc/service/wms",
            {service:'WMS',request:'GetMap',layers:'ALA:geo_feature',format:'image/png',transparent:true,version:'1.1.1',srs:'EPSG:900913',attribution:"Geoscience Australia"}),
        //https://geoserver.imas.utas.edu.au/geoserver/wms?service=WMS&request=GetMap&layers=seamap%3ASeamapAus_BOUNDARIES_MEOW4326_PROV&styles=&format=image%2Fpng&transparent=true&version=1.1.1&FILTER=&height=256&width=256&srs=EPSG%3A4326&bbox=0,45,45,90
        "Ecoregions of the World (WWF)": L.sqlayer("tileLayer.wmsInfo", "https://geoserver.imas.utas.edu.au/geoserver/wms",
            {service:'WMS',request:'GetMap',layers:'seamap:SeamapAus_BOUNDARIES_MEOW4326_PROV',format:'image/png',transparent:true,version:'1.1.1',srs:'EPSG:4326',attribution:"World Wildlife Fund"}),

        // AODN IMAGE LAYER
        //https://geoserver-123.aodn.org.au/geoserver/wms?LAYERS=imos%3Aauv_images_vw&STYLES=&SRS=EPSG%3A4326&FORMAT=image%2Fpng&TRANSPARENT=TRUE&TILED=true&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&CQL_FILTER=site_code%20like%20%27r20120902_220852_si_06_patch_dense2%27&BBOX=153.2015991211,-30.441741943359,153.20297241212,-30.440368652344&WIDTH=256&HEIGHT=256
        "Example deployment: r20120902_220852_si_06_patch_dense": L.sqlayer("tileLayer.wmsInfo", "https://geoserver-123.aodn.org.au/geoserver/wms",
            {service:'WMS',request:'GetMap',layers:'imos:auv_images_vw',format:'image/png',transparent:true, tiled:true,version:'1.1.1',srs:'EPSG:4326',attribution:"AODN", exceptions:"application/vnd.ogc.se_inimage",cql_filter:"site_code = 'r20120902_220852_si_06_patch_dense2'"}),
        //AODN track layer
        //https://geoserver-123.aodn.org.au/geoserver/wms?LAYERS=imos%3Aauv_tracks_vw&STYLES=&SRS=EPSG%3A4326&FORMAT=image%2Fpng&TRANSPARENT=TRUE&TILED=true&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&BBOX=150.23323059082,-35.832595825196,150.23391723633,-35.831909179688&WIDTH=256&HEIGHT=256
        "Example deployment track: r20120902_220852_si_06_patch_dense": L.sqlayer("tileLayer.wmsInfo", "https://geoserver-123.aodn.org.au/geoserver/wms",
            {service:'WMS',request:'GetMap',layers:'imos:auv_tracks_vw',format:'image/png',transparent:true, tiled:true,version:'1.1.1',srs:'EPSG:4326',attribution:"AODN", exceptions:"application/vnd.ogc.se_inimage",cql_filter:"site_code like 'r20120902_220852_si_06_patch_dense2' or site_code like 'r20150905_202410_10_wilson1and2'"}),

    };

    map.addControl(new L.control.groupedLayers(baselayers, overlays, {exclusiveGroups: ["Other layers"], groupCheckboxes:false, position: "topleft"}));
    //L.control.layers(baselayers, overlays, {autoZIndex: true, position: "topleft"}).addTo(map);
    //L.control.layers(external, {}, {autoZIndex: true, position: "topleft"}).addTo(map);  // only allow single layer selected at a time
    map.addControl(new L.Control.ZoomMin({minBounds:_this.get_bounds}));
    map.addControl(L.control.scale());
    map.addLayer(baselayers["eAtlas Bright Earth"]).addLayer(overlays["Data"]["Deployments"]);


    this.update_filter = function(options) {
        this.q = {filters:[]};

        if (options.hasOwnProperty('deployment_list') && options.deployment_list) {
            this.q.filters.push({"name": "id", "op": "in", "val": options.deployment_list});
        }
        if (options.hasOwnProperty('alt_range') ){
            if (options.alt_range.hasOwnProperty('max') && options.alt_range.max)
                this.q.filters.push({"name": "poses", "op": "any", "val": {"name": "alt", "op": "lt", "val": options.alt_range.max}});
            if (options.alt_range.hasOwnProperty('min') && options.alt_range.min)
                this.q.filters.push({"name": "poses", "op": "any", "val": {"name": "alt", "op": "gt", "val": options.alt_range.min}});
        }
        if (options.hasOwnProperty('dep_range')) {
            if (options.dep_range.hasOwnProperty('max') && options.dep_range.max)
                this.q.filters.push({"name": "poses", "op": "any", "val": {"name": "dep", "op": "lt", "val": options.dep_range.max}});
            if (options.dep_range.hasOwnProperty('min') && options.dep_range.min)
                this.q.filters.push({"name": "poses", "op": "any", "val": {"name": "dep", "op": "gt", "val": options.dep_range.min}});
        }
        if (options.hasOwnProperty('platform_list') && options.platform_list) {
            this.q.filters.push({"name": "campaign", "op": "has", "val": {"name":"platform_id","op":"in","val":options.platform_list}});
        }

        this.needs_refresh = true;
    };

    this.reload = function(q, page, results_per_page) {
        console.log("RELOAD!");

        if (q !== "" && typeof q !== 'undefined') _this.q = JSON.parse(decodeURIComponent(q));
        if (typeof page === 'undefined') page = 1;
        if (typeof results_per_page === 'undefined') results_per_page = 50000;
        _this.needs_refresh = false;


        var query = addParam("", "q", JSON.stringify(_this.q));
        query = addParam(query, "page", page);
        query = addParam(query, "results_per_page", results_per_page);

        _this.$progress_bar.css('width', "0%");
        _this.$loading.show();

        overlays["Data"]['Deployments'].clearLayers();
        $.getJSON("/api/deployment/map" + query, function (data) {
            _this.total_pages = data['total_pages'];
            page = data['page'];

            $(data.objects).each(function (i, deployment) {
                if (deployment.latlon) {
                    //console.log(L.latLng(deployment.latlon));
                    var marker = L.circleMarker(L.latLng(deployment.latlon), {
                        color: '#000',
                        fillColor: deployment.color,
                        fillOpacity: 0.7,
                        opacity: 0.9,
                        weight: 2,
                        fill: true,
                        radius: 7
                    });
                    var popup = L.popup().setContent("<i class='fa fa-spinner fa-pulse fa-4x'></i>");
                    marker.bindPopup(popup);
                    //marker.on('mouseover', function (e) {
                    marker.on('click', function (e) {
                        this.openPopup();
                        update_popup(deployment.id, popup);
                    });
                    overlays["Data"]["Deployments"].addLayer(marker);
                }
            });


            // if first page, go forth and call the rest!
            if (page == 1 && page < _this.total_pages) {
                //console.log("START LOOP")
                while (page < _this.total_pages) {
                    page++;
                    _this.reload(JSON.stringify(_this.q), page, results_per_page);
                    //console.log(page, total_pages);
                }
            }


        }).done(function () {
            if (page == _this.total_pages) {
                //console.log("Refresh!")
                reset_view();
                //var hash = new L.Hash(map);
                setTimeout(function(){
                    _this.$loading.hide(1000);
                }, 2000);
            }
        });
    };

    this.resetSize = function() {
        if (_this.$container.is(":visible"))
            _this.map.invalidateSize();
    };

    this.get_bounds = function() {
        if (overlays["Data"]["Deployments"].getBounds().isValid()) {
            var map_bounds = overlays["Data"]["Deployments"].getBounds();
            _this.default_bounds = map_bounds;
            return map_bounds;
        }
        return _this.default_bounds;
    };

    function addParam(qs, name, value) {
        var delim = (qs === "") ? "?" : "&";
        return (value) ? qs + delim + name +"="+ value : qs;
    }


    function reset_view() {
        map.fitBounds(_this.get_bounds());
    }

    function get_cluster_color(markers) {
        if (markers.length == 0) return null;
        var modeMap = {};
        var maxEl = markers[0].options.fillColor, maxCount = 0;
        for (var i = 0; i < markers.length; i++) {
            var el = markers[i].options.fillColor;
            if (!modeMap.hasOwnProperty(el)) modeMap[el] = 0;
            modeMap[el]++;
            if (modeMap[el] > maxCount) {
                maxEl = el;
                maxCount = modeMap[el];
            }
        }
        return maxEl;
    }

    function update_popup(id, popup) {
        //$.getJSON('/api/deployment/'+id+'/stats', function(data){
        var template = "models/deployment/get_map_popup.html";
        $.get('/api/deployment/'+id+'?template='+template, function(data){
            popup.setContent(data);
            popup.update();
        }, "html");
    }
}


function CampaignDeployments($searchbox){   //, success) {
    //var $searchbox = $("#deployment-list-filter");
    //var $container = ;
    this.$searchbox = $searchbox;
    this.$container = $searchbox.find(".filter-list");
    //this.success = success;
    this.q = "";
    this.needs_refresh = true;
    var _this = this;


    this.update_filter = function(options) {
        this.q = {"order_by":[{"field":"name","direction":"asc"}], filters:[]};

        if (options.hasOwnProperty('platform_list') && options.platform_list) {
            //this.q.filters.push({"name": "platform_id", "op": "in", "val": options.platform_list});
            this.q.filters.push({name:"deployments",op:"any",val:{name:"platform_id",op:"in",val:options.platform_list}});
        }

        this.needs_refresh = true;
    };


    this.reload = function (options) {  //, success) {

        var selected_ids = (options.hasOwnProperty('deployment_ids')) ? options.deployment_ids : [];
        var success = (options.hasOwnProperty('success')) ? options.success : null;

        $.getJSON('/api/campaign-deployments?q='+JSON.stringify(_this.q)+"&results_per_page=10000", function (data) {

            _this.$container.html("");
            $(data.objects).each(function (i, cmp) {
                var $campaign_list = $("<div class='campaign-deployment-list'></div>");
                var $deployment_list = $("<ul class='deployment-ul'></ul>");
                var $platform = (cmp.platform) ? $("<span class='deployment-info'>" + cmp.platform.name + "</span>") : "";
                $campaign_list.append($platform, $("<h2>" + cmp.name + "</h2>"), $deployment_list);
                $(cmp.deployments).each(function (j, dpl) {
                    var search_text = cmp.name + " " + dpl.name + " " + dpl.timestamp;
                    var $dpl_link = $("<a id='dpl-" + dpl.id + "' href='javascript:void(0)'><i class='fa fa-square deployment-bullet' style='color:" + dpl.color + "'></i> " + dpl.name + " <span class='deployment-info'>" + dpl.timestamp + "</span></a>");
                    //var li_active = ($.inArray(dpl.id, selected_ids) >= 0) ? "active" : "";
                    var $dpl_li = $("<li data-id='" + dpl.id + "' data-search_text='" + search_text + "'></li>")
                            .data('info', dpl)
                            .append($dpl_link);
                    $deployment_list.append($dpl_li);
                    $dpl_link.click(function (e) {
                        //console.log($(this).data("id"));
                        toggle_list_link($dpl_link);
                        // TODO: remove this dependency! Pass in click function as param
                        update_filters();
                    });
                    if ($.inArray(dpl.id, selected_ids) >= 0) toggle_list_link($dpl_link, true);
                });
                _this.$container.append($campaign_list);
            });

            _this.$searchbox.find('.deployment-show').on('click', function () {
                _this.$container.show()
            });


            // hide container if clicked outside of search box / container
            $(document).mouseup(function (e) {
                if (!_this.$container.is(e.target) // if the target of the click isn't the container...
                        && !_this.$searchbox.is(e.target)  // if target isn't the search box
                        && _this.$container.has(e.target).length === 0) {  // ... nor a descendant of the container
                    _this.$container.hide();
                }
            });

            // search deployments
            var $dpl_list_items = _this.$container.find(".deployment-ul li");
            var $dpl_list_uls = _this.$container.find(".campaign-deployment-list");
            var $search_input = _this.$searchbox.find('.deployment-search');
            $search_input.keyup(function () {
                var val = $search_input.val().toLowerCase().trim().split(" ");  // split keywords by space
                $dpl_list_uls.hide();
                $dpl_list_items.hide();
                $dpl_list_items.each(function (i, li) {
                    var text = $(li).data("search_text").toLowerCase();
                    var show = true;
                    for (var j in val)
                        show = show && (text.indexOf(val[j]) >= 0);  // check all keywords are in text
                    if (show)
                        $(li).show().parents(".campaign-deployment-list").show();
                });
            });

            _this.needs_refresh = false;

            // run success function when done
            //_this.success();
            if (success != null) success();
        });
    };

    function toggle_list_link($link, force_on) {
        if (typeof force_on === 'undefined')  force_on=false;

        if (!$link.parent('li').hasClass('active') || force_on) {
            $link.parent('li').addClass('active');
            $link.find('i.deployment-bullet').removeClass('fa-square').addClass('fa-check-square');
        }
        else {
            $link.parent('li').removeClass('active');
            $link.find('i.deployment-bullet').addClass('fa-square').removeClass('fa-check-square');
        }
    }
}

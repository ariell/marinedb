function Media($container, filters, options) {

    var _this = this;

    this.$container = $container;
    this.filters = filters;
    this.q = {};

    this.options = {
        apiurl_media: "/api/media",
        template_media_list: "models/media/get_many.html",
        //template_media_detail: "models/media/preview_single.html",
        on_reload: function(){},
        on_click: function(media_id) {
            dataset_modal.media.view(media_id, {oncompleted:function(data){
                _this.$container.trigger("reload"); // force-reload container
            }});
            /*
            open_modal('/api/media/'+media_id+'?template='+_this.options.template_media_detail, {
                modal_id:'media-modal', modal_class:'modal-xl modal-dark', oncompleted:function(data){
                    _this.$container.trigger("reload"); // force-reload container
                }
            });
            */
        },
        //$isloading: $('<div style="margin: 50px"><i class="fa fa-circle-o-notch fa-spin"></i> Loading...</div>'),
        order_by: [{field: "deployment_id", direction: "asc"}, {field: "timestamp_start", direction: "asc"}],
        thm_width_orig: 150,
        thm_rows: 15,
        thm_spacing: 6,
        thms_per_row_orig: 5,
        thms_selector: ".media-thm"
        //qsparam_selector:".qs-param"
        //reload_btn_selector:"#update-media-list-btn"
    };

    this.show_counts = true;

    $.extend(this.options, options);

    _this.current_url = null;

    var ajax_xhr = null;
    var popover_timer;
    var popover_delay = 500;
    var popover_xhr;
    //var query_params = {q:{},results_per_page:null, page:null};

    this.build_query = function(filters) {
        if (typeof filters === "undefined") filters = _this.filters;
        var f = [];

        //console.log(options);

        if (filters.hasOwnProperty('deployment_ids') && filters.deployment_ids.length>0) {
            f.push({"name": "deployment_id", "op": "in", "val": filters.deployment_ids});
        }
        if (filters.hasOwnProperty('alt_max') && filters.alt_max)
            f.push({"name": "poses", "op": "any", "val": {"name": "alt", "op": "lt", "val": filters.alt_max}});
        if (filters.hasOwnProperty('alt_min') && filters.alt_min)
            f.push({"name": "poses", "op": "any", "val": {"name": "alt", "op": "gt", "val": filters.alt_min}});
        if (filters.hasOwnProperty('dep_max') && filters.dep_max)
            f.push({"name": "poses", "op": "any", "val": {"name": "dep", "op": "lt", "val": filters.dep_max}});
        if (filters.hasOwnProperty('dep_min') && filters.dep_min)
            f.push({"name": "poses", "op": "any", "val": {"name": "dep", "op": "gt", "val": filters.dep_min}});
        if (filters.hasOwnProperty('ts_max') && filters.ts_max)
            f.push({"name": "poses", "op": "any", "val": {"name": "timestamp", "op": "lt", "val": filters.ts_max}});
        if (filters.hasOwnProperty('ts_min') && filters.ts_min)
            f.push({"name": "poses", "op": "any", "val": {"name": "timestamp", "op": "gt", "val": filters.ts_min}});
        if (filters.hasOwnProperty('media_collection_id') && filters.media_collection_id) {
            f.push({"name":"media_collections","op":"any","val":{"name":"id","op":"eq","val":filters.media_collection_id}});
        }
        if (filters.hasOwnProperty('platform_ids') && filters.platform_ids.length>0) {
            f.push(
                {"name": "deployment", "op": "has", "val":
                    {"name":"platform_id","op":"in","val":filters.platform_ids}});
        }

        return {filters:f, order_by: _this.options.order_by};
    };

    this.get_filters = function () {
        return (_this.q.hasOwnProperty("filters")) ? _this.q.filters : [];
    };

    this.reload = function(q, page, results_per_page, force_load) {

        if (!_this.$container.is(":visible") && !force_load){
            console.log("NOT RELOADING MEDIA: container not visible");
        }
        else {
            var thms_per_row = _this.options.thms_per_row_orig;
            var thm_width = _this.options.thm_width_orig;
            if (_this.$container.width()) {
                thms_per_row = Math.floor(_this.$container.width() / _this.options.thm_width_orig);
                //thm_width = _this.options.thm_width_orig - _this.options.thm_spacing +
                //    Math.floor((_this.$container.innerWidth()-(thms_per_row*_this.options.thm_width_orig))/thms_per_row);
                thm_width =  Math.floor(_this.$container.width()/thms_per_row) - _this.options.thm_spacing;
            }

            if (q === "" || typeof q === 'undefined')
                q = _this.build_query();
            if (page === "" || typeof page === 'undefined')
                page = '1';
            if (results_per_page === "" || typeof results_per_page === 'undefined')
                results_per_page = thms_per_row * _this.options.thm_rows;  // default to equal rows and cols

            //query_params = {q:q, page:page, results_per_page: results_per_page};

            _this.q = q;

            var url = _this.options.apiurl_media+'?q=' + JSON.stringify(q)+"&page="+page+"&results_per_page="+
                results_per_page+"&thm_width="+thm_width+"&template="+_this.options.template_media_list+
                ((_this.show_counts) ? "&include=counts" : "");


            if (url === _this.current_url) {
                console.log("NOT RELOADING MEDIA: query has not changed");
            }
            else {
                console.log("RELOADING MEDIA");
                //_this.$container.html(_this.options.$isloading);
                if (ajax_xhr) ajax_xhr.abort();
                // ajax_xhr = $.get(url, function (html) {
                //     _this.$container.html(html);
                //     update_links();
                //     _this.options.on_reload();
                // }, "html")
                ajax_xhr = load_content.paginated_html(url, _this.$container, function(){
                    update_links();
                    attach_popover();
                    //attach_reload_events();
                    _this.options.on_reload();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    if (textStatus === "abort")
                        _this.$container.html(_this.options.$is_loading);
                    else {
                        _this.$container.html(
                            '<span class="text-danger"><b><i class="fa fa-exclamation-triangle"></i> Unable to load:</b> '
                            + jqXHR.responseText + '</span>'
                        );
                        console.log("Error loading content: " + url, jqXHR, textStatus, errorThrown);
                    }
                });
                _this.current_url = url;
            }
        }
        return _this;
    };


    _this.get_params = function() {
        //return query_params;
        console.log(_this.$container.find(".media-list").data());
        return _this.$container.find(".media-list").data();
    };

    var update_links = function() {
        _this.$container.find(_this.options.thms_selector).click(function(e){
            var media_id = $(this).data("media_id");
            _this.options.on_click.call(this, media_id); // keep "this" context
        });
    };

    /*
    var attach_reload_events = function() {
        _this.$container.find(_this.options.reload_btn_selector).click(function(e){
            var params = _this.get_params();
            _this.reload(params.q, params.page, params.results_per_page)
        });
    };
    */

    // Handle popover hovering
    var attach_popover = function() {
        $container.find(_this.options.thms_selector).hover(function (e) {
            // on mouse in, start a timeout
            var $thm = $(this);
            popover_timer = setTimeout(function () {
                if (!$thm.data('has-popover')) {
                    var tmplt = "models/media/detail_single.html";
                    var url = '/api/media/' + $thm.data("media_id");
                    popover_xhr = $.get(url.updateURIParam("template", tmplt), function (data) {
                        $thm.popover({
                            content: data,
                            placement: _get_popover_placement,
                            html: true,
                            trigger: 'manual',
                            container: $container
                        });
                        $thm.data('has-popover', true).popover("show");
                    }, "html");
                }
                else {
                    $thm.popover("show");
                }
            }, popover_delay);
        }, function () {
            // on mouse out, cancel the timer
            clearTimeout(popover_timer);
            if (popover_xhr && popover_xhr.readyState !== 4) popover_xhr.abort();
            if ($(this).data('has-popover')) $(this).popover("hide");
        }).click(function () {
            if ($(this).data('has-popover')) $(this).popover("hide");
        });
    };

    function _get_popover_placement(pop, dom_el) {
        if ($container.width() < 500) return 'bottom';
        if ($container.width() - $(dom_el).offset().left > 400) return 'right';
        return 'left';
    }


    //$container.data("onreload", update_links);

    return _this;
}
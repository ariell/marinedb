function FramePlayerGUI(opts) {
    var _this = this;
    var pose_xhr = null;
    var media_image = new Image();
    var is_loading_media = false;
    var loading_media_timer = null;
    var help_template = "/static/html/instructions_frame_player.html";

    this.options = {
        results_per_page: 50,
        offset: 0,
        delay: 1000,
        media_load_timeout: 10000,
        deployment_id: null,
        $thumbnails_container: null,
        $media_container: null,
        $main_container: null,
        $pose_info_container: null,
        //start_logger: null,
        // stop_logger: null,
        media_url: "",
        media_sort_order: "desc",
        media_sort_field: "created_at",  //"timestamp_start"
        media_key_filter:""
    };

    // this.capture_endpoints = {              // endpoints can be urls or functions. If urls, called with $.getJSON
    //     start: "/geodata/frame_player/start",
    //     stop: "/geodata/frame_player/stop"
    // };
    this.capture_actions = {};

    this.is_paused = true;
    this.is_stopped = true;
    this.is_initialised = false;
    this.media_id = null;
    this.pose_data = null;
    this.active_thm_index = 0;

    var classes = {
        paused: "playback-paused",
        playing: "playback-playing",
        stopped: "logging-stopped",
        started: "logging-started"
    };


    this.update_options = function(options) {
        $.extend(this.options, options);
    };



    this.init = function(options) {
        _this.update_options(options);

        media_image.onload = function() {
            is_loading_media = false;
            if (!_this.is_stopped) _this.options.$media_container.empty().html("<img class='fade-stopped' src='"+media_image.src+"'>");
        };
        media_image.onerror = function() {
            is_loading_media = false;
            console.log("ERROR LOADING IMAGE: "+media_image.src)
        };
        media_image.onabort = function() {
            is_loading_media = false;
            console.log("ERROR LOADING IMAGE - ABORTED: "+media_image.src)
        };

        _this.options.$thumbnails_container.on("mouseenter mouseleave", _this.autoscroll_thumbnails);

        _this.start(false);
        _this.is_initialised = true;
    };

    this.play = function() {
        _this.is_paused = false;
        _this.options.$main_container.removeClass(classes.paused).addClass(classes.playing);
        _this.options.$main_container.find(".fade-paused").find(":input").attr('disabled',false);
        _this.options.$main_container.find(".fade-playing").find(":input").attr('disabled',true);
        _this.options.$thumbnails_container[0].scrollLeft = _this.options.$thumbnails_container[0].scrollWidth;
        _this.refresh_media();
    };

    this.pause = function () {
        _this.is_paused = true;
        _this.options.$main_container.addClass(classes.paused).removeClass(classes.playing);
        _this.options.$main_container.find(".fade-paused").find(":input").attr('disabled',true);
        _this.options.$main_container.find(".fade-playing").find(":input").attr('disabled',false);
    };

    this.start = function() {
        _this.options.$main_container.find(".fade-stopped").find(":input").attr('disabled',false);
        _this.options.$main_container.find(".fade-started").find(":input").attr('disabled',true);
        _this.options.$main_container.removeClass(classes.stopped).addClass(classes.started);
        //_this.toggle_fullscreen(true);
        _this.play(false);
        _this.is_stopped = false;
        _this.active_thm_index = 0;

        // if (typeof _this.options.start_logger === "function")
        //     _this.options.start_logger();
    };

    this.stop = function() {
        _this.options.$main_container.find(".fade-stopped").find(":input").attr('disabled',true);
        _this.options.$main_container.find(".fade-started").find(":input").attr('disabled',false);
        _this.options.$main_container.addClass(classes.stopped).removeClass(classes.started);
        _this.pause(false);
        _this.is_stopped = true;
        _this.options.$media_container.load(help_template);
        _this.options.$pose_info_container.html("");
        _this.options.$thumbnails_container.html("");
        _this.toggle_fullscreen(false);
        _this.options.deployment_id = null;  // reset deployment_id to avoid resending old one

        // if (typeof _this.options.stop_logger === "function")
        //     _this.options.stop_logger();
    };

    this.refresh = function() {
        _this.refresh_media(true);  // force refresh of media
    };

    this.state = function () {
        if (_this.options.$main_container.hasClass(classes.stopped)) return "stopped";
        else if (_this.options.$main_container.hasClass(classes.paused)) return "paused";
        else if (_this.options.$main_container.hasClass(classes.playing)) return "playing";
        else return "undefined";
    };

    /**
     * Swap image once fully loaded in background
     *
     * @param media
     */
    this.load_media = function(media) {
        // Update media item
        if (!is_loading_media) {
            is_loading_media = true;
            if (loading_media_timer) clearTimeout(loading_media_timer);
            try {
                //console.log("*** SETTING MEDIA PATH");
                media_image.src = media.path_best;
                _this.media_id = media.id;  // current media ID
                if (_this.options.$media_container.find('.media-loader').length <= 0)
                    _this.options.$media_container.append("<i class='fa fa-circle-o-notch fa-spin fa-2x fa-fw media-loader' style='position: absolute; top:10px; right:10px'></a>");
            }
            catch(e) {
                is_loading_media = false;
                console.log("*** ERROR SETTING IMAGE SOURCE", e, media);
            }
            // ensure that image is reloaded if nothing changes after 10 seconds
            // this is to deal with state where main image hangs if fails to load but doesn't error out
            loading_media_timer = setTimeout(function(){
                is_loading_media = false;
                console.log("*** MEDIA LOAD TIMEOUT", media);
            }, _this.options.media_load_timeout);
        }

        // Update pose display
        if (pose_xhr != null) pose_xhr.abort();
        pose_xhr = $.getJSON(get_pose_url(media.id), function(data){
            _this.pose_data = data.pose;
            var $dataset_ul = $("<ul>").append(
                $("<li title='"+data.key+"'>"+data.key+"</li>"),
                $("<li title='"+data.deployment.name+"'>"+data.deployment.name+"</li>")
            );
            var $pose_ul = $("<ul>").append(
                $("<li><b>TS</b>: "+_this.pose_data.timestamp+"</li>"),
                $("<li><b>LAT</b>: "+_this.pose_data.lat+"</li>"),
                $("<li><b>LON</b>: "+_this.pose_data.lon+"</li>"),
                $("<li><b>DEP</b>: "+_this.pose_data.dep+"</li>"),
                $("<li><b>ALT</b>: "+_this.pose_data.alt+"</li>")
            );

            _this.options.$pose_info_container.html("");
            _this.options.$pose_info_container.append($("<b>MEDIA</b>"), $dataset_ul);
            _this.options.$pose_info_container.append($("<b>POSE:</b>"), $pose_ul);
            if (_this.pose_data.data.length > 0) {
                var $sensor_ul = $("<ul>");
                $(_this.pose_data.data).each(function (i, d) {
                    $sensor_ul.append("<li><b>"+d.name+"</b>: "+d.value+"</li>");
                });
                _this.options.$pose_info_container.append($("<b>SENSORS:</b>"), $sensor_ul);
            }
            pose_xhr = null;
        });
        //}

    };

    this.refresh_media = function(force) {
        console.log(_this.options);
        if (typeof force === "undefined") force = false;
        if (!_this.is_paused || force===true) {
            if (_this.options.deployment_id) {
                var url = get_media_url(_this.options.deployment_id, _this.options.results_per_page, _this.options.offset);
                console.log("REFRESHING URL: "+url);
                $.getJSON(url, function (data) {
                    _this.options.$thumbnails_container.html("");
                    if (data.objects.length > 0) {
                        _this.load_media(data.objects[_this.active_thm_index]);
                        $(data.objects).each(function (i, m) {
                            var active = (i == _this.active_thm_index) ? "active" : "";
                            var $thm = $("<a class='thm fade-stopped " + active + "' href='javascript:void(0)'><img src='" + m.path_best_thm + "'></a>");
                            $thm.data("media", m).click(function () {
                                _this.pause();
                                _this.load_media(m);
                                $(this).addClass("active").siblings(".thm.active").removeClass("active");
                            });
                            _this.options.$thumbnails_container.append($thm);
                        });
                        _this.autoscroll_thumbnails();
                    }
                    else {
                        console.log("No media items found...");
                    }
                }).fail(function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR, textStatus, errorThrown);
                    notify.error("ERROR: cannot refresh media ("+textStatus+")", 5, "media_refresh_error");
                }).always(function(){
                    setTimeout(function () {_this.refresh_media();}, _this.options.delay);
                });
            }
            else {
                console.log("No deployment_id found... CAN'T REFRESH MEDIA!");
                _this.stop(false);
                // setTimeout(function () {
                //     console.log("No deployment_id found...");
                //     _this.refresh_media();
                // }, _this.options.delay);
            }
        }
    };

    this.autoscroll_thumbnails = function(){
        var $container =  _this.options.$thumbnails_container;
        if ($container.is(":visible")) {
            var $target = $container.find("a.active");
            if ($target.length > 0)
                $container.scrollLeft($container.scrollLeft() + $target.position().left - $container.width() / 2 + $target.width() / 2);
        }
    };


    this.save_to_collection = function(media_collection_id, annotation_set_id, callback, media_id) {
        var run_callback = (typeof callback === 'function');
        if (typeof media_id === "undefined") media_id = _this.media_id;

        if (!parseInt(media_id)>0)
            // TODO: deal with external dependency properly!!!
            notify.error("ERROR! Frame not saved. There is no valid media item to add...");
        else if (!parseInt(media_collection_id)>0)
            // TODO: deal with external dependency properly!!!
            notify.error("ERROR! Frame not saved. You first need to select or create a Media Collection");
        else if (!parseInt(annotation_set_id)>0 && run_callback)
            // TODO: deal with external dependency properly!!!
            notify.error("ERROR! Frame not saved. You first need to select or define an Annotation Method");
        else {
            _this.pause();
            var url = "/api/media_collection_media";
            var data = {media_id: media_id, media_collection_id: media_collection_id};

            return ajax.post(url, JSON.stringify(data), {success: function(resp){
                console.log("Frame has been added", resp);
            }}).complete(function(jqXHR, textStatus){
                console.debug(textStatus, jqXHR);
                if (run_callback && (jqXHR.status===403 || jqXHR.status < 300))   // stats<300 is success, status=403, it already exists, so all is good!
                    callback(media_collection_id, annotation_set_id, media_id);
                else {
                    _this.play();
                }
            });
        }
    };


    this.toggle_fullscreen = function(fullscreen) {
        if (typeof fullscreen === "undefined") fullscreen=null;

        if (fullscreen!==false &&
            ((document.fullScreenElement && document.fullScreenElement !== null) ||
            (!document.mozFullScreen && !document.webkitIsFullScreen))) {
            if (document.documentElement.requestFullScreen)
                document.documentElement.requestFullScreen();
            else if (document.documentElement.mozRequestFullScreen)
                document.documentElement.mozRequestFullScreen();
            else if (document.documentElement.webkitRequestFullScreen)
                document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
        else if (fullscreen!==true) {
            if (document.cancelFullScreen)
              document.cancelFullScreen();
            else if (document.mozCancelFullScreen)
              document.mozCancelFullScreen();
            else if (document.webkitCancelFullScreen)
              document.webkitCancelFullScreen();
        }
    };


    function get_media_url(deployment_id, results_per_page, offset) {
        var q = {
            filters: [{name: "deployment_id", op: "in", val: [deployment_id]}],
            order_by: [{field: _this.options.media_sort_field, direction: _this.options.media_sort_order}],
            offset: offset
        };
        if (_this.options.media_key_filter.trim()){
            q.filters.push({name: "key", op: "ilike", val: '%'+_this.options.media_key_filter+'%'});
        }
        return '/api/media?q=' + JSON.stringify(q) + '&results_per_page=' + results_per_page;
    }

    function get_pose_url(media_id) {
        return '/api/media/'+media_id;
    }


    return _this;
}

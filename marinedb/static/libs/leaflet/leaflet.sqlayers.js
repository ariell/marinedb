L.TileLayer.WMSInfo = L.TileLayer.WMS.extend({

  onAdd: function (map) {
    // Triggered when the layer is added to a map.
    //   Register a click listener, then do all the upstream WMS things
    L.TileLayer.WMS.prototype.onAdd.call(this, map);
    map.on('click', this.getFeatureInfo, this);
    this._legend_control = this.addLegendControl(map);
  },

  onRemove: function (map) {
    // Triggered when the layer is removed from a map.
    //   Unregister a click listener, then do all the upstream WMS things
    L.TileLayer.WMS.prototype.onRemove.call(this, map);
    map.off('click', this.getFeatureInfo, this);
    this._legend_control.remove(map);
  },

  getFeatureInfo: function (evt) {
    // Make an AJAX request to the server and hope for the best
    var url = this.getFeatureInfoUrl(evt.latlng);
    var showResults = L.Util.bind(this.showGetFeatureInfo, this);
    $.ajax({
      url: url,
      crossDomain: true,
      success: function (data, status, xhr) {
        var err = typeof data === 'string' ? null : data;
        showResults(err, evt.latlng, data);
      },
      error: function (xhr, status, error) {
        console.log("ERROR! Could not process feature info AJAX request", xhr, status, error);
      }
    });
  },

  getFeatureInfoUrl: function (latlng) {
    // Construct a GetFeatureInfo request URL given a point
    var point = this._map.latLngToContainerPoint(latlng, this._map.getZoom());
    var size = this._map.getSize();
    var params = {
      request: 'GetFeatureInfo',
      service: 'WMS',
      srs: 'EPSG:4326',
      // srs: (this.wmsParams.hasOwnProperty('srs')) ? this.wmsParams.srs : 'EPSG:4326',
      // crs: (this.wmsParams.hasOwnProperty('srs')) ? this.wmsParams.srs : 'EPSG:4326',
      styles: this.wmsParams.styles,
      transparent: this.wmsParams.transparent,
      version: this.wmsParams.version,
      format: this.wmsParams.format,
      bbox: this._map.getBounds().toBBoxString(),
      height: size.y,
      width: size.x,
      layers: this.wmsParams.layers,
      query_layers: this.wmsParams.layers,
      info_format: 'text/html',
      feature_count: 10
    };
    //console.log(params);
    if (this.wmsParams.hasOwnProperty("cql_filter"))
        params.cql_filter = this.wmsParams.cql_filter;

    params[params.version === '1.3.0' ? 'i' : 'x'] = Math.round(point.x);
    params[params.version === '1.3.0' ? 'j' : 'y'] = Math.round(point.y);
    // params[params.version === '1.3.0' ? 'i' : 'x'] = point.x;
    // params[params.version === '1.3.0' ? 'j' : 'y'] = point.y;

    return this._url + L.Util.getParamString(params, this._url, true);
  },

  showGetFeatureInfo: function (err, latlng, content) {
    if (err) { console.log(err); return; } // do nothing if there's an error
    var doc = content;
    try {doc = /<body.*?>([\s\S]*)<\/body>/.exec(content)[1].trim();}  // attempt to extract body if HTML page
    catch(e) {}

    // console.log("CONTENT:", content, "\n\nDOC:", doc);
    // Otherwise show the content in a popup, or something.
    if (doc)
        L.popup({ maxWidth: 600})
          .setLatLng(latlng)
          .setContent(doc)
          .openOn(this._map);
  },

  addLegendControl: function(map) {
      var legend = L.control({position: 'bottomright'});

      var params = {
          request: 'GetLegendGraphic',
          service: 'WMS',
          //styles: this.wmsParams.styles,
          transparent: this.wmsParams.transparent,
          version: this.wmsParams.version,
          layers: this.wmsParams.layers,
          layer: this.wmsParams.layers.split(",")[0],  // support for multiple layers (use legend from first only)
          format: 'image/png'
        };

      if (this.wmsParams.hasOwnProperty("styles")) {
          params.styles = this.wmsParams.styles;
          params.style = this.wmsParams.styles.split(",")[0];
      }

      var url = this._url + L.Util.getParamString(params, this._url, true);

      legend.onAdd = function (map) {
        var div = L.DomUtil.create('div', 'info wms-legend');
        div.innerHTML = "<img src="+ url +">";
        L.DomEvent.on(div, 'mousewheel', L.DomEvent.stopPropagation);  // enable scrolling of control - prevent map scroll
        return div;
      };
      legend.addTo(map);
      return legend;
  }

});


L.TileLayer.WMSDeploymentTracks = L.TileLayer.WMS.extend({

  onAdd: function (map) {
    // Triggered when the layer is added to a map.
    //   Register a click listener, then do all the upstream WMS things
    L.TileLayer.WMS.prototype.onAdd.call(this, map);
    map.on('click', this.getFeatureInfo, this);
  },

  onRemove: function (map) {
    // Triggered when the layer is removed from a map.
    //   Unregister a click listener, then do all the upstream WMS things
    L.TileLayer.WMS.prototype.onRemove.call(this, map);
    map.off('click', this.getFeatureInfo, this);
  },

  getFeatureInfo: function (evt) {
    // Make an AJAX request to the server and hope for the best
    if (this._map.getZoom() >10) {
        var radius = 5, //meters
            ll = evt.latlng,
            ll_bl = this.pos_offset(ll, 225, radius),  // bottom left
            ll_tr = this.pos_offset(ll, 45, radius);   // top right
        var q = {
          filters:[{name:"geom",op:"geo_in_bbox",val:[{lat:ll_bl.lat,lon:ll_bl.lng},{lat:ll_tr.lat,lon:ll_tr.lng}]}],
          order_by:[{field: {method:"distance", args:[ll.lat, ll.lng]}, direction:'asc'}]
        };
        var url = '/api/pose?template=models/pose/pose_media_thumbnails.html&q='+JSON.stringify(q)
            +'&results_per_page=8&lon='+ll.lng+'&lat='+ll.lat+'&radius='+radius;

        var $content = $("<div>");
        var map = this._map;

        load_content.paginated_html(url, $content, function(){
            var $media_links = $content.find("a.media-thm-link").tooltip({placement:'auto'}).click(function(e){
                e.preventDefault();
                open_modal($(this).attr("href"), {modal_class:"modal-xl"});
            });
            if ($media_links.length > 0) {
                L.popup({maxWidth: 600, minWidth: 600})
                    .setLatLng(evt.latlng)
                    .setContent($content[0])
                    .openOn(map);
            }
        })
    }
  },

  pos_offset: function(latlng, heading, distance) {
        heading = (heading + 360) % 360;
        var rad = Math.PI / 180,
            radInv = 180 / Math.PI,
            R = 6378137, // approximation of Earth's radius
            lon1 = latlng.lng * rad,
            lat1 = latlng.lat * rad,
            rheading = heading * rad,
            sinLat1 = Math.sin(lat1),
            cosLat1 = Math.cos(lat1),
            cosDistR = Math.cos(distance / R),
            sinDistR = Math.sin(distance / R),
            lat2 = Math.asin(sinLat1 * cosDistR + cosLat1 * sinDistR * Math.cos(rheading)),
            lon2 = lon1 + Math.atan2(Math.sin(rheading) * sinDistR * cosLat1, cosDistR - sinLat1 * Math.sin(lat2));
        lon2 = lon2 * radInv;
        lon2 = lon2 > 180 ? lon2 - 360 : lon2 < -180 ? lon2 + 360 : lon2;
        return L.latLng([lat2 * radInv, lon2]);
    }
});

L.GeoJSON.AjaxGeoJson = L.GeoJSON.extend({
    _isLoaded: false,  // keep track of whether or not layer has been loaded

    onAdd: function (map) {
        var _this = this;
        // Add onEachFeature to options if it does not exist
        if (! _this.options.hasOwnProperty("onEachFeature")) {
            _this.options.onEachFeature = function (feature, layer) {
                if (feature.properties) {
                    var popuptxt = "<h4>Feature properties:</h4>";
                    for (var k in feature.properties)
                        popuptxt += "<b>" + k + "</b>: " + feature.properties[k] + "<br>";
                    layer.bindPopup(popuptxt);
                }
            };
        }

        // Call base class
        L.GeoJSON.prototype.onAdd.call(_this, map);

        // Add data through ajax request
        if (! _this._isLoaded) {
            $.getJSON(_this.options.url, function (data) {
                _this.addData(data);
                _this._isLoaded = true;
            });
        }
    }
});


L.tileLayer.wmsInfo = function (url, options) {
    // If SRS is supplied, convert into the leaflet CRS object
    if (options.hasOwnProperty("srs")) {
        var srs = options.pop("srs");
        options.crs = L.CRS[srs.replace(":","")];
        console.log(options.crs);
    }
    return new L.TileLayer.WMSInfo(url, options);
};

L.tileLayer.WMSDeploymentTracks = function (url, options) {
    // If SRS is supplied, convert into the leaflet CRS object
    if (options.hasOwnProperty("srs")) {
        var srs = options.pop("srs");
        options.crs = L.CRS[srs.replace(":","")];
        console.log(options.crs);
    }
    return new L.TileLayer.WMSDeploymentTracks(url, options);
};

L.tileLayer.wmsNoInfo = function (url, options) {
    // If SRS is supplied, convert into the leaflet CRS object
    if (options.hasOwnProperty("srs")) {
        var srs = options.pop("srs");
        options.crs = L.CRS[srs.replace(":","")];
        console.log(options.crs);
    }
    return new L.TileLayer.WMS(url, options);
};

L.geoJSON.ajax = function(data, options){
    return new L.GeoJSON.AjaxGeoJson(data, options);
};

L.sqlayer = function(layertype, data, options) {
    // if (layertype.toLowerCase() === "ajaxgeojson") {
    //     return new L.GeoJSON.AjaxGeoJson(data, options);
    // }
    // else if (layertype.toLowerCase() === "wmsinfo") {
    //     return new L.TileLayer.WMSInfo(data, options);
    // }
    var layerobj = L;
    var props = layertype.split(".");
    for (var i=0; i<props.length; i++) layerobj = layerobj[props[i]];
    return new layerobj(data, options);
};
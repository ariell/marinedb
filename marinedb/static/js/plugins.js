// prevent bootstrap modals from stealing focus - leads to problems with select2
// see: https://select2.org/troubleshooting/common-problems
//$.fn.modal.Constructor.prototype.enforceFocus = function() {};

function open_modal(url, opts) {
    // open_modal(url, {modal_id: , modal_class: , onshown: , onhidden: , iframe:})
    var _prevent_hide_class = "modal-prevent-auto-hide";
    var options = {
        modal_id: "modal-"+Date.now(),
        modal_class: "modal-lg", //modal-lg|modal-md|modal-sm|modal-xl|modal-full|modal-dark
        onshown: function(){},
        onhidden: function(){},
        oncompleted: function(data){},
        iframe: false,          // url should be loaded as iframe
        show_close: true,       // whether or not to show the close button
        remember_state: false,  // do not remove on close (just hide) - requires using 'modal_id' to reopen
        load_func: load_content.html,  // load_content.html | load_content.paginated_html
        backdrop: 'static',  // true|false|'static'
        keyboard: true,  // true|false, true=close on ESC
        hide_open_modals: false,   // hide previously opened submodals (that have not been set to 'prevent_submodal_hide')
        prevent_submodal_hide: false,  // prevent this modal from being hidden by submodals
        tab_index: -1
    };
    $.extend(options, opts);

    //if (typeof modal_id === 'undefined') modal_id = $("body.modal-open").find(".modal.in").attr('id');
    var $modal = $("#"+options.modal_id);
    var modal_class = "modal" + ((options.prevent_submodal_hide) ? " "+_prevent_hide_class : "");
    var $modalcontent;
    if ($modal.length > 0)
        $modalcontent = $modal.find(".modal-html");
    else {
        var $close = (options.show_close) ? $("<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>") : $("");
        var body_style = (options.show_close) ? "padding: 20px" : "padding: 0";
        $modalcontent = $("<div class='modal-html'>");
        $modal = $("<div id='" + options.modal_id + "' class='"+modal_class+"' tabindex='" + options.tab_index + "' role='dialog' data-backdrop='static'>").append(
            $("<div class='modal-dialog " + options.modal_class + "'>").append(
                $("<div class='modal-content'>").append(
                    $("<div class='modal-body' style='"+body_style+"'>").append($close, $modalcontent)
                )
            )
        );

        // method for completion function passing data
        $modal.bind('completed',function(e, data){
            e.stopPropagation();
            e.stopImmediatePropagation();
            if (typeof options.oncompleted === 'function')
                options.oncompleted(data);
            $modal.modal("hide");
        });

        // bind reload function
        $modal.bind('reload', function(e){
            $modalcontent.trigger("reload");
        });

        // check if a parent modal is open, and if so, hide it and reopen it on close
        var $open_modals = $("body .modal.in:visible");
        if (options.hide_open_modals) {
            var $hidable_open_modals = $open_modals.not("."+_prevent_hide_class);
            if ($hidable_open_modals.length > 0) {
                $hidable_open_modals.addClass('modal-hidden');                            // hide open modals
                $modal.on('hidden.bs.modal', function () {      // show them again on close
                    $hidable_open_modals.removeClass('modal-hidden');
                });
                options.backdrop = false;                       // prevent adding extra backdrop
            }
        }

        // Attach modal to body
        // Remove it from DOM on close (if set to not remember)
        $('body').append($modal);
        $modal.on('hidden.bs.modal', function(){
            var modal_options = $modal.data("modal_options");
            if (typeof modal_options.onhidden === 'function') {
                modal_options.onhidden.call(this);
            }
            if (!modal_options.remember_state) {
                $modal.remove();
                console.log("removed modal id="+modal_options.modal_id);
            }
            // ensure body maintains modal-open class if multiple modals are used
            if ($(".modal.in").length>0 && !$("body").hasClass("modal-open"))
                $("body").addClass("modal-open");

            // ensure focus is back on open modal if previous one is closed
            $open_modals.focus();
        });

        // initalise with options
        $modal.modal({backdrop:options.backdrop, keyboard: options.keyboard, show:false});
    }
    $modal.data("modal_options", options);


    //console.log("displaying modal id="+options.modal_id+", url="+url);

    if (options.remember_state && $modal.data("url") === url) {
        $modal.modal("show");
    }
    else {
        if (options.iframe) {
            var iframe_height = $(window).height() - 100; // modal padding plus border
            var $iframe = $("<iframe id='modal-iframe' allowtransparency='true' src='" + url + "' style='min-height:100px;width:100%;border:none; background-color: transparent; overflow: auto;'></iframe>");
            $modalcontent.html($iframe);
            $iframe.load(function () {
                //$iframe.contents().find('body').css('backgroundColor', 'transparent');  // doesn't work - problematic
                $modal.modal("show");
                $iframe.height(iframe_height);
                if (typeof options.onshown === 'function') options.onshown.call($modal);
            });
        }
        else {
            $modalcontent.html(""); //.hide();
            $modal.modal("show");  // try
            options.load_func(url, $modalcontent, function(data){
                //$modal.modal("show");  // why show only after load?
                //$modalcontent.show();
                if (typeof options.onshown === 'function') options.onshown.call($modal);
            }).fail(function(){
                $modal.modal("show");
                if (options.show_close === false)   // add close button to prevent getting stuck on error
                    $modalcontent.prepend($("<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>"))
                //$modalcontent.show();
            });
        }
    }

    $modal.data({options: options, url:url});
    return $modal;
}

var load_content = {
    html: function(url, $container, onsuccess, opts) {
        var options = $.extend({
            fail_notify_container: $container
        }, opts);
        $container.data("url", url);
        load_content.loader_overlay($container).unbind("reload").bind("reload", function(e, url_or_params){
            var loadurl = $container.data("url");
            if (typeof url_or_params === "object")  // treat as url params to update
                $.each(url_or_params, function(k,v) {loadurl = loadurl.updateURIParam(k, v)});
            else if (typeof url_or_params === "string")
                loadurl = url_or_params;
            load_content.html(loadurl, $container, onsuccess, opts);
            e.stopPropagation();  // stop propagation of reloads through containers
        });
        var ajax_params = {
            dataType: "html",
            contentType: "text/plain",
            success: function (data) {
                $container.html(data);
                if (typeof onsuccess === "function") {
                    onsuccess.call($container, data);
                    $container.data("onreload", onsuccess);
                }
                else if (typeof $container.data("onreload") === "function") {
                    $container.data("onreload").call($container, data);
                }
                // if google analytics is active on this instance, register page view
                if (typeof gtag === "function") {
                    gtag('set', 'page_path', url);
                    gtag('event', 'page_view');
                }
            }
        };
        // Put fail notify into nominated container or just use default notifcation
        if (options.fail_notify_container !== null){
            ajax_params.fail_notify = function (msg, ajax_params) {
                options.fail_notify_container.html($('<div class="alert alert-danger" style="overflow: hidden">')
                    .html(msg + "<div style='font-size: xx-small' title='" + ajax_params.url + "'><b>" + ajax_params.type + "</b>: " + ajax_params.url + "</div>"));
            }
        }

        return ajax.get(url, ajax_params).always(function () {
            load_content.loader_overlay($container, true);
        });
    },
    paginated_html: function(url, $container, onsuccess, opts) {
        // add pagination to success function
        //console.log("loading paginated content!", url);
        var successfunc = function(data) {
            $container.find("a[rel='paginate-list']").click(function(e){
                // console.log($(this).data("url"), $(this).data("page"));
                e.preventDefault();
                var url = $container.data("url").updateURIParam("page", $(this).data("page"));
                load_content.paginated_html(url, $container, onsuccess, {fail_notify_container: null});
            });
            $container.find("form.paginate-list-form").on("submit", function(e){
                e.preventDefault();
                var url = $container.data("url");
                $.each($(this).serializeObject(), function(k,v){ url = url.updateURIParam(k,v)});
                load_content.paginated_html(url, $container, onsuccess, {fail_notify_container: null});
            });
            $container.find("a[rel='toggle-form']").click(function(e){
                e.preventDefault();
                $(this).parents(".paginate-list-params").find("form.paginate-list-form").toggle(200);
            });
            if (typeof onsuccess === "function") onsuccess.call($container, data);
        };
        return load_content.html(url, $container, successfunc, opts);
    },
    loader_overlay: function($container, hide) {
        if (hide===true){
            $container.find(".loader-mask").remove();
        }
        else {
            $container.append("<div class='loader-mask'><i class='fa fa-circle-o-notch fa-spin'></i></div>");
        }
        return $container;
    }
};


var ajax = {
    get: function(url, ajax_params) {
        return this.request({url:url, type:"GET"}, ajax_params);
    },
    post: function(url, data, ajax_params) {
        return this.request({url:url, type:"POST", data:data}, ajax_params);
    },
    patch: function(url, data, ajax_params) {
        return this.request({url: url, type: "PATCH", data: data}, ajax_params);
    },
    /**
     *
     * @param url
     * @param ajax_params
     * @param confirm_msg: true|false|question string
     * @param confirm_opts
     * @returns {*}
     */
    delete: function(url, ajax_params, confirm_msg, confirm_opts) {
        var ajax_xhr=null;
        var confirm_options = $.extend({
            title: "Confirm Delete?",
            icon: "<i class='fa fa-trash'></i>"
        }, confirm_opts);
        if (typeof confirm_msg === "undefined" || confirm_msg===true)
            confirm_msg = "Are you sure you want to delete this record? This operation is not reversible.";
        if (confirm_msg !== false) {
            confirm_options.ok = function(){
                ajax_xhr = ajax.request({url:url, type:"DELETE"}, ajax_params);
                return ajax_xhr;
            };
            /*
            // TODO: this doesn't work. Would be good to abort a delete if possible.
            confirm_options.cancel = function() {
                if (ajax_xhr) ajax_xhr.abort();   // attempt abort on cancel
            };
            */
            notify.confirm(confirm_msg, confirm_options);
            return ajax_xhr;
        }
        else
            return this.request({url:url, type:"DELETE"}, ajax_params);
    },
    /**
     * Accepts multiple input args: ajax.request(ajax_params1, ajax_params2, ...)
     * @returns {*|void}
     */
    request: function() {
        var ajax_params = {
            url : null,
            data : null,
            dataType: 'json',
            type : 'GET',
            contentType : 'application/json',
            xhr: function() {
                return window.XMLHttpRequest == null ||
                new window.XMLHttpRequest().addEventListener == null ?
                    new window.ActiveXObject("Microsoft.XMLHTTP") : $.ajaxSettings.xhr();
            },
            success: function(data){console.log("Success: ", data)},
            fail_notify: function(msg, ajax_params) {
                notify.error(msg+"<div style='font-size: xx-small'><b>"+ajax_params.type+"</b>: "+ajax_params.url+"</div>");
                console.error(msg, ajax_params);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.error(jqXHR, textStatus, errorThrown);
                if (typeof ajax_params.fail_notify === "function") {
                    ajax_params.fail_notify(ajax._parse_error(jqXHR, textStatus, errorThrown), ajax_params);
                }
            }
        };
        // Assume all arguments are dictionaries for ajax_params and extend accordingly
        $.each(arguments, function(i,el){
            $.extend(ajax_params, el);
        });
        return $.ajax(ajax_params);
    },
    _parse_error: function(jqXHR, textStatus, errorThrown) {
        return (jqXHR.hasOwnProperty("responseJSON") && jqXHR.responseJSON && jqXHR.responseJSON.hasOwnProperty("message"))
            ? jqXHR.responseJSON.message                                            // try json message
            : (jqXHR.responseText && jqXHR.responseText.length < 300)
                ? jqXHR.responseText                                                // if responsetext<300 chars
                : errorThrown || "Unknown error occurred. Are you connected?";      // otherwise try errthrown or default
    }
};


var notify = {
    error: function(msg, delay, id) {
        msg = "<i class='fa fa-fw fa-exclamation-triangle'></i> "+msg;
        return this.general(msg, "danger", delay, id);
    },
    info: function(msg, delay, id) {
        msg = "<i class='fa fa-fw fa-info-circle'></i> "+msg;
        return this.general(msg, "info", delay, id);
    },
    warning: function(msg, delay, id) {
        msg = "<i class='fa fa-fw fa-exclamation-circle'></i> "+msg;
        return this.general(msg, "warning", delay, id);
    },
    success: function(msg, delay, id) {
        msg = "<i class='fa fa-fw fa-check-square'></i> "+msg;
        return this.general(msg, "success", delay, id);
    },
    general: function(msg, type, delay, id) {
        if (typeof delay === "undefined") delay = 10000;
        if (typeof id === "undefined") id = "notify-"+Date.now();
        var $notify_panel = $("body>#notifications");
        if ($notify_panel.length === 0)
            $notify_panel = $("<div id='notifications'>").appendTo("body");
        var $alert = $notify_panel.find("#"+id).html("");
        var $close = $('<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="fa fa-times"></i></a>');
        if ($alert.length <= 0) {
            $alert = $('<div id="'+id+'" class="alert alert-' + type + ' alert-dismissible"><div>').hide();
            $notify_panel.append($alert);
        }
        $alert.append($close, msg).fadeIn(500);

        // timout for hide
        if ($alert.data('timer'))
            clearTimeout($alert.data('timer'));

        if (delay>0) {
            // set timer to clear alert
            $alert.data('timer', setTimeout(function () {$alert.remove()}, delay));
        }
        // keep alert showing on hover and reset timer on mouse out
        $alert.hover(function(e){
            if (delay>0) clearTimeout($alert.data('timer'));
            $alert.css("opacity", 1.0);
        }, function(e){
            if (delay>0) $alert.data('timer', setTimeout(function () {$alert.remove()}, delay));
            $alert.css("opacity", "");
        });
        return $alert;
    },
    hide: function(id) {
        $("body>#notifications").find("#"+id).remove();
    },
    confirm: function(question, opts) {
        var options = {
            ok: function () {console.log("Confirm: OK!")},
            cancel: function () {console.log("Confirm: cancelled!")},
            ok_text: "Yes",
            cancel_text: "Close",
            ok_icon: "<i class='fa fa-check'></i>",
            cancel_icon: "<i class='fa fa-times'></i>",
            title: "Confirm?",
            icon: "<i class='fa fa-question-circle'></i>"
        };
        $.extend(options, opts);

        var $confirm_backdrop = $("body>#confirmation-backdrop");
        var $confirm = $("<div class='confirm-dialog'>");
        var $ok = $("<button class='btn btn-primary' type='button'>"+options.ok_icon+" "+options.ok_text+"</button>");
        var $cancel = $("<button class='btn btn-default' type='button'>"+options.cancel_icon+" "+options.cancel_text+"</button>");
        if ($confirm_backdrop.length === 0)
            $confirm_backdrop = $("<div id='confirmation-backdrop'>").appendTo("body");

        $confirm.hide().append(
            $("<h4>"+options.icon+" "+options.title+"</h4>"),
            $("<p>"+question+"</p>"),
            $("<div class='text-right'>").append($cancel, $ok)
        );
        $confirm_backdrop.html($confirm.fadeIn(500));

        $ok.click(function(e) {
            e.preventDefault();
            $ok.attr("disabled", true).find("i").addClass("fa-circle-o-notch fa-spin").removeClass("fa-check");
            var r = options.ok();
            if(r && $.isFunction(r.done) && $.isFunction(r.fail)) {  // if ajax request, only close when done
                r.done(function (data) {
                    $confirm_backdrop.remove();
                }).fail(function() {
                    $ok.attr("disabled", false).find("i").removeClass("fa-circle-o-notch fa-spin").addClass("fa-check");
                });
            }
            else {
                $confirm_backdrop.remove();
            }
        });
        $cancel.click(function(e) {
            e.preventDefault();
            options.cancel();
            $confirm_backdrop.remove();
        });
    }
};




/**
 * Format string with arguments:
 * eg: "{1}/something/{0} arg3: {2}".format("first_arg", "arg_2", 999)
 * results in: "arg_2/something/first_arg arg3: 999"
 */
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] !== 'undefined'
        ? args[number]
        : match;
    });
  };
}
/**
 * Similar to above, but with named arguments
 * data = {'who':'Gendry', 'what':'sit', 'where':'in the Iron Throne'}
 * text = 'GOT: {who} did {what} and {where}';
 * text.formats(data)
 */
if (!String.prototype.formats) {
    String.prototype.formats = function (args) {
        var text = this
        for (var attr in args) {
            var rgx = new RegExp('\\{' + attr + '}', 'g');
            text = text.replace(rgx, args[attr]);
        }
        return text
    };
}


/**
 * Update uri querystring parameter
 * eg1: "http://localhost:5000/geodata/annotation_set/95#img".updateURIParam("test",123)
 * results in: "http://localhost:5000/annotation_set/95&test=123#img"
 */
if (!String.prototype.updateURIParam) {
    String.prototype.updateURIParam = function(key, value, encode_value) {
        if (typeof value === 'object' && value !== null)                    // convert objects to strings if necessary
            value = JSON.stringify(value);
        if (typeof encode_value === 'undefined' || encode_value===true)
            value = encodeURIComponent(value);
        var uri = this;
        // remove the hash part before operating on the uri
        var i = uri.indexOf('#');
        var hash = i === -1 ? ''  : uri.substr(i);
        uri = i === -1 ? uri : uri.substr(0, i);
        var re = new RegExp("([?&])"+key+"=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) uri = uri.replace(re, '$1' + key + "=" + value + '$2');
        else uri += separator + key + "=" + value;
        return uri + hash;
    }
}


/**
 * Plugin to serialise form into object
 * @returns {{}}
 */
$.fn.serializeObject = function() {
    var o = {};
    var disabled = this.find(':input:disabled').removeAttr('disabled');
    var a = this.serializeArray();
    $.each(a, function() {
        var val = (this.value === "true") ? true : (this.value === "false") ? false : this.value;
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(val);
        } else {
            o[this.name] = val;
        }
    });
    disabled.attr('disabled','disabled');
    return o;
};


/**
 * add a pop method to objects to pop properties - this normally only exists for arrays
 */
Object.defineProperty(Object.prototype, 'pop',{
    writable: false
    , configurable: false
    , enumerable: false
    , value: function (name) {
        var value = this[name];
        delete this[name];
        return value;
    }
});


//Object.byString = function(o, s) {
//    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
//    s = s.replace(/^\./, '');           // strip a leading dot
//    var a = s.split('.');
//    for (var i = 0, n = a.length; i < n; ++i) {
//        var k = a[i];
//        if (k in o) {
//            o = o[k];
//        } else {
//            return;
//        }
//    }
//    return o;
//};

(function( $ ) {

    /**
     * JQuery plugin for setting values of ajax_select elements (initialised using plugin below)
     * @param ids
     * @returns {*}
     */
    $.fn.ajax_select_val = function (ids) {
        if (typeof this.data("_ajax_select_val_fnc") !== "function")
            throw "Error setting ajax values. The 'ajax_select' plugin has not been initialised on this object."
        return this.data("_ajax_select_val_fnc")(ids);
    };

    /**
     * JQuery plugin to ajax forms using SQ+ API.
     * Note depends on:
     *    - jquery
     *    - this also adds a method to the select `.ajax_select_val(ids)` which allows setting of options through ajax
     *    - mustache.js 3.1.0 for templates
     *    - select2.js 4.1.0 for fancy select lists (this plugin is basically a wrapper for that)
     * @param url
     * @param options
     */
    $.fn.ajax_select = function (url, options) {
        var defaults = {
            width: '100%',
            placeholder: 'Search / select an option...',
            allow_clear: false,   // When set to true, "x" icon appears allowing resetting to its placeholder value.
            delay: 250,
            //theme: "classic",
            results_per_page: 100,
            min_input_length: 0,
            dropdown_parent: $(document.body),
            template_tags: [ '[[', ']]' ],   // override the default {{}} to play nice with Jinja2
            template_option: "[[name]]",
            template_item: "[[&name]]",      // & is to prevent escaping of html entities, eg: & -> &amp;
            process_objects: function(objects) {return objects},  // optional, process objects (eg: remove children property)
            build_query: function(search_term) {
                var q = {filters: [],order_by: [{field:"name",direction:"asc"}]};
                if (search_term) {
                    $(search_term.split(/(\s+)/)).each(function(i,s){
                        if (s.trim()) q.filters.push({"name":"name","op":"ilike","val":'%'+s+'%'});
                    });
                }
                return q;
            }
        };
        options = $.extend(defaults, options);
        //Mustache.tags = options.template_tags;

        return this.each(function(){

            var $this_select = $(this);

            $this_select.select2({
                width: options.width,
                dropdownParent: options.dropdown_parent,
                placeholder: options.placeholder,
                minimumInputLength: options.min_input_length,
                allowClear: options.allow_clear,
                //theme: options.theme,
                ajax: {
                    url: url,
                    dataType: 'json',
                    delay: options.delay,
                    data: function (params) {
                        return {
                            q: JSON.stringify(options.build_query(params.term)), // search term
                            page: params.page,
                            results_per_page: options.results_per_page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = data.page || 1;

                        return {
                            results: options.process_objects(data.objects),
                            pagination: {
                                more: data.page < data.total_pages
                            }
                        };
                    },
                    cache: true
                },
                templateResult: function (o) {
                    if (o.loading || !o.id) return o.text;
                    //console.debug(o);
                    return $("<div class='select2-option'>").html(
                        Mustache.render(options.template_option, o, {}, options.template_tags)
                    );
                },
                templateSelection: function (o) {
                    if (o.loading || !o.id) return o.text;
                    //console.debug(o);
                    var item = Mustache.render(options.template_item, o, {}, options.template_tags);
                    return item.trim() || o.text;
                }
            });

            var ajax_select_val = function(ids){
                // get value using original val function if no arguments
                if (ids) {
                    if (!Array.isArray(ids))
                        ids = [ids];  // convert into an array (incase of multiselect)
                    var q = options.build_query(null);
                    if (!q.hasOwnProperty("filters")) q.filters = [];
                    q.filters.push({name: "id", op: "in", val: ids});
                    // todo: clear previous values?
                    $.getJSON(url, {q: JSON.stringify(q)}, function (data) {
                        var results = options.process_objects(data.objects);
                        $(results).each(function (i, o) {
                            var text = Mustache.render(options.template_item, o, {}, options.template_tags);
                            var option = new Option(text, o.id, true, true);
                            $this_select.append(option);
                        });
                        $this_select.trigger("change.select2");
                    });
                }
                return $this_select;
            };

            // add value selector to data for future reference
            $this_select.data("_ajax_select_val_fnc", ajax_select_val);

            if ($this_select.data("selected")) {
                $this_select.ajax_select_val($this_select.data("selected"))
            }

            return $this_select;
        });
    };
}( jQuery ));
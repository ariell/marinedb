$.fn.multiSelect = function(o) {
    var defaults = {
        multiselect:    true,               // true|false -if false you can select only one element
        toggleSelection: false,             // true|false - if true, won't deselect, will just toggle clicked element
        selected:       'selected',         // 'selected' - class that will be added to selected element
        filter:         ' > *',             // ' > *' - what elements are we going to select
        unselectOn:     false,              // false|$element - if set then if clicked on set selector selectio would be removed
        keepSelection:  true,               // true|false - quite an important flag. If set to true (default), then selection won't be cleared if you click on already selected element (as it works in with multiple prop)
        list:           $(this).selector,
        e:              null,
        element:        null,
        start:          false,              // false|function - callback on start
        stop:           false,              // false|function - callback on stop
        unselecting:    false,              // false|function - callback when clicked on set "unselectOn" option
        start_multi:    false,              // false|function - callback when multi-select has started
        stop_multi:     false               // false|function - callback when multi-select has ended
    };

    var in_multi_mode = false;
    var $last_selected = null;

    function multiSelect(o) {

        var target = o.e.target;
        var element = o.element;
        var list = o.list;
        // var first, last, firstHolder;

        if ($(element).hasClass('ui-sortable-helper')) {
            return false;
        }

        if (o.start !== false) {
            var start = o.start(o.e, $(element));
            if (start === false) {
                return false;
            }
        }

        if (o.e.shiftKey && o.multiselect) {
            //
            // get one already selected row
            $(element).addClass(o.selected);

            if ($last_selected) {
                if ($(element).index() > $last_selected.index()){
                    $last_selected.nextUntil($(element)).addClass(o.selected);
                }
                else {
                    $(element).nextUntil($last_selected).addClass(o.selected);
                }
            }

            $last_selected = $(element);

            o.e.preventDefault();

            /*
            // get one already selected row
            $(element).addClass(o.selected);
            first = $(o.list).find('.'+o.selected).first().index();
            last = $(o.list).find('.'+o.selected).last().index();

            // if we hold shift and try to select last element that is upper in the list
            if (last < first) {
                firstHolder = first;
                first = last;
                last = firstHolder;
            }

            if (first === -1 || last === -1) {
                return false;
            }

            $(o.list).find('.'+o.selected).removeClass(o.selected);

            var num = last - first;
            var x = first;

            for (i=0;i<=num;i++) {
                $(list).find(o.filter).eq(x).addClass(o.selected);
                x++;
            }
            o.e.preventDefault();
            */
        } else if (((o.e.ctrlKey || o.e.metaKey) || o.toggleSelection) && o.multiselect) {
        //} else if (o.multiselect) {
            // reset selection
            if ($(element).hasClass(o.selected)) {
                $(element).removeClass(o.selected);
                $last_selected = null;
            } else {
                $(element).addClass(o.selected);
                $last_selected = $(element);
            }
        } else {
            // reset selection
            if (o.keepSelection && !$(element).hasClass(o.selected)) {
               //$(list).find('.'+o.selected).removeClass(o.selected);
               $(element).addClass(o.selected);
            } else {
               $(list).find('.'+o.selected).removeClass(o.selected);
               $(element).addClass(o.selected);
            }
            $last_selected = $(element);
        }

        if (o.stop !== false) {
            o.stop($(list).find('.'+o.selected), $(element));
        }

    }

    return this.each(function(k,v) {
        var options = $.extend({}, defaults, o || {});
        // selector - parent, assign listener to children only
        $(this).find(options.filter).on('mousedown', function(e) {
            if (e.which === 1){
                if (options.handle !== undefined && !$(e.target).is(options.handle)) {
                    // TODO:
                    // keep propagation?
                    // return true;
                }
                options.e = e;
                options.element = $(this);
                multiSelect(options);
            }
            return true;
        });

        // handle start/stop multi events
        if (options.start_multi !== false && options.multiselect) {
            $(this).keydown(function(e){
                if ((e.ctrlKey || e.metaKey || e.shiftKey) && in_multi_mode===false) {
                    options.start_multi();
                    in_multi_mode=true;
                }
            })
        }
        if (options.stop_multi !== false && options.multiselect) {
            $(this).keyup(function(e){
                if (in_multi_mode===true){
                    options.stop_multi();
                    in_multi_mode=false;
                }
            })
        }

        if (options.unselectOn) {
            // event to unselect
            options.unselectOn.on('mousedown', function(e) {
                if (!$(e.target).parents().is(options.list) && e.which !== 3) {
                    $(options.list+' .'+options.selected).removeClass(options.selected);
                    if (options.unselecting !== false) {
                        options.unselecting();
                    }
                }
            });

        }

    });


};


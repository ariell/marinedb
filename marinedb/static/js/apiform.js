
// the semi-colon before the function invocation is a safety
// net against concatenated scripts and/or other plugins
// that are not closed properly.

// undefined is used here as the undefined global
// variable in ECMAScript 3 and is mutable (i.e. it can
// be changed by someone else). undefined isn't really
// being passed in so we can ensure that its value is
// truly undefined. In ES5, undefined can no longer be
// modified.

// window and document are passed through as local
// variables rather than as globals, because this (slightly)
// quickens the resolution process and can be more
// efficiently minified (especially when both are
// regularly referenced in your plugin).

;(function ( $, window, document, undefined ) {

    // Create the defaults once
    var pluginName = 'APIForm';

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        this.$element = $(element);

        var defaults = {
            callback: function (data, form) {
                alert("SUCCESS!\n" + JSON.stringify(data));
            },
            method: this.$element.attr("method"),
            url: this.$element.attr("action"),
            onload: false,
            ajax_prefill: true
        };

        // The first object is empty because we don't want to alter the default
        // options for future instances of the plugin
        this.options = $.extend({}, defaults, options);

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype = {

        init : function () {
            var plugin = this;
            var $form = plugin.$element;

            // change submit behaviour to use ajax
            plugin.$element.submit(function (e) {
                e.stopPropagation();
                e.preventDefault();
                plugin.submitform();
            });

            // Add extra fields required for showing errors (only fields with valid names)
            plugin.$element.find(":input[name]").parent().append("<div class='form-error'></div>");
            // if no api-form error element is found, add a new one
            if (plugin.$element.find('.api-formerrors').length <= 0)
                plugin.$element.prepend($("<div class='api-formerrors alert alert-danger'><a class='close' title='Close' href='javascript:void(0)' onclick='$(this).parent().hide()'>&times;</a><div class='alert-content'></div></div>").hide());
            plugin.$element.find(":input[name]").change(function(){
                if ($(this).hasClass("error")) {
                    $(this).removeClass("error");
                    $(this).nextAll(".form-error").first().html("");
                }
            });

            // Method == patch, pre-fill form fields with get data
            if (plugin.options.method.toLowerCase() == "patch" && plugin.options.ajax_prefill) {
                plugin.getformdata(plugin.$element, plugin.options.url);
            }
            // execute onload function
            else if (typeof plugin.options.onload === 'function'){
                plugin.options.onload({}, $form);
            }
        },

        submitform : function () {
            var plugin = this;
            var $form = plugin.$element;
            var formdata = {};
            var errors = {};

            // reset any open quick add forms to prevent them from being submitted
            $form.find("form.quickadd").each(function (i, qaform) {
                $(qaform).html("");
            });

            // prep form data
            $form.find(':input').each(function (i, e) {
                if ($(e).data("nosubmit")) {
                    var nsdata = $(e).data("nosubmit");
                    if (nsdata['use'] === 'xvalidate') {
                        var $field = $form.find("input[name='" + nsdata['field'] + "']");
                        if ($(e).val() !== $field.val()) {
                            if (! errors.hasOwnProperty("validation_errors")) errors["validation_errors"] = {};
                            errors["validation_errors"][$(e).attr('name')] = $("label[for='" + $field.attr('id') + "']").text() + " values do not match";
                        }
                    }
                    else if (nsdata['use'] === 'reformat') {
                        var newval = [];
                        if ($(e).val())
                            $.each($(e).val(), function (i, el) {
                                var newel = {};
                                newel[nsdata['field']] = el;
                                newval.push(newel);
                            });
                        formdata[$(e).attr("name")] = newval;
                    }
                }
                else if ($(e).attr('name')) {
                    var val = $(e).val();
                    if ($(e).data("convert") === "json") {
                        val = JSON.parse(val);
                    }

                    //console.log($(e).attr('name'), $(e).attr('type'), val);
                    if ($(e).attr('type') === "radio"){
                        // if radio, check if 'true' or 'false' and convert to bool
                        if ($(e).is(':checked'))
                            formdata[$(e).attr("name")] = (val === "true") ? true : (val === "false") ? false : val;
                    }
                    else if ($(e).attr('type') === "checkbox") {
                        // if checkbox, see if 'true', and if checked and convert to bool
                        formdata[$(e).attr("name")] = (val === "true" && $(e).is(':checked')) ? true
                            : (val === "true" && !$(e).is(':checked')) ? false : val;
                        //console.log("CHECKBOX", $(e).attr("name"), formdata[$(e).attr("name")]);
                    }
                    // else if ($(e).attr('type') === "checkbox") {
                    //     if ($(e).is(':checked')) formdata[$(e).attr("name")] = val;
                    // }
                    else {
                        formdata[$(e).attr("name")] = (val === "") ? null :
                            (val === "true") ? true : (val === "false") ? false : val;
                    }
                }
            });

            //console.log(formdata);

            if (!$.isEmptyObject(errors))
                showformerrors($form, errors);
            else {
                //console.log(JSON.stringify(formdata));
                //console.log(plugin.options);
                ajax[plugin.options.method.toLowerCase()](plugin.options.url, JSON.stringify(formdata), {
                    success: function (data) {
                        $form.find("div.alert").hide();
                        plugin.options.callback(data, plugin.element);
                    },
                    fail_notify: function (msg, ajax_params) {
                        $form.find("div.alert").show().children(".alert-content").html(msg);
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    console.debug(jqXHR);
                    if (jqXHR.hasOwnProperty('responseJSON'))
                        showformerrors($form, jqXHR.responseJSON);
                });
                /*
                $.ajax({
                    type: plugin.options.method,
                    url: plugin.options.url,
                    data: JSON.stringify(formdata),
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        //if (data.hasOwnProperty('error'))  showformerrors($form, data['error']);
                        //else {
                            $form.find("div.alert").hide();
                            plugin.options.callback(data, plugin.element);
                        //}
                    },
                    dataType: "json"
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.hasOwnProperty('responseJSON'))
                        showformerrors($form, jqXHR.responseJSON);
                    console.log(jqXHR);
                });
                */
            }
        },
        getformdata: function ($form, url) {
            var plugin = this;
            $.get( url, function( data ) {
                //console.log(data);
                $form.find(":input[name]").each(function(i,e){
                    if (data.hasOwnProperty($(e).attr('name'))) {
                        if ($(e).data('nosubmit')) {
                            var nsdata = $(e).data("nosubmit");
                            if (nsdata['use'] == 'reformat') {
                                $.each(data[$(e).attr('name')], function(j,f){
                                    $(e).find("option[value='" + f[nsdata['field']] + "']").prop("selected", true);
                                });
                                // TODO: calling multiselect here? Is that generic? Check this is correct.
                                // NOTE: this is now better handled by the callback option "onload"
                                $(e).multiselect("refresh");
                            }
                        }
                        else {
                            $(e).val(data[$(e).attr('name')]);
                        }
                    }
                });
                // execute optional plugin onload
                if (typeof plugin.options.onload === 'function')
                    plugin.options.onload(data, $form);
            });
        }
    };


    function showformerrors($form, errors) {
        $form.find(":input").removeClass("error");
        $form.find('.form-error').html("");
        // $form.find("div.alert").hide();
        // var errshown = false;

        console.debug(errors);
        if (errors.hasOwnProperty('validation_errors')) {
            $.each(errors.validation_errors, function (k,v) {
                var $input = $form.find(":input[name='"+k+"']");
                $input.addClass("error");
                $input.nextAll(".form-error").first().html(v);
            });
            // errshown = true;
        }
        // NOTE: this is now handled by alert through ajax call (notify plugin)
        // if (errors.hasOwnProperty('message')) {
        //     $form.find("div.alert").show().children(".alert-content").html(errors.message);
        //     errshown = true;
        // }
        // if (! errshown) {
        //     $form.find("div.alert").show().children(".alert-content").html(JSON.stringify(errors));
        // }
    }

    // Register the plugin
    registerPlugin(pluginName, Plugin);

})( jQuery, window, document );





/**************************************************************************
  Lightweight plugin wrapper around the constructor,
  preventing against multiple instantiations
**************************************************************************/
function registerPlugin(pluginName, Plugin) {
    $.fn[pluginName] = function ( options, args ) {
        if (typeof args === "undefined") args = {};
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
            }
            // If called with string, then execute method with args
            else if (typeof options === 'string' || options instanceof String) {
                $(this).data('plugin_' + pluginName)[options](args);
            }
        });
    }
}
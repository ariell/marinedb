/******************************************************************************
 * HTML5 Multiple File Uploader Demo                                          *
 ******************************************************************************/

/*
var upload_options = {
        emstereo: {
            name: "EM/Stereo",
            fields: {
                campaign_key: {required: true},
                campaign_description: {required: false}
            },
            files: {
                source: {qtyreq: 1, pattern: "_(Source.txt)$", autoname_pattern:"^(.+)_([^_]+)$"},
                point3D: {qtyreq: 1, pattern: "_(3DPoints.txt)$"},
                camera: {qtyreq: 1, pattern: "_(Camera.txt)$"},
                imgptpair: {qtyreq: 1, pattern: "_(ImagePtPair.txt)$"},
                lengths: {qtyreq: 1, pattern: "_(Lengths.txt)$"},
                movieseq: {qtyreq: 1, pattern: "_(MovieSeq.txt)$"},
                period: {qtyreq: 1, pattern: "_(Period.txt)$"},
                points: {qtyreq: 1, pattern: "_(Points.txt)$"}
            }
        },
        misc: {
            name: "General",
            fields: {
                campaign_key: {required: true},
                campaign_description: {required: false}
            },
            files: {
                source: {required: true, qty: 1, pattern: "_(Source.txt)$"}
            }
        }
    };
 */



function Uploader (uploadurl, nexturl, $dropbox, upload_options) {

//    console.log("Uploader");


    // Constants
    var MAX_UPLOAD_FILE_SIZE = 1024 * 1024; // 1 MB
    var UPLOAD_URL = uploadurl;
    var NEXT_URL = nexturl;

    // List of pending files to handle when the Upload button is finally clicked.
    var PENDING_FILES = [];

    var _this = this;


    // Set up the drag/drop zone.
    initDropbox();

    // Set up the handler for the file input box.
    $("#file-picker").on("change", function () {
//        console.log("file picker change");
        handleFiles(this.files);
    });

    // Handle the submit button.
    $("#upload-button").on("click", function (e) {
        // If the user has JS disabled, none of this code is running but the
        // file multi-upload input box should still work. In this case they'll
        // just POST to the upload endpoint directly. However, with JS we'll do
        // the POST using ajax and then redirect them ourself when done.
        e.preventDefault();
        e.stopPropagation();
        doUpload();
    });



    function doUpload() {
        // Gray out the form.
        $("#upload-form :input").attr("disabled", "disabled");

        $("#upload-form").find("form.quickadd").each(function (i, qaform) {
            $(qaform).html("");
        });

        // Collect the form data.
        var formdata = collectFormData(upload_options);

        if (formdata.errors.length == 0) {
            $("#progress").show();
            var $progressBar = $("#progress-bar");
            // Initialize the progress bar.
            $progressBar.css({"width": "0%"});

            // Inform the back-end that we're doing this over ajax.
            formdata.fd.append("__ajax", "true");
            var xhr = $.ajax({
                xhr: function () {
                    var xhrobj = $.ajaxSettings.xhr();
                    if (xhrobj.upload) {
                        xhrobj.upload.addEventListener("progress", function (event) {
                            var percent = 0;
                            var position = event.loaded || event.position;
                            var total = event.total;
                            if (event.lengthComputable) {
                                percent = Math.ceil(position / total * 100);
                            }

                            // Set the progress bar.
                            $progressBar.css({"width": percent + "%"});
                            $progressBar.text(percent + "%");
                        }, false)
                    }
                    return xhrobj;
                },
                url: UPLOAD_URL,
                method: "POST",
                contentType: false,
                processData: false,
                cache: false,
                data: formdata.fd,
                success: function (data) {
                    $progressBar.css({"width": "100%"});
                    data = JSON.parse(data);

                    // How'd it go?
                    if (data.status === "error") {
                        // Uh-oh.
                        //window.alert(data.msg);
                        show_errors([data.msg]);

                        return;
                    }
                    else {
                        // Ok! Get the DIRID.
                        var cmpid = data.msg;

                        // TODO: This is DODGY - nested ajax requests. Fix this through proper API implementation
                        // Collect info fields and patch campaign
                        var info = [];
                        $(".campaigninfo_content").each(function (i, el) {
                            var name = $(el).find('[name="campaigninfo_name"]').val(),
                                desc = $(el).find('[name="campaigninfo_description"]').val();
                            if (name!=="" && desc!=="") {
                                info.push({name: name, description: desc});
                            }
                        });
                        //console.log(info);
                        if (info.length > 0) {
                            $.ajax({
                                method: "patch",
                                url: "/api/campaign/"+cmpid,
                                //async: false,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({info:info}),
                                success: function (data) {
                                    window.location = NEXT_URL + cmpid;
                                    //console.log(data, {info: info});
                                }
                            });
                        }
                        else {
                            window.location = NEXT_URL + cmpid;
                        }
                    }
                }
            });
        }
        else {
            show_errors(formdata.errors);
        }
    }

    function show_errors(errors) {
        $("#upload-error").html("<b>ERRORS:</b>");
        $(errors).each(function(i, er){
            $("#upload-error").append("<br>"+er);
        });
        $("#upload-form :input").removeAttr("disabled");
        $("#upload-error").show();
    }


    function collectFormData(upload_options) {
        // Go through all the form fields and collect their names/values.
        var fd = new FormData();

        var val;
        var type = getFieldValue('campaign_type');
        var fieldinfo = upload_options[type];
        var errors = [];


//        $("#upload-form :input").each(function () {
//            var $this = $(this);
//            var name = $this.attr("name");
//            var type = $this.attr("type") || "";
//            var value = $this.val();
//
//            // No name = no care.
//            if (name === undefined) {return;}
//
//            // Skip the file upload box for now.
//            if (type === "file") {return;}
//
//            // Checkboxes? Only add their value if they're checked.
//            if (type === "checkbox" || type === "radio") {
//                if (!$this.is(":checked")) {
//                    return;
//                }
//            }
//
//            fd.append(name, value);
//        });

        // Attach the files.
        var re;
        for (f in fieldinfo.files) fieldinfo.files[f].qty = 0;
        for (var i = 0, ie = PENDING_FILES.length; i < ie; i++) {
            // Collect the other form data.
            fd.append("file", PENDING_FILES[i]);

            for (f in fieldinfo.files) {
                re=new RegExp(fieldinfo.files[f].pattern,"g");
                if (re.test(PENDING_FILES[i].name)) {
                    fieldinfo.files[f].qty ++;
                    break;
                }
            }
        }

        // Validate text
        for (var f in fieldinfo.fields) {
            val = getFieldValue(f);
            fd.append(f, val);
            if (fieldinfo.fields[f].required && val=="")
                errors.push($("label[for='"+f+"']").text()+" cannot be empty.");
        }


        // Check all the required files were collected
        for (f in fieldinfo.files)
            if (fieldinfo.files[f].qtyreq != fieldinfo.files[f].qty)
                errors.push("Need "+fieldinfo.files[f].qtyreq+" file matching pattern: "+fieldinfo.files[f].pattern);
        return {fd: fd, errors: errors};
    }

    function getFieldValue(name) {
        return $("#upload-form :input[name='"+name+"']").val();
    }

    function getAutoName (files, fname) {
        var re, autoname='';
        for (f in files) {
            re=new RegExp(files[f].pattern,"g");
            if (re.test(fname)) {
                if (files[f].hasOwnProperty("autoname_pattern")) {
                    re = new RegExp(files[f].autoname_pattern, "g");
                    autoname = re.exec(fname)[1];
                }
                break;
            }
        }
        return autoname;
    }

    function handleFiles(files) {
        var type = getFieldValue('campaign_type');
        var fieldinfo = upload_options[type];
        var cmpname = "";
        var $filelist = $dropbox.find('.filelist');
//        console.log(files);
        // Add them to the pending files list.
        for (var i = 0, ie = files.length; i < ie; i++) {
            PENDING_FILES.push(files[i]);
            $filelist.append('<br><i class="fa fa-file"> '+files[i].name+' ('+Math.round(files[i].size/10.24)/100+' KB)</i>');
            cmpname = getAutoName (fieldinfo.files, files[i].name);
            if (cmpname) $("#campaign_key").val(cmpname);
        }

        // Update the display to acknowledge the number of pending files.
//        $dropbox.text(PENDING_FILES.length + " files ready for upload!");
    }


    function initDropbox() {
        // On drag enter...
//        console.log("initDropbox");
        $dropbox.on("dragenter", function (e) {
            stopDefault(e);
            $(this).addClass("active");
        });

        // On drag over...
        $dropbox.on("dragover", stopDefault);

        // On drop...
        $dropbox.on("drop", function (e) {
            //e.preventDefault();
            stopDefault(e);
            $(this).removeClass("active");

            // Get the files.
//            console.log("drop box drop");
            var files = e.originalEvent.dataTransfer.files;
            handleFiles(files);
        });

        // If the files are dropped outside of the drop zone, the browser will
        // redirect to show the files in the window. To avoid that we can prevent
        // the 'drop' event on the document.
        function stopDefault(e) {
            e.stopPropagation();
            e.preventDefault();
        }

        $(document).on("dragenter", stopDefault);
        $(document).on("dragover", stopDefault);
        $(document).on("drop", stopDefault);
    }
}
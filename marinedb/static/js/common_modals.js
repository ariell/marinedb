var dataset_modal = {
    resource_groups: function(resource, options) {
        var opts = $.extend({
            modal_id:'resource-groups-modal',
            modal_class:'modal-xl',
            load_func: load_content.paginated_html,
            select_group_id: "all",  // select_group_id: (all|mine|shared|public|group_id)
            list_args: {},  // list_args: dict containing key values to pass to object list
            title: null,
            q: null,
            oncompleted: function(data) {console.debug("Modal complete", data)}
        }, options);
        var q = {
            filters:[
                {name:"shared_"+resource+"s",op:"any",val:{name:"id",op:"is_not_null"}},
                {or:[{name:"current_user_is_owner",op:"eq",val:true},{name:"current_user_is_member",op:"eq",val:true},
                {name:"is_public",op:"eq",val:true}]}],
            order_by: [
                {field:"current_user_is_owner", direction:"desc"},{field:"current_user_is_member",direction:"desc"},
                {field:"is_public", direction:"desc"},{field:"name",direction:"asc"}]};
        var template = "models/groups/group_resource_list.html";
        var url = '/api/groups?results_per_page=200&template='+template+'&resource='+resource+'&q='+JSON.stringify(q);
        url=url.updateURIParam('group_id',opts.select_group_id);
        url=url.updateURIParam('list_args',opts.list_args, false);
        if (opts.title) url=url.updateURIParam('title',opts.title);
        if (opts.q) url=url.updateURIParam('q', opts.q);
        open_modal(url, opts);
    },
    /*
    media_collection_groups: function(current_media_collection_id, current_annotation_set_id, options) {
        var url = '/api/media_collection/groups?results_per_page=200&template=models/media_collection/group_select_list.html' +
            '&media_collection_id='+current_media_collection_id+'&annotation_set_id='+current_annotation_set_id;
        var opts = $.extend({
            modal_id:'collection-groups-modal',
            modal_class:'modal-xl',
            load_func: load_content.paginated_html,
            oncompleted: function(data) {
                location.href = "/geodata/collection" +
                    "?media_collection_id=" + data.media_collection_id +
                    "&annotation_set_id=" + data.annotation_set_id;
            }
        }, options);
        open_modal(url, opts);
    },
    */
    media_collection_groups: function(current_media_collection_id, current_annotation_set_id, options) {
        var opts = $.extend({
            oncompleted: function(data) {
                location.href = "/geodata/annotation_set/" + data.annotation_set_id;
            },
            select_group_id: "mine",
            title: "Manage My Datasets",
            list_args: {media_collection_id: current_media_collection_id, annotation_set_id: current_annotation_set_id},
        }, options);
        dataset_modal.resource_groups("media_collection", opts);
    },
    manage_groups: function(options) {
        /*
        var group_q = {filters:[{name:"user_id",op:"is_not_null"}], order_by:[
            {field:"current_user_is_owner", direction:"desc"},{field:"current_user_is_member",direction:"desc"},
            {field:"current_user_is_pending_approval",direction:"desc"},{field:"is_public", direction:"desc"},
            {field:"name",direction:"asc"}]};
        var url = '/api/groups?results_per_page=200&q='+JSON.stringify(group_q)+'&template=models/groups/manage_my_groups.html';
        */
        var url = "/ui/models/groups/manage_my_groups.html";

        var opts = $.extend({
            modal_id: 'manage_groups-modal',
            modal_class: 'modal-lg',
            load_func: load_content.paginated_html
        }, options);
        open_modal(url, opts);
    },
    /*
    label_scheme_groups: function(id, options) {
        var url = '/api/label_scheme?template=models/label_scheme/select_list.html';
        var opts = $.extend({
            modal_id:'label_scheme-modal',
            modal_class:'modal-xl',
            load_func: load_content.paginated_html,
            oncompleted: function(data) {
                console.debug("COMPLETED TAG_SCHEME MODAL, data: ", data);
            },
            title: "Manage Annotation Schemes"
        }, options);
        open_modal(url, opts);
    },
    */
    media: {
        view: function(id, options) {
            var opts = $.extend({
                modal_id:'media-modal',
                modal_class:'modal-xl modal-dark'
            }, options);
            open_modal('/api/media/'+id+'?template=models/media/preview_single.html', opts)
        },
        remove: function(id, ajax_options) {
            ajax_options = $.extend({
                success: function(){
                    //$modal.trigger('completed', null);
                    notify.success("Media Object has been deleted.")
                }}, ajax_options);
            ajax.get("/api/media/"+id).done(function(data){
                ajax.delete('/api/media/'+id, ajax_options,
                    'Are you sure you want to delete this Media object: <br><br>' +
                    data.deployment.campaign.name+' > '+ data.deployment.name +' > '+ data.key +"? <br><br>"+
                    'It contains '+data.annotation_count+' annotations. This cannot be undone.'
                )
            });
        },
        annotate: function(media_id, annotation_set_id, params, options) {
            var url = '/geodata/annotate/'+media_id+'/'+annotation_set_id;
            var opts = $.extend({
                modal_id:'media-modal-annotate',
                modal_class:'modal-full modal-dark',
                show_close:false,
                keyboard:false,
                prevent_submodal_hide: true
            }, options);
            var qparams = $.extend({}, params);
            //console.debug(params, qparams, url);
            $.each(qparams, function(k,v){
                url = url.updateURIParam(k, (typeof v==='object') ? JSON.stringify(v) : v);
            });
            //console.debug(url);
            open_modal(url, opts);
        }
    },
    media_collection: {
        remove: function(id, ajax_options) {
            var ajopts = $.extend({
                success: function(data){console.error("TODO: implement callback for media_collection delete.")}
            }, ajax_options);
            return ajax.delete('/api/media_collection/'+id, ajopts, "Are you sure you want to delete this Media " +
                "Collection? <b>You can not undo this operation.</b><br><br><i class='fa fa-exclamation-triangle'></i>" +
                " All Annotation Sets and annotations will be permanently lost!");
        },
        update: function(id, options, qsparams) {
            var url = "/api/media_collection/"+id+
                "?template=models/media_collection/edit.html";  // edit.html | edit_simple.html
            var opts = $.extend({
                modal_id: 'edit-media_collection-modal',
                modal_class: 'modal-md',
                hide_open_modals: true,
                oncompleted: function(data){console.error("TODO: implement oncompleted option for modal: ",data)}
            }, options);

            $.each(qsparams || {}, function (k, v) {
                url = url.updateURIParam(k, v);
            });

            return open_modal(url, opts);
        },
        create: function(qsparams, options) {
            var url = '/ui/models/media_collection/edit.html';
            var opts = $.extend({
                modal_class:'modal-md',
                oncompleted: function(data){dataset_modal.media_collection_groups(data.id, null, {})}
            }, options);
            $.each(qsparams || {}, function (k, v) {
                url = url.updateURIParam(k, v);
            });
            return open_modal(url, opts);
        },
        share: function(id, options) {
            dataset_modal.share_resource(id, "media_collection", options);
        },
        export: function(id, options) {
            var template = 'geodata/partials/export_dataset.html&resource=media_collection';
            // var template = 'models/media_collection/export_form.html';
            var url = '/api/media_collection/'+id+'?template='+template;
            var opts = $.extend({
                modal_class:'modal-lg',
                hide_open_modals: true
            }, options);
            return open_modal(url, opts);
        },
        remove_media_item: function(id, media_id, ajax_options) {
            ajax_options = $.extend({
                success: function(data){
                    notify.success("Media Object has been removed from this Media Collection.");
                    console.debug("TODO: implement callback for media_collection delete.");
                }
            }, ajax_options);
            return ajax.delete('/api/media_collection/'+id+'/media/'+media_id, ajax_options,
                "Are you sure you want to remove this Media Object from this Media Collection? " +
                "All annotations from associated Annotation Sets will also be removed.");
        },
        add_media_item: function (id, media_id, options) {
            notify.info("coming soon...")
        }
    },
    annotation_set: {
        remove: function(id, ajax_options) {
            var ajopts = $.extend({
                success: function(data){console.error("TODO: implement callback for annotation_set delete.")}
            }, ajax_options);
            return ajax.delete('/api/annotation_set/'+id, ajopts, "Are you sure you want to delete this Annotation Set." +
                " <b>You can not undo this operation.</b><br><br><i class='fa fa-exclamation-triangle'></i> All " +
                "annotations will be permanently lost!");
        },
        update: function(id, options) {
            var url = "/api/annotation_set/"+id+
                "?template=models/annotation_set/edit.html";  // edit.html | edit_simple.html
            var opts = $.extend({
                modal_id: 'edit-annotation_set-modal',
                modal_class: 'modal-md',
                hide_open_modals: true,
                oncompleted: function(data){console.error("TODO: implement oncompleted option for modal: ",data)}
            }, options);
            return open_modal(url, opts);
        },
        create: function(media_collection_id, qsparams, options) {
            var url = '/ui/models/annotation_set/edit.html?media_collection_id='+media_collection_id;
            var opts = $.extend({
                modal_class:'modal-md',
                oncompleted: function(data){console.log("TODO: implement callback for annotation_set create.")}
            }, options);
            $.each(qsparams || {}, function (k, v) {
                url = url.updateURIParam(k, v);
            });
            return open_modal(url, opts);
        },
        share: function(id, options) {
            dataset_modal.share_resource(id, "annotation_set", options);
        },
        export: function(id, options) {
            var template = 'geodata/partials/export_dataset.html&resource=annotation_set';
            // var template = 'models/annotation_set/export_form.html';
            var url = '/api/annotation_set/'+id+'?template='+template;
            var opts = $.extend({
                modal_class:'modal-lg',
                hide_open_modals: true
            }, options);
            return open_modal(url, opts);
        },
        remove_media_item: function(id, media_id, ajax_options) {
            ajax_options = $.extend({
                success: function(data){
                    notify.success("Media Object has been removed from this Media Collection.");
                    console.debug("TODO: implement callback for media_collection delete.");
                }
            }, ajax_options);
            return ajax.delete('/api/annotation_set/'+id+'/media/'+media_id, ajax_options,
                "Are you sure you want to remove this Media Object from this Media Collection? " +
                "All annotations from associated Annotation Sets will also be removed.");
        }
    },
    label_scheme: {
        remove: function(id, ajax_options) {
            var ajopts = $.extend({
                success: function(data){console.error("TODO: implement callback for dataset delete.")}
            }, ajax_options);
            return ajax.delete('/api/label_scheme/'+id, ajopts, "Are you sure you want to delete this Annotation Scheme." +
                " <b>You can not undo this operation.</b><br><br><i class='fa fa-exclamation-triangle'></i> All " +
                "annotations will be permanently lost!");
        },
        update: function(id, options) {
            var url = "/api/label_scheme/"+id+
                "?template=models/label_scheme/edit.html";  // edit.html | edit_simple.html
            var opts = $.extend({
                modal_id: 'edit-label_scheme-modal',
                modal_class: 'modal-md',
                hide_open_modals: true,
                oncompleted: function(data){console.error("TODO: implement oncompleted option for modal: ",data)}
            }, options);
            return open_modal(url, opts);
        },
        create: function(qsparams, options) {
            var url = '/ui/models/label_scheme/edit.html';
            var opts = $.extend({
                modal_class:'modal-md',
                oncompleted: function(data){console.log("TODO: implement callback for label_scheme create.")}
            }, options);
            $.each(qsparams || {}, function (k, v) {
                url = url.updateURIParam(k, v);
            });
            return open_modal(url, opts);
        },
        share: function(id, options) {
            dataset_modal.share_resource(id, "label_scheme", options);
        }
    },
    label: {
        update: function(id, options) {
            var url = "/api/label/"+id+ "?template=models/label/edit.html";
            var opts = $.extend({
                modal_id: 'edit-label-modal',
                modal_class: 'modal-lg',
                hide_open_modals: true,
                oncompleted: function(data){console.error("TODO: implement oncompleted option for modal: ",data)}
            }, options);
            return open_modal(url, opts);
        },
        manage_mappings: function(id, options) {
            var url = "/api/label/"+id+ "?template=models/label/map_vocab_elements.html";
            var opts = $.extend({
                modal_id: 'manage-mappings-label-modal',
                modal_class: 'modal-lg',
                hide_open_modals: true,
                oncompleted: function(data){console.error("TODO: implement oncompleted option for modal: ",data)}
            }, options);
            return open_modal(url, opts);
        },
        create: function(label_scheme_id, options) {
            var url = '/ui/models/label/edit.html';
            var opts = $.extend({
                modal_class:'modal-md'
            }, options);
            // switch to update window after create to add additional options and instead run oncompleted after update window
            opts.oncompleted = function(data){
                var uopt = options.hasOwnProperty('oncompleted') ? {oncompleted: options.oncompleted} : {};
                dataset_modal.label.update(data.id, uopt);
            };
            url = url.updateURIParam('label_scheme_id', label_scheme_id);
            return open_modal(url, opts);
        }
    },
    share_resource: function (id, resource, options) {
        //var url = "/api/"+resource+"/"+id+"/groups" +
        var url = "/api/"+resource+"/"+id +
            "?template=models/groups/add_resource_to_group.html&resource="+resource;
        var opts = $.extend({
            modal_class: 'modal-lg',
            hide_open_modals: true,
            onhidden: function(){console.log("TODO: onhidden option for share modal not implemented")}
        }, options);
        return open_modal(url, opts);
    }
};

var user_modal = {
    login: function(confirm, options) {
        if (confirm) {
            notify.confirm(confirm, {ok: function(){user_modal.login(false, options)}});
        }
        else {
            var opts = $.extend({
                modal_class: 'modal-lg',
                oncompleted: function(){location.reload()}
            }, options);
            open_modal("/ui/models/users/login.html", opts);
        }
    },
    logout: function (ajax_options) {
        var opts = $.extend({
            success: function(data) {
                window.location.href = '/';
            }
        }, ajax_options);
        ajax.delete("/api/users/login", opts, "Are you sure you want to sign out?", {
            title:"Sign out", icon:"<i class='fa fa-user-times'></i>"
        });
    },
    create: function(options) {
        var opts = $.extend({
            modal_class: 'modal-lg',
            oncompleted: function(data){
                user_modal.login();
                notify.success("Your account has been created. Please login.");
            }
        }, options);
        open_modal("/ui/models/users/edit.html", opts);
    },
    update: function(user_id, options) {
        var opts = $.extend({
            modal_class: 'modal-lg',
            oncompleted: function(data){
                user_modal.update(user_id);
                notify.success("User details have been updated.");
            }
        }, options);
        open_modal("/api/users/"+user_id+"?template=models/users/edit.html", opts);
    },
    api_token: function(options) {
        var opts = $.extend({
            modal_id:'user-api_token-modal',
            modal_class: 'modal-md'
        }, options);
        open_modal('/ui/models/users/api_token.html', opts);
    },
    reset_password: function(options) {
        var opts = $.extend({
            modal_id:'user-password_reset-modal',
            modal_class: 'modal-md',
            hide_open_modals: true
        }, options);
        open_modal('/api/users/reset_password', opts);
    },
    contact: function(user_id, options) {
        var opts = $.extend({
            modal_id: 'message_member-modal',
            modal_class: 'modal-md',
            hide_open_modals: true
        }, options);
        open_modal('/api/users/'+user_id+'?template=models/users/message_user.html', opts);
    },
    admin_switch: function(username) {
        notify.confirm("Are you sure you want to switch to user: "+username, {ok: function(){
            ajax.post('/api/users/login', JSON.stringify({username:username}), {success:function(data){
                notify.info("<b>"+data.message+"</b><br>Refresh view to update. Logout or click " +
                    "<a href='#' class='' onclick='user_modal.logout()'>here</a> to switch back", -1);
            }})
        }, title:"ADMIN USER SWITCH?", icon:"<i class='fa fa-lock'></i>"});
    }
};

var info_modal = {
    api_help: function(collection, options) {
        var url = (typeof collection !== "undefined") ? "/api/help/"+collection : "/api/help";
        // // var template = (typeof collection !== "undefined") ? "partials/api_help_detailed.html" : "partials/api_help.html";
        // var template = "partials/api_help.html";
        // url = url.updateURIParam("template", template);
        // var opts = $.extend({modal_class: 'modal-xl'}, options);
        // open_modal(url, opts);
        var template = "api_help_page.html";
        url = url.updateURIParam("template", template);
        window.open(url)
    }
};

var doc_modal = {
    wiki: function(doc_path, options) {
        var url = "/wiki_page/"+doc_path;
        var opts = $.extend({$container:null}, options)
        if (opts.$container) {
            return load_content.html(url, options.$container, function(){
                doc_modal._prep_content(options.$container, false)
            })
        }
        else {    // open in modal
            opts = $.extend({
                modal_id: 'wiki-modal',
                modal_class: 'modal-lg wiki-content',
                backdrop: true,
                onshown: function () {doc_modal._prep_content($(this), true)}
            }, options);
            return open_modal(url, opts);
        }
    },
    _prep_content: function($container, ext_all_links) {
        // external links open in blank doc
        if (ext_all_links) $container.find("a:not([href^='#'])").attr("target","_blank");
        else $container.find("a[href^='http']").attr("target","_blank");
        // youtube vids
        // TODO
    }

};
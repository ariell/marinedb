# -*- coding: utf-8 -*-
'''The app module, containing the app factory function.'''
# import inspect
# import json
import inspect
import json
import os
import re

from flask import Flask, render_template, url_for, request, redirect
# from flask_sqlalchemy import models_committed
# from sqlalchemy.orm.session import sessionmaker
from pandas import to_datetime, json_normalize

from marinedb.settings import ProdConfig
from marinedb.assets import assets
from marinedb.extensions import (
    bcrypt,
    db,
    login_manager,
    migrate,
    debug_toolbar,
    apimanager,
    marshmallow,
    compress,
    humanize,
    mail)
from marinedb import public, user, geodata, annotation
# from marinedb.utils import init_app_config
from marinedb.utils import init_app_config
from modules.datatransform.data_modules import json_dumps
from modules.flask_restless.mixins import register_api
from marinedb.user.api import user_token_login
from modules.flask_restless.helpers import url_for as api_url_for
# import flask_monitoringdashboard as dashboard

# import markdown
# import markdown.extensions.fenced_code
# import markdown.extensions.codehilite

from dateutil.relativedelta import relativedelta

# Attempt to fix recursion limit error seen when exporting annotations
# TODO: this should be fixed properly to avoid error from occuring - do proper stacktrace
import sys
sys.setrecursionlimit(1500)


def create_app(config_object=ProdConfig):
    '''An application factory, as explained here:
        http://flask.pocoo.org/docs/patterns/appfactories/

    :param config_object: The configuration object to use.
    '''
    app = init_app_config(config_object)
    # app = Flask(__name__)
    # app.config.from_object(config_object)
    # app.config.from_pyfile('settings.cfg', silent=True)  # extend settings from cfg file if available

    register_extensions(app)
    register_blueprints(app)
    register_errorhandlers(app)
    register_template_filters(app)
    register_template_functions(app)
    return app


def register_extensions(app):
    assets.init_app(app)
    bcrypt.init_app(app)
    # cache.init_app(app)
    db.init_app(app)
    # register_dbviews(app)  # some DB views backref to models
    login_manager.init_app(app)
    migrate.init_app(app=app, db=db)
    build_api(app, db)
    marshmallow.init_app(app)
    compress.init_app(app)
    humanize.init_app(app)
    debug_toolbar.init_app(app)
    mail.init_app(app)
    # Markdown(app)
    # dashboard.bind(app)
    # register_events(app)
    return None


def register_blueprints(app):
    app.register_blueprint(public.views.blueprint)
    app.register_blueprint(user.views.blueprint)
    app.register_blueprint(geodata.views.blueprint)
    app.register_blueprint(annotation.views.blueprint)
    return None


def build_api(app, db, build_docs=False):
    if not build_docs:
        apimanager.init_app(app, session=db.session, flask_sqlalchemy_db=db)

        # Check if in maintenance mode, and divert to
        @app.before_request
        def check_for_maintenance():
            user_token_login()  # Check headers to log in user with API key if included in header

            # TODO: check for admin user and if ADMIN, allow override of maintenance mode for testing
            if app.config.get("IN_MAINTENANCE_MODE", False) is True:
                # Redirect for maintenance mode (if applicable)
                maintenance_url = url_for('public.maintenance_mode')
                if request.path != maintenance_url:
                    return redirect(maintenance_url)

    with app.app_context():
        register_api(apimanager, app, build_docs=build_docs)

    return None


# def register_dbviews(app):
#     # database views need to be registered at runtime because they need the application context for the db engine
#     # registration happens by importing
#     with app.app_context():
#         from marinedb.geodata.dbviews import CampaignMV, DeploymentMV

# def register_events(app):
#     # TODO: move this to a more appropriate place
#     # add commit delete message to database models (allows running of code on delete)
#     @models_committed.connect_via(app)
#     def on_models_committed(sender, changes):
#         for obj, change in changes:
#             if change == 'delete' and hasattr(obj, '__commit_delete__'):
#                 obj.__commit_delete__()
#             elif change == 'insert' and hasattr(obj, '__commit_insert__'):
#                 obj.__commit_insert__()
#             elif change == 'update' and hasattr(obj, '__commit_update__'):
#                 obj.__commit_insert__()
#
#     # @before_models_committed.connect_via(app)
#     # def on_before_models_commited(sender, changes):
#     #     print "\n\n\nCHANGES BEFORE COMMIT", changes
#     #     for obj, change in changes:
#     #         if change == 'delete' and hasattr(obj, '__before_commit_delete__'):
#     #             obj.__before_commit_delete__()
#     #         elif change == 'insert' and hasattr(obj, '__before_commit_insert__'):
#     #             obj.__before_commit_insert__()
#     #         elif change == 'update' and hasattr(obj, '__before_commit_update__'):
#     #             obj.__before_commit_update__()
#     return None


def register_errorhandlers(app):
    def render_error(error):
        # If a HTTPException, pull the `code` attribute; default to 500
        error_code = getattr(error, 'code', 500)
        return render_template("{0}.html".format(error_code)), error_code
    for errcode in [401, 404, 500]:
        app.errorhandler(errcode)(render_error)
    return None


def register_template_filters(app):
    # Custom filter for jinja template
    # @app.template_filter('to_json')  # alternative method for registering filter
    # def as_json(value):
    #     return jsonify(value)  # better than json.dumps as it handles dates

    # register extensions
    app.jinja_env.add_extension('jinja2.ext.do')

    # this registration method makes it possible to add imported functions
    app.jinja_env.filters['as_json'] = lambda value: json_dumps(value)
    app.jinja_env.filters['from_json'] = lambda value: json.loads(value)

    # This helps to convert a list of dicts to a dataframe, for using dataframe templates (data.objects)
    app.jinja_env.filters['to_dataframe'] = lambda value: json_normalize(value)

    #
    app.jinja_env.filters['basename'] = lambda value: os.path.basename(value)



    # api_docs links
    @app.template_filter('md_to_html')
    def md_to_html(value, wrap_paragraphs=False):
        try: value = inspect.cleandoc(value)    # remove indentation
        except: pass
        #try: value = re.sub(r'@{(.*?)}', r'<code><a href="#\1" target="doc_link">\1</a></code>', value)
        #except: pass
        try: value = re.sub(r'\[(.*?)\]\((.*?)\)', r'<code><a href="\2" target="doc_link">\1</a></code>', value)
        except: pass
        try: value = re.sub(r'```\n?(.*?)\n?```', r'<pre><code class="hljs bash">\1</code></pre>', value, flags=re.S)   # multiline match
        except: pass
        try: value = re.sub(r'`(.*?)`', r'<code>\1</code>', value)
        except: pass
        try: value = re.sub(r'\*\*(.*?)\*\*', r'<strong>\1</strong>', value)
        except: pass
        try: value = re.sub(r'\*(.*?)\*', r'<em>\1</em>', value)
        except: pass
        if wrap_paragraphs:
            try:value = re.sub(r'(\n\n)+(?![<code>]*</code>)', '</p><p>', value)
            except:pass
            value = "<p>{}</p>".format(value)
        # try:
        #     value = markdown.markdown(
        #         value  #, extensions=["fenced_code", "codehilite"]
        #     )
        # except: pass
        return value

    @app.template_filter('format_datestring')
    def format_datestring(value, format="%Y-%m-%d %H:%M:%S", offset_days=None, offset_months=None):
        try:
            ts = to_datetime(value)
            if offset_days is not None:
                ts = ts + relativedelta(days=offset_days)
            if offset_months is not None:
                ts = ts + relativedelta(months=offset_months)
            return ts.strftime(format)
        except:
            return value

    @app.template_filter('parse_datestring')
    def parse_datestring(value, format=None):
        try: return to_datetime(value, format=format)
        except: return value






def register_template_functions(app):
    app.jinja_env.globals.update(api_url_for=api_url_for)


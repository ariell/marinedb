# -*- coding: utf-8 -*-
"""Extensions module. Each extension is initialized in the app factory located
in app.py
"""

from flask_bcrypt import Bcrypt
bcrypt = Bcrypt()

from flask_login import LoginManager
login_manager = LoginManager()

from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()

from flask_migrate import Migrate
migrate = Migrate()

# from flask_cache import Cache
# cache = Cache(config={'CACHE_TYPE': 'simple'})

from flask_debugtoolbar import DebugToolbarExtension
debug_toolbar = DebugToolbarExtension()

from flask_marshmallow import Marshmallow
marshmallow = Marshmallow()

from flask_compress import Compress
compress = Compress()

from werkzeug.contrib.cache import SimpleCache
cache = SimpleCache()


from modules.flask_restless import APIManager
apimanager = APIManager()
# from modules.flask_restless.mixins import APIManagerAutodoc
# apimanager = APIManagerAutodoc()

# humanise filters for flask
from flask_humanize import Humanize
humanize = Humanize()

# email
from flask_mail import Mail
mail = Mail()

# # Markdown rendering
# from flaskext.markdown import Markdown

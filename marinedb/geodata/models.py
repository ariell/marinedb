# -*- coding: utf-8 -*-
import datetime
import datetime as dt
import json
import os
from uuid import UUID

# import requests
import dateutil.parser
from dateutil.parser import parse
from flask import current_app, request, g
from flask_login import current_user
from geoalchemy2 import Geometry, Geography
from geoalchemy2.comparator import Comparator
from geoalchemy2.shape import to_shape, from_shape
from pandas import to_datetime
from shapely import wkb

from sqlalchemy.dialects.postgresql import JSON, INTERVAL
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy import asc, desc, distinct, text, Computed, Table
# from sqlalchemy.orm import column_property
import numpy as np
from sqlalchemy.orm import column_property
from werkzeug.urls import url_fix
from werkzeug.utils import secure_filename
from urllib import parse as urlparse

from marinedb.utils import get_db_engine
from marinedb.database import (
    Column,
    db,
    Model,
    ReferenceCol,
    relationship,
    backref,
    SurrogatePK,
    UniqueConstraint,
    DBFileMixin,
    JSONDataFieldMixin #, create_mat_view, MaterializedView,
)

from sqlalchemy.ext.associationproxy import association_proxy

import random
from marinedb.annotation.models import Label, Tag, LabelAssociation
from sqlalchemy.sql import select
from sqlalchemy import func, case
# import timeit
from marinedb.user.models import Group
from modules.flask_restless import ProcessingException
from modules.flask_restless.helpers import count
# from modules.flask_restless.usergroups import APIUserGroupMixin
import math
from timezonefinder import TimezoneFinderL
import pytz


# from modules.flask_restless.materialized_view_factory import MaterializedView, create_mat_view
from modules.flask_restless.mixins import APIModel
from modules.flask_restless.search import search
from modules.flask_restless.usergroups import APIUserGroupMixin
from modules.datatransform.utils import nullifnan

db_engine = get_db_engine()            # for views

tf = TimezoneFinderL()


def get_random_hex_color():
    return "#%06x" % random.randint(0, 0xFFFFFF)


def gc_distance(lat1, lon1, lat2, lon2, math=math):
    deg2rad = 57.29577951308232
    return (6371000 *
            math.acos(math.sin(lat1 / deg2rad) * math.sin(lat2 / deg2rad) +
                      math.cos(lat1 / deg2rad) * math.cos(lat2 / deg2rad) *
                      math.cos(lon2 / deg2rad - lon1 / deg2rad)))


def euclid_dist(x1, y1, x2, y2, math=math):
    return math.pow((math.pow((x2-x1),2) + math.pow((y2-y1),2)),0.5)




class AnnotationPoint(APIModel, Model, SurrogatePK, JSONDataFieldMixin):
    __tablename__ = 'point'
    __mapper_args__ = {'polymorphic_identity': __tablename__}

    x = Column(db.Float(precision=53), unique=False, nullable=True,
               doc="X location of point as a proportion across the frame width (0-1.0, measured from the left, positive right) or `latitude` for map objects")
    y = Column(db.Float(precision=53), unique=False, nullable=True,
               doc="Y location of point as a proportion across the frame height (0-1.0, measured from the top, positive down) or `longitude` for map objects")
    t = Column(db.Float(precision=53), unique=False, nullable=True,
               doc="Number of seconds of annotation relative to `media.start_time` (eg: for video)")
    media_id = ReferenceCol('media', nullable=False, cascade=True, index=True,
               doc="Foreign key `id` of [media](#media) object linked to [point](#point)")
    media = relationship('Media', doc="Related [media](#media) object",
               backref=backref('annotations', passive_deletes=True, lazy="dynamic", doc="List of [point](#point) objects"))
    annotation_set_id = ReferenceCol('annotation_set', nullable=False, cascade=True,
               doc="Foreign key `id` of the parent/origin [annotation_set](#annotation_set). I.e.: the one linked to the initial creation of the [point](#point)")
    annotation_set = relationship('AnnotationSet', doc="Parent/origin [annotation_set](#annotation_set) object",
               backref=backref('points', passive_deletes=True, lazy="dynamic", doc="List of [point](#point) objects"))
    updated_at = Column(db.TIMESTAMP, default=dt.datetime.utcnow, onupdate=dt.datetime.utcnow, doc="Updated time (UTC)")

    def __init__(self, user_id=None, labels_per_point=1, annotation_label=None, set_media=None, x=None, y=None,
                 t=None, row=None, col=None, annotations=None, **kwargs):
        db.Model.__init__(self, **kwargs)

        # condition some values in case
        if x is not None and y is not None:
            self.x, self.y = nullifnan(x), nullifnan(y)
        elif isinstance(row, int) and isinstance(col, int):
            self.pixels = dict(row=row, col=col, polygon=kwargs.pop('polygon', None))

        self.t = nullifnan(t)

        # initialise media object if relevant
        if isinstance(set_media, dict):
            self.set_media = set_media

        if isinstance(annotations, list):  # use passed in annotations
            self.annotations = annotations
        elif isinstance(annotation_label, dict):    # set with annotation_label property setter
            self.annotation_label = annotation_label
        else:                       # initialise with empty labels
            labels_per_point = max(min(labels_per_point, 10), 1) if isinstance(labels_per_point, int) else 1
            for i in range(labels_per_point):
                self.annotations.append(AnnotationLabel(user_id=user_id, annotation_set_id=self.annotation_set_id))
            # self.mungedfile = None

    def __repr__(self):
        return '<AnnotationPoint(ID: {})>'.format(self.id)

    def bbox(self, default_size=0.2, max_dp=3):
        """Returns point, bbox size in format [x,y], [left, top, right, bottom]"""
        try:
            polygon = self.data.get("polygon", None)
            if polygon is not None:
                xs, ys = zip(*polygon)
                return ([round(self.x,max_dp), round(self.y,max_dp)], [max(0.0, round(min(xs)+self.x, max_dp)), max(0.0, round(min(ys)+self.y,max_dp)),
                        min(1.0, round(max(xs)+self.x, max_dp)), min(1.0, round(max(ys)+self.y, max_dp))])
        except Exception as e:
            print("Warning: no bbox found. {}:{}".format(e.__class__, e))
        return (None, None) if self.x is None or self.y is None else \
            ([round(self.x,max_dp), round(self.y,max_dp)], [max(0.0, round(self.x-default_size/2, max_dp)), max(0.0, round(self.y-default_size/2, max_dp)),
             min(1.0, round(self.x+default_size/2, max_dp)), min(1.0, round(self.y+default_size/2, max_dp))])

    @hybrid_property
    def timestamp(self):
        """The in-situ timestamp for when the annotated object was captured/sampled in the media frame.
        Returns `media.timestamp_start + t` if `t` is set, otherwise it just returns `media.timestamp_start` """
        if isinstance(self.t, float):
            return self.media.timestamp_start + dt.timedelta(seconds=self.t)
        return self.media.timestamp_start

    @timestamp.expression
    def timestamp(cls):
        return db.case([(cls.t.isnot(None), db.session.query(Media.timestamp_start + cls.t*db.cast('1 sec'.format(cls.t), INTERVAL)).filter(cls.media_id == Media.id).as_scalar())],
                       else_=db.session.query(Media.timestamp_start).filter(cls.media_id == Media.id).as_scalar())

    @property
    def pose(self):
        """Get closest linked pose to timestamp"""
        return Pose.query.filter(Pose.media_id==self.media_id).order_by(db.asc(Pose.timestamp_proximity(self.timestamp))).first()

    # @hybrid_property
    # def pose(self):
    #     return Pose.query.filter(
    #         Pose.media_id == self.media_id, Pose.lat.isnot(None), Pose.lon.isnot(None), Pose.lat != float('NaN'),
    #         Pose.lon != float('NaN')).order_by(db.asc(Pose.timestamp_proximity(self.timestamp))).first()


    @hybrid_property
    def has_xy(self):
        """BOOLEAN, whether or not the point has x-y coordinates. If `false`, it is a whole-frame annotation, not a point annotation"""
        return self.x is not None and self.y is not None

    @hybrid_method
    def xy_distance(self, x, y):
        """FLOAT, euclidean distance to `x` and `y` (which are proportions of the image width and height)"""
        return euclid_dist(x, y, float(self.x), float(self.y))

    @xy_distance.expression
    def xy_distance(cls, x, y):
        return euclid_dist(x, y, cls.x.cast(db.Float), cls.y.cast(db.Float), math=db.func)

    @has_xy.expression
    def has_xy(cls):
        return db.and_(cls.x.isnot(None), cls.y.isnot(None))

    @property
    def polygon(self):
        """List of lists containing coordinated of vertices for polygons. Coordinates are in proportions of the image (0-1.0).
        For point annotations, measurements are relative to point's xy location. For whole-frame annotations, they are absolute.
        When setting this property, it can be a list of lists or a JSON string which will automatically be converted to a list of lists."""
        return self.data.get('polygon', [])

    @polygon.setter
    def polygon(self, val):
        if isinstance(val, str):
            val = json.loads(val)
        data = self.data or {}
        data['polygon'] = val
        self.data = data

    @property
    def pixels(self):
        """Use to convert `x`, `y` and `polygon` between pixel coordinates from top left to native proportions coords.

        **Returns** `x`, `y` and `polygon` converted to `col`, `row` and `polygon` values in pixel coordinates from top
        left of frame using the pixel width and height of the
        associated [media](#media) object and are set by the model definition as per the `pixel_width`
        and `pixel_height` properties defined in the [media](#media) object resource, but
        can be overridden by url parameters: `&pixel_width=<int:width>&pixel_height=<int:height>`.

        **SETTER** convert from pixels coordinates `col`, `row` and `polygon` to to native proportional coords
        during batch annotation importing. As above, pixel with and height can be overridden by the url parameters:
        `&pixel_width=<int:width>&pixel_height=<int:height>`, and in addition, they can be set by dict parameters
        `width` and `height`. If no `row` or `col` are defined, a centroid point is computed for the `polygon`.
        See examples below.

        **Examples**
        ```
        # Convert x and y pixels to proportions using url params
        # URL QS params &pixel_width=<int:width>&pixel_height=<int:height>
        point.pixels = {"col":...,"row":...}
        # Convert x and y pixels to proportions using property imports for each row/entry
        point.pixels = {"col":...,"row":...,"width":...,"height":...}
        # As above, but include a polygon in pixel coordinates that also needs to be converted
        point.pixels = {..., "polygon": [[p1x, p1y],...]}
        ```
        where `col` and `row` are the point coordinates in pixels from the top-left of the frame.
        `polygon` is optional and defines a list of lists containing x-y coordinates of polygon in pixels from top-left
        of the frame.
        """
        width = int(self.media.pixel_width)
        height = int(self.media.pixel_height)
        polygon = self.data.get("polygon", []) or []  # handle possible null values
        if width > 0 and height > 0:
            if self.has_xy:
                return dict(col=self.x*width, row=self.y*height,
                            polygon=[[(i[0]+(self.x or 0))*width, (i[1]+(self.y or 0))*height] for i in polygon])
            else:
                return dict(polygon=[[(i[0] + (self.x or 0)) * width, (i[1] + (self.y or 0)) * height] for i in polygon])
        else:
            return None

    @pixels.setter
    def pixels(self, val):
        width = int(val.get('width') or self.media.pixel_width)
        height = int(val.get('height') or self.media.pixel_height)
        row = val.get('col', None)
        col = val.get('row', None)
        if row is None or col is None:  # if row or col are not set, calculate the point at the centroid
            col, row = self.polygon_centroid(val.get('polygon'))
        self.x = col/width
        self.y = row/height
        if 'polygon' in val:
            polygon = val.get('polygon') or []
            if isinstance(polygon, str):
                polygon = json.loads(polygon)
            if isinstance(polygon, list):
                self.polygon = [[(i[0]-(col or 0)) / width, (i[1]-(row or 0)) / height] for i in polygon]

    def polygon_centroid(self, vertices):
        x_sum, y_sum, area = 0, 0, 0
        n = len(vertices)
        for i in range(n):
            x0, y0 = vertices[i]
            x1, y1 = vertices[(i + 1) % n]
            cross_product = (x0 * y1) - (x1 * y0)
            area += cross_product
            x_sum += (x0 + x1) * cross_product
            y_sum += (y0 + y1) * cross_product

        area /= 2
        x = x_sum / (6 * area)
        y = y_sum / (6 * area)

        return x, y

    @property
    def set_media(self):
        """**Returns** `media.key`

        **SETTER** used to link the [media](#media) object to the point annotation during batch annotation importing.
        If the matched [media](#media) object is not already in the associated [media_collection](#media_collection),
        it is added. Matching is done on `media.key` and optionally `deployment.key` or `deployment.id`. Matching on
        `media.key` can be very slow, plus it is not always guaranteed to be unique, so it is highly recommended you
        include deployment info. If multiple matches are found, it will raise an error.
        Deployment info can be set as a dict property or as a query string parameter.

        **Examples**
        ```
        # Set media key as a scalar and use deployment ID from query string
        # URL QS params: &deployment_id=407
        point.set_media = "PR_20150301_220629_593_LC16"
        # Set media key as a scalar and use deployment key from query string
        # URL QS params: &deployment_key=r20150301_211555_01_TF_ABCDE_South_West_Mew_Stone
        point.set_media = "PR_20150301_220629_593_LC16"
        # Set media key and deployment key as dict
        point.set_media = {"key":"PR_20150301_220629_593_LC16", "deployment_key":"r20150301_211555_01_TF_ABCDE_South_West_Mew_Stone"}
        ```
        """
        return self.media.key

    @set_media.setter
    def set_media(self, value):
        # print(f"Setting media: {value}")
        if not isinstance(value, dict):
            value = dict(key=value)
            value['deployment_id'] = request.args.get('deployment_id', None)
            value['deployment_key'] = request.args.get('deployment_key', None)

        # check if media item is in request cache, otherwise search for it
        hash_key = hash(frozenset(value.items()))
        media_id, is_cached = self.request_cache("_media_key_cache", hash_key)
        if not is_cached:
            annotation_set_id = int(self.annotation_set_id or request.args.get('annotation_set_id'))
            annotation_set = AnnotationSet.get_by_id(annotation_set_id)
            # ensure current user has permission to edit media_collection
            MediaCollection.patch_auth_preprocessor(instance_id=annotation_set.media_collection_id, instance=annotation_set.media_collection)
            if value.get("id"):
                filts = [Media.id == str(value.get('id'))]
            else:
                filts = [Media.key == str(value['key'])]
            if value.get('deployment_id'):
                filts.append(Media.deployment_id == int(value.get('deployment_id')))
            if value.get('deployment_key'):
                filts.append(Media.deployment_id == Deployment.id)
                filts.append(Deployment.key == str(value.get("deployment_key")))
            media = annotation_set.media_collection.media.filter(*filts).one_or_none()  # error if > 1, none if 0
            # if not in collection, try find it and add it
            if not media:
                media = Media.query.filter(*filts).one()  # error if none or >1
                annotation_set.media_collection.media.append(media)
                # db.session.commit()
            if not media:
                raise ProcessingException(description=f"No media object found for set_media={value}")
            # print(f"Found media: {media.key}")
            media_id, is_cached = self.request_cache("_media_key_cache", hash_key, media.id)
        self.media_id = media_id

    @property
    def annotation_label(self):
        """**Returns** list of dicts `[{annotation_id:...,label_id:...}, ...]`

        **SETTER** used to set a label based on lookup condition / parameters during batch annotation importing.
        Parameters are passed by scalar value with query string properties or by setting value as dict.
        The property value can be `scalar`, `dict`, `list of dicts` or `list of scalars`.

        **Examples**
        ```
        # using a query
        point.annotation_label = {"filters":[{"name":...,"op":...,"val":...}],"comment":...,"likelihood":...,"needs_review":...}
        # using the origin code for the label
        point.annotation_label = {"origin_code":...,"comment":...,"likelihood":...,"needs_review":...}
        # using the label UUID
        point.annotation_label = {"uuid":...,"comment":...,"likelihood":...,"needs_review":...}
        # using the label ID (fastest)
        point.annotation_label = {"id":...,"comment":...,"likelihood":...,"needs_review":...}
        # To set multiple annotation labels per point, formulate dicts as above but combine in a list:
        point.annotation_label = [{...}, ...]
        ```
        where `comment`, `likelihood` and `needs_review` are optional. Label search can be done using `origin_code`,
        `uuid`, `id` and/or defining `filters`, which is a list of filter objects for the [label](#label) resource.
        See [Making API search queries](#api_query).

        Coming soon: setting labels using semantic translation (or vocab elements)
        """
        return [dict(annotation_id=i.id, label_id=i.label_id) for i in self.annotations]

    @annotation_label.setter
    def annotation_label(self, values):
        # print(f"Setting label: {values}")
        if values is not None:
            user_id = current_user.id
            # Get annotation set ID as query_string request arg, or as request variable
            annotation_set_id = int(self.annotation_set_id or getattr(g, 'annotation_set_id', None) or request.args.get('annotation_set_id'))
            parent_label_scheme_ids, is_cached = self.request_cache("_parent_label_scheme_ids", annotation_set_id)
            if not is_cached and annotation_set_id is not None:  # ensure current user has permission to edit annotation_set, also check auth
                annotation_set = AnnotationSet.get_by_id(annotation_set_id)
                AnnotationSet.patch_auth_preprocessor(instance_id=annotation_set_id, instance=annotation_set)
                parent_label_scheme_ids, is_cached = self.request_cache("_parent_label_scheme_ids", annotation_set_id, annotation_set.label_scheme.parent_label_scheme_ids())
            if not isinstance(values, list):
                values = [values]
            for v in values:
                props = dict(comment=nullifnan(v.get('comment')), likelihood=nullifnan(v.get('likelihood', 1.0)),
                             tag_names=nullifnan(v.get('tag_names')), needs_review=nullifnan(v.get('needs_review')),
                             annotation_set_id=v.get('annotation_set_id',annotation_set_id), user_id=user_id)
                label_id = nullifnan(v.get("id") or v.get("label_id"))
                if v.get("updated_at", None):
                    props["updated_at"] = to_datetime(v.get("updated_at"))
                if label_id:
                    lbl_id = nullifnan(label_id)
                    lbl_id = int(lbl_id) if lbl_id is not None else None
                else:
                    # check if media item is in request cache, otherwise search for it
                    hash_key = hash(frozenset(v.items()))
                    lbl_id, is_cached = self.request_cache("_label_search_cache", hash_key)
                    if not is_cached:
                        # AnnotationSet.current_user_can_edit
                        q = Label.query.filter(Label.label_scheme_id.in_(parent_label_scheme_ids))
                        if "filters" in v:
                            filters = v.get('filters')
                            q = search(db.session, Label, dict(filters=filters), query=q)
                        if "origin_code" in v:
                            q = q.filter(getattr(Label, "origin_code") == v.get('origin_code'))
                        if "uuid" in v:
                            try:
                                uid = UUID(str(v.get('uuid')))
                                q = q.filter(getattr(Label, "uuid") == uid)
                            except:
                                raise ValueError(f"Invalid UUID format for {v.get('uuid')}")
                        # TODO: set label by mappings?
                        lbl = q.first()
                        if lbl is None:
                            raise FileNotFoundError(f"No label found for set_label: {v}")
                        lbl_id, is_cached = self.request_cache("_label_search_cache", hash_key, lbl.id)
                self.annotations.append(AnnotationLabel(label_id=lbl_id, **props))

        # db.session.commit()


class AnnotationLabel(APIModel, Model, SurrogatePK, JSONDataFieldMixin):
    __tablename__ = 'annotation'
    __versioned__ = {}
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    # id = db.Column(db.Integer, primary_key=True, doc="Primary key `id`")
    label_id = ReferenceCol('label', nullable=True,
        doc="Foreign key `id` for related [label](#label) (or class label)")
    label = relationship('Label', doc="Related [label](#label) object",
        backref=backref("annotations", doc="List of related [annotation](#annotation) objects", lazy="dynamic"))
    likelihood = Column(db.Float, default=1.0, doc="(Optional) likelihood/probability of the label assignment")
    needs_review = Column(db.Boolean, default=False, doc="Whether or not this annotation requires review")
    created_at = Column(db.DateTime, default=dt.datetime.utcnow, doc="Creation time (UTC)")
    updated_at = Column(db.TIMESTAMP, default=dt.datetime.utcnow, onupdate=dt.datetime.utcnow, doc="Updated time (UTC)")
    user_id = ReferenceCol('users', nullable=False, doc="Foriegn key `id` for owner [user](#user) object")
    user = relationship('User', doc="Related owner [user](#user) object")
    point_id = ReferenceCol('point', nullable=False, cascade=True, index=True,
        doc="Foriegn key `id` for [point](#point) object")
    point = relationship('AnnotationPoint', doc="Related [point](#point) object",
        backref=backref('annotations', passive_deletes=True, lazy='dynamic', doc="List of [annotation](#annotation) objects"))
    annotation_set_id = ReferenceCol('annotation_set', nullable=False, cascade=True, index=True,
        doc="Foriegn key `id` for [annotation_set](#annotation_set) object")
    annotation_set = relationship('AnnotationSet', doc="Related [annotation_set](#annotation_set) object",
        backref=backref('annotations', passive_deletes=True, doc="List of related [annotation](#annotation) objects", lazy="dynamic"))
    comment = Column(db.Text, nullable=True, doc="User comment")

    parent_id = ReferenceCol('annotation', nullable=True, cascade=True, index=True, doc="`id` of parent [annotation](#annotation) for group of linked observations")
    parent = relationship('AnnotationLabel', remote_side='AnnotationLabel.id', doc="Parent annotation in a linked set of observations", backref=backref(
        'children', passive_deletes=True, lazy='dynamic', doc="Collection of linked child observations"
    ))
    # children = relationship('AnnotationLabel', remote_side=[parent_id], passive_deletes=True, lazy='dynamic', uselist=True)

    # association proxy to convert backref label_tags
    tags = association_proxy('annotation_tags', 'tag', creator=lambda tag: TagAnnotationAssociation(tag=tag))

    # child_count = column_property(select([func.count(children)]).as_scalar())

    # @hybrid_property
    # def label_info(self):
    #     return {"name": self.label.name, "id": self.label.id} if self.label_id else None
    #
    # @label_info.expression
    # def label_info(self):
    #     return db.session.query(Label.name, Label.id).filter(Label.id == self.label_id).all()

    # @hybrid_property
    # def tag_info(self):
    #     # if not inspect.isclass(self):
    #     #   return [t.name for t in self.tags]
    #         return [{"name": t.name, "id": t.id} for t in self.tags]
    #
    # @tag_info.expression
    # def tag_info(self):
    #     return db.session.query(Tag.name, Tag.id).filter(TagAnnotationAssociation.tag_id == Tag.id, TagAnnotationAssociation.annotation_id == self.id).all()

    def __init__(self, user_id=None, **kwargs):
        self.user_id = user_id or current_user.id

        # annotation_set_id = kwargs.pop("annotation_set_id", None) or request.args.get('annotation_set_id', None)
        # if annotation_set_id is not None:
        #     self.annotation_set_id = int(annotation_set_id)

        db.Model.__init__(self, **kwargs)
        # self.mungedfile = None


    def __repr__(self):
        return '<AnnotationLabel(Label ID: {})>'.format(self.label_id)

    @hybrid_property
    def is_child(self):
        """
        BOOLEAN, whether or not this annotation has a linked parent annotation
        (queryable)
        """
        return self.parent_id is not None

    @is_child.expression
    def is_child(cls):
        return cls.parent_id.isnot(None)

    # @hybrid_property
    # def is_parent(self):
    #     """
    #     BOOLEAN, whether or not this annotation has linked children / descendants
    #     (not currently queryable)
    #     """
    #     return self.children.count() > 0
    #
    # @is_parent.expression
    # def is_parent(cls):
    #     # TODO: can this be improved?
    #     parent_ids = [i[0] for i in db.session.query(cls.parent_id).filter(cls.parent_id.isnot(None)).distinct().all()]
    #     return cls.id.in_(parent_ids)

    @hybrid_property
    def timestamp(self):
        """The in-situ timestamp for when the annotated object was captured/sampled in the media frame.
        Returns `media.timestamp_start + point.t` if `point.t` is set, otherwise it just
        returns `media.timestamp_start` """
        return self.point.timestamp

    @timestamp.expression
    def timestamp(cls):
        return db.session.query(AnnotationPoint.timestamp).filter(cls.point_id == AnnotationPoint.id).as_scalar()

    # @hybrid_property
    # def object_id(self):
    #     """
    #     INT, denoting the observation group id for linked observations of the same object (returns None if not linked)
    #     (not currently queryable)
    #     """
    #     return self.id if self.is_parent else self.parent_id if self.is_child else None
    #
    # @object_id.expression
    # def object_id(cls):
    #     return db.case([(cls.is_child, cls.parent_id), (cls.is_parent, cls.id)], else_=None)

    @hybrid_method
    def timestamp_proximity(self, ts):
        """Absolute difference in seconds of the in-situ timestamp and input `ts`, which is datetime string
        eg: `"2020-09-25T00:57:11.945009"` (or in a format that is parsable by `dateutil.parser.parse`)"""
        # self.point.pose.timestamp_proximity(ts)
        if isinstance(ts, str): ts = parse(ts)
        return abs((self.timestamp-ts).total_seconds())

    @timestamp_proximity.expression
    def timestamp_proximity(cls, ts):
        if isinstance(ts, str): ts = parse(ts)
        return select([db.func.abs(db.func.trunc(db.extract('epoch', AnnotationPoint.timestamp)-db.extract('epoch', ts)))]).where(
            cls.point_id == AnnotationPoint.id).label("timestamp_proximity")

    @hybrid_method
    def distance(self, lat, lon):
        """FLOAT, distance (in meters) of closest linked [pose](#pose) relative to `lat` and `lon` (which are both in decimal degrees)"""
        # TODO: this should adjust for point.t, which will be relevant for video
        return self.point.media.distance(lat, lon)

    @distance.expression
    def distance(cls, lat, lon):
        return db.session.query(Media.distance(lat, lon)).filter(
            Media.id==AnnotationPoint.media_id, AnnotationPoint.id==cls.point_id).label('distance')
        # return db.session.query(db.func.min(Pose.distance(lat, lon))).filter(Pose.media_id == Media.id,
        #             AnnotationPoint.media_id==Media.id, cls.point_id==AnnotationPoint.id).label("distance")

    @hybrid_method
    def xy_distance(self, x, y):
        """FLOAT, euclidean distance to `x` and `y` (which are proportions of the image width and height)"""
        return self.point.xy_distance(x, y)

    @xy_distance.expression
    def xy_distance(cls, x, y):
        return db.session.query(AnnotationPoint.xy_distance(x, y)).filter(
            AnnotationPoint.id == cls.point_id).label('xy_distance')

    @property
    def object_id(self):
        """
        ID of observation group: ID of primary observation (parent annotation) if linked, otherwise own ID.
        """
        # return self.parent_id if self.is_child else self.id if self.children.count()>0 else None
        return self.parent_id or self.id

    @property
    def translated_label_id(self):
        """
        Returns `id` of `label.translated` for [label](#label) if defined and if a match is found, otherwise  `None`.
        """
        label = self.label
        if label:
            if label.translated:
                return label.translated.id
        return None

    def version_count(self):
        """INT, denoting the number of changes in the version history of this object"""
        return self.versions.count()

    def observation_count(self):
        """
        INT, denoting the number of linked observations of this object
        """
        # TODO: IS THIS SLOWING EVERYTHING DOWN??? ITS AN INCLUDED METHOD IN API - LOOK AT ALTERNATIVES
        # return self.children.count()+1 if self.is_parent else self.parent.observation_count() if self.is_child else 0
        # (data.parent_id)
        # ? { or: [{name: "id", op: "eq", val: data.parent_id}, {name: "parent_id", op: "eq", val: data.parent_id}]}
        # : {name: "parent_id", op: "eq", val: data.id}
        primary_observation_id = self.parent_id or self.id
        return AnnotationLabel.query.filter(
            db.or_(AnnotationLabel.id == primary_observation_id, AnnotationLabel.parent_id == primary_observation_id)
        ).count()

    def suggested_tags(self):
        """List of suggested [tag](#tag) implied from [label](#label) definition"""
        return [dict(id=t.id, name=t.name) for t in self.label.tags] if self.label else []

    # @hybrid_property
    def color(self):
        """Implied color code for this annotation based on [label](#label) assignment"""
        return self.label.color if self.label else "#555"

    def pose(self):
        """Properties for related [pose](#pose) item excluding [posedata](#posedata) fields"""
        p = self.point.pose
        return dict(lat=p.lat, lon=p.lon, alt=p.alt, dep=p.dep, timestamp=p.timestamp)

    def pose_data(self):
        """Properties for related [pose](#pose) item including any linked [posedata](#posedata) fields"""
        p = self.point.pose
        return dict(lat=p.lat, lon=p.lon, alt=p.alt, dep=p.dep, timestamp=p.timestamp, data={d.name: d.value for d in p.data})

    @property
    def supplementary_label(self):
        """Selected fiedls of supplementary labels for relates [point](#point). Returns `id`, `likelihood`, `name`,
        `lineage_names`, `username`. Can optionally filter through URL param: `supplementary_annotation_set_id`, which
        will filter for specific supplementary annotation_set. If not supplied will return all."""
        result = {}
        q = self.point.annotations.filter(AnnotationLabel.annotation_set_id != self.annotation_set_id, AnnotationLabel.label_id.isnot(None))
        supplementary_annotation_set_ids = [i for i in request.args.getlist("supplementary_annotation_set_id") if i]
        if len(supplementary_annotation_set_ids) > 0:
            q = q.filter(AnnotationLabel.annotation_set_id.in_(supplementary_annotation_set_ids))
        for i, a in enumerate(q):
            result[f'{i+1}'] = dict(id=a.label_id, likelihood=a.likelihood, name=a.label.name,
                             lineage_names=a.label.lineage_names(), username=a.user.username)
        return result or None

    @property
    def tag_names(self):
        """Comma-delimited names of applied tags. Can also be used to set tags using comma-delimited tags when creating annotaions"""
        return ",".join([t.tag.name for t in self.annotation_tags])

    @tag_names.setter
    def tag_names(self, value):
        if isinstance(value, str):
            for t in value.split(','):
                tag, is_cached = self.request_cache("_annotation_tags_cache", t)
                if not is_cached:
                    tag = Tag.query.filter(Tag.name.ilike(t)).first()
                    _, is_cached = self.request_cache("_annotation_tags_cache", t, tag)
                if tag:
                    # TagAnnotationAssociation.create(annotation_id=self.id, tag_id=tag.id, commit=False)
                    self.tags.append(tag)

    # def media(self):
    #     """Properties for related [media](#media) item"""
    #     return MediaAPI.model_to_dict(self.point.media, run_get_single_processors=False)

    def remove_tag(self, tag_id):
        ta = TagAnnotationAssociation.query.filter(
            TagAnnotationAssociation.tag_id == tag_id, TagAnnotationAssociation.annotation_id == self.id).one()
        ta.delete()

    # def _get_pose(self):
    #     return self.point.poses.order_by(Pose.timestamp.asc()).first()

    def set_annotation(self, label_id=None, tags=None, comment=None, user_id=None, commit=True, update_linked_observations=True, **kw):
        if label_id is False:  # reset label
            self.label_id = None
            self.tags = []
            self.comment = None
            self.needs_review = False
            self.likelihood = 1.0
            self.unlink_observation(commit=False, silent=True)  # unlink, but don't throw error if not linked
        elif label_id is not None:
            # label = Label.query.filter_by(id=label_id).one()
            # self.label_id = label.id
            # self.tags = label.tags
            self.label_id = label_id
            self.user_id = user_id
            self.needs_review = False  # reset needs review flag if label is updated
            # update label_id for linked observations in the same annotation set
            if update_linked_observations:
                oid = self.object_id
                for i in AnnotationLabel.query.filter(
                        db.or_(AnnotationLabel.id == oid, AnnotationLabel.parent_id == oid),
                        AnnotationLabel.id != self.id, AnnotationLabel.annotation_set_id == self.annotation_set_id):
                    i.set_annotation(label_id=label_id, user_id=user_id, commit=False, update_linked_observations=False)
        if comment is not None:
            self.comment = comment
        if tags is not None:  # TODO: this is untested - test appending tags!
            self.tags = Tag.query.filter(Tag.id.in_(tags))
            # for t in Tag.query.filter(Tag.id.in_(tags)):
            #     if t not in self.tags:
            #         self.tags.append(t)
        for k,v in kw.items():
            setattr(self, k, v)

        if commit:
            db.session.commit()

    def unlink_observation(self, commit=True, silent=False):
        """Unlink this annotation from the observation group. If it is a group parent, the first child will be set as the
        new parent.
        An observation group is used to link observations of the same object that has been annotated multiple times."""
        if self.is_child:
            self.parent_id = None
        elif self.children.count() > 0:
            first_child = self.children.first()
            for c in self.children.filter(self.__class__.id != first_child.id):
                c.parent_id = first_child.id
            first_child.parent_id = None
        elif not silent:
            raise ProcessingException("Cannot unlink an annotation that is not part of an observation group.")
        if commit:
            db.session.commit()

    def set_as_parent_observation(self, commit=True):
        """Set this annotation as the parent of the observation group.
        An observation group is used to link observations of the same object that has been annotated multiple times."""
        if self.is_child:
            for c in self.parent.children.filter(self.__class__.id!=self.id):
                c.parent_id = self.id
            self.parent.parent_id = self.id
            self.parent_id = None
            if commit:
                db.session.commit()
        elif self.children.count() <= 0:
            raise ProcessingException("Cannot set as primary annotation if it is not part of an observation group.")

    def link_parent_observation(self, link_id, commit=True):
        """Updates `parent_id` of annotation to group observations of the same object. If `link_id` points to a child annotation, the observation
        will be linked to its `parent`.
        An observation group is used to link observations of the same object that has been annotated multiple times."""
        assert self.observation_count() <= 1,\
            "This observation is already part of an observation group. You need to unlink it before it can be relinked."
        linked_instance = self.get_by_id(link_id)
        parent_id = linked_instance.parent_id if linked_instance.is_child else linked_instance.id
        self.update(parent_id=parent_id, commit=commit)


class TagAnnotationAssociation(APIModel, Model):
    __tablename__ = 'annotation_tags'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    annotation_id = ReferenceCol('annotation', primary_key=True, cascade=True, index=True, doc="`id` of linked [annotation](#annotation)")
    tag_id = ReferenceCol('tag', primary_key=True, cascade=True, index=True, doc="`id` of linked [tag](#tag)")
    assoc_created_at = Column(db.DateTime, default=dt.datetime.utcnow)
    annotation = relationship("AnnotationLabel", enable_typechecks=False, doc="Linked [annotation](#annotation) object",
                              backref=backref("annotation_tags", passive_deletes=True, cascade='all, delete-orphan', doc="Collection of [annotation_tags](#annotation_tags) associations"))
    tag = relationship("Tag", backref=backref("annotation_tags", passive_deletes=True, doc="Collection of [annotation_tags](#annotation_tags) associations"), doc="Linked [tag](#tag) object")

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)


class AnnotationSet(APIUserGroupMixin, APIModel, Model, SurrogatePK, JSONDataFieldMixin):  #, InheritUserGroupMixin):
    __tablename__ = 'annotation_set'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False)
    description = Column(db.Text, nullable=True)
    media_collection_id = ReferenceCol('media_collection', nullable=False, cascade=True, index=True)
    media_collection = relationship('MediaCollection', backref=backref('annotation_sets', passive_deletes=True, lazy='dynamic'))
    label_scheme_id = ReferenceCol('label_scheme', nullable=False, cascade=True)
    label_scheme = relationship('LabelScheme', foreign_keys=[label_scheme_id], back_populates="annotation_sets") # backref=backref('annotation_sets', passive_deletes=True, lazy='dynamic')) #, primaryjoin="and_(AnnotationSet.label_scheme_id==LabelScheme.id, AnnotationSet.is_exemplar.isnot(True))"))
    user_id = ReferenceCol('users', nullable=False)
    user = relationship('User')  # , backref='campaigns')
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    parent_id = ReferenceCol('annotation_set', nullable=True, cascade=True)
    parent = relationship('AnnotationSet', remote_side='AnnotationSet.id',
                          backref=backref('children', passive_deletes=True))
    is_exemplar = Column(db.Boolean(), default=False)

    is_full_bio_score = Column(db.Boolean(), default=False,
                               doc="whether or not the intent was full biodiversity scoring, or just targeted scoring")
    is_real_science = Column(db.Boolean(), default=False,
                             doc="whether or not this is for science, or was for education / training / testing")
    is_qaqc = Column(db.Boolean(), default=False,
                     doc="whether or not the annotation set has been QA/QC'd")
    is_final = Column(db.Boolean(), default=False,
                      doc="whether or not the annotator has completed annotating")



    # inherit_usergroup_column_name = "media_collection"

    # TODO: REMOVE THIS!!!
    type = Column(db.String(80), nullable=True, default='base')

    # backup_include_relations = {
    #     'points': {
    #         'annotations': {
    #             'annotation_tags': None
    #         }
    #     }
    # }

    # @hybrid_property
    # def media_count(self):
    #     if not inspect.isclass(self):
    #         return len(self.media_collection.media)

    # @hybrid_property
    # def is_exemplar(self):
    #     return self.exemplar_label_schemes.count() > 0
    #
    # @is_exemplar.expression
    # def is_exemplar(cls):
    #     return cls.exemplar_label_schemes.any()

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<AnnotationSet({})>'.format(self.name)

    def annotation_count(self):
        return self.annotations.with_entities(AnnotationLabel.id).filter(AnnotationLabel.label_id.isnot(None)).count()

    def point_count(self):
        return self.points.with_entities(AnnotationPoint.id).count()

    def unannotated_point_count(self):
        return self.annotations.with_entities(AnnotationLabel.id).filter(AnnotationLabel.label_id.is_(None)).count()
        # return self.points.with_entities(AnnotationPoint.id).filter(db.not_(AnnotationPoint.annotations.any(AnnotationLabel.label_id.isnot(None)))).count()

    @hybrid_property
    def media_count(self):
        """Number of media items in the media_collection"""
        return self.media_collection.media_count

    @media_count.expression
    def media_count(cls):
        return (db.select([db.func.count(MediaCollectionMediaAssociation.media_id)]).where(
            MediaCollectionMediaAssociation.media_collection_id == cls.media_collection_id).label("media_count"))


    @hybrid_property
    def is_child(self):
        """BOOLEAN, whether or not this annotation has a linked parent annotation"""
        return self.parent_id is not None

    @is_child.expression
    def is_child(cls):
        return cls.parent_id.isnot(None)

    def move_media_annotations(self, media, target_annotation_set):
        """
        Convenient method for moving media and annotations from one media_collection and associated annotation_set to another
        @param media:
        @param target_annotation_set:
        @return:
        """
        media_collection_id = self.media_collection_id
        target_annotation_set_id = target_annotation_set.id
        target_media_collection_id = target_annotation_set.media_collection_id
        annotation_count, point_count, media_count = 0, 0, 0
        for m in media:
            mca = MediaCollectionMediaAssociation.query.filter_by(media_collection_id=media_collection_id, media_id=m.id).one()
            mca.media_collection_id = target_media_collection_id  # update media collection associate
            media_count += 1
            for p in m.annotations.filter_by(annotation_set_id = self.id):
                p.annotation_set_id = target_annotation_set_id
                point_count += 1
                for a in p.annotations.filter_by(annotation_set_id = self.id):
                    a.annotation_set_id = target_annotation_set_id
                    annotation_count += 1
        db.session.commit()
        return dict(
            media_collection_id=media_collection_id, target_annotation_set_id=target_annotation_set_id,
            target_media_collection_id=target_media_collection_id, annotation_count=annotation_count,
            point_count=point_count, media_count=media_count)

    # @hybrid_property
    # def is_parent(self):
    #     """BOOLEAN, whether or not this annotation has linked children / descendants"""
    #     return self.children.count() > 0
    #
    # @is_parent.expression
    # def is_parent(cls):
    #     return db.true(cls.children.count() > 0)

# def _create_item(name, value):
#     return PoseData(ItemName=name, ItemValue=value)
# from sqlalchemy.orm import relation


class AnnotationSetFile(DBFileMixin, APIModel, Model):
    __tablename__ = 'annotation_set_file'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    user_id = ReferenceCol('users', nullable=False)
    user = relationship('User')
    annotation_set_id = ReferenceCol('annotation_set', nullable=False, cascade=True)
    annotation_set = relationship('AnnotationSet', backref=backref('files', passive_deletes=True, lazy="dynamic"), enable_typechecks=False)
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    updated_at = Column(db.DateTime, default=dt.datetime.utcnow, onupdate=dt.datetime.utcnow, doc="Updated time (UTC)")
    __table_args__ = (UniqueConstraint('annotation_set_id', 'name', name='_annotation_set_file_name_uc'),)


    def __init__(self, **kwargs):
        """Add user_id to post payload if not supplied"""
        if "user_id" not in kwargs:
            kwargs["user_id"] = current_user.id
        super().__init__(**kwargs)


class Pose(APIModel, SurrogatePK, Model):
    __tablename__ = 'pose'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    lat = Column(db.Float(precision=53), unique=False, nullable=True, index=True)
    lon = Column(db.Float(precision=53), unique=False, nullable=True, index=True)
    alt = Column(db.Float, unique=False, nullable=True, index=True)
    dep = Column(db.Float, unique=False, nullable=True, index=True)
    timestamp = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow, index=True)
    media_id = ReferenceCol('media', nullable=False, cascade=True, index=True)
    media = relationship('Media', backref=backref('poses', passive_deletes=True, lazy="dynamic"))
    geom = Column(Geometry(geometry_type='POINT', srid=4326), Computed("ST_SetSRID(ST_MakePoint(lon,lat), 4326)", persisted=True))
    # geom = Column(Geography(geometry_type='POINT', srid=4326), Computed("ST_SetSRID(ST_MakePoint(lon,lat), 4326)", persisted=True))

    # _items = relation('PoseData', collection_class=attribute_mapped_collection('name'))
    # items = association_proxy('_items', 'value', creator=_create_item)

    def __init__(self, data=None, **kwargs):
        # Add data if provided as a dict of key-value pairs
        kwargs['alt'] = nullifnan(kwargs.get("alt"))
        kwargs['dep'] = nullifnan(kwargs.get("dep"))
        kwargs['lat'] = nullifnan(kwargs.get("lat"))
        kwargs['lon'] = nullifnan(kwargs.get("lon"))

        if kwargs.get("lat") is not None and (not isinstance(kwargs.get("lat"), float) or not (-90.0 <= kwargs.get("lat") <= 90.0)):
            raise ValueError("Invalid latitude '{}'. Expects decimal degrees between -90 and +90".format(kwargs.get("lat")))
        if kwargs.get("lon") is not None and (not isinstance(kwargs.get("lon"), float) or not (-180.0 <= kwargs.get("lon") <= 180.0)):
            raise ValueError("Invalid longitude '{}'. Expects decimal degrees between -180 and +180".format(kwargs.get("lon")))

        if isinstance(data, dict):
            kwargs["data"] = data
        # Add data if provided as list of dicts [{name:...,value:...},{name:...,value:...}]
        elif isinstance(data, list):
            kwargs['_data'] = []
            for d in data:
                kwargs['_data'].append(PoseData(**d))   #PoseData(name=d.get('name'), value=d.get('value'))
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<Pose(Lat:{}, Lon:{})>'.format(self.lat, self.lon)

    @hybrid_method
    def distance(self, lat, lng):
        """FLOAT, distance (in meters) to `lat` and `lon` (which are both in decimal degrees)"""
        # return func.ST_DistanceSphere(self.geom, func.ST_GeomFromText(f'POINT({lng} {lat})', 4326)).scalar()
        return gc_distance(lat, lng, float(self.lat), float(self.lon))

    @distance.expression
    def distance(cls, lat, lng):
        # return func.ST_DistanceSphere(cls.geom, func.ST_GeomFromText(f'POINT({lng} {lat})', 4326))
        return gc_distance(lat, lng, cls.lat.cast(db.Float), cls.lon.cast(db.Float), math=db.func)

    @hybrid_method
    def proximity(self, lat, lon):
        geom = func.ST_GeomFromText(f'POINT({lon} {lat})', 4326)
        return Comparator.distance_centroid(self.geom, geom)

    @hybrid_method
    def timestamp_proximity(self, ts):
        """Absolute difference in seconds of the in-situ timestamp and input `ts`, which is datetime string
        eg: `"2020-09-25T00:57:11.945009"` (or in a format that is parsable by `dateutil.parser.parse`)"""
        if isinstance(ts, str): ts = parse(ts)
        return abs((self.timestamp - ts).total_seconds())

    @timestamp_proximity.expression
    def timestamp_proximity(cls, ts):
        if isinstance(ts, str): ts = parse(ts)
        return db.func.abs(db.func.trunc(db.extract('epoch', cls.timestamp) - db.extract('epoch', ts)))

    @property
    def timestamp_local(self):
        """LOCAL datetime, converted using `lat`, `lon` and `timestamp`. SETTER converts Local to UTC"""
        try:
            _deployment_timezone_cache = getattr(g, "_deployment_timezone_cache", None)  # cache deployment timezone
            if _deployment_timezone_cache is None:
                _deployment_timezone_cache = g._deployment_timezone_cache = dict()
            if self.media.deployment_id not in _deployment_timezone_cache.keys():
                _deployment_timezone_cache[self.media.deployment_id] = pytz.timezone(
                    tf.timezone_at(lng=self.lon, lat=self.lat))
            utcmoment = self.timestamp.replace(tzinfo=pytz.utc)
            return utcmoment.astimezone(_deployment_timezone_cache.get(self.media.deployment_id))
        except Exception as e:
            return None

    @timestamp_local.setter
    def timestamp_local(self, val):
        dt = dateutil.parser.parse(val)
        tz = tf.timezone_at(lng=self.lon, lat=self.lat)
        tzmoment = dt.replace(tzinfo=pytz.timezone(tz))
        self.timestamp = tzmoment.astimezone(pytz.utc)

    @property
    def data(self):
        """Properties for related [pose](#pose) item including any linked [posedata](#posedata) fields"""
        return {d.name: d.value for d in self._data}

    @data.setter
    def data(self, value):
        self._data = []         # clear pose._data
        for k, v in value.items():
            if isinstance(v, (float, int)) and v is not None and not np.isnan(v):
                self._data.append(PoseData(name=k, value=v))


class PoseData(APIModel, SurrogatePK, Model):
    __tablename__ = 'posedata'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False)
    value = Column(db.Float, unique=False, nullable=True)
    pose_id = ReferenceCol('pose', nullable=False, cascade=True, index=True)
    pose = relationship('Pose', backref=backref('_data', passive_deletes=True, lazy="dynamic"))  #, collection_class=attribute_mapped_collection('name')))

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<PoseData({})>'.format(self.name)


class MediaType(APIModel, SurrogatePK, Model):
    __tablename__ = 'media_type'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False, doc="The name of the media type")

    def __init__(self, **kwargs):
        if not (current_user.is_authenticated() and current_user.is_admin):
            raise ProcessingException('Only admin users can create new media types', code=401)
        db.Model.__init__(self, **kwargs)


class Media(APIModel, Model, SurrogatePK, JSONDataFieldMixin):
    __tablename__ = 'media'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    key = Column(db.String(80), nullable=False)
    path = Column(db.Text, nullable=True)
    path_thm = Column(db.Text, nullable=True)
    is_valid = Column(db.Boolean(), default=True)
    deployment_id = ReferenceCol('deployment', nullable=False, cascade=True, index=True)
    deployment = relationship('Deployment', backref=backref('media', passive_deletes=True, lazy="dynamic"))
    media_type_id = ReferenceCol('media_type', nullable=False, cascade=True, index=True)
    media_type = relationship('MediaType', backref=backref('media', passive_deletes=True, lazy="dynamic"))
    timestamp_start = Column(db.DateTime, nullable=True, default=dt.datetime.utcnow)
    created_at = Column(db.DateTime, nullable=True, default=dt.datetime.utcnow, server_default=func.now())
    # timestamp_end = Column(db.DateTime, nullable=True, default=dt.datetime.utcnow)
    parent_id = ReferenceCol('media', nullable=True, cascade=True, index=True)
    parent = relationship('Media', remote_side='Media.id', backref=backref('children', passive_deletes=True))

    # poses = association_proxy('mediaposes', 'pose', creator=lambda pose: MediaPoseAssociation(pose=pose))
    media_collections = association_proxy('media_collection_media', 'media_collection')

    __table_args__ = (UniqueConstraint('deployment_id', 'key', name='_media_deployment_uc'),)

    _media_type_cache = dict()   # used to speed up the creation of multiple media items - prevents repeat media_type lookups


    def __init__(self, pose=None, poses=None, media_type="image", **kwargs):
        """
        In addition to the `MODEL COLUMNS` below, the JSON body can contain the following parameters:
        `pose`: a dict containing parameters for a single [pose](#pose) including associated pose data,
        `poses`: a list of dicts as above if linking multiple poses to a single media object (eg: video),
        `media_type`: OPTIONAL (default="image"), a string containing the [media_type](#media_type) name.

        Here are some example payloads for creating [media](#media) objects:
        ```
        # example JSON payload for media item with a single pose (eg: still image)
        {
            "pose": {"lat":-35.53153264, "lon":150.43962917, "alt":3.558, "dep":28.85, "data":[
                {"name":"temperature","value":16.602},
                {"name":"salinity","value":35.260222}
            ]},
            "key": "PR_20101117_002241_698_LC16",
            "deployment_id": 5,
            "timestamp_start": "2018-06-07T00:47:17.273514"
        }

        # Same as above, but with dict data
        {
            "pose": {"lat":-35.53153264, "lon":150.43962917, "alt":3.558, "dep":28.85, "data":{
                "temperature":16.602,
                "salinity":35.260222
            }},
            "key": "TEST_20101117_002241_698_LC16",
            "deployment_id": 13170,
            "timestamp_start": "2018-06-07T00:47:17.273514"
        }

        # To add multiple poses to a media object (i.e. video)
        {
            "poses":
                [{
                    "timestamp": "2018-06-07T00:47:17.273517",
                    "lat":-35.53153264,
                    "lon":150.43962917,
                    "alt":3.558,
                    "dep":28.85,
                    "data":{
                        "temperature":16.602,
                        "salinity":35.260222
                    }
                },{
                    "timestamp": "2018-06-07T00:47:18.273517",
                    "lat":-35.531532654,
                    "lon":150.439629345,
                    "alt":6.558,
                    "dep":29.85,
                    "data":{
                        "temperature":16.602,
                        "salinity":35.260222
                    }
                },{
                    "timestamp": "2018-06-07T00:47:19.273517",
                    "lat":-35.531534564,
                    "lon":150.43962934,
                    "alt":4.558,
                    "dep":28.85,
                    "data":{
                        "temperature":16.60256,
                        "salinity":35.260234
                    }
                }],
            "key": "KEY_OF_VIDEO_FILE_NAME",
            "deployment_id": 13170,
            "timestamp_start": "2018-06-07T00:47:17.273514",
            "media_type": "rawvideo"
        }
        ```
        """
        poses = ([pose] if isinstance(pose, dict) else []) + (poses if isinstance(poses, list) else [])
        kwargs['poses'] = []
        for p in poses:
            if "timestamp" not in p:                                    # add timestamp from media if not provides
                p['timestamp'] = kwargs.get('timestamp_start')
            kwargs['poses'].append(Pose(**p))
        if "media_type_id" not in kwargs:                               # lookup/get/create media_type_id
            if media_type not in self._media_type_cache:
                mt, created = MediaType.get_or_create(name=media_type)
                self._media_type_cache[mt.name] = mt.id
            kwargs["media_type_id"] = self._media_type_cache.get(media_type, None)
        if 'key' not in kwargs and 'path' in kwargs:                    # generate key from path if not present
            kwargs['key'], ext = os.path.splitext(os.path.basename(kwargs.get('path')))
        db.Model.__init__(self, **kwargs)

    def current_user_can_edit(self):
        """Whether or not the current user owns the deployment and can edit this media item"""
        return self.deployment.user_id == current_user.id if current_user.is_authenticated() else False

    @property
    def path_best(self):
        if self.path is not None:
            p = self.path
        elif self.deployment.datasource.media_pattern is not None:
            p = self.deployment.datasource.media_pattern.format(
                campaign=self.deployment.campaign, deployment=self.deployment, media=self)
        else:
            p = ""
        return self._fix_url(p)

    # @path_best.expression
    # def path_best(cls):
    #     # TODO: this returns a string pattern and does not replace result with substituded values
    #     return case([
    #         (cls.path == None, select([Campaign.media_path_pattern]).where(cls.deployment_id == Deployment.id).where(Deployment.campaign_id == Campaign.id).as_scalar()),
    #     ], else_=cls.path)

    @property
    def path_best_thm(self):
        if self.path_thm:
            p = self.path_thm
        elif self.deployment.datasource.thumb_pattern:
            p = self.deployment.datasource.thumb_pattern.format(
                campaign=self.deployment.campaign, deployment=self.deployment, media=self)
        else:
            p = current_app.config['MEDIA_THM_CACHE_URL'].format(media=self)
        return self._fix_url(p)

    @property
    def timestamp_start_local(self):
        """LOCAL datetime, converted using `pose.lat`, `pose.lon` and `timestamp_start`"""
        # # Lookup timezone for every media item
        # try:
        #     tz = tf.timezone_at(lng=self.pose.lon, lat=self.pose.lat)
        #     utcmoment = self.timestamp_start.replace(tzinfo=pytz.utc)
        #     return utcmoment.astimezone(pytz.timezone(tz))
        # except Exception as e:
        #     return None
        # Lookup timezone for deployment and apply to every media item in that deployment (faster, less accurate)
        try:
            _deployment_timezone_cache = getattr(g, "_deployment_timezone_cache", None)  # cache deployment timezone
            if _deployment_timezone_cache is None:
                _deployment_timezone_cache = g._deployment_timezone_cache = dict()
            if self.deployment_id not in _deployment_timezone_cache.keys():
                _deployment_timezone_cache[self.deployment_id] = pytz.timezone(tf.timezone_at(lng=self.pose.lon, lat=self.pose.lat))
            utcmoment = self.timestamp_start.replace(tzinfo=pytz.utc)
            return utcmoment.astimezone(_deployment_timezone_cache.get(self.deployment_id))
        except Exception as e:
            return None

    @timestamp_start_local.setter
    def timestamp_start_local(self, val):
        dt = dateutil.parser.parse(val)
        tz = tf.timezone_at(lng=self.pose.lon, lat=self.pose.lat)
        tzmoment = dt.replace(tzinfo=pytz.timezone(tz))
        self.timestamp_start = tzmoment.astimezone(pytz.utc)


    # def path_absolute(self, use_thm=False):
    #     impath = self.path_best_thm if use_thm is True or use_thm == "true" else self.path_best
    #     imurl = self._enforce_abs_path(impath)
    #     # Deal with dodgy URLS (eg: RLS images with strange characters and spaces)
    #     return url_fix(imurl.strip().encode('utf-8').decode("utf-8", "ignore"))

    @staticmethod
    def _fix_url(p):
        url = p if p.startswith("http") or p == "" else urlparse.urljoin(request.url_root, p)
        return url_fix(url.strip().encode('utf-8').decode("utf-8", "ignore"))
    # @path_best_thm.expression
    # def path_best_thm(cls):
    #     # TODO: this returns a string pattern and does not replace result with substituded values
    #     return case([
    #         (cls.path_thm == None, select([Campaign.thm_path_pattern]).where(cls.deployment_id == Deployment.id).where(Deployment.campaign_id == Campaign.id).as_scalar()),
    #     ], else_=cls.path_thm)

    @property
    def pose(self):
        """
        The first pose from related `poses` or if child media item, it gets the closest matching pose from the
        `parent_media` item based on `timestamp_start`
        """
        # get first pose for this item
        # if no poses, try get most recent pose from parent before media.timestamp_start
        return self.poses.order_by(Pose.timestamp.asc()).first() or \
               (self.parent.poses.filter(Pose.timestamp<=self.timestamp_start).order_by(Pose.timestamp.desc()).first()
                if self.is_child and self.timestamp_start is not None else None)

    # @pose.expression
    # def pose(cls):
    #     # TODO: this is not correct! Needs to replicate parent child logic of instance property
    #     try:
    #         return Pose.query.filter(Pose.media_id == cls.id).order_by(Pose.timestamp.asc()).first()
    #     except Exception as e:
    #         return None

    @pose.setter
    def pose(self, value):
        self.poses.delete()                         # clear previous poses
        if isinstance(value, dict):
            if "timestamp" not in value:            # add timestamp from media if not provides
                value['timestamp'] = self.timestamp_start
            #value['media_id'] = self.id            # set media_id
            setattr(self, 'poses',[Pose(**value)])  # add new pose
        elif isinstance(value, Pose):
            setattr(self, 'poses', [value])

    @property
    def color(self):
        return self.deployment.campaign.color

    @property
    def pixel_width(self):
        """Width of media item in pixels. Not implemented yet..."""
        # todo: get width from camera/platform
        return request.args.get('pixel_width')

    @property
    def pixel_height(self):
        """Height of media item in pixels. Not implemented yet..."""
        # todo: get height from camera/platform
        return request.args.get('pixel_height')

    @hybrid_property
    def geom(self):
        return self.pose.geom

    @geom.expression
    def geom(cls):
        return db.session.query(Pose.geom).filter(Pose.media_id == cls.id).limit(1).label("geom")

    @hybrid_property
    def is_child(self):
        """BOOLEAN, whether or not this has a parent"""
        return self.parent_id is not None

    @is_child.expression
    def is_child(cls):
        return cls.parent_id.isnot(None)

    # @hybrid_method
    # def has_annotations(self, annotation_set_id):
    #     """BOOL, whether or not the [media object](#media) contains annotations from the [annotation_set](annotation_set) with the id `annotation_set_id`"""
    #     return self.annotations.any(db.and_(
    #         AnnotationLabel.label_id.isnot(None), AnnotationLabel.point_id==AnnotationPoint.id,
    #         AnnotationPoint.annotation_set_id == annotation_set_id))

    @hybrid_method
    def has_unlabeled_annotations(self, annotation_set_id):
        """BOOL, whether or not the [media object](#media) contains unlabeled annotations from the
        [annotation_set](annotation_set) with the id `annotation_set_id`"""
        return bool(self.annotations.filter(
            AnnotationLabel.label_id.is_(None), AnnotationLabel.point_id == AnnotationPoint.id,
            AnnotationPoint.annotation_set_id == annotation_set_id).first())

    @has_unlabeled_annotations.expression
    def has_unlabeled_annotations(cls, annotation_set_id):
        return cls.annotations.any(db.and_(
            AnnotationLabel.label_id.is_(None), AnnotationLabel.point_id == AnnotationPoint.id,
            AnnotationPoint.annotation_set_id == annotation_set_id))

    @hybrid_method
    def has_annotations(self, annotation_set_id=None):
        """BOOL, whether or not the [media object](#media) contains any annotations from the
        [annotation_set](annotation_set) with the id `annotation_set_id`"""
        if annotation_set_id is not None:
            return bool(self.annotations.filter(
                AnnotationLabel.label_id.isnot(None), AnnotationLabel.point_id == AnnotationPoint.id,
                                                      AnnotationPoint.annotation_set_id == annotation_set_id).first())
        else:
            return bool(self.annotations.filter(
                AnnotationLabel.label_id.isnot(None), AnnotationLabel.point_id == AnnotationPoint.id).first())

    @has_annotations.expression
    def has_annotations(cls, annotation_set_id=None):
        if annotation_set_id is not None:
            return cls.annotations.any(db.and_(
                AnnotationLabel.label_id.isnot(None), AnnotationLabel.point_id == AnnotationPoint.id,
                                                      AnnotationPoint.annotation_set_id == annotation_set_id))
        else:
            return cls.annotations.any(db.and_(
                AnnotationLabel.label_id.isnot(None), AnnotationLabel.point_id == AnnotationPoint.id))

    @hybrid_method
    def distance(self, lat, lon):
        """FLOAT, distance (in meters) of closest linked [pose](#pose) relative to `lat` and `lon` (which are both in decimal degrees)"""
        # TODO: this should use timestamp_start if is_child media item and return best matching pose for comparison
        p = self.poses.order_by(Pose.distance(lat, lon)).first()
        return p.distance(lat, lon)

    @distance.expression
    def distance(cls, lat, lon):
        # TODO: this should use timestamp_start if is_child media item and return best matching pose for comparison
        return db.session.query(db.func.min(Pose.distance(lat, lon))).filter(Pose.media_id==cls.id).label("distance")

    @hybrid_method
    def timestamp_proximity(self, ts):
        """Absolute difference in seconds of the in-situ timestamp and input `ts`, which is datetime string
        eg: `"2020-09-25T00:57:11.945009"` (or in a format that is parsable by `dateutil.parser.parse`)"""
        # TODO: this should use timestamp_start if is_child media item and return best matching pose for comparison
        p = self.poses.order_by(Pose.timestamp_proximity(ts)).first()
        return p.timestamp_proximity(ts)

    @timestamp_proximity.expression
    def timestamp_proximity(cls, ts):
        # TODO: this should use timestamp_start if is_child media item and return best matching pose for comparison
        return db.session.query(db.func.min(Pose.timestamp_proximity(ts))).filter(Pose.media_id==cls.id).label("timestamp_proximity")

    def event_log(self):
        return ";".join(["{e.deployment_event_type.name} [{e.user.username}]: {e.description}".format(e=e) for e in self.events])
    # @hybrid_property
    # def is_parent(self):
    #     """BOOLEAN, whether or not this class label has children / descendants"""
    #     return self.children.count() > 0
    #
    # @is_parent.expression
    # def is_parent(cls):
    #     return db.true(cls.children.count() > 0)


    def __repr__(self):
        return '<Media({})>'.format(self.key)


class MediaCollection(APIUserGroupMixin, APIModel, SurrogatePK, Model, JSONDataFieldMixin):
    __tablename__ = 'media_collection'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False, doc="The name of the `media_collection`")
    description = Column(db.Text, nullable=True, doc="A concise description of the `media_collection`")
    parent_collection_id = ReferenceCol('media_collection', nullable=True, cascade=True, doc="A reference to the ID of a parent `media_collection`")
    parent = relationship('MediaCollection', remote_side='MediaCollection.id', doc="The parent `media_collection` instance",
                          backref=backref("children", passive_deletes=True, doc="A list of childen `media_collections` that belong to this one"))
    # user_id = ReferenceCol('users', nullable=True)
    # user = relationship('User')  # , backref='campaigns')
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow, doc="The creation date-time")

    # media = association_proxy('media_collection_media', 'media',
    #                           creator=lambda media: MediaCollectionMediaAssociation(media=media))
    media = relationship("Media", secondary="media_collection_media", lazy="dynamic", doc="A list of `media` contained in this `media_collection`")

    # add the usergroups column to this object
    # NB: MUST HAVE A FIELD NAMED user_id for this to work!!!
    # usergroups = UserGroupAssociationMixin.get_usergroups_column(__tablename__)

    # backup_include_relations = {
    #     'annotation_sets': None,
    #     'media_collection_media': None,
    #     'user': None,
    #     'usergroups': None
    # }

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<MediaCollection({})>'.format(self.name)

    @hybrid_property
    def media_count(self):
        """Number of Media objects in this MediaCollection"""
        return MediaCollectionMediaAssociation.query.with_entities(MediaCollectionMediaAssociation.media_id).filter(
            MediaCollectionMediaAssociation.media_collection_id == self.id).count()
        #return count(db.session, Media.query.filter(Media.media_collections.any(MediaCollection.id == self.id)))

    @media_count.expression
    def media_count(cls):
        return (db.select([db.func.count(MediaCollectionMediaAssociation.media_id)]).where(
            MediaCollectionMediaAssociation.media_collection_id == cls.id).label("media_count"))

    def add_media(self, media, q, append=False):
        # Update media collection and set custom data attribute
        # max_stored_queries = 20
        data = self.data or {}
        if append:
            self.media = self.media.all() + media
            media_queries = data.get('media_queries', {})
        else:
            self.media = media
            media_queries = {}
        media_queries[str(datetime.datetime.utcnow())] = q
        data['media_queries'] = media_queries
        self.data = data
        db.session.commit()
        # self.update(media=media, data=data)  # replace query data

    def extent(self):
        """
        Compute spatial extent of Media Collection
        :return: list with format [[SW_LAT, SW_LON],[NE_LAT,NE_LON]]
        """
        ext = db.session.query(db.func.min(Pose.lat).label('min_lat'), db.func.max(Pose.lat).label('max_lat'),
                               db.func.min(Pose.lon).label('min_lon'), db.func.max(Pose.lon).label('max_lon')
                               ).filter(Pose.media_id == MediaCollectionMediaAssociation.media_id,
                                        MediaCollectionMediaAssociation.media_collection_id == self.id).one()

        return [[ext.min_lat, ext.min_lon],[ext.max_lat, ext.max_lon]]



class MediaCollectionMediaAssociation(APIModel, Model):
    __tablename__ = 'media_collection_media'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    media_id = ReferenceCol('media', primary_key=True, cascade=True, index=True)
    media_collection_id = ReferenceCol('media_collection', primary_key=True, cascade=True, index=True)
    media = relationship("Media", backref=backref("media_collection_media", passive_deletes=True))
    media_collection = relationship("MediaCollection", backref=backref("media_collection_media", passive_deletes=True))
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow, server_default=func.now())

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)


# class MediaPoseAssociation(Model):
#     __tablename__ = 'media_poses'
#     media_id = ReferenceCol('media', primary_key=True, cascade=True)
#     pose_id = ReferenceCol('pose', primary_key=True, cascade=True)
#     media = relationship("Media", backref=backref("mediaposes", passive_deletes=True, lazy="dynamic"))
#     pose = relationship("Pose", backref=backref("mediaposes", passive_deletes=True, lazy="dynamic"))
#
#     def __init__(self, **kwargs):
#         db.Model.__init__(self, **kwargs)


class DeploymentEventType(APIModel, SurrogatePK, Model, JSONDataFieldMixin):
    __tablename__ = 'deployment_event_type'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False, unique=True)


class DeploymentEvent(APIModel, Model, SurrogatePK):
    __tablename__ = 'deployment_event'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    description = Column(db.Text, nullable=False)
    user_id = ReferenceCol('users', nullable=False)
    user = relationship('User')
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    timestamp = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    pose_id = ReferenceCol('pose', nullable=True, cascade=True)
    pose = relationship('Pose', backref=backref('events', passive_deletes=True))
    media_id = ReferenceCol('media', nullable=True, cascade=True)
    media = relationship("Media", backref=backref("events", passive_deletes=True))
    deployment_event_type_id = ReferenceCol('deployment_event_type', nullable=False, cascade=True, index=True)
    deployment_event_type = relationship('DeploymentEventType', backref=backref('deployment_events', passive_deletes=True))
    deployment_id = ReferenceCol('deployment', nullable=False, cascade=True, index=True)
    deployment = relationship('Deployment', backref=backref('events', passive_deletes=True))


class Deployment(APIModel, Model, SurrogatePK):
    __tablename__ = 'deployment'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False)
    key = Column(db.String(80), nullable=False)
    path = Column(db.Text, nullable=True)
    # lat = Column(db.Float(precision=53), unique=False, nullable=True)
    # lon = Column(db.Float(precision=53), unique=False, nullable=True)
    campaign_id = ReferenceCol('campaign', nullable=False, cascade=True)
    campaign = relationship('Campaign', backref=backref('deployments', lazy="dynamic", passive_deletes=True))
    user_id = ReferenceCol('users', nullable=False)
    user = relationship('User')
    description = Column(db.Text, nullable=True)
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    platform_id = ReferenceCol('platform', nullable=False, cascade=True)
    platform = relationship('Platform', backref=backref('deployments', lazy="dynamic", passive_deletes=True))
    datasource_id = ReferenceCol('datasource', nullable=False, cascade=True)
    datasource = relationship('DataSource', backref=backref('deployments', passive_deletes=True, lazy="dynamic"))
    is_valid = Column(db.Boolean(), default=True)
    #timestamp = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)

    deployment_mv = relationship('DeploymentMV', uselist=False)   # MATERIAL VIEW OBJECT DATA

    # Enforce uniqueness constraint on user and name
    # __table_args__ = (UniqueConstraint('campaign_id', 'key', name='_deployment_campaign_uc'),)
    # Enforce uniqueness constraint on user and key
    __table_args__ = (UniqueConstraint('user_id', 'campaign_id', 'key', 'platform_id', name='_deployment_key_uc'),)


    @property
    def latlon(self):
        try:
            if self.deployment_mv is not None:
                p = to_shape(self.deployment_mv.point)
                return dict(lat=float(p.y), lon=float(p.x))
            return db.session.query(Pose.lat, Pose.lon).filter(
                Pose.media_id == Media.id, Media.deployment_id == self.id, Pose.lat.isnot(None), Pose.lon.isnot(None),
                Pose.lat != float('NaN'), Pose.lon != float('NaN')).first()._asdict()
        except Exception as e:
            #db.session.rollback()
            return None

    @property
    def pose(self):
        try:
            return db.session.query(Pose.lat, Pose.lon, Pose.dep, Pose.alt, Pose.timestamp).filter(
                Pose.media_id == Media.id, Media.deployment_id == self.id, Pose.lat.isnot(None), Pose.lon.isnot(None),
                Pose.lat != float('NaN'), Pose.lon != float('NaN')).first()._asdict()
        except Exception as e:
            db.session.rollback()
            return None

    @hybrid_property
    def media_count(self):
        """Total number of [media](#media) objects contained in this `deployment`"""
        return self.deployment_mv.media_count if self.deployment_mv else \
            Media.query.with_entities(Media.id).filter(Media.deployment_id == self.id).count()

    @media_count.expression
    def media_count(cls):
        return (db.select([db.func.sum(DeploymentMV.media_count)]).where(
            DeploymentMV.id == cls.id)
        ).label("media_count")
        # return (db.select([db.func.count(Media.id)]).where(
        #     Media.deployment_id == cls.id).label("media_count"))

    @hybrid_property
    def annotated_media_count(self):
        """Number of [media](#media) objects with one or more labeled [annotations](#annotation) in this `deployment`"""
        return Media.query.with_entities(Media.id).filter(Media.deployment_id == self.id, Media.has_annotations()==True).count()

    @annotated_media_count.expression
    def annotated_media_count(cls):
        return (db.select([db.func.count(Media.id)]).where(
            Media.deployment_id == cls.id).where(Media.has_annotations()==True).label("annotated_media_count"))

    @hybrid_property
    def geom(self):
        """The centroid `geom` point of all the [poses](#pose) contained in this `deployment`"""
        return db.session.query(db.func.ST_Centroid(db.func.ST_Collect(Pose.geom))).filter(
            Pose.media_id == Media.id, Media.deployment_id ==  self.id).first()

    @geom.expression
    def geom(cls):
        return (db.select([db.func.ST_Centroid(db.func.ST_Collect(Pose.geom))]).where(Pose.media_id == Media.id).where(
            Media.deployment_id == cls.id)).label("geom")

    @hybrid_property
    def timestamp(self):
        """A `timestamp` from the [poses](#pose) contained in this `deployment`"""
        return list(db.session.query(Pose.timestamp).filter(
            Pose.media_id == Media.id, Media.deployment_id == self.id).first())[0]

    @timestamp.expression
    def timestamp(cls):
        return (db.select([Pose.timestamp]).where(
            Pose.media_id == Media.id).where(Media.deployment_id == cls.id).limit(1).label("timestamp"))

    @hybrid_property
    def total_annotation_count(self):
        """Total number of labeled [annotations](#annotation) linked to [media](#media) objects from this `deployment`"""
        return self.deployment_mv.total_annotation_count if self.deployment_mv else \
            AnnotationLabel.query.with_entities(AnnotationLabel.id).filter(
            AnnotationLabel.point_id == AnnotationPoint.id, AnnotationPoint.media_id == Media.id,
            Media.deployment_id == self.id,
            AnnotationLabel.label_id.isnot(None)).count()

    @total_annotation_count.expression
    def total_annotation_count(cls):
        return (db.select([db.func.sum(DeploymentMV.total_annotation_count)]).where(
            DeploymentMV.id == cls.id)
        ).label("total_annotation_count")
        # return (db.select([db.func.count(AnnotationLabel.id)]).where(
        #     AnnotationLabel.point_id == AnnotationPoint.id).where(AnnotationPoint.media_id == Media.id).where(
        #     Media.deployment_id == cls.id).where(
        #     AnnotationLabel.label_id.isnot(None)
        # ).label("total_annotation_count"))

    @hybrid_property
    def public_annotation_count(self):
        """Total number of publicly shared labeled [annotations](#annotation) linked to [media](#media) objects from this `deployment`"""
        return self.deployment_mv.public_annotation_count if self.deployment_mv else \
            AnnotationLabel.query.with_entities(AnnotationLabel.id).filter(
            AnnotationLabel.point_id == AnnotationPoint.id, AnnotationPoint.media_id == Media.id,
            Media.deployment_id == self.id,
            AnnotationLabel.label_id.isnot(None),
            AnnotationLabel.annotation_set.has(AnnotationSet.usergroups.any(Group.is_public.is_(True)))).count()

    @public_annotation_count.expression
    def public_annotation_count(cls):
        return (db.select([db.func.sum(DeploymentMV.public_annotation_count)]).where(
            DeploymentMV.id == cls.id)
        ).label("public_annotation_count")
        # return (db.select([db.func.count(AnnotationLabel.id)]).where(
        #     AnnotationLabel.point_id == AnnotationPoint.id).where(AnnotationPoint.media_id == Media.id).where(
        #     Media.deployment_id == cls.id).where(AnnotationLabel.label_id.isnot(None)).where(
        #     AnnotationLabel.annotation_set.has(AnnotationSet.usergroups.any(Group.is_public.is_(True)))
        # ).label("public_annotation_count"))

    # @hybrid_property
    def color(self):
        return self.campaign.color

    # def ecoregion(self):
    #     ll = self.latlon
    #     r = get_ecoregion(ll.get("lat"), ll.get("lon"), fields="ECOREGION,PROVINCE,REALM,Lat_Zone")
    #     return {k.lower(): v for k, v in r["features"][0]["attributes"].items()}   # return disc for first features with lowercase keys

    # @color.expression
    # def color(cls):
    #     return select([Campaign.color]).where(cls.campaign_id == Campaign.id).as_scalar()

    def pose_stats(self):
        result = db.session.query(
            db.func.min(Pose.timestamp).label('ts_min'), db.func.max(Pose.timestamp).label('ts_max'),
            db.func.count(distinct(Pose.id)).label("pose_count")
        ).filter(Pose.media_id == Media.id, Media.deployment_id == self.id).first()
        return result._asdict()

    def timestamp_range(self):
        try:
            result = db.session.query(db.func.min(Pose.timestamp).label('min'),
                                      db.func.max(Pose.timestamp).label('max')
                                      ).filter(Pose.media_id == Media.id, Media.deployment_id == self.id).first()
            return result._asdict()
        except:
            return None

    def layers(self):
        layers = []
        for l in self.platform.layers.order_by(PlatformMapLayer.id.asc()):
            properties = {k: v.format(deployment=self) if isinstance(v, str) else v for k,v in list(l.properties.items())}
            # properties.update({"cql_filter":"site_code = '{deployment.key}'".format(deployment=self)})
            layers.append(dict(baseurl=l.baseurl, layertype=l.layertype, properties=properties, name=l.name, minzoom=l.minzoom))
        return layers

    def extent(self):
        """
        Compute spatial extent of Campaign
        :return: list with format [[SW_LAT, SW_LON],[NE_LAT,NE_LON]]
        """
        ext = db.session.query(db.func.min(Pose.lat).label('min_lat'), db.func.max(Pose.lat).label('max_lat'),
                               db.func.min(Pose.lon).label('min_lon'), db.func.max(Pose.lon).label('max_lon')
                               ).filter(Pose.media_id == Media.id, Media.deployment_id == self.id).first()

        return [[ext.min_lat, ext.min_lon], [ext.max_lat, ext.max_lon]]

        # return [dict(baseurl=l.baseurl, layertype=l.layertype, properties=l.properties, name=l.name)
        #         for l in self.platform.layers]

    @classmethod
    def get_latest(cls, user_id=None):
        return cls.query.filter(cls.user_id == user_id).order_by(cls.created_at.desc()).first()


    # def delete_cascade(self):
    #     for pd in PoseData.query.filter(Pose.id == PoseData.pose_id, Pose.deployment_id == self.id).all():
    #         pd.delete(commit=False)
    #     db.session.commit()
    #     print "Deleted PoseData"
    #     for mp in MediaPoseAssociation.query.filter(MediaPoseAssociation.pose_id==Pose.id, Pose.deployment_id == self.id).all():
    #         mp.delete(commit=False)
    #     db.session.commit()
    #     print "Deleted MediaPoseAssociations"
    #     for m in Media.query.filter(Media.deployment_id == self.id).all():
    #         m.delete(commit=False)
    #     db.session.commit()
    #     print "Deleted Media"
    #     for p in Pose.query.filter(Pose.deployment_id == self.id).all():
    #         p.delete(commit=False)
    #     db.session.commit()
    #     print "Deleted Poses"

    def __init__(self, **kwargs):
        # if no datasource_id, try to set it if there is only one data source for the platform, otherwise do nothing
        if "datasource_id" not in kwargs:
            try: kwargs["datasource_id"]=DataSource.query.filter(DataSource.platform_id==kwargs.get("platform_id")).one().id
            except: pass
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<Deployment({name})>'.format(name=self.name)

    # @classmethod
    # def dict_to_db(cls, dictlist):
    #     for i in dictlist:
    #         db.session.add(cls(**i))
    #     db.session.commit()
    #     # TODO: this shouldn't have to clear the entire cache - should clear only the deployment cache
    #     # cache.clear()
    #
    #     # def update_latlons(self):
    #     #     if len(self.poses) > 0:
    #     #         self.update(lat=numpy.median([p.lat for p in self.poses]), lon=numpy.median([p.lon for p in self.poses]))


# class DeploymentPath(SurrogatePK, Model):
#     __tablename__ = 'deployment_path'
#     type = Column(db.String(80), nullable=False)
#     pattern = Column(db.Text, nullable=False)
#     deployment_id = ReferenceCol('deployment', nullable=False)
#     campaign = relationship('Deployment', backref=backref('paths', cascade="all, delete"))
#
#     def __init__(self, **kwargs):
#         db.Model.__init__(self, **kwargs)
#
#     def __repr__(self):
#         return '<DeploymentPath({type!r}: {pattern!r})>'.format(type=self.type, pattern=self.pattern)
class DeploymentFile(DBFileMixin, APIModel, Model):
    __tablename__ = 'deployment_file'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    user_id = ReferenceCol('users', nullable=False)
    user = relationship('User')
    deployment_id = ReferenceCol('deployment', nullable=False, cascade=True)
    deployment = relationship('Deployment', backref=backref('files', passive_deletes=True, lazy="dynamic"), enable_typechecks=False)
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    updated_at = Column(db.DateTime, default=dt.datetime.utcnow, onupdate=dt.datetime.utcnow, doc="Updated time (UTC)")
    __table_args__ = (UniqueConstraint('deployment_id', 'name', name='_deployment_file_name_uc'),)


    def __init__(self, **kwargs):
        """Add user_id to post payload if not supplied"""
        if "user_id" not in kwargs:
            kwargs["user_id"] = current_user.id
        super().__init__(**kwargs)


class DataSourceType(APIModel, SurrogatePK, Model):
    __tablename__ = 'datasource_type'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False, unique=True)
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)


class DataSource(APIModel, Model, SurrogatePK):
    __tablename__ = 'datasource'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False, unique=True)

    campaign_search = Column(db.Text, nullable=True)
    datafile_pattern = Column(db.Text, nullable=True)
    datafile_search = Column(db.Text, nullable=True)
    deployment_search = Column(db.Text, nullable=True)
    media_pattern = Column(db.Text, nullable=True)
    mediadir_search = Column(db.Text, nullable=True)
    thumb_pattern = Column(db.Text, nullable=True)
    thumbdir_search = Column(db.Text, nullable=True)
    urlbase_browse = Column(db.Text, nullable=True)
    credential_key = Column(db.Text, nullable=True)
    datafile_operations = Column(db.JSON, nullable=True)

    datasource_type_id = ReferenceCol('datasource_type', nullable=False, cascade=True)
    datasource_type = relationship('DataSourceType', backref=backref('datasources', passive_deletes=True))
    platform_id = ReferenceCol('platform', nullable=False, cascade=True)
    platform = relationship('Platform', backref=backref('datasources', passive_deletes=True))
    user_id = ReferenceCol('users', nullable=False)
    user = relationship('User')  # , backref='campaigns')
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)

    def __init__(self, datasource_type=None, **kwargs):
        if isinstance(datasource_type, str):
            dst, created = DataSourceType.get_or_create(name=datasource_type)
            kwargs.update(dict(datasource_type_id=dst.id))  # add datasource_type_id if datasource_type is supplied
        db.Model.__init__(self, **kwargs)


class Platform(APIModel, Model, SurrogatePK, JSONDataFieldMixin):
    __tablename__ = 'platform'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False)
    key = Column(db.String(80), nullable=False, unique=True)
    description = Column(db.Text, nullable=True)
    reference = Column(db.Text, nullable=True)
    user_id = ReferenceCol('users', nullable=True)
    user = relationship('User')  # , backref='campaigns')
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    # track_layer = Column(db.String(255), nullable=True)

    def __init__(self, **kwargs):
        # If not key, set automatically based on "name"
        if "key" not in kwargs:
            try: kwargs["key"]=secure_filename(kwargs["name"])
            except: pass
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<Platform({name})>'.format(name=self.name)

    # # API registration
    # def fileurl(self):
    #     if self._rawfiledata is None:
    #         return "/static/images/placeholder.jpg"
    #     else:
    #         return "/api/{}_file/{}".format(self.__tablename__, self.id)

    # Add apifield for current user denoting whether or not they own the group
    def current_user_is_owner(self):
        return current_user.id == self.user_id if current_user.is_authenticated() else False


class MapLayer(APIModel, SurrogatePK, Model):
    __tablename__ = "maplayer"
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(255), nullable=False)
    baseurl = Column(db.String(255), nullable=False)
    layertype = Column(db.String(128), nullable=False)
    properties = Column(JSON, nullable=True)



class PlatformMapLayer(APIModel, SurrogatePK, Model):
    __tablename__ = "platform_maplayer"
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(255), nullable=False)
    baseurl = Column(db.String(255), nullable=False)
    layertype = Column(db.String(128), nullable=False)
    minzoom = Column(db.Integer)
    properties = Column(JSON, nullable=True)
    platform_id = ReferenceCol('platform', nullable=True)
    platform = relationship('Platform', backref=backref('layers', passive_deletes=True, lazy="dynamic"))


class Campaign(APIModel, Model, SurrogatePK):
    __tablename__ = 'campaign'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False, doc="Name of this `campaign`")
    key = Column(db.String(80), nullable=False, unique=True, doc="Key of this `campaign` (used for generating paths and links in repo)")
    path = Column(db.Text, nullable=True)
    color = Column(db.String(10), unique=False, default=get_random_hex_color, doc="Hex colour code for this `campaign`")
    description = Column(db.Text, nullable=True, doc="Description for this `campaign`")
    user_id = ReferenceCol('users', nullable=False, doc="ID of the [user][#users] who created this `campaign`")
    user = relationship('User')  # , backref='campaigns')
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow, doc="Datetime of creation")

    media_path_pattern = Column(db.Text, nullable=True)
    thm_path_pattern = Column(db.Text, nullable=True)

    campaign_mv = relationship('CampaignMV', uselist=False)  # MATERIAL VIEW OBJECT DATA

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<Campaign({name!r})>'.format(name=self.name)

    @hybrid_property
    def deployment_count(self):
        """Total number of [deployments](#deployment) contained in this `campaign`"""
        return self.campaign_mv.deployment_count if self.campaign_mv else \
            self.deployments.with_entities(Deployment.id).filter(Deployment.is_valid.is_(True)).count()
    #
    @deployment_count.expression
    def deployment_count(cls):
        return (db.select([db.func.sum(CampaignMV.deployment_count)]).where(
            CampaignMV.id == cls.id)
        ).label("deployment_count")
        # return (db.select([db.func.count(Deployment.id)]).where(
        #     Campaign.id == Deployment.campaign_id).where(Campaign.id == cls.id).where(Deployment.is_valid.is_(True)
        # ).label("deployment_count"))

    @hybrid_property
    def media_count(self):
        """Total number of [media](#media) objects contained in this `campaign`"""
        return self.campaign_mv.media_count if self.campaign_mv else \
            Media.query.with_entities(Media.id).filter(Media.deployment_id==Deployment.id, Deployment.campaign_id == self.id).count()

    @media_count.expression
    def media_count(cls):
        return (db.select([db.func.sum(CampaignMV.media_count)]).where(
            CampaignMV.id == cls.id)
        ).label("media_count")
        # return (db.select([db.func.count(Media.id)]).where(
        #     Media.deployment_id==Deployment.id).where(Deployment.campaign_id == cls.id
        # ).label("media_count"))

    @hybrid_property
    def geom(self):
        """The centroid `geom` point of all the [poses](#pose) contained in this `campaign`"""
        return db.session.query(db.func.ST_Centroid(db.func.ST_Collect(Pose.geom))).filter(
            Pose.media_id == Media.id, Media.deployment_id == Deployment.id, Deployment.campaign_id == self.id).first()

    @geom.expression
    def geom(cls):
        return (db.select([db.func.ST_Centroid(db.func.ST_Collect(Pose.geom))]).where(Pose.media_id == Media.id).where(
            Media.deployment_id == Deployment.id).where(Deployment.campaign_id == cls.id)).label("geom")

    @hybrid_property
    def timestamp(self):
        """A `timestamp` from the [poses](#pose) contained in this `campaign`"""
        return db.session.query(Pose.timestamp).filter(
            Pose.media_id == Media.id, Media.deployment_id == Deployment.id, Deployment.campaign_id == self.id).first()

    @timestamp.expression
    def timestamp(cls):
        return (db.select([Pose.timestamp]).where(
            Pose.media_id==Media.id).where(Media.deployment_id == Deployment.id).where(
            Deployment.campaign_id == cls.id).limit(1).label("timestamp"))

    @hybrid_property
    def total_annotation_count(self):
        """Total number of labeled [annotations](#annotation) linked to [media](#media) objects from this `campaign`"""
        return self.campaign_mv.total_annotation_count if self.campaign_mv else \
            AnnotationLabel.query.with_entities(AnnotationLabel.id).filter(
            AnnotationLabel.point_id == AnnotationPoint.id, AnnotationPoint.media_id == Media.id,
            Media.deployment_id == Deployment.id, Deployment.campaign_id == self.id,
            AnnotationLabel.label_id.isnot(None)).count()

    @total_annotation_count.expression
    def total_annotation_count(cls):
        return (db.select([db.func.sum(CampaignMV.total_annotation_count)]).where(
            CampaignMV.id == cls.id)
        ).label("total_annotation_count")
        # return (db.select([db.func.count(AnnotationLabel.id)]).where(
        #     AnnotationLabel.point_id == AnnotationPoint.id).where(AnnotationPoint.media_id == Media.id).where(
        #     Media.deployment_id == Deployment.id).where(Deployment.campaign_id == cls.id).where(
        #     AnnotationLabel.label_id.isnot(None)
        # ).label("total_annotation_count"))

    @hybrid_property
    def public_annotation_count(self):
        """Total number of publicly shared labeled [annotations](#annotation) linked to [media](#media) objects from this `campaign`"""
        return self.campaign_mv.public_annotation_count if self.campaign_mv else \
            AnnotationLabel.query.with_entities(AnnotationLabel.id).filter(
            AnnotationLabel.point_id == AnnotationPoint.id, AnnotationPoint.media_id == Media.id,
            Media.deployment_id == Deployment.id, Deployment.campaign_id == self.id,
            AnnotationLabel.label_id.isnot(None), AnnotationLabel.annotation_set.has(AnnotationSet.usergroups.any(Group.is_public.is_(True)))).count()

    @public_annotation_count.expression
    def public_annotation_count(cls):
        return (db.select([db.func.sum(CampaignMV.public_annotation_count)]).where(
            CampaignMV.id == cls.id)
        ).label("public_annotation_count")
        # return (db.select([db.func.count(AnnotationLabel.id)]).where(
        #     AnnotationLabel.point_id == AnnotationPoint.id).where(AnnotationPoint.media_id == Media.id).where(
        #     Media.deployment_id == Deployment.id).where(Deployment.campaign_id == cls.id).where(
        #     AnnotationLabel.label_id.isnot(None)).where(
        #     AnnotationLabel.annotation_set.has(AnnotationSet.usergroups.any(Group.is_public.is_(True)))
        # ).label("public_annotation_count"))

    def file_count(self):
        return self.files.count()

    @property
    def latlon(self):
        try:
            if self.campaign_mv is not None:
                p = to_shape(self.campaign_mv.centroid)
                return dict(lon=float(p.x), lat=float(p.y))
            ll = db.session.query(Pose.lat, Pose.lon).filter(
                Pose.media_id == Media.id, Media.deployment_id == Deployment.id, Deployment.campaign_id == self.id,
                Pose.lat.isnot(None), Pose.lon.isnot(None), Pose.lat != float('NaN'), Pose.lon != float('NaN')).first()
            return ll._asdict() if ll else None
        except Exception as e:
            return None

    def timestamp_range(self):
        result = db.session.query(db.func.min(Pose.timestamp).label('min'),
                                  db.func.max(Pose.timestamp).label('max')).filter(
            Pose.media_id == Media.id, Media.deployment_id == Deployment.id, Deployment.campaign_id == self.id).one()
        return result._asdict()
        # tsr = db.session.query(db.func.min(Pose.timestamp).label('min'), db.func.max(Pose.timestamp).label('max')
        #                        ).filter(Pose.deployment_id == Deployment.id, Deployment.campaign_id == self.id).first()
        # return dict(min=str(tsr.min), max=str(tsr.max), duration=str(tsr.max - tsr.min))

    def stats(self):
        result = db.session.query(
            db.func.min(Pose.timestamp).label('ts_min'), db.func.max(Pose.timestamp).label('ts_max'),
            db.func.count(Pose.id).label("pose_count"), db.func.count(distinct(Deployment.id)).label("deployment_count")
        ).filter(Pose.media_id == Media.id, Media.deployment_id == Deployment.id, Deployment.campaign_id == self.id).one()
        return result._asdict()

    def extent(self):
        """
        Compute spatial extent of Campaign
        :return: list with format [[SW_LAT, SW_LON],[NE_LAT,NE_LON]]
        """
        ext = db.session.query(db.func.min(Pose.lat).label('min_lat'), db.func.max(Pose.lat).label('max_lat'),
                               db.func.min(Pose.lon).label('min_lon'), db.func.max(Pose.lon).label('max_lon')
                               ).filter(Pose.media_id == Media.id, Media.deployment_id == Deployment.id, Deployment.campaign_id == self.id).one()

        return [[ext.min_lat, ext.min_lon],[ext.max_lat, ext.max_lon]]

    def platforms(self):
        return [dict(id=i[0], name=i[1]) for i in Platform.query.filter(Platform.deployments.any(Deployment.campaign_id==self.id)).with_entities(Platform.id,Platform.name).all()]


class CampaignInfo(APIModel, SurrogatePK, Model):
    __tablename__ = 'campaigninfo'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    name = Column(db.String(80), nullable=False)
    description = Column(db.Text, nullable=False)
    created_at = Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    campaign_id = ReferenceCol('campaign', nullable=False, cascade=True)
    # project = relationship('Group', backref='campaigns')
    campaign = relationship('Campaign', backref=backref('info', passive_deletes=True))

    def __init__(self, **kwargs):
        db.Model.__init__(self, **kwargs)

    def __repr__(self):
        return '<CampaignInfo({name!r}: {description!r})>'.format(name=self.name, description=self.description)


class CampaignFile(DBFileMixin, APIModel, Model):
    __tablename__ = 'campaign_file'
    __mapper_args__ = {'polymorphic_identity': __tablename__}
    user_id = ReferenceCol('users', nullable=False)
    user = relationship('User')
    campaign_id = ReferenceCol('campaign', nullable=False, cascade=True)
    campaign = relationship('Campaign', backref=backref('files', passive_deletes=True, lazy="dynamic"))
    created_at = Column(db.DateTime, nullable=True, default=dt.datetime.utcnow)
    updated_at = Column(db.DateTime, default=dt.datetime.utcnow, onupdate=dt.datetime.utcnow, doc="Updated time (UTC)")
    __table_args__ = (UniqueConstraint('campaign_id', 'name', name='_campaign_file_name_uc'),)

    def __repr__(self):
        return '<File({name})>'.format(name=self.name)

    def __init__(self, **kwargs):
        """Add user_id to post payload if not supplied"""
        if "user_id" not in kwargs:
            kwargs["user_id"] = current_user.id
        super().__init__(**kwargs)


# ---------------------------------------------------------------------------------------------------
# MATERIALIZED VIEWS
# ---------------------------------------------------------------------------------------------------
class CampaignMV(Model):
    '''deal with an existing table'''
    __table__ = Table(
        "campaigns_mv",
        db.MetaData(),
        Column("id", db.Integer, db.ForeignKey(Campaign.id), primary_key=True),
        autoload=True,
        autoload_with=db_engine
        # autoload_with=create_engine('postgresql://localhost/marinedb')
    )
    #campaign = relationship('Campaign', backref=backref('campaign_mv', uselist=False)) #, lazy="dynamic"))


class DeploymentMV(Model):
    '''deal with an existing table'''
    __table__ = Table(
        "deployments_mv",
        db.MetaData(),
        Column("id", db.Integer, db.ForeignKey(Deployment.id), primary_key=True),
        Column("platform_id", db.Integer, db.ForeignKey(Platform.id)),
        Column("campaign_id", db.Integer, db.ForeignKey(Campaign.id)),
        autoload=True,
        autoload_with=db_engine
        # autoload_with=create_engine('postgresql://localhost/marinedb')
    )
    #deployment = relationship('Deployment')

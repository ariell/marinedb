import io
# import traceback
import calendar
from urllib import request as urlrequest
import uuid

import os

# import sys
# import math
from PIL import Image, ImageDraw
from pickle import UnpicklingError

# from pandas import to_datetime
# from dateutil.parser import parse
# from dateutil.relativedelta import relativedelta
from geoalchemy2.comparator import Comparator
from numpy import mean, median
from savalidation import ValidationMixin
from werkzeug.urls import url_fix
from werkzeug.utils import secure_filename

from marinedb.annotation.models import Label, LabelScheme
from marinedb.geodata.utils import subsample_media, get_points, get_ecoregion
# from modules.datatransform.models import render_data_transformer, get_data_transformer
from marinedb.user.models import User, Group, GroupType, UserGroupAssociation
from modules.datatransform.data_modules import json_dumps
from modules.datatransform.models import export_transformed_query, DataTransformer
from modules.flask_restless.usergroups import APIUserGroupMixin
from .models import Campaign, CampaignFile, CampaignInfo, Deployment, Platform, Pose, PoseData, Media, \
    AnnotationLabel, TagAnnotationAssociation, MediaCollection, AnnotationSet, MediaType, \
    AnnotationPoint, MediaCollectionMediaAssociation, DeploymentEvent, DeploymentEventType, MapLayer, \
    PlatformMapLayer, DataSource, DeploymentFile, DataSourceType, AnnotationSetFile  # , MediaPoseAssociation
from sqlalchemy.ext.declarative import declared_attr
from savalidation.helpers import before_flush
# from savalidation import ValidationMixin, ValidationError
import savalidation.validators as val
# from sqlalchemy.orm.exc import NoResultFound
from flask import send_file, current_app, g

from flask import request, jsonify

# from modules.datasample.annotations import AnnotationPointSampler
from modules.flask_restless import ProcessingException
from modules.flask_restless.mixins import APIModelEndpoint, docstring_parameter, assert_authenticated_user

import hashlib

from flask_login import current_user

# from werkzeug.contrib.cache import SimpleCache, FileSystemCache
# import numpy
from modules.flask_restless.helpers import count
from modules.flask_restless.search import search

import json
from marinedb.database import db

# import random
from datetime import datetime, timedelta

# import numpy as np
# import pandas as pd
# cache = SimpleCache()
from sqlalchemy import func, or_, text, distinct
#from sqlalchemy import or_
from modules.flask_restless.mixins import APIModel, add_to_api  # , APIInheritModelPermissions
#from modules.flask_restless.usergroups import APIUserGroupMixin

# from modules.datatransform.models import DataTransformer
import pandas as pd

# from collections import namedtuple
from .data_exporters import media_collection_export_query, deployment_export, nullifnan, \
    export_annotations  # , media_collection_export,

from modules.flask_restless.views import render_data_response


class MediaCollectionAPI(MediaCollection, ValidationMixin):
    """
    Media Collection resource: a user-defined collection of [media](#media) objects grouped for further analysis.
    Can be a subset of [media](#media) objects from one or more [deployments](#deployment)
    """
    # __tablename__ = MediaCollection.__tablename__
    # __mapper_args__ = {"polymorphic_identity": MediaCollection.__tablename__}

    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    api_include_columns = ['id', 'name', 'parent_collection_id', 'description', 'user', 'created_at',
                           'user.id', 'user.first_name', 'user.last_name', "user.username", #'usergroups',
                           'current_user_can_view', 'current_user_is_owner', 'current_user_is_member', 'current_user_can_edit', "is_public"]
    # api_exclude_columns = ['media_collection_media', 'media', 'data_json', 'annotation_sets', 'user']
    api_max_results_per_page = 10000
    api_include_methods = ["usergroup_count"]
    api_add_columns_single = []
    api_add_methods_single = ['media_count', 'data', 'extent']

    api_custom_endpoints = APIUserGroupMixin.build_api_endpoints() + [
        APIModelEndpoint("export_media", endpoint='{tablename}/<int:id>/export', methods=["GET"]),
        # APIModelEndpoint("addmedia", endpoint='{tablename}/<int:id>/media', methods=["GET"]),
        APIModelEndpoint("get_media_many", endpoint='{tablename}/<int:media_collection_id>/media', methods=["GET"]),
        APIModelEndpoint("post_media_many", endpoint='{tablename}/<int:media_collection_id>/media', methods=["POST"]),
        # APIModelEndpoint("delete_media_many", endpoint='{tablename}/<int:media_collection_id>/media', methods=["GET","POST","DELETE"]),
        APIModelEndpoint("get_media_single", endpoint='{tablename}/<int:media_collection_id>/media/<int:media_id>', methods=["GET"]),
        APIModelEndpoint("post_media_single", endpoint='{tablename}/<int:media_collection_id>/media/<int:media_id>', methods=["POST"]),
        APIModelEndpoint("delete_media_single", endpoint='{tablename}/<int:media_collection_id>/media/<int:media_id>', methods=["DELETE"]),
    ]

    #api_preprocessors = APIUserGroupMixin.get_preprocessors()
    # api_postprocessors = {"GET_SINGLE": ["get_single_postprocessor"]}

    # validations
    val.validates_constraints()

    @classmethod
    def remove_from_group_api(cls, instance_id, group_id):
        """Remove instance with ID of `instance_id` from the group with the ID `group_id`"""
        # if AnnotationSetAPI.query.filter(AnnotationSetAPI.media_collection_id == instance_id, AnnotationSetAPI.usergroups.filter(UserGroupAssociation.group_id == group_id)).count()>0:
        #     raise ProcessingException(description="Unable to remove from Group. This Media Collection contains an "
        #                                           "Annotation Set that is shared in this Group. You need to remove "
        #                                           "it from the Group first.", code=400)
        for a in AnnotationSetAPI.query.filter(AnnotationSetAPI.media_collection_id == instance_id):
            if a.is_in_group(group_id):
                raise ProcessingException(description="Unable to remove from Group. This Media Collection contains an "
                                                      "Annotation Set that is shared in this Group. You need to remove "
                                                      "it from the Group first.", code=400)
        return super(cls, cls).remove_from_group_api(instance_id, group_id)

    @classmethod
    # override method from Mixin to add annotation sets as well (if not already added)
    def add_to_group_api(cls, instance_id, group_id, cascade=True, **kw):
        """
        Add instance with ID of `instance_id` to the group with the ID `group_id`
        """
        if cascade:
            instance = cls.get_by_id(instance_id)
            for a in instance.annotation_sets:
                try:
                    AnnotationSetAPI.add_to_group_api(a.id, group_id, cascade=False)
                except ProcessingException as e:
                    current_app.logger.debug("Annotation Set was not added to group: {}: {}".format(e, e.description))
                except Exception as e:
                    current_app.logger.debug("Annotation Set was not added to group: {}".format(e))
        return super(cls, cls).add_to_group_api(instance_id, group_id, **kw)

    # # TODO: this should be a POST not a GET - update it to use more generic endpoint with configurable sampling
    # @classmethod
    # def addmedia(cls, id):
    #     cls.patch_auth_preprocessor(instance_id=id)
    #     return media_collection_addmedia(cls, id)

    @classmethod
    def get_media_many(cls, media_collection_id=None):
        """
        Get list of [media](#media) objects contained in the [media_collection](#media_collection) with an ID matching `media_collection_id`.
        Accepts search query parameters. See [Making API search queries](#api_query) for instructions on making search queries
        """
        cls.get_single_auth_preprocessor(instance_id=media_collection_id)
        result = MediaAPI.get_search_results(
            filters=(MediaAPI.media_collections.any(MediaCollection.id == media_collection_id)),
            allow_qs_includes=False)
        return render_data_response(result)

    @classmethod
    def post_media_many(cls, media_collection_id=None):
        """
        Add [media](#media) objects matching the search query defined in the json post parameter `q` to the [media_collection](#media_collection)
        with an ID matching `media_collection_id`. `q` is a search query in the format defined in [Making API search queries](#api_query) section
        and is sent as either a URL parameter or form post parameter.
        Request returns the corresponding [media_collection](#media_collection).
        """
        cls.patch_auth_preprocessor(instance_id=media_collection_id)
        media_collection = cls.get_by_id(media_collection_id)
        # get q from QS param or form json param
        q = json.loads(request.args.get('q', request.get_json(force=True).get('q', '{}')))
        append = request.args.get("append", request.get_json(force=True).get('q', 'false')).lower() == "true"
        sample_params = q.get('sample', None)
        if q:
            query = search(db.session, Media, q)
            media = subsample_media(query, sample_params=sample_params)
            media_collection.add_media(media=media, q=q, append=append)
        return render_data_response(media_collection.instance_to_dict())

    @classmethod
    def get_media_single(cls, media_collection_id=None, media_id=None):
        """
        Get the single [media](#media) object with the ID `media_id` from the [media_collection](#media_collection) with the ID `media_collection_id`
        """
        cls.get_single_auth_preprocessor(instance_id=media_collection_id)
        result = MediaAPI.model_to_dict(MediaCollectionMediaAssociation.query.filter(
            MediaCollectionMediaAssociation.media_collection_id == media_collection_id,
            MediaCollectionMediaAssociation.media_id == media_id
        ).one().media, is_single=True)
        return render_data_response(result)

    @classmethod
    def post_media_single(cls, media_collection_id=None, media_id=None):
        """
        Add the [media](#media) object with the ID `media_id` to the [media_collection](#media_collection) with the ID `media_collection_id`
        """
        cls.patch_auth_preprocessor(instance_id=media_collection_id)
        m, created = MediaCollectionMediaAssociation.get_or_create(media_collection_id=media_collection_id,
                                                                   media_id=media_id)
        return render_data_response(MediaAPI.model_to_dict(m.media))

    @classmethod
    def delete_media_single(cls, media_collection_id=None, media_id=None):
        """
        Remove the [media](#media) object with the ID `media_id` from the [media_collection](#media_collection) with the ID `media_collection_id`
        """
        cls.patch_auth_preprocessor(instance_id=media_collection_id)
        instance = cls.get_by_id(media_collection_id)

        # Delete points (and annotations) from associated media_collections
        annotation_set_ids = []
        for a in instance.annotation_sets:
            if not a.current_user_can_edit and a.parent_id is None: # allow if derived annotation_set (parent_id != None)
                raise ProcessingException(
                    'Unable to remove media from media_collection. You do not have edit permission for the "{}" '
                    'annotation_set, so you cannot remove the associated annotations'.format(a.name), code=401)
            annotation_set_ids.append(a.id)
        points = AnnotationPoint.query.filter(AnnotationPoint.annotation_set_id.in_(annotation_set_ids), AnnotationPoint.media_id==media_id)
        points.delete(synchronize_session=False)

        # Delete media_collection-media object
        m = MediaCollectionMediaAssociation.query.filter_by(media_collection_id=media_collection_id, media_id=media_id).one()
        m.delete(commit=False)
        db.session.commit()
        return render_data_response(instance.instance_to_dict())

    @classmethod
    def export_query(cls, id):
        cls.get_single_auth_preprocessor(instance_id=id)
        return media_collection_export_query(cls, id)

    # @classmethod
    # def export(cls, id):
    #     cls.get_single_auth_preprocessor(instance_id=id)
    #     return media_collection_export(cls, id)

    @classmethod
    @docstring_parameter(export_transformed_query.__doc__)
    def export_media(cls, id):
        """
        Export [media](#media) collection from [media_collection](#media_collection) matching `id`.
        Search queries are executed on the [media](#media) model.

        %s

        Allowed columns include: `id`, `key`, `timestamp_start`, `path_best`,  `path_best_thm`, `pose.timestamp`, `pose.lat`,
            `pose.lon`, `pose.alt`, `pose.dep`, `pose.data`, `pose.id`, `deployment.key`,
            `deployment.campaign.key`, `deployment.id`, `deployment.campaign.id`, `event_log`

        ```
        # Example1:
        /api/media_collection/1776/export?template=dataframe.csv&f={"operations": [{"module": "pandas", "method": "json_normalize"}]}
        ```

        NB: the UI contains an "ADVANCED" option which can help build more complex export queries.
        """
        allowed_columns = [
            "id", "key", "timestamp_start", "path_best",  "path_best_thm", "pose.timestamp", "pose.lat",
            "pose.lon", "pose.alt", "pose.dep", "pose.data", "pose.id", "deployment.key",
            "deployment.campaign.key", "deployment.id", "deployment.campaign.id", "event_log"]
        cls.get_single_auth_preprocessor(instance_id=id)  # check authentication
        instance = cls.get_by_id(id)
        filename_pattern = "collection-u{metadata[user][id]}-{metadata[name]}-{metadata[id]}-{hash}"
        return export_transformed_query(instance.media, allowed_columns=allowed_columns,
                                        filename_pattern=filename_pattern, metadata=instance.instance_to_dict())


class AnnotationPointAPI(AnnotationPoint, ValidationMixin):
    """
    Annotation Point resource: an annotatable object on a media frame, captures x,y,t and can have multiple
    [annotations](#annotation) associated with it. A [point](#point) can also be associated with a bounding box,
    polygon, pixel map, etc... Each [point](#point) can have one or more [annotations](#annotation).
    """


    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'DELETE', 'PATCH']
    api_max_results_per_page = 1000

    # api_exclude_columns = ['data_json', 'media', 'annotation_set', 'annotations.data_json']
    api_include_columns = ["media_id", "annotation_set_id", "t", "x", "y", "supplementary_annotations", "data", "id", "timestamp",
                           "annotations", "annotations.id", "annotations.annotation_set_id","updated_at"]

    # api_validation_exceptions = [ValidationError]

    api_custom_endpoints = [
        APIModelEndpoint("post", endpoint='{tablename}', methods=["POST"])
    ]

    api_postprocessors = {
        "GET_SINGLE": ['get_single_postprocessor']
    }
    # api_preprocessors = {
    #     "PATCH_SINGLE": ['patch_auth_preprocessor'],
    #     "DELETE_SINGLE": ['delete_auth_preprocessor'],
    #     "GET_SINGLE": ['get_single_auth_preprocessor'],
    #     "GET_MANY": ['get_many_auth_preprocessor'],
    #     "POST": ['post_auth_preprocessor']
    #     # "GET_MANY": ['view_many_not_allowed_preprocessor']
    #     # "DELETE_MANY": ['edit_auth_many_preprocessor']
    # }

    # validations
    val.validates_constraints()

    # @classmethod
    # def view_many_not_allowed_preprocessor(cls, **kw):
    #     raise ProcessingException(description="Get many not allowed.", code=405)

    @classmethod
    def get_many_auth_preprocessor(cls, search_params, **kw):
        """
        Filter results based on permissions from parent [annotation_set](#annotation_set)
        """
        cls._append_filter(search_params, dict(name="annotation_set", op="has", val=dict(name="current_user_can_view", op="eq", val=True)))

    @classmethod
    def post_auth_preprocessor(cls, data=None, **kw):
        """Enforce implied permissions from parent [annotation_set](#annotation_set)"""
        annotation_set_id = data["annotation_set_id"]
        AnnotationSetAPI.patch_auth_preprocessor(instance_id=annotation_set_id)

    @classmethod
    def patch_auth_preprocessor(cls, instance_id=None, **kw):
        """Enforce implied permissions from parent [annotation_set](#annotation_set)"""
        instance = cls.get_by_id(instance_id)
        if not instance.annotation_set.current_user_can_edit:
            raise ProcessingException(description="Unauthorised. You don't have permission to edit this Annotation Set", code=401)

    @classmethod
    def delete_auth_preprocessor(cls, instance_id=None, *args, **kw):
        """Enforce implied permissions from parent [annotation_set](#annotation_set)"""
        cls.patch_auth_preprocessor(instance_id=instance_id, *args, **kw)

    @classmethod
    def get_single_auth_preprocessor(cls, instance_id=None, **kw):
        """Enforce implied permissions from parent [annotation_set](#annotation_set)"""
        instance = cls.get_by_id(instance_id)
        if not instance.annotation_set.current_user_can_view:
            raise ProcessingException(description="Unauthorised. You don't have permission to view this Annotation Set", code=401)
        # AnnotationSetAPI.get_single_auth_preprocessor(instance_id=instance.annotation_set_id)

    @classmethod
    def post(cls):
        """Custom post endpoint"""
        data = request.get_json()
        cls.post_auth_preprocessor(data=data)
        if "annotations" in data:
            for i in range(len(data.get("annotations"))):
                data["annotations"][i] = AnnotationLabel(**data["annotations"][i])
        inst = AnnotationPoint.create(commit=True, **data)
        result = []
        for al in inst.annotations:
            result.append({"id": al.id, "label_id": al.label_id, "color": al.color()})
        return jsonify(id=inst.id, x=inst.x, y=inst.y, t=inst.t, media_id=inst.media_id, data=inst.data,
                       annotation_set_id=inst.annotation_set_id, annotations=result)

    @staticmethod
    def get_single_postprocessor(result=None, instance=None, **kw):
        """Adds `annotations` and `supplementary_annotations` fields to the response"""
        # annotations = result.get('annotations', [])
        result['annotations'] = []
        result['supplementary_annotations'] = []
        current_annotation_set_id = int(request.args.get('current_annotation_set_id', result['annotation_set_id']))
        for a in instance.annotations.order_by(AnnotationLabel.likelihood.desc()):
            # lbl = AnnotationLabel.get_by_id(al.get("id"))
            obj = dict(
                id=a.id, annotation_set_id=a.annotation_set_id,
                label=dict(name=a.label.name, id=a.label.id, lineage_names=a.label.lineage_names()) if a.label_id else None,
                user=dict(full_name=a.user.full_name(), id=a.user.id, username=a.user.username),
                tags=[dict(name=t.name, id=t.id) for t in a.tags],
                data=a.data,
                likelihood=a.likelihood,
                needs_review=a.needs_review,
                color=a.color(),
                comment=a.comment,
                object_id=a.object_id,
                observation_count=a.observation_count(),
                version_count=a.version_count()
            )
            # check if from this set, otherwise it is a suggestion
            if a.annotation_set_id == current_annotation_set_id:
                result['annotations'].append(obj)
            elif a.label_id is not None:
                result['supplementary_annotations'].append(obj)


    # @classmethod
    # def register_api(cls, apimanager):
    #     exclude_columns = ['data_json', 'media', 'annotation_set', 'annotations.data_json']
    #
    #     def get_single_postprocessor(result=None, **kw):
    #         annotations = result['annotations']
    #         result['annotations'] = []
    #         result['supplementary_annotations'] = []
    #         for al in annotations:
    #             annotation_label = AnnotationLabel.query.filter_by(id=al['id']).one()
    #             al['label'] = {'name': annotation_label.label.name, 'id': annotation_label.label.id} if annotation_label.label else None
    #             al['user'] = {'full_name': annotation_label.user.full_name(), 'id': annotation_label.user.id, 'email': annotation_label.user.email}
    #             #al['tags'] = [{'name': t.name, 'id': t.id} for t in annotation_label.tags]
    #             del al['label_id']
    #             del al['user_id']
    #             if al['annotation_set_id'] == result['annotation_set_id']:
    #                 # add tags to annotation labels
    #                 result['annotations'].append(al)
    #             else:
    #                 result['supplementary_annotations'].append(al)
    #
    #     apimanager.create_api(AnnotationPoint, methods=['GET', 'POST', 'DELETE', 'PATCH'], max_results_per_page=1000,
    #                           exclude_columns=exclude_columns,
    #                           postprocessors={"GET_SINGLE": [get_single_postprocessor]})


class AnnotationSetAPI(AnnotationSet, ValidationMixin):
    """
    Annotation Set resource: defines how the [media](#media) objects contained in a [media_collection](#media_collection) should be annotated /
    analysed. Each [media_collection](#media_collection) can have multiple [annotation_sets](#annotation_set) attached to it.
    """

    # __tablename__ = AnnotationSet.__tablename__

    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    # api_exclude_columns = ['annotations', 'points', 'media_collection.data','user.api_token','user.password']
    api_include_columns = ['id', 'name', 'parent_id', 'description', 'user', 'user.id', "user.username",
                           'user.first_name', 'user.last_name', 'created_at', 'children',
                           'media_collection', 'media_collection.id', 'media_collection.name', 'label_scheme',
                           'label_scheme.id', 'label_scheme.name', 'data',
                           'current_user_can_view', 'current_user_is_owner', 'current_user_is_member',
                           'current_user_can_edit', 'is_public', 'is_exemplar', 'is_full_bio_score', 'is_real_science',
                           'is_qaqc', 'is_final']
    api_add_methods_single = ["media_count", "point_count", "unannotated_point_count"]
    api_add_columns_single = ["files", "files.id", "files.name", "files.file_url", "files.created_at"]
    api_max_results_per_page = 1000
    api_include_methods = ["annotation_count", "usergroup_count"]
    # api_postprocessors = {"GET_SINGLE": ["get_single_postprocessor"]}
    # api_validation_exceptions = [ValidationError]
    api_allow_functions = True

    #api_preprocessors = APIUserGroupMixin.get_preprocessors()

    api_custom_endpoints = APIUserGroupMixin.build_api_endpoints() + [
        #dict(endpoint='{apiprefix}/{tablename}/<int:id>/export', methods=["GET"], classmethod="export_query"),
        # APIModelEndpoint("export", endpoint='{tablename}/<int:id>/export', methods=["GET"]),
        # APIModelEndpoint("export_query", endpoint='{tablename}/<int:id>/export2', methods=["GET"]),
        APIModelEndpoint("export_annotations", endpoint='{tablename}/<int:id>/export', methods=["GET"]),
        APIModelEndpoint("get_media", endpoint='{tablename}/<int:id>/media', methods=["GET"]),
        APIModelEndpoint("get_annotations", endpoint='{tablename}/<int:id>/annotations', methods=["GET"]),
        #APIModelEndpoint("get_point_counts", endpoint='{tablename}/<int:id>/media/annotation_counts', methods=["GET"]),
        APIModelEndpoint("annotation_tally", endpoint='{tablename}/<int:id>/annotations/<group_by>', methods=["GET"]),
        APIModelEndpoint("post_media_single", endpoint='{tablename}/<int:annotation_set_id>/media/<int:media_id>',methods=["POST"]),
        APIModelEndpoint("delete_media_single", endpoint='{tablename}/<int:annotation_set_id>/media/<int:media_id>',methods=["DELETE"]),
        APIModelEndpoint("clone", endpoint='{tablename}/<int:id>/clone',methods=["POST"])
    ]

    #api_allow_nonowner_delete = True  # avoid default owner test, since children can be deleted by parent owners

    # validations
    val.validates_constraints()

    # @classmethod
    # def get_single_postprocessor(cls, result=None, instance=None, **kw):
    #     # instance_id = result.get('id', None)
    #     # if instance_id is not None:
    #     #     instance = cls.get_by_id(instance_id)
    #     if instance is not None:
    #         result["media_count"] = instance.media_collection.media_count
    #         result["point_count"] = instance.point_count()
    #         result["unannotated_point_count"] = instance.unannotated_point_count()

    @classmethod
    def patch_auth_preprocessor(cls, instance_id=None, instance=None, data=None, **kw):
        # if this is an exemplar set and the current user is an editor for the label_scheme, allow editing of annotation set
        if instance is None:
            instance = cls.get_by_id(instance_id)
        if instance.is_exemplar is True and instance.label_scheme.current_user_can_edit:
             return
        super(cls, cls).patch_auth_preprocessor(instance_id=instance_id, instance=instance, **kw)

    @classmethod
    def delete_auth_preprocessor(cls, instance_id=None, instance=None, **kw):
        if instance is None:
            instance = cls.get_by_id(instance_id)
        if instance.is_child:
            if instance.parent.current_user_is_owner:
                return  # override auth processor to check if user is parent owner
        super(cls, cls).delete_auth_preprocessor(instance_id=instance_id, instance=instance, **kw)

    # @classmethod
    # # TODO: FIX QUERY METHOD.
    # # QUERY IS MUCH QUICKER THAN LAZY LOADING (BELOW) BUT THIS IS NOT WORKING PROPERLY YET - VERY FIDDLY
    # def export_query(cls, id):
    #     cls.get_single_auth_preprocessor(instance_id=id)
    #     return annotation_set_export_query(cls, id)

    # @classmethod
    # def export(cls, id):
    #     cls.get_single_auth_preprocessor(instance_id=id)
    #     return annotation_set_export(cls, id)

    # @classmethod
    # def export2(cls, id):
    #     """
    #     Returns list of [annotation](#annotation) objects contained in the `annotation_set` with an ID matching `id`.
    #     The results can be filtered and searched. See [Making API search queries](#api_query) for instructions on making search queries.
    #     In addition it accepts `include_columns` and `include_methods` query parameters which override returned
    #     fields of [annotation](#annotation) model.
    #     """
    #     # observation_timestamp = request.args.get('observation_timestamp', None)
    #     cls.get_single_auth_preprocessor(instance_id=id)  # check authentication
    #     # order_by = AnnotationLabel.timestamp_proximity(to_datetime(observation_timestamp)).asc() \
    #     #     if observation_timestamp else None
    #     results = AnnotationLabelAPI.get_search_results(
    #         filters=(AnnotationLabelAPI.annotation_set_id == id), allow_qs_includes=True)
    #     return render_data_response(results)

    @classmethod
    def post_media_single(cls, annotation_set_id=None, media_id=None):
        """
        Add the [media](#media) object with the ID `media_id` to the [media_collection](#media_collection) of the [annotation_set](#annotation_set) matching `annotation_set_id`
        """
        annotation_set = cls.get_by_id(annotation_set_id)
        return MediaCollectionAPI.post_media_single(media_collection_id=annotation_set.media_collection_id, media_id=media_id)

    @classmethod
    def delete_media_single(cls, annotation_set_id=None, media_id=None):
        """
        Remove the [media](#media) object with the ID `media_id` from the [media_collection](#media_collection) of the [annotation_set](#annotation_set) matching `annotation_set_id`
        """
        annotation_set = cls.get_by_id(annotation_set_id)
        return MediaCollectionAPI.delete_media_single(media_collection_id=annotation_set.media_collection_id, media_id=media_id)

    @classmethod
    def annotation_tally(cls, id, group_by):
        """
        **THIS ENDPOINT IS DEPRECATED! USE `/api/annotation/tally/<group_by>` in the [annotation](#annotation) resource**

        Returns lists of the labels used in the `annotation_set` with an ID matching `id`. The `group_by` parameter
        must one of [`label_updated`, `label_frequency`, `deployment`] which returns the most recently used labels,
        counts by label (most frequently used) or counts by deployment, respectively.
        The number of results and page can be controlled with the `results_per_page` and `page` query parameters,
        respectively.
        """
        # TODO: add tags to lists to keep unique label / tag combinations
        label = 'count'
        allowed_group_by = ["label_frequency", "label_updated", "deployment"]
        if group_by.startswith("label"):  # /label/.*/labeltally
            if group_by == "label_frequency":   # /label/.*/frequent
                column = func.count(Label.id)
            elif group_by == "label_updated":                     # /label/.*/updated_at
                column = func.max(AnnotationLabel.updated_at)
                label = "updated_at"
            else:
                raise ProcessingException("Unrecognised 'group_by' parameter: '{}', must be one of: {}".format(group_by, allowed_group_by))

            query = db.session.query(
                Label.name.label("name"), Label.color, Label.id.label("label_id"), column.label(label),
            #     func.string_agg(Tag.name.distinct(), ",").label('tags'),  # TODO: almost working - rather concat to list
            # ).outerjoin(
            #     AnnotationLabel, TagAnnotationAssociation, Tag
            ).filter(
                AnnotationLabel.label_id == Label.id, AnnotationLabel.annotation_set_id == id,
                AnnotationLabel.annotation_set_id == AnnotationSet.id
            ).group_by(
                Label.name, Label.color, Label.id
            ).order_by(column.desc())
        elif group_by == "deployment":   # /label/.*/deploymenttally
            query = db.session.query(
                Deployment.name.label("name"), Campaign.color, Deployment.id.label("id"),
                func.count(AnnotationLabel.label_id).label('count')
            ).filter(
                AnnotationLabel.point_id == AnnotationPoint.id, AnnotationPoint.media_id == Media.id,
                Media.deployment_id == Deployment.id, AnnotationLabel.annotation_set_id == id,
                Deployment.campaign_id == Campaign.id, AnnotationLabel.annotation_set_id == AnnotationSet.id,
            ).group_by(Deployment.name, Deployment.id, Campaign.color).order_by(text("count DESC"))
        else:
            raise ProcessingException("Unrecognised 'group_by' parameter: '{}', must be one of: {}".format(group_by, allowed_group_by))


        results = AnnotationLabelAPI.paginated_results([{'name': a[0], 'color': a[1], 'id': a[2], label: a[3]} for a in query], convert_to_dicts=False)
        results['metadata'] = cls.get_by_id(id).instance_to_dict()
        # results['max_value'] = max([i[label] for i in results.get('objects',[])])
        # results = AnnotationLabelAPI.paginated_results([{'name': a[0], 'color': a[1], 'id': a[2], label: a[3], 'tags': a[4]} for a in annotations], return_dicts=False)

        return render_data_response(results)

    @classmethod
    def get_media(cls, id):
        annotation_set = cls.get_by_id(id)
        # media = MediaAPI.get_search_results(filters=(MediaAPI.media_collections.any(MediaCollection.id == annotation_set.media_collection_id)), allow_qs_includes=False)
        q = json.loads(request.args.get("q", "{}") or "{}")
        query = search(db.session, MediaAPI, q).filter(MediaAPI.media_collections.any(MediaCollection.id == annotation_set.media_collection_id))
        results = MediaAPI.paginated_results(query)
        if "counts" in request.args.getlist("include"):
            # fast but a bit obfuscated
            # TODO: this runs query on full set of media items in annotation set and then matches them. It should be
            #       improved to rather filter for set of paginated media items only. Or even better be returned from a
            #       single query without linking in a loop.
            points_annotation_set_id = annotation_set.id if annotation_set.parent_id is None else annotation_set.parent_id
            media_ids = [i['id'] for i in results.get("objects", [])]
            count_query = query.with_entities(
                Media.id, func.count(Media.annotations), func.count(Label.id)
            ).outerjoin(
                AnnotationPoint, AnnotationLabel, Label
            ).filter(
                AnnotationPoint.annotation_set_id == points_annotation_set_id,
                AnnotationLabel.point_id == AnnotationPoint.id,
                Media.id.in_(media_ids)
            ).group_by(Media.id)
            # print(count_query.count())

            counts = {m[0]: [m[1], m[2]] for m in count_query.all()}
            # counts = annotation_set.get_point_counts(media)
            for m in results["objects"]:
                c = counts.get(m['id'], [0, 0])
                m["point_count"] = c[0]
                m["annotation_count"] = c[1]

            # # simple, but slow
            # for m in results["objects"]:
            #     annotations = AnnotationPoint.query.filter(AnnotationPoint.media_id==m['id'], AnnotationPoint.annotation_set_id==id)
            #     m["annotation_count"] = count(db.session, annotations)
            #     m["annotation_label_count"] = count(db.session, annotations.join(AnnotationLabel).filter(AnnotationLabel.label_id.isnot(None)))
        return render_data_response(results)


    @classmethod
    def get_annotations(cls, id):
        """
        Returns list of [annotation](#annotation) objects contained in the `annotation_set` with an ID matching `id`.
        The results can be filtered and searched. See [Making API search queries](#api_query) for instructions on making search queries.
        In addition it accepts `include_columns` and `include_methods` query parameters which override returned
        fields of [annotation](#annotation) model.
        """
        #observation_timestamp = request.args.get('observation_timestamp', None)
        cls.get_single_auth_preprocessor(instance_id=id)   # check authentication
        # order_by = AnnotationLabel.timestamp_proximity(to_datetime(observation_timestamp)).asc() \
        #     if observation_timestamp else None
        results = AnnotationLabelAPI.get_search_results(
            filters=(AnnotationLabelAPI.annotation_set_id == id), allow_qs_includes=True)
        return render_data_response(results)

    # @classmethod
    # def export_xvalidation_annotations(cls, parent_annotation_set_id, child_annotation_set_id):
    #     cls.get_single_auth_preprocessor(instance_id=parent_annotation_set_id)  # check authentication
    #     instance = cls.get_by_id(parent_annotation_set_id)
    #     filename_pattern = "annotations-u{metadata[user][id]}-{metadata[media_collection][name]}-{metadata[name]}-{metadata[id]}-{hash}"
    #
    #     allowed_columns = [
    #         "id", "label.id", "label.name", "label.lineage_names", "comment", "tag_names", "label.uuid",
    #         "updated_at", "user.username", "point.id", "point.x", "point.y",
    #         "point.t", "point.data", "point.media.id", "point.media.key",
    #         "point.media.path_best", "point.media.timestamp_start",
    #         "point.media.path_best_thm", "point.pose.timestamp",
    #         "point.pose.lat", "point.pose.lon", "point.pose.alt",
    #         "point.pose.dep", "point.pose.data", "point.pose.id",
    #         "point.media.deployment.key", "point.media.deployment.campaign.key",
    #         "point.media.deployment.id", "point.media.deployment.campaign.id", "object_id"]
    #     return export_transformed_query(instance.annotations, allowed_columns=allowed_columns,
    #                                     filename_pattern=filename_pattern, metadata=instance.instance_to_dict())
    #
    #     q = json.loads(request.args.get("q", "{}") or "{}")
    #     query = instance.annotations
    #     operations = [dict(module="data", method="query_to_dicts", kwargs=dict(include_columns=include_columns))]
    #     collection_class = query._entity_zero().class_  # get class of model for query (assumes model query)
    #     query = search(db.session, collection_class, q, query=query)
    #
    #     # create data transformer
    #     dt = get_data_transformer(query, operations=operations)
    #
    #     # render result of data transformer
    #     result = render_data_transformer(dt, filename_pattern=filename_pattern, metadata=metadata)
    #     return result

    @classmethod
    # @docstring_parameter(export_transformed_query.__doc__)
    @docstring_parameter(export_annotations.__doc__)
    def export_annotations(cls, id):
        """
        Export [annotations](#annotation) from [annotation_set](#annotation_set) matching `id`.
        %s

        **EXAMPLES**

        ```
        # Example1: get all columns as a downloaded CSV file, only point annotations - filter out whole frame labels
        /api/annotation_set/2194/export?template=dataframe.csv&f={"operations":[{"module":"pandas", "method":"json_normalize"}]}&q={"filters":[{"name":"point", "op":"has", "val":{"name":"has_xy", "op":"eq","val":true}}]}

        # Example2: aggregate label counts per frame (percent cover, no point information) as a downloaded csv
        /api/annotation_set/2171/export?template=dataframe.csv&include_columns=["label.name", "label.lineage_names", "point.media.key", "point.pose.timestamp", "point.pose.lat", "point.pose.lon", "point.pose.alt", "point.pose.dep"]&f={"operations":[{"module":"pandas", "method":"json_normalize"},{"method":"fillna", "kwargs":{"value":"-"}},{"module":"pandas", "method":"count_unstack", "kwargs":{"columns":["label.id", "label.uuid", "label.name", "label.lineage_names", "comment", "tag_names"]}}]}

        # Example3: show selected columns as an HTML table in the browser, filter out unlabeled annotations
        /api/annotation_set/2171/export?template=dataframe.html&disposition=inline&include_columns=["label.name","label.lineage_names", "updated_at", "point.x", "point.y", "point.media.key", "point.pose.timestamp", "point.pose.lat", "point.pose.lon", "point.pose.alt", "point.pose.dep", "point.media.deployment.key", "point.media.deployment.campaign.key"]&f={"operations":[{"module":"pandas", "method":"json_normalize"}]}&q={"filters":[{"name":"label_id", "op":"is_not_null"}]}

        # Example4: show HTML table in browser with tally of label counts (can be refreshed for readable live summary)
        /api/annotation_set/2171/export?template=dataframe.html&disposition=inline&include_columns=["label.uuid","label.name","label.lineage_names","label.id"]&f={"operations":[{"module":"pandas","method":"json_normalize"},{"module":"pandas","method":"groupby_count"}]}&q={"filters":[{"name":"label_id","op":"is_not_null"}]}
        ```

        NB: the UI contains an "ADVANCED" option which can help build more complex export queries.
        """

        instance = cls.get_by_id(id)
        cls.get_single_auth_preprocessor(instance=instance)  # check authentication
        filename_pattern = "annotations-u{metadata[user][id]}-{metadata[media_collection][name]}-{metadata[name]}-{metadata[id]}-{hash}"
        return export_annotations(
            instance.annotations, metadata=instance.instance_to_dict(), filename_pattern=filename_pattern, limit=None)

    @classmethod
    def clone(cls, id):
        """
        Clone the [annotation_set](#annotation_set) matching `id` to create a new one under the same collection.
        This will clone all annotation_set properties along with all [points](#point), [annotations](#annotation),
        [tags](#tag), [labels](#labels) including all annotation properties (`needs_review`, `likelihood`, `comment`).
        **Note:** this does not currently preserve linking between annotations in observation groups.

        This endpoint also supports the translation of labels from a source to a target label_scheme using the semantic
        translation framework. In order to do this, you need to specify the additional `translate` url query parameter:

        ```
        &translate={"target_label_scheme_id":..., "vocab_registry_keys": ..., "mapping_override": ...}
        ```
        Where `target_label_scheme_id` [required] is an INT with the `id` of the target label scheme,
        `vocab_registry_keys` [optional] is a list containing the priority order of keys for `vocab_registries` for
        which to perform the semantic translation and `mapping_override` defines a set of key-value pairs containing
        `source label.id : target label.id` for which to override the translation.
        Note: for extended schemes, labels are translated through `tree_traversal` and no semantic translation is
        required.
        """
        # excludes = request.args.getlist("exclude")
        inst = cls.get_by_id(id)
        cls.get_single_auth_preprocessor(instance=inst)  # check auth
        MediaCollectionAPI.get_single_auth_preprocessor(instance=inst.media_collection)
        translate = json.loads(request.args.get("translate") or "{}")
        label_scheme_id = translate.get("target_label_scheme_id") or inst.label_scheme_id
        name = request.form.get("name", f"{inst.name}-copy{'-translated' if translate else ''}")
        description = f"This is a copy of the annotation_set: '{inst.name}' [ID:{inst.id}]."
        with db.session.no_autoflush:
            annotation_set_copy = AnnotationSet.create(
                commit=False, name=name, description=description, data_json=inst.data_json, is_qaqc=inst.is_qaqc,
                media_collection_id=inst.media_collection_id, is_full_bio_score=inst.is_full_bio_score,
                label_scheme_id=int(label_scheme_id), user_id=current_user.id, is_real_science=inst.is_real_science)
            points, annotations = [], []
            for p in inst.points:
                annotation_labels = []
                for a in p.annotations.filter(AnnotationLabel.annotation_set_id == inst.id):
                    label_id = a.translated_label_id if bool(translate) else a.label_id
                    annotation_labels.append(
                        AnnotationLabel(
                            label_id=label_id, comment=a.comment, likelihood=a.likelihood, tag_names=a.tag_names,
                            needs_review=a.needs_review, annotation_set=annotation_set_copy))
                annotations += annotation_labels
                points.append(AnnotationPoint(annotation_set=annotation_set_copy, x=p.x, y=p.y, t=p.t,
                                              media_id=p.media_id, data_json=p.data_json, annotations=annotation_labels))
            # annotation_set_copy.update(points=points, annotations=annotations, commit=True)
            db.session.commit()
        return render_data_response(cls.model_to_dict(annotation_set_copy, is_single=False))

    @classmethod
    # override method from Mixin to add media collection as well (if not already added)
    def add_to_group_api(cls, instance_id, group_id, cascade=True, **kw):
        """
        Add object with id `instance_id` to group with id `group_id`
        """
        if cascade:
            instance = cls.get_by_id(instance_id)
            try:
                # media_collection = MediaCollectionAPI.get_by_id(instance.media_collection_id)
                MediaCollectionAPI.add_to_group_api(instance.media_collection_id, group_id, cascade=False)
            except ProcessingException as e:
                current_app.logger.debug("Media collection was not added to group: {}: {}".format(e, e.description))
            except Exception as e:
                current_app.logger.debug("Media collection was not added to group: {}".format(e))
        return super(cls, cls).add_to_group_api(instance_id, group_id, **kw)



    # @classmethod
    # def get_single_auth_preprocessor(cls, instance_id=None, **kw):
    #     MediaCollectionAPI.get_single_auth_preprocessor(instance_id=cls.get_by_id(instance_id).media_collection_id)

    # @classmethod
    # def patch_auth_preprocessor(cls, instance_id=None, data=None, **kw):
    #     MediaCollectionAPI.patch_auth_preprocessor(instance_id=cls.get_by_id(instance_id).media_collection_id)

    # @classmethod
    # def post_auth_preprocessor(cls, data=None, **kw):
    #     if not cls.get_by_id(instance_id).current_user_can_edit:
    #         raise ProcessingException(description="You do not have access to edit this record", code=401)

    # @classmethod
    # def check_auth_delete(cls, instance_id=None, **kw):
    #     MediaCollectionAPI.delete_auth_preprocessor(instance_id=cls.get_by_id(instance_id).media_collection_id)

#GntTKZLjgWvVUPfGLhxp
class AnnotationSetFileAPI(AnnotationSetFile, ValidationMixin):
    """
    Annotation Set File resource: a file associated with a [annotation_set](#annotation_set) object. Can be used for
    batch importing / updating of [annotation](#annotation) and [point](#point) objects, as well as batch adding
    [media](#media) objects to a [media_collection](#media_collection).
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    api_include_columns = ['user_id', 'description', 'iscompressed', 'annotation_set_id', 'file_url', 'id', 'name', 'last_save']

    api_custom_endpoints = [
        APIModelEndpoint("get_data", endpoint="{tablename}/<int:fid>/data", methods=["GET"]),
        APIModelEndpoint("set_data", endpoint="{tablename}/data", methods=["POST"])
    ]

    @classmethod
    @docstring_parameter(AnnotationSetFile.get_data.__doc__)
    def get_data(cls, *args, **kwargs):
        """
        Annotation Set File resource. %s

        **Example1:** TODO
        ```
        /api/annotation_set_file/2/data?save={"collection": "annotations"}&f={"operations": [{"module": "pandas", "method": "read_csv", "kwargs": {"parse_dates": ["time"], "skiprows": 2, "usecols": [2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]}}, {"method": "rename", "kwargs": {"columns": {"image_filename": "key", "longitude": "pose.lon", "latitude": "pose.lat", "depth_sensor": "pose.data.dep", "altitude_sensor": "pose.alt", "depth": "pose.dep", "sea_water_temperature": "pose.data.temp", "sea_water_salinity": "pose.data.salinity", "chlorophyll_concentration_in_sea_water": "pose.data.chlorophyll", "backscattering_ratio": "pose.data.backscater", "colored_dissolved_organic_matter": "pose.data.colored_dissolved_organic_matter", "time": "timestamp_start", "cluster_tag": "pose.data.cluster_tag"}}}, {"method": "to_dict", "kwargs": {"orient": "records"}}, {"module": "data", "method": "unflatten_dicts", "kwargs": {}}]}
        ```
        """
        return super().get_data(*args, **kwargs)


class AnnotationLabelAPI(AnnotationLabel, ValidationMixin):
    """
    Annotation resource. Links [label](#label) objects to [point](#point) objects.
    Also stores comments, [tag](#tag) associations, review flags, probability estimates, linked observations, etc...
    """
    api_restless_methods = ['GET', 'POST', 'DELETE']
    # api_exclude_columns = ['annotation_set', 'annotation_tags', 'data_json', 'media', 'user']
    api_include_columns = ["id", "created_at", "updated_at", "annotation_set_id", "data", "label", "label.id",
                           "label.name", "label.uuid", "label.parent_id", "label.label_scheme_id",
                           "point", "point.media_id", "point.x", "point.y", "user", "user.id", "user.username",
                           "point.t", "point.data", "point.annotation_set_id", "likelihood", "needs_review",
                           "point.id", "tags", "tags.id", "tags.name", "comment", "timestamp", "parent_id"]  #, "object_id"]
    api_add_columns_single = ["label_id", "point.updated_at"]
    api_include_methods = ["color", "suggested_tags", "object_id", "label.lineage_names"]  #, "observation_count"
    api_add_methods_single = ["observation_count","linked_observations","media","version_count"]

    api_custom_endpoints = [
        APIModelEndpoint("patch_many", endpoint='{tablename}', methods=['PATCH']),
        APIModelEndpoint("patch_one", endpoint='{tablename}/<int:id>', methods=['PATCH']),
        APIModelEndpoint("api_remove_tag", endpoint='{tablename}/<int:annotation_id>/tag/<int:tag_id>', methods=['DELETE']),
        APIModelEndpoint("api_link_parent", endpoint='{tablename}/<int:id>/link/<int:link_id>', methods=['PATCH']),
        APIModelEndpoint("get_versions", endpoint='{tablename}/<int:id>/versions', methods=['GET']),
        APIModelEndpoint("api_unlink", endpoint='{tablename}/<int:id>/link', methods=['DELETE']),
        APIModelEndpoint("api_set_as_parent", endpoint='{tablename}/<int:id>/link', methods=['PATCH']),
        APIModelEndpoint("export_annotations", endpoint='{tablename}/export', methods=["GET"]),
        APIModelEndpoint("tally_annotations", endpoint='{tablename}/tally/<group_by>')
    ]

    # validations
    val.validates_constraints()


    @property
    def linked_observations(self):
        """List of [annotation](#annotation) objects (as dicts) linked with the same `object_id`"""
        return [a.instance_to_dict(is_single=False) for a in AnnotationLabelAPI.query.filter(
                AnnotationLabel.annotation_set_id==self.annotation_set_id, AnnotationLabel.id != self.id,
                db.or_(AnnotationLabel.parent_id==self.object_id, AnnotationLabel.id == self.object_id))]

    @property
    def media(self):
        """[media](#media) object information for this [annotation](#annotation) via `point.media` link"""
        return MediaAPI.model_to_dict(self.point.media, is_single=True)

    @classmethod
    def get_many_auth_preprocessor(cls, search_params, **kw):
        """
        Filter results based on permissions from parent [annotation_set](#annotation_set)
        """
        cls._append_filter(search_params, dict(name="annotation_set", op="has", val=dict(name="current_user_can_view", op="eq", val=True)))

    # def _assert_edit_auth(self):
    #     AnnotationSetAPI.patch_auth_preprocessor(instance=self.annotation_set)

    @classmethod
    def post_auth_preprocessor(cls, data=None, **kw):
        """Enforce implied permissions from parent [annotation_set](#annotation_set)"""
        annotation_set_id = data["annotation_set_id"]
        AnnotationSetAPI.patch_auth_preprocessor(instance_id=annotation_set_id)

    @classmethod
    def delete_auth_preprocessor(cls, instance_id=None, **kw):
        """Enforce implied permissions from parent [annotation_set](#annotation_set)"""
        instance = cls.get_by_id(instance_id)
        AnnotationSetAPI.patch_auth_preprocessor(instance_id=instance.annotation_set_id, instance=instance.annotation_set)

    @classmethod
    def get_single_auth_preprocessor(cls, instance_id=None, **kw):
        """Enforce implied permissions from parent [annotation_set](#annotation_set)"""
        instance = cls.get_by_id(instance_id)
        if instance is not None:
            AnnotationSetAPI.get_single_auth_preprocessor(instance_id=instance.annotation_set_id, instance=instance.annotation_set)

    def current_user_can_edit(self):
        """BOOLEAN, whether or not the current user has edit rights for this annotation as implied by the permissions of the parent [annotation_set](#annotation_set)"""
        return AnnotationSetAPI.get_by_id(self.annotation_set_id).current_user_can_edit

    # @classmethod
    # def edit_auth_many_preprocessor(cls, search_params=None, **kw):
    #     # TODO: find all matches and check permissions
    #     pass

    @classmethod
    def get_versions(cls, id):
        """View all previous versions of this annotation, i.e.: a change log."""
        instance = cls.get_by_id(id)
        AnnotationSetAPI.patch_auth_preprocessor(instance=instance.annotation_set)
        versions = [dict(label=dict(id=i.label.id, lineage_names=i.label.lineage_names()) if i.label else dict(id=None, lineage_names=None),
                                    likelihood=i.likelihood, parent_id=i.parent_id,
                                    updated_by=i.user.full_name() if i.user else None,
                         comment=i.comment, updated_at=i.updated_at, needs_review=i.needs_review) for i in instance.versions]
        return render_data_response(dict(objects=versions))

    @classmethod
    def api_unlink(cls, id):
        """Unlink this annotation from the observation group. If it is a group parent, the first child will be set as the
        new parent.
        An observation group is used to link observations of the same object that has been annotated multiple times."""
        instance = cls.get_by_id(id)
        AnnotationSetAPI.patch_auth_preprocessor(instance=instance.annotation_set)
        instance.unlink_observation(commit=True)
        return jsonify(message="Label has been removed from observation group", **instance.instance_to_dict())

    @classmethod
    def api_set_as_parent(cls, id):
        """Set this annotation as the parent of the observation group.
        An observation group is used to link observations of the same object that has been annotated multiple times."""
        instance = cls.get_by_id(id)
        AnnotationSetAPI.patch_auth_preprocessor(instance=instance.annotation_set)
        instance.set_as_parent_observation(commit=True)
        return jsonify(message="Primary annotation of observation group has been updated.", **instance.instance_to_dict())

    @classmethod
    def api_link_parent(cls, id, link_id):
        """Updates `parent_id` of annotation to group observations of the same object. If `link_id` points to a child annotation, the observation
        will be linked to its `parent`.
        An observation group is used to link observations of the same object that has been annotated multiple times."""
        instance = cls.get_by_id(id)
        AnnotationSetAPI.patch_auth_preprocessor(instance=instance.annotation_set)
        instance.link_parent_observation(link_id, commit=True)
        return jsonify(message="Annotation has been linked to observation group", **instance.instance_to_dict())

    @classmethod
    def api_remove_tag(cls, annotation_id, tag_id):
        instance = cls.get_by_id(annotation_id)
        AnnotationSetAPI.patch_auth_preprocessor(instance=instance.annotation_set)
        # instance._assert_edit_auth()
        instance.remove_tag(tag_id)
        return jsonify(instance.instance_to_dict())

    @classmethod
    def patch_one(cls, id):
        # cls.assert_user_login()
        params = request.get_json()
        instance = cls.get_by_id(id)
        # instance._assert_edit_auth()
        AnnotationSetAPI.patch_auth_preprocessor(instance=instance.annotation_set)
        instance.set_annotation(commit=True, user_id=current_user.id, **params)
        # TODO return jsonify(cls.api_to_dict())   # test that this doesn't break things before changing
        return jsonify(dict(id=instance.id, label_id=instance.label_id, color=instance.color(), needs_review=instance.needs_review))

    @classmethod
    def patch_many(cls):
        """
        Batch update [annotation](#annotation) objects. Set `q` in querystring to filter selected [annotations](#annotation) as
        per [Making API search queries](#api_query). Returned results are pre-filtered for only the annotations that come from
        [annotation_sets](#annotation_set) where `current_user_can_edit` is set to `true`. As a safety measure, there
        is a limit of `200` annotations that can be updated in a single request.

        **CAUTION**: this endpoint can batch update many annotations in a single request! If used incorrectly, you can
        loose your annotations and any annotations that have been shared with you for which you have been granted edit
        access. Please use with caution.
        **TIP**: Test your search query (`q`) on a `GET` request first to see what is returned before updating.

        PATCH request data must be JSON with the following fields:
        ```{ "label_id": ..., "tags": [...], "comment": "..." }```
        Where `label_id` is the `id` of the [label](#label) object that you would like to assign to the list of matched
        [annotations](#annotation). Setting `label_id` to `false` will reset the label and any assigned tags.
        `tags` is a list of `[tag.id](#tag)'s` and `comment` is a string that can be assigned to each annotation.

        ```
        # Example: batch change ALL annotations matching filter. Set label_id=7326 ("Ecklonia radiata") for all annotations in an annotation_set annotation_set_id=2171 annotated by user user_id=1 with a label matching label_id=7331 ("Sargasm spp")

        # PATCH: (TIP first run as GET request to check filter before PATCHING)
        /api/annotation?q={"filters": [{"name": "annotation_set_id", "op": "eq", "val": 2171}, {"name": "label_id", "op": "eq", "val": 7331}, {"name": "user_id", "op": "eq", "val": 1}]}

        # JSON PATCH DATA:
        { "label_id": 7326 }
        ```
        """
        # cls.assert_user_login()  # check user is logged in and log in with token if not
        # tic = time.time()
        params = request.get_json()
        q = json.loads(request.args['q'])
        annotation_count_max = 200

        # ensure that only editable annotations are returned
        query = AnnotationLabel.query.filter(AnnotationLabel.annotation_set.has(AnnotationSet.current_user_can_edit==True))
        query = search(db.session, AnnotationLabel, q, query=query)
        annotation_count = query.count()
        if annotation_count <= 0:
            raise ProcessingException("No editable annotations found. If you have edit access, try select/create the "
                                      "annotations you wish to label (if using the GUI), or formulate a proper search "
                                      "query containing editable annotations (if using the API).")
        if annotation_count > annotation_count_max:
            raise ProcessingException("Too many annotations to update. Your request matched {}, but the limit is {}. "
                                      "If you meant to do this, please update in multiple requests (eg: by setting "
                                      "`offset` and `limit` parameters).".format(annotation_count, annotation_count_max))
        # current_app.logger.debug("Ran query in {}s".format(time.time()-tic))
        # tic = time.time()

        results = []
        for al in query:
            # al._assert_edit_auth()
            al.set_annotation(commit=False, user_id=current_user.id, **params)
            results.append(dict(id=al.id, label_id=al.label_id, color=al.color(), needs_review=al.needs_review))
        # current_app.logger.debug("Set values in {}s".format(time.time() - tic))
        # tic = time.time()
        db.session.commit()
        # current_app.logger.debug("Committed results {}s".format(time.time() - tic))
        return jsonify(dict(params=params, q=q, annotations=results))

    @classmethod
    @docstring_parameter(export_annotations.__doc__)
    def export_annotations(cls):
        """
        General [annotation](#annotation) export endpoint. This can export annotations across multiple
        [annotation_sets](#annotation_set). However, this endpoint is limited to a maximum of 200,000 annotations
        in any single query. You can export more in separate queries using a combination of `limit` and `offset`
        parameters. To export a single `annotation_set` without limits, see the `export` endpoint in the
        [annotation_set](annotation_set) resource.

        %s

        **EXAMPLES**

        ```
        # TODO
        ```
        """
        query = cls.query.filter(cls.annotation_set.has(AnnotationSet.current_user_can_view.is_(True)))
        return export_annotations(query, limit=200000)

    @classmethod
    def tally_annotations(cls, group_by):
        """
        Get a tally of [annotations](#annotation) grouped by different properties. `group_by` can be one of
        [`month`,'year_collected', 'month_collected', `user`, `platform`, `campaign`, `deployment`, `label`,
        `affiliation`^, `usergroup`^]. Set `q` in querystring to filter
        selected [annotations](#annotation) as per [Making API search queries](#api_query). Note, the same query in `q`
        can be applied to all `group_by` types to filter the list of [annotations](#annotation) that are included in the
        tally. Results are also paginated with a default of `500` and a maximum on `5000` returned per page. This can be
        controlled by setting the `results_per_page` parameter in the query string.
        Returned results are pre-filtered to return only the annotations that have been labeled (`label_id != null`).
        Also accepts a query string parameter `template` which formats the data using a predefined template. Some example
        templates include: `data.html` which returns an HTML table, `json.html` which returns HTML-formatted JSON, `data.csv`
        which returns a CSV file for download. If left blank, raw JSON will be returned.

        ^ **NOTE**: the counts in the `affiliation` and `usergroup` tally should be interpreted with care.
        If someone belongs to more than one group, then their annotations will contribute to both so they may be double
        counted. The corollary of that is if someone has not linked themselves to an affiliation or group, then their
        annotations will not be counted in the tally.

        **TRANSLATING TO OTHER LABEL_SCHEMES**

        When using `group_by=label` it is possible translate labels from a source to a target label_scheme using the
        semantic translation framework. In order to do this, you need to specify the additional `translate` url query
        parameter:

        ```
        &translate={"target_label_scheme_id":..., "vocab_registry_keys": ..., "mapping_override": ...}
        ```
        Where `target_label_scheme_id` [required] is an INT with the `id` of the target label scheme,
        `vocab_registry_keys` [optional] is a list containing the priority order of keys for `vocab_registries` for
        which to perform the semantic translation and `mapping_override` defines a set of key-value pairs containing
        `source label.id : target label.id` for which to override the translation.
        Note: for extended schemes, labels are translated through `tree_traversal` and no semantic translation is
        required. When passing translation parameters, there will the following additional output columns:
        `label_lineage_names`, `translated_id`, `translated_lineage_names`, `translated_name`,
        `translated_label_scheme_name`, `translated_info`.

        ```
        # Example: tally of annotations for top 500 most commonly used labels returned as an HTML table
        /api/annotation/tally/label?template=data.html&results_per_page=500

        # Example: tally of annotations for the top 1000 users returned as a CSV file
        /api/annotation/tally/user?&results_per_page=1000&template=data.csv

        # Example: tally of annotations for the top 20 affiliations returned as an HTML table
        /api/annotation/tally/affiliation?&results_per_page=20&template=data.html

        # Example: tally of annotations by the top 200 usergroups returned as an HTML table
        /api/annotation/tally/usergroup?&template=data.html&results_per_page=200

        # Example: tally of annotations by month between 2020-01-01 and 2021-01-01 returned as an HTML table
        /api/annotation/tally/month?q={"filters": [{"name":"updated_at","op":"gt","val":"2020-01-01"}, {"name":"updated_at","op":"lt","val":"2021-01-01"}] }&template=data.html

        # Example: tally of annotations by platform returned as raw JSON
        /api/annotation/tally/platform

        # Example: tally of annotations by campaign returned as a HTML-formatted JSON
        /api/annotation/tally/campaign?&template=json.html

        # Example: tally of annotations for top 2000 deployments returned as an HTML table
        /api/annotation/tally/deployment?&template=data.html&results_per_page=2000

        # Example: tally annotations by deployment filtered by annotation_set.usergroup with ID=63 returned as an HTML table
        /api/annotation/tally/deployment?&results_per_page=2000&template=data.html&q={"filters": [{"name":"annotation_set","op":"has","val": {"name":"usergroups","op":"any","val": {"name":"id","op":"eq","val":63}}}]}

        # Example: tally of annotations for the top 20 users for platform(s) containing "IMOS" in their name that were labeled during the year 2020. Return the result as an HTML table using the template "data.html", GET:
        /api/annotation/tally/user?q={"filters": [{"name":"point","op":"has","val": {"name":"media","op":"has","val": {"name":"deployment","op":"has","val": {"name":"platform","op":"has","val": {"name":"name","op":"ilike","val":"%IMOS%"}}}}}, {"name":"created_at","op":"gt","val":"2020-01-01"}, {"name":"created_at","op":"lt","val":"2021-01-01"}] }&results_per_page=20&template=data.html

        # Example: tally of annotations by top 100 labels for a give deployment (id=10306) returned as an HTML table
        # Translated into the catami annotation scheme (id=2) using the "caab" and "worms" vocab_registries (only works with compatible schemas)
        /api/annotation/tally/label?q={"filters":[{"name":"point","op":"has","val":{"name":"media","op":"has","val":{"name":"deployment_id","op":"eq","val":10306}}}]}&results_per_page=100&unannotated_point_count=294&translate={"vocab_registry_keys":["caab","worms"],"target_label_scheme_id":"2"}&template=data.html

        # Example: tally annotations by label in a deployment matching the key "r20200131_021337_NG02_middleton_22", filtered for occurrences between 20-30m, showing only primary observation (no linked children, i.e. only count an observation group/object once)
        /api/annotation/tally/label?q={"filters":[{"name":"point","op":"has","val":{"name":"media","op":"has","val":{"name":"deployment","op":"has","val":{"name":"key","op":"eq","val":"r20200131_021337_NG02_middleton_22"}}}},{"name":"point","op":"has","val": {"name":"media","op":"has","val": {"name":"poses","op":"any","val": {"name":"dep","op":"gt","val":20}}}},{"name":"point","op":"has","val": {"name":"media","op":"has","val": {"name":"poses","op":"any","val": {"name":"dep","op":"lt","val":30}}}},{"name":"is_child","op":"eq","val":false}]}
        ```
        """
        # make sure that the user is logged in
        cls.assert_user_login()

        today = datetime.today()
        last_month = today.replace(day=1) - timedelta(days=1)
        _, eom = calendar.monthrange(last_month.year, last_month.month)
        q = json.loads(request.args.get("q") if request.args.get("q", "").strip() else "{}") #or dict(filters=[
        #     dict(name="created_at", op="gt", val=(today.replace(day=1) - relativedelta(months=12)).isoformat()),
        #     dict(name="created_at", op="lt", val=(datetime(last_month.year, last_month.month, eom)).isoformat()),
        # ])

        # default_order_by = [db.desc("count")]
        if group_by == "month":
            query = db.session.query(
                db.func.count(AnnotationLabel.id).label('count'),
                db.extract('year', AnnotationLabel.created_at).label('year'),
                db.extract('month', AnnotationLabel.created_at).label('month')
            ).group_by(
                db.extract('year', AnnotationLabel.created_at), db.extract('month', AnnotationLabel.created_at)
            ).order_by(db.asc("year")).order_by(db.asc("month"))
            # default_order_by = [db.asc("year"), db.asc("month")]
        elif group_by == "year_collected":
            query = db.session.query(
                db.func.count(AnnotationLabel.id).label('count'),
                db.extract('year', Media.timestamp_start).label('year')
            ).filter(
                AnnotationLabel.point_id == AnnotationPoint.id,
                AnnotationPoint.media_id == Media.id
            ).group_by(
                db.extract('year', Media.timestamp_start)
            ).order_by(db.asc("year"))
        elif group_by == "month_collected":
            query = db.session.query(
                db.func.count(AnnotationLabel.id).label('count'),
                db.extract('year', Media.timestamp_start).label('year'),
                db.extract('month', Media.timestamp_start).label('month')
            ).filter(
                AnnotationLabel.point_id == AnnotationPoint.id,
                AnnotationPoint.media_id == Media.id
            ).group_by(
                db.extract('year', Media.timestamp_start), db.extract('month', Media.timestamp_start)
            ).order_by(db.asc("year")).order_by(db.asc("month"))
            # default_order_by = [db.asc("year"), db.asc("month")]
        elif group_by == "user":
            query = db.session.query(
                db.func.count(AnnotationLabel.id).label('count'), User.first_name, User.last_name,
                User.id.label("id")
            ).filter(
                AnnotationLabel.user_id == User.id
            ).group_by(User.first_name, User.last_name, User.id).order_by(db.desc("count"))
        elif group_by == "usergroup":
            # count only annotations from annotation sets that have been added to the usergroup
            query = db.session.query(
                db.func.count(AnnotationLabel.id).label('count'), Group.name.label("name"),
                Group.groupinfo.label("info"), Group.id.label("id")
            ).join(AnnotationSet._group_association_table, AnnotationSet, AnnotationLabel).filter(
                Group.group_type_id==GroupType.id, GroupType.name=="usergroup", AnnotationLabel.user_id==User.id,
                AnnotationSet.id == AnnotationLabel.annotation_set_id
            ).group_by(Group.name, Group.groupinfo, Group.id).order_by(db.desc("count"))
        elif group_by == "affiliation":
            # count labels from any members of the affiliation group
            query = db.session.query(
                db.func.count(AnnotationLabel.id).label('count'), Group.name.label("name"),
                Group.groupinfo.label("info"), Group.id.label("id")
            ).filter(
                Group.group_type_id==GroupType.id, GroupType.name=="affiliation", AnnotationLabel.user_id==User.id,
                UserGroupAssociation.user_id==User.id, UserGroupAssociation.group_id == Group.id,
                AnnotationSet.id == AnnotationLabel.annotation_set_id
            ).group_by(Group.name, Group.groupinfo, Group.id).order_by(db.desc("count"))
        elif group_by == "platform":
            query = db.session.query(
                db.func.count(AnnotationLabel.id).label('count'), Platform.name.label("name"),
                Platform.key.label("key"), Platform.id.label("id")
            ).filter(
                AnnotationLabel.point_id == AnnotationPoint.id,
                AnnotationPoint.media_id == Media.id, Media.deployment_id == Deployment.id,
                Deployment.platform_id == Platform.id
            ).group_by(Platform.name, Platform.id, Platform.key).order_by(db.desc("count"))
        elif group_by == "deployment":
            query = db.session.query(
                db.func.count(AnnotationLabel.id).label('count'), Deployment.name.label("name"),
                Deployment.key.label("key"), Deployment.id.label("id"),
                Deployment.platform_id.label("platform_id"), Platform.name.label("platform_name"), Campaign.color.label("color"),
                Campaign.name.label("campaign_name"), Campaign.key.label("campaign_key"), Campaign.id.label("campaign_id")
            ).filter(
                AnnotationLabel.point_id == AnnotationPoint.id, Deployment.campaign_id==Campaign.id,
                AnnotationPoint.media_id == Media.id, Media.deployment_id == Deployment.id, Deployment.platform_id == Platform.id
            ).group_by(
                Deployment.name, Deployment.key, Deployment.id, Deployment.platform_id, Campaign.name, Campaign.key,
                Campaign.color, Campaign.id, Platform.name
            ).order_by(db.desc("count"))
        elif group_by == "campaign":
            query = db.session.query(
                db.func.count(AnnotationLabel.id).label('count'), Campaign.name.label("name"),
                Campaign.key.label("key"), Campaign.id.label("id"), Campaign.color
            ).filter(
                AnnotationLabel.point_id == AnnotationPoint.id,
                AnnotationPoint.media_id == Media.id, Media.deployment_id == Deployment.id,
                Deployment.campaign_id == Campaign.id
            ).group_by(Campaign.name, Campaign.key, Campaign.id, Campaign.color).order_by(db.desc("count"))
        elif group_by == "label":
            query = db.session.query(
                db.func.count(AnnotationLabel.id).label('count'), LabelScheme.name.label("label_scheme_name"), Label.color,
                LabelScheme.id.label("label_scheme_id"), Label.name.label("name"), Label.id.label("id")
            ).filter(
                AnnotationLabel.label_id == Label.id, Label.label_scheme_id==LabelScheme.id
            ).group_by(Label.name, Label.id, LabelScheme.name, LabelScheme.id, Label.color).order_by(db.desc("count"))
        else:
            raise ProcessingException("Unknown `group_by` argument: '{}'.".format(group_by))

        # apply query param search string
        query = search(db.session, cls, q, query=query.filter(AnnotationLabel.label_id.isnot(None)))

        # Get total count (pre pagination)

        # paginate results
        result = cls.paginated_results(query, convert_to_dicts=False, max_results_per_page=5000, default_results_per_page=100)

        # Check translation parameters & convert to dicts
        t = json.loads(request.args.get("translate", '{}') or '{}')
        if group_by == "label" and t.get('target_label_scheme_id', None):
            label_ids = [l.id for l in result.get('objects',[])]
            translation_labels = {l.id: {
                "label_lineage_names": l.lineage_names(),
                "translated_id": l.translated.id if l.translated else None,
                "translated_lineage_names": l.translated.lineage_names() if l.translated else None,
                "translated_name": l.translated.name if l.translated else None,
                "translated_label_scheme_name": l.translated.label_scheme.name if l.translated else None,
                "translated_info": l.translated.translation_info if l.translated else None,
            } for l in Label.query.filter(Label.id.in_(label_ids))}
            result['objects'] = [dict(**u._asdict(), **translation_labels.get(u.id, {})) for u in result.get('objects',[])]  # convert to dicts
        else:
            result['objects'] = [u._asdict() for u in result.get('objects', [])]  # convert to dicts

        counts = [i.get('count', 0) for i in result['objects']]
        page_stats = dict(mean=mean(counts), median=median(counts), min=min(counts), max=max(counts), sum=sum(counts)) if len(counts) > 0 else dict()
        result["metadata"] = dict(
            page_stats=page_stats, num_bins=len(result['objects']), group_by=group_by, q=q, today=today.isoformat()
        )
        return render_data_response(data=result)


class MediaTypeAPI(MediaType, ValidationMixin):
    """
    Media Type resource: a modifier denoting the type for a [media](#media) resource. This is associated with
    """
    api_restless_methods = ['GET', 'POST', 'DELETE']
    api_include_columns = ['id', 'name']

    api_preprocessors = {
        "POST": ['assert_current_user_is_admin'],
        "PATCH_SINGLE": ['assert_current_user_is_admin'],
        "PATCH_MANY": ['assert_current_user_is_admin'],
        "DELETE_SINGLE": ['assert_current_user_is_admin']
    }

    # validations
    val.validates_constraints()


class TagAnnotationAssociationAPI(TagAnnotationAssociation, ValidationMixin):
    """
    Tag Annotation Associate resource: links [tag](#tag) objects to [annotation](#annotation) objects.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE']

    # validations
    val.validates_constraints()


class PoseAPI(Pose, ValidationMixin):
    """
    Pose resource: associates nav / sensor data to a [media](#media) item.
    """
    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE']
    # api_exclude_columns = ['mediaposes', 'media']
    api_include_columns = ['id', 'dep', 'alt', 'lat', 'lon', 'events', 'timestamp', 'data',
                           'media', 'media.key', 'media.id', 'media.timestamp_start', 'media.path_best_thm']
    # api_include_methods = ['dataset']
    api_include_methods = ['deployment','campaign','platform','color','media.path_best_thm']
    api_max_results_per_page = 10000
    api_allow_functions = True

    # validations
    val.validates_constraints()

    api_custom_endpoints = [
        APIModelEndpoint("get_stats", endpoint='{tablename}/stats', methods=["GET"]),
        # APIModelEndpoint("get_poses_at_location", endpoint='{tablename}/location', methods=["GET"]),
    ]

    # @property
    # def dataset(self):
    #     return dict(
    #         deployment=dict(name=self.media.deployment.name, id=self.media.deployment_id),
    #         campaign=dict(name=self.media.deployment.campaign.name, id=self.media.deployment.campaign_id),
    #         platform=dict(name=self.media.deployment.platform.name, id=self.media.deployment.platform_id),
    #         media=dict(key=self.media.key, id=self.media.id, timestamp_start=self.media.timestamp_start, path_best_thm=self.media.path_best_thm),
    #         color=self.media.color
    #     )

    @property
    def deployment(self):
        return dict(name=self.media.deployment.name, id=self.media.deployment_id,key=self.media.deployment.key)

    @property
    def campaign(self):
        return dict(name=self.media.deployment.campaign.name, id=self.media.deployment.campaign_id, key=self.media.deployment.campaign.key)

    @property
    def platform(self):
        return dict(name=self.media.deployment.platform.name, id=self.media.deployment.platform_id, key=self.media.deployment.platform.key)

    @property
    def color(self):
        return self.media.color

    @classmethod
    def get_stats(cls):
        # cls.assert_user_login()
        q = json.loads(request.args.get("q") if request.args.get("q", "").strip() else "{}")
        extended = request.args.get("extended") == "true"
        query = cls.query.filter(cls.lat.isnot(None), cls.lon.isnot(None))
        query = search(db.session, cls, q, query=query)
        extrastats = [db.func.count(Pose.id).label("count"), db.func.min(Pose.timestamp).label('ts_min'),
                      db.func.max(Pose.timestamp).label('ts_max')] if extended else []
        query = query.with_entities(
            db.func.max(Pose.dep).label("dep_max"), db.func.min(Pose.dep).label("dep_min"), db.func.avg(Pose.dep).label("dep_avg"),
            db.func.max(Pose.alt).label("alt_max"), db.func.min(Pose.alt).label("alt_min"), db.func.avg(Pose.alt).label("alt_avg"),
            db.func.max(Pose.lat).label("lat_max"), db.func.min(Pose.lat).label("lat_min"),
            db.func.max(Pose.lon).label("lon_max"), db.func.min(Pose.lon).label("lon_min"), *extrastats
        )
        return render_data_response(query.first()._asdict())

    # @property
    # def media_path_best_thm(self):
    #     return self.media.path_best_thm


    # @classmethod
    # def get_poses_at_location(cls):
    #     lat = float(request.args.get("lat"))
    #     lon = float(request.args.get("lon"))
    #     results_per_page = min(int(request.args.get('results_per_page', 5)), 100)
    #     # geog = func.ST_GeographyFromText(f'POINT({lon} {lat})')
    #     geom = func.ST_GeomFromText(f'POINT({lon} {lat})', 4326)
    #     poses = cls.query.filter(
    #         # func.ST_DWithin(func.ST_GeogFromWKB(cls.geom), geog, 5, use_spheroid=False)
    #         func.ST_DWithin(cls.geom, geom, 0.00005, use_spheroid=False)
    #     ).order_by(
    #         # Comparator.distance_centroid(func.ST_GeogFromWKB(cls.geom), geog)
    #         Comparator.distance_centroid(cls.geom, geom)
    #     ).limit(results_per_page)
    #     # poses = cls.query.order_by(Comparator.distance_centroid(cls.geom, geom)).filter(Comparator.distance_centroid(cls.geom, geom) < 0.001).limit(results_per_page)
    #     # poses = cls.query.order_by(db.func.ST_DistanceSphere(cls.geom, geom)).filter(db.func.ST_DistanceSphere(cls.geom, geom) < 10).limit(results_per_page)
    #     # poses = cls.query.order_by(db.func.ST_DistanceSphere(cls.geom, geom)).filter(db.func.ST_DWithin(cls.geom, geom, 10)).limit(results_per_page)
    #
    #     if poses.count() <= 0:
    #         raise ProcessingException("No points found!")
    #     results = []
    #     for p in poses:
    #         o = cls.model_to_dict(
    #             p, include_methods=cls.api_include_methods+["media.path_best_thm", "media.color"],
    #             include_columns=cls.api_include_columns+['media.timestamp_start','media.deployment_id']
    #         )
    #         o['distance'] = p.distance(lat,lon)
    #         o['campaign'] = p.media.deployment.campaign.name
    #         o['deployment'] = p.media.deployment.name
    #         o['platform'] = p.media.deployment.platform.name
    #         results.append(o)
    #     return render_data_response(dict(objects=results))


class PoseDataAPI(PoseData, ValidationMixin):
    """
    Pose Data resource: enables linking of name-value data to [pose](#pose) objects. This can be used to attach additional
    sensor information to each [pose](#pose) object (and by proxy to each [media](#media) object)
    """
    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE']
    api_max_results_per_page = 10000

    # validations
    val.validates_constraints()


class MediaAPI(Media, ValidationMixin):
    """
    Media resource: a generic media object definition that can be used to link in media in a variety of formats,
    including images, videos, photo-mosaics and other media formats. Note that at the moment only images and video
    framegrabs have annotation support. Annotation viewers for video and mosaics coming soon.
    """

    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'DELETE', 'PATCH']
    # api_exclude_columns = ['media_collection_media', 'mediaposes', 'path', 'path_thm', 'media_collections',
    #                        'deployment', 'annotations', 'poses', 'poses.media', 'data_json', 'events']
    api_include_columns = ["created_at", "timestamp_start", "is_valid", "key", "deployment", "deployment.id","deployment.key",
                           "campaign","campaign.id","campaign.key",
                           "deployment_id", "data", "id", "media_type", "media_type.id", "media_type.name"]
    api_include_methods = ["path_best", "path_best_thm"]
    # api_add_columns_single = []
    api_add_methods_single = ["current_user_can_edit"]

    api_max_results_per_page = 20000
    api_results_per_page = 100
    api_postprocessors = {"GET_SINGLE": ['get_single_postprocessor']}
    #api_preprocessors = {"DELETE_SINGLE": ["assert_user_is_owner"]}
    # api_validation_exceptions = [ValidationError]

    api_custom_endpoints = [
        APIModelEndpoint("get_annotations", endpoint='{tablename}/<int:media_id>/annotations/<int:annotation_set_id>', methods=["GET"]),
        APIModelEndpoint("upload", endpoint='{tablename}/save', methods=["POST"]),
        APIModelEndpoint("custom_post", endpoint='{tablename}', methods=["POST"]),
        APIModelEndpoint("media_poses", endpoint='{tablename}/<int:id>/poses', methods=["GET"]),
        APIModelEndpoint("api_get_thumbnail", endpoint='{tablename}/<int:id>/thumbnail', methods=["GET"]),
        APIModelEndpoint("download", endpoint="{tablename}/<int:id>/download", methods=["GET"])
    ]

    # validations
    val.validates_constraints()

    api_cache_threshold = 20  # items


    @classmethod
    def patch_auth_preprocessor(cls, instance_id=None, *args, **kw):
        """Restrict request to object OWNER only"""
        assert_authenticated_user()
        instance = cls.get_by_id(instance_id)
        if not instance.current_user_can_edit():
            raise ProcessingException(description="Unauthorised. Only the deployment owner can do this.", code=401)

    @classmethod
    def delete_auth_preprocessor(cls, instance_id=None, *args, **kw):
        cls.patch_auth_preprocessor(instance_id=instance_id, *args, **kw)

    @classmethod
    def get_single_postprocessor(cls, result=None, instance=None, **kw):
        # instance_id = result.get('id', None)
        # if instance_id is not None:
        if instance is not None:
            # instance = cls.get_by_id(instance_id)
            d = instance.deployment
            # nposes = len(m.mediaposes)
            result["deployment"] = dict(name=d.name, key=d.key, id=d.id, timestamp=d.timestamp,
                                        campaign=dict(name=d.campaign.name,key=d.campaign.key, id=d.campaign.id),
                                        platform=dict(name=d.platform.name,key=d.platform.key, id=d.platform.id))
            result["pose_count"] = instance.poses.count()

            # just consider first pose
            p = instance.pose
            if p:
                result["pose"] = dict(id=p.id, lat=nullifnan(p.lat), lon=nullifnan(p.lon), alt=nullifnan(p.alt),
                                      dep=nullifnan(p.dep), timestamp=p.timestamp, data=[])
                for pd in p._data:
                    result["pose"]["data"].append(dict(name=pd.name, value=nullifnan(pd.value)))

            # Add info for events
            result["events"] = [dict(deployment_event_type=e.deployment_event_type.name, description=e.description, timestamp=e.timestamp) for e in instance.events]

            # Add info for the number of annotations
            result["annotation_count"] = count(db.session, instance.annotations)

    def get_create_thumbnail(self, size=None, annotation_id=None, autocreate=True, use_thm=False, **kw):
        assert current_app.config['MEDIA_THM_CACHE_DIR'] is not None and os.path.isdir(current_app.config["MEDIA_THM_CACHE_DIR"]), \
            "This server does not have a thumbnail directory set up. Cannot generate thumbnails automatically."
        if size is None or int(size) not in current_app.config['MEDIA_THM_ALLOWED_SIZES']:
            size = current_app.config['MEDIA_THM_DEFAULT_SIZE']
        thm_param_string = "s{}".format(size)
        bbox = None
        point = None
        if annotation_id is not None:
            annotation = AnnotationLabel.get_by_id(annotation_id)
            if annotation.point.media_id != self.id:
                raise ProcessingException("Media annotation does not occur on this media item (annotation.media_id != media.id)")
            point, bbox = annotation.point.bbox(default_size=current_app.config['MEDIA_THM_BBOX_DEFAULT_SIZE'])
            thm_param_string += "_bb{}_p{}".format("-".join(map(str, bbox)),"-".join(map(str, point))) if bbox is not None else ""
        # thm_file_name = current_app.config['MEDIA_THM_CACHE_PATH'].format(media=self, params=secure_filename(thm_param_string))
        thm_file_path = os.path.join(current_app.config['MEDIA_THM_CACHE_DIR'],
                                     current_app.config['MEDIA_THM_CACHE_PATH'].format(media=self, params=secure_filename(thm_param_string)))
        # print("GENERATING: {}".format(thm_file_path))
        if not os.path.isfile(thm_file_path) and autocreate:
            if self.media_type.name == "image":
                img = self.load_image(use_thm=use_thm)
                orig_width, orig_height = img.size
                if bbox is not None:
                    bbox = (bbox[0]*orig_width, bbox[1]*orig_height, bbox[2]*orig_width, bbox[3]*orig_height)
                    if point is not None:
                        # overlay point, two circle white over black to show white with black border
                        pt_offset = int(current_app.config['MEDIA_THM_POINT_DEFAULT_SIZE']*orig_width/2)
                        canvas = ImageDraw.Draw(img)
                        canvas.ellipse((point[0] * orig_width - (pt_offset+2), point[1] * orig_height - (pt_offset+2),
                             point[0] * orig_width + (pt_offset+2), point[1] * orig_height + (pt_offset+2)),
                            outline=(0, 0, 0, 100), width=8)  # , fill=(0,0,0,10))
                        canvas.ellipse((point[0] * orig_width - pt_offset, point[1] * orig_height - pt_offset,
                                        point[0] * orig_width + pt_offset, point[1] * orig_height + pt_offset),
                                       outline=(255, 255, 255, 100), width=4)  # , fill=(0,0,0,10))
                    img = img.crop(bbox)
                    orig_width, orig_height = img.size                          # update size to size of crop
                size = float(min(int(size), max(orig_width, orig_height)))      # max size of original
                height = size if orig_height > orig_width else (float(orig_height) * (size/float(orig_width)))
                width = float(orig_width) * size / float(orig_height) if orig_height > orig_width else size
                img = img.resize((int(width), int(height)), Image.ANTIALIAS)
                # TODO: check if image is empty and if it is, raise an exception / try something else
                #  - seems there may be issues with occasional empty images being saved
                if min(img.size) <= 0:
                    raise ProcessingException("Image appears to be empty. Cannot load or save.")
                if not os.path.isdir(os.path.dirname(thm_file_path)):
                    os.makedirs(os.path.dirname(thm_file_path))
                img.save(thm_file_path)
            else:
                raise ProcessingException("No thumbnail generator function defined for media type: {}".format(self.media_type.name), 400)
        return thm_file_path

    def load_image(self, use_thm=False):
        """Get image from url using cache (if available)"""
        imurl = self.path_best_thm if use_thm else self.path_best
        # print("DEBUG: URL: {}".format(imurl))
        cachekey = hashlib.md5(imurl.encode('utf-8')).hexdigest()
        if self._cache.get(cachekey):
            return self._cache.get(cachekey)
        else:
            img = Image.open(io.BytesIO(urlrequest.urlopen(imurl).read()))
            try:
                self._cache.set(cachekey, img)
            except UnpicklingError as e:
                current_app.logger.exception("{}: {}".format(type(e).__name__, e))
        return img

    @classmethod
    def api_get_thumbnail(cls, id):
        """
        Dynamically create thumbnail.

        If user is logged in, then they can request different sizes
        """
        try:
            thm_args = request.args.to_dict() if current_user.is_authenticated() else {}
            instance = cls.get_by_id(id)
            thumbnail = instance.get_create_thumbnail(**thm_args)
            if os.path.isfile(thumbnail):
                return send_file(thumbnail, mimetype='image/png')
            raise ProcessingException("Thumbnail file '{}' does not exist!".format(thumbnail), 404)
        except Exception as e:
            current_app.logger.exception("{}: {}".format(type(e).__name__, e))
        # use placeholder if image file is not found, do not cache to retry on next attempt
        return send_file(current_app.config['MEDIA_PLACEHOLDER_IMG'], mimetype='image/png', cache_timeout=-1)

    @classmethod
    def media_poses(cls, id):
        """
        The list of [pose](#pose) for the [media](#media) item matching `id`. For still images, it will normally just
        be a single [pose](#pose), but video-like [media_types](#media_type) can have more.
        """
        cls.assert_user_login()
        instance = cls.get_by_id(id)
        return render_data_response(PoseAPI.paginated_results(instance.poses))

    @classmethod
    def custom_post(cls):
        """
        In addition to the `MODEL COLUMNS` below, the JSON body can contain the following parameters:
        `pose`: a dict containing parameters for a single [pose](#pose) including associated pose data,
        `poses`: a list of dicts as above if linking multiple poses to a single media object (eg: video),
        `media_type`: OPTIONAL (default="image"), a string containing the [media_type](#media_type) name.

        Here are some example payloads for creating [media](#media) objects:
        ```
        # example JSON payload for media item with a single pose (eg: still image)
        {
            "pose": {"lat":-35.53153264, "lon":150.43962917, "alt":3.558, "dep":28.85, "data":[
                {"name":"temperature","value":16.602},
                {"name":"salinity","value":35.260222}
            ]},
            "key": "PR_20101117_002241_698_LC16",
            "deployment_id": 5,
            "timestamp_start": "2018-06-07T00:47:17.273514"
        }

        # Same as above, but with dict data
        {
            "pose": {"lat":-35.53153264, "lon":150.43962917, "alt":3.558, "dep":28.85, "data":{
                "temperature":16.602,
                "salinity":35.260222
            }},
            "key": "TEST_20101117_002241_698_LC16",
            "deployment_id": 13170,
            "timestamp_start": "2018-06-07T00:47:17.273514"
        }

        # To add multiple poses to a media object (i.e. video)
        {
            "poses":
                [{
                    "timestamp": "2018-06-07T00:47:17.273517",
                    "lat":-35.53153264,
                    "lon":150.43962917,
                    "alt":3.558,
                    "dep":28.85,
                    "data":{
                        "temperature":16.602,
                        "salinity":35.260222
                    }
                },{
                    "timestamp": "2018-06-07T00:47:18.273517",
                    "lat":-35.531532654,
                    "lon":150.439629345,
                    "alt":6.558,
                    "dep":29.85,
                    "data":{
                        "temperature":16.602,
                        "salinity":35.260222
                    }
                },{
                    "timestamp": "2018-06-07T00:47:19.273517",
                    "lat":-35.531534564,
                    "lon":150.43962934,
                    "alt":4.558,
                    "dep":28.85,
                    "data":{
                        "temperature":16.60256,
                        "salinity":35.260234
                    }
                }],
            "key": "KEY_OF_VIDEO_FILE_NAME",
            "deployment_id": 13170,
            "timestamp_start": "2018-06-07T00:47:17.273514",
            "media_type": "rawvideo"
        }
        ```
        """

        if request.method == "POST":
            cls.assert_user_login()
            data = request.get_json()
            instance = Media(**data)
            db.session.add(instance)
            db.session.commit()
            return jsonify(cls.model_to_dict(instance))
        else:
            raise ProcessingException("Method not allowed", code=405)

    @classmethod
    def upload(cls):
        """
        Media UPLOAD resource.

        Used to upload Media to predefined location set out in server config file.

        `file`: file data input containing image file to upload.
        `json`: json string containing parameters for new [media](#media) item
        """

        if request.method == "POST":
            cls.assert_user_login()
            assert "file" in request.files, "Uploaded file is expected to be found in 'file' parameter"
            assert "json" in request.form, "Expected media data as a json string in the 'json' form parameter"
            if current_app.config["MEDIA_UPLOAD_DIR"] is None or not os.path.isdir(current_app.config["MEDIA_UPLOAD_DIR"]):
                raise ProcessingException("This server does not allow uploads. Upload directory is not configured.", code=405)

            # form + data need to be strings only, so need to pass nested/structured objects as json strings
            data = json.loads(request.form.get("json"))
            # assign default key if key not provided
            if "key" not in data:
                data["key"] = str(uuid.uuid4())
            # print(json.dumps(data, indent=2))
            upload = request.files.get("file")
            dpl = Deployment.get_by_id(int(data.get('deployment_id')))
            [_, file_ext] = os.path.splitext(upload.filename)
            filepath_rel = current_app.config["MEDIA_UPLOAD_PATH"].format(deployment=dpl, filename=data["key"]+file_ext)
            filepath = os.path.join(current_app.config["MEDIA_UPLOAD_DIR"], filepath_rel)

            if upload and file_ext.lower() in set(current_app.config["MEDIA_UPLOAD_EXTENSIONS"]):
                if not os.path.isdir(os.path.dirname(filepath)):
                    os.makedirs(os.path.dirname(filepath))
                upload.save(filepath)
            else:
                raise ProcessingException("Invalid file or fileype.", code=405)

            data['path'] = current_app.config["MEDIA_UPLOAD_URL"].format(path=filepath_rel)

            mediainstance = Media(**data)
            db.session.add(mediainstance)
            db.session.commit()

            return jsonify(cls.model_to_dict(mediainstance))
        else:
            raise ProcessingException("Method not allowed", code=405)

    @classmethod
    def download(cls, id):
        """Download media object with associated metadata as a zip file"""
        cls.assert_user_login()
        from zipfile import ZipFile
        from io import BytesIO
        import requests
        instance = cls.get_by_id(id)
        # TODO: check media object type and only download if relevant (eg: vids may be too big).
        r = requests.get(instance.path_best)
        filename = instance.path_best.split('/')[-1] or "file.jpg"  # RLS has trailing slash - this is to deal with that
        annotations = AnnotationLabelAPI.query.filter(
            AnnotationLabel.annotation_set_id == AnnotationSet.id,
            AnnotationLabel.point_id == AnnotationPoint.id, AnnotationLabel.label_id.isnot(None), AnnotationPoint.media_id==id)
        total_annotations = annotations.count()
        annotations = annotations.filter(AnnotationSet.current_user_can_view == True)
        objects = [a.instance_to_dict(is_single=False) for a in annotations.all()]
        annotation_info = dict(objects=objects, media_object_total_annotation_count=total_annotations,
                               media_object_viewable_annotation_count=annotations.count())
        # TODO - encode position info int exif data (if relevant)
        tf = BytesIO()
        with ZipFile(tf, mode='w') as zf:
            zf.writestr(filename, r.content)
            zf.writestr(f"{instance.key}.json", json_dumps(instance.instance_to_dict(), indent=2))
            zf.writestr(f"{instance.key}-annotations.json", json_dumps(annotation_info, indent=2))
        tf.seek(0)
        return send_file(tf, attachment_filename=f"media-{instance.key}-{instance.id}.zip", as_attachment=True)

    @classmethod
    def get_annotations(cls, media_id, annotation_set_id):
        AnnotationSetAPI.get_single_auth_preprocessor(instance_id=annotation_set_id)
        return_types = request.args.getlist("return") if len(request.args.getlist("return")) > 0 else ["points", "frame"]
        media = Media.get_by_id(media_id)
        annotation_set = AnnotationSet.get_by_id(annotation_set_id)

        # TODO: make this use the pagination models, api_to_dict and q constructs from API
        points_annotation_set = annotation_set if annotation_set.parent_id is None else annotation_set.parent
        point_list = get_points(media, points_annotation_set)

        annotation_list = []
        for p in point_list:  # loop through annotation points
            # check if point is frame ot point label and return accordingly
            if ("points" in return_types and p.x is not None and p.y is not None) \
                    or ("frame" in return_types and p.x is None and p.y is None):
                a_info = dict(id=p.id, x=p.x, y=p.y, t=p.t, annotations=[], supplementary_annotations=[], data=p.data)
                for l in p.annotations:  # loop though labels
                    # if annotation_set id for label matches annotation_set id for point, add to list of labels
                    al = dict(color=l.color(), label_id=l.label_id, id=l.id, data=l.data, likelihood=l.likelihood, needs_review=l.needs_review)
                    if l.annotation_set_id == annotation_set.id:
                        if "label" in return_types:
                            al.update(dict(label=dict(name=l.label.name, id=l.label.id) if l.label else {},
                                           comment=l.comment, tags=[t.name for t in l.tags]))
                        a_info['annotations'].append(al)
                    # if the label comes from a different annotation set, consider it to be a suggestion
                    elif l.label_id is not None:
                        al.update(dict(label=dict(name=l.label.name, id=l.label_id),
                                       annotation_set=dict(id=l.annotation_set_id),
                                       user=dict(full_name=l.user.full_name(), id=l.user_id)))
                        # al = dict(label={"name": l.label.name, "id": l.label_id}, color=l.color(), id=l.id,
                        #           annotation_set={"id": l.annotation_set_id}, data=l.data,
                        #           user={"full_name": l.user.full_name(), "id": l.user_id})
                        a_info['supplementary_annotations'].append(al)
                # If no labels,
                if len(a_info.get('annotations')) < 1:
                    l = AnnotationLabel.create(user_id=current_user.id, annotation_set_id=annotation_set_id, point_id=p.id)
                    al = dict(color=l.color(), label_id=l.label_id, id=l.id, data=l.data, likelihood=l.likelihood)
                    al.update(dict(label={}, comment=l.comment, tags=[t.name for t in l.tags]))
                    a_info['annotations'].append(al)

                annotation_list.append(a_info)

            # annotation_list = [{"point_id":a.point.id, "x": a.point.x, "y": a.point.y, "t": a.point.t, "color":a.color, "label_id":a.label_id, "id":a.id} for a in annotations]
        media_info = {
            "id": media.id,
            "path_best": media.path_best,
            "key": media.key,
            "type": media.media_type.name,
            "deployment": {
                "name": media.deployment.name, "id": media.deployment.id,
                "campaign": {"name": media.deployment.campaign.name, "id": media.deployment.campaign.id}
            }
        }
        annotation_set_info = {
            "label_scheme_id": annotation_set.label_scheme_id,
            "data": annotation_set.data,
            "name": annotation_set.name,
            "id": annotation_set.id,
            "media_collection": {
                "name": annotation_set.media_collection.name, "id": annotation_set.media_collection.id
            }
        }
        data = dict(media=media_info, annotation_set=annotation_set_info, annotations=annotation_list)
        return render_data_response(data=data)


class MediaCollectionMediaAssociationAPI(MediaCollectionMediaAssociation, ValidationMixin):
    """
    Media Collection Media Associate resource: a model for associating a subset of [media](#media) objects to a
    [media_collection](#media_collection)
    """
    api_restless_methods = ['GET', 'POST', 'DELETE']
    api_include_columns = ["created_at", "media_id", "media_collection_id"]
    # api_max_results_per_page = 10

    api_preprocessors = {
        "POST": ['edit_auth_preprocessor'],
        "DELETE_SINGLE": ['edit_auth_preprocessor']
    }

    # validations
    val.validates_constraints()

    @before_flush
    def _check_exists(self):
        if MediaCollectionMediaAssociation.query.filter(
                MediaCollectionMediaAssociation.media_id == self.media_id,
                MediaCollectionMediaAssociation.media_collection_id == self.media_collection_id).first():
            raise ProcessingException(description="Already saved to Media Collection!", code=403)

    @classmethod
    def edit_auth_preprocessor(cls, data=None, instance_id=None, **kw):
        if instance_id is not None:
            media_collection_id = cls.get_by_id(instance_id).media_collection_id
        elif data is not None:
            media_collection_id = data['media_collection_id']
        else:
            raise ProcessingException("No `media_collection_id` could be found. Unable to check edit permissions")
        MediaCollectionAPI.patch_auth_preprocessor(instance_id=media_collection_id)

# class MediaPoseAssociationAPI(MediaPoseAssociation, APIModel):
#     api_restless_methods = ['GET', 'POST', 'DELETE']
#
#     # validations
#     val.validates_constraints()

class DeploymentFileAPI(DeploymentFile, ValidationMixin):
    """
    Deployment File resource: a file associated with a [deployment](#deployment) object. Can be used for batch importing / updating
    of [media](#media), [pose](#pose) and [pose_data](#pose_data) objects.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    api_include_columns = ['user_id', 'description', 'iscompressed', 'campaign_id','file_url', 'id', 'name', 'last_save']

    api_custom_endpoints = [
        APIModelEndpoint("get_data", endpoint="{tablename}/<int:fid>/data", methods=["GET"]),
        APIModelEndpoint("set_data", endpoint="{tablename}/data", methods=["POST"])
    ]

    @classmethod
    @docstring_parameter(DeploymentFile.get_data.__doc__)
    def get_data(cls, *args, **kwargs):
        """
        Deployment file resource. %s

        **Example1:** convert CSV file into serialized, nested JSON expected for [media](#media) resource and batch save rows to the related `deployment.media` collection for this model
        ```
        /api/deployment_file/2/data?save={"collection": "deployment.media"}&f={"operations": [{"module": "pandas", "method": "read_csv", "kwargs": {"parse_dates": ["time"], "skiprows": 2, "usecols": [2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]}}, {"method": "rename", "kwargs": {"columns": {"image_filename": "key", "longitude": "pose.lon", "latitude": "pose.lat", "depth_sensor": "pose.data.dep", "altitude_sensor": "pose.alt", "depth": "pose.dep", "sea_water_temperature": "pose.data.temp", "sea_water_salinity": "pose.data.salinity", "chlorophyll_concentration_in_sea_water": "pose.data.chlorophyll", "backscattering_ratio": "pose.data.backscater", "colored_dissolved_organic_matter": "pose.data.colored_dissolved_organic_matter", "time": "timestamp_start", "cluster_tag": "pose.data.cluster_tag"}}}, {"method": "to_dict", "kwargs": {"orient": "records"}}, {"module": "data", "method": "unflatten_dicts", "kwargs": {}}]}
        ```

        **Example 2:** similar to above, but perform an update rather than an insert. Can optinally create missing. This is slower.
        ```
        /api/deployment_file/4/data?save={"collection": "deployment.media", "update_existing": true, "match_on": ["key"], "create_missing": true}&f={"operations": [{"module": "pandas", "method": "read_csv", "kwargs": {"parse_dates": ["time"], "skiprows": 2, "usecols": [2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]}}, {"method": "rename", "kwargs": {"columns": {"image_filename": "key", "longitude": "pose.lon", "latitude": "pose.lat", "depth_sensor": "pose.data.dep", "altitude_sensor": "pose.alt", "depth": "pose.dep", "sea_water_temperature": "pose.data.temp", "sea_water_salinity": "pose.data.salinity", "chlorophyll_concentration_in_sea_water": "pose.data.chlorophyll", "backscattering_ratio": "pose.data.backscater", "colored_dissolved_organic_matter": "pose.data.colored_dissolved_organic_matter", "time": "timestamp_start", "cluster_tag": "pose.data.cluster_tag"}}}, {"method": "to_dict", "kwargs": {"orient": "records"}}, {"module": "data", "method": "unflatten_dicts", "kwargs": {}}]}
        ```
        """
        return super().get_data(*args, **kwargs)


class CampaignFileAPI(CampaignFile, ValidationMixin):
    """
    Campaign File resource: a file associated with a [campaign](#campaign) object.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    api_include_columns = ['user_id', 'description', 'iscompressed', 'campaign_id','file_url', 'id', 'name']

    # api_custom_endpoints = [
    #     APIModelEndpoint("get_file", endpoint="{tablename}/get/<int:fid>", methods=["GET"]),
    #     APIModelEndpoint("post_file", endpoint="{tablename}/save", methods=["POST"])
    # ]
    api_custom_endpoints = [
        APIModelEndpoint("get_data", endpoint="{tablename}/<int:fid>/data", methods=["GET"]),
        APIModelEndpoint("set_data", endpoint="{tablename}/data", methods=["POST"])
    ]

    # validations
    val.validates_constraints()



    # api_prefix = "/api"  # this gets set/updated in register_api as well
    #
    # @classmethod
    # def register_api(cls, apimanager, app):
    #     cls.api_prefix = apimanager.APINAME_FORMAT.format("/")  # "/api"
    #
    #     apimanager.create_api(CampaignFile, methods=['GET', 'POST', 'DELETE'],
    #                           exclude_columns=['rawfiledata', 'filedata'],
    #                           max_results_per_page=50000)
    #
    #     # Custom API for FILES (methods from DBFileMixin)
    #     app.add_url_rule(cls.load_resource(cls.api_prefix, '<int:fid>'), view_func=cls.get_file, methods=['GET'],
    #                             endpoint=cls.load_resource(cls.api_prefix, 'id'))
    #     app.add_url_rule(cls.save_resource(), view_func=cls.post_file, methods=['POST'],
    #                             endpoint=cls.save_resource())
    #
    #
    #     # # CUSTOM API CLASSES ###################
    #     # # Normal restless API is insufficient in this case, so we need a custom endpoint
    #     # # TODO: Move file operations out of view and into API. This is incomplete, but working!
    #     # # TODO: flesh this out to reflect all the functionality in view and utils to make a proper API call
    #     # class FileAPI(MethodView):
    #     # def get(self, fid):
    #     # try:
    #     #             file = CampaignFileAPI.query.filter_by(id=fid).one()
    #     #             # return send_file(memory_file, attachment_filename=cf.name, mimetype='image/png')  # image
    #     #             return send_file(BytesIO(file.filedata), attachment_filename=file.name, as_attachment=True)  # attachment
    #     #         except ProcessingException as perror:
    #     #             return jsonify({'message': perror.description}), perror.code, {'ContentType': 'application/json'}
    #     #         except NoResultFound as perror:
    #     #             return jsonify({'message': "No result found"}), 404, {'ContentType': 'application/json'}


class DeploymentAPI(Deployment, ValidationMixin):
    """
    Deployment resource: a dataset containing a group of [media](#media) objects collected from an operation of the [platform](#platform), eg: a survey, transect or mission
    """
    # cache = SimpleCache()
    # api_has_cache = True  # do not cache, since there are too many permutations
    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    # api_exclude_columns = ['user.active', 'user.info', 'user.last_login',
    #                        'user.password', 'user.role_id', 'user.user_created_at', 'user.is_admin',
    #                        'user.email', 'user.username',
    #                        'campaign.user_id', 'campaign.created_at', 'media_count',
    #                        'media.path', 'media.path_thm', 'events', 'media', 'poses', 'user']
    api_include_columns = ["name", "key", "id", "created_at", "campaign", "campaign.id", "campaign.name", "platform",
                           "platform.id", "platform.name", "is_valid"]
    api_include_methods = ["color", "pose"]
    api_add_columns_single = ["user_id","created_at","description", "campaign.key", "platform", "platform.id",
                              "platform.name", "platform.key", "files", "files.id",
                              "files.name", "files.file_url"]
    api_add_methods_single = ["media_count", "pose_stats", "layers", "total_annotation_count", "public_annotation_count"]
    # api_include_methods = ["timestamp_start", "timestamp_end", "media_count"]
    api_max_results_per_page = 2000
    # api_validation_exceptions = [ValidationError]

    api_custom_endpoints = [
        APIModelEndpoint("get_map", endpoint='{tablename}/map', methods=["GET"]),
        # APIModelEndpoint("post_deployment", endpoint='{tablename}/upload', methods=["POST"]),
        # dict(endpoint='{apiprefix}/{tablename}/<int:id>/events', methods=["GET"], classmethod="export_events"),
        APIModelEndpoint("export", endpoint='{tablename}/<int:id>/export', methods=["GET"]),
        # APIModelEndpoint("get_pose_stats", endpoint='{tablename}/<int:id>/stats', methods=["GET"]),
        APIModelEndpoint("get_ecoregion", endpoint='{tablename}/<int:id>/ecoregion', methods=["GET"])
    ]

    # api_postprocessors = {"GET_SINGLE": ['get_single_postprocessor']}

    # validations
    val.validates_constraints()


    def __init__(self, **kwargs):
        """Add user_id to post payload if not supplied"""
        if "user_id" not in kwargs:
            kwargs["user_id"] = current_user.id
        super().__init__(**kwargs)


    # @classmethod
    # def get_single_postprocessor(cls, result=None, instance=None, **kw):
    #     """Optionally include properties from URL parameters. Supported includes are: `annotation_sets`,
    #     `annotation_counts`"""
    #     includes = request.args.getlist("include")
    #     if "annotation_counts" in includes:
    #         q = AnnotationLabelAPI.query.filter(AnnotationLabelAPI.label_id.isnot(None), AnnotationLabelAPI.point.has(
    #             AnnotationPointAPI.media.has(DeploymentAPI.id == instance.id)))
    #         result["total_annotation_count"] = q.quick_count()
    #         # result["visible_annotation_count"] = q.filter(AnnotationLabelAPI.current_user_can_view==True).quick_count()
    #     if "annotation_sets" in includes:
    #         result["annotation_sets"] = None

    @classmethod
    def export(cls, id):
        """
        Export deployment information for a deployment matching `id`. Parameters:

        ```
        ?format=...&file=...&return=[...]
        ```

        Where:
        `format` is one of either [`csv` or `json`]
        `file` is one of either [`attachment` or `text`]
        `return` list of attributes / properties to include in the export, which can be a combination of:

        `datasetkeys`: media, deployment and campaign keys

        `mediapaths`: saved path of media file (in most cases, you'll want `mediapaths-resolved` instead)

        `mediapaths-resolved`: generated path of media file (from datasource plugin - USE THIS)

        `pose`: lat, long, alt, depth, timestamp info

        `posedata`: if `pose` is requested, all auxiliary pose_data will be added. Otherwise, returns nothing.

        **EXAMPLES**

        ```
        # Example
        /api/deployment/11/export?format=csv&file=attachment&return=datasetkeys&return=mediapaths-resolved&return=pose&return=posedata
        ```

        **WARNING!:** this endpoint will be changed in a future update to be more consistent with other export endpoints. For
        now, feel free to use it, with the caveat that any third-party integrations may require updating the query format / parameters
        (reminder: this project is still in active development mode).
        """
        return deployment_export(cls, id)

    @classmethod
    def get_map(cls):
        """
        Deployment resource

        Minimal Deployment Resource for map data - result is cached for fast loading. Cache is cleared when new deployments are saved.

        :param GET: json: q: restless search query.
        :returns GET: json: a json string containing the structure of stuff
        """
        q = json.loads(request.args.get("q") if request.args.get("q", "").strip() else "{}")
        deployments = search(db.session, Deployment, q)
        deployment_count = deployments.count()
        # deployment_list = [] if deployment_count > 2000 else deployments.all()
        deployment_list = deployments.all()

        deployments_json = {
            "objects": [dict(latlon=d.latlon, id=d.id, color=d.color(),
                             total_annotation_count=d.total_annotation_count) for d in deployment_list],
            "num_results": deployment_count,
            "page": 1,
            "total_pages": 1
        }
        return render_data_response(deployments_json)

    # @classmethod
    # def post_deployment(cls):
    #     data = request.get_json()
    #     dpl, created = Deployment.get_or_create(name=data['name'], key=data['key'], campaign_id=data['campaign_id'],
    #                                             user_id=data['user_id'])
    #
    #     if 'timestamp' in data:
    #         dpl.update(timestamp=datetime.strptime(data['timestamp'], "%Y%m%d_%H%M%S"))
    #     if 'media' in data:
    #         # TODO: check if append or replace. If replace, first delete!
    #         media = data['media']
    #         for m in media:
    #             m['deployment_id'] = dpl.id
    #         Media.dict_to_db(media)
    #
    #     return jsonify({'id': dpl.id, 'name': dpl.name, 'media_count': len(media)})

    @classmethod
    def get_ecoregion(cls, id):
        """
        Get ecoregion info from ESRI API Marine Ecoregions Of the World (MEOW) feature layer. Uses `latlon` of deployment to
        determine the relevant ecoregion. Accepts querystring parameter: `fields` which is a comma-separated list
        of properties to return, which can be a subset of:
        `fields = FID, ECO_CODE, ECOREGION, PROV_CODE, PROVINCE, RLM_CODE, REALM, ALT_CODE, ECO_CODE_X, Lat_Zone, Shape__Area, Shape__Length`
        if not set, it will default to `fields=*`, which will return all properties.
        """
        instance = cls.get_by_id(id)
        pose = instance.pose
        fields = request.args.get("fields", "*")
        return render_data_response(get_ecoregion(pose.get('lat'), pose.get('lon'), fields))


class PlatformAPI(Platform, ValidationMixin):
    """
    Platform resource: the equipment/method used to collect the imagery data (eg: a specific AUV, ROV or camera platform)
    """
    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    # api_exclude_columns = ['rawfiledata', 'iscompressed', 'campaigns', 'data_json', 'user']
    api_include_columns = ["created_at", "description", "id", "name", "reference", "user_id"]
    api_max_results_per_page = 1000
    # api_validation_exceptions = [ValidationError]
    api_add_columns_single = ["datasources", "data"]

    api_preprocessors = {
        "POST": ['assert_current_user_is_admin'],
        "PATCH_SINGLE": ['assert_current_user_is_admin'],
        "PATCH_MANY": ['assert_current_user_is_admin'],
        "DELETE_SINGLE": ['assert_current_user_is_admin']
    }

    # api_custom_endpoints = [
    #     APIModelEndpoint("api_fileop", endpoint='{tablename}_file/<int:fid>', methods=['PUT', 'GET'])
    # ]

    # api_postprocessors = {"GET_SINGLE": ['get_single_postprocessor']}
    # api_postprocessors = {"POST": ["post_create_default_datasource_postprocessor"]}

    # validations
    val.validates_constraints()

    # @classmethod
    # def get_single_postprocessor(cls, result=None, instance=None, **kw):
    #     # instance_id = result.get('id', None)
    #     if instance is not None:
    #         # instance = cls.get_by_id(instance_id)
    #         # p = Platform.get_by_id(result.get("id"))
    #         result["data"] = instance.data
    #         result["datasources"] = [DataSourceAPI.model_to_dict(i) for i in instance.datasources]

    # Handle validation #######################
    @before_flush
    def _check_unique(self):
        existing = self.query.filter_by(name=self.name).first()
        if existing:
            if existing.id != self.id:  # check that we are not updating
                self.add_validation_error('name', 'Name is already in use')


    # @classmethod
    # def post_create_default_datasource_postprocessor(cls, data, *args, **kw):
    #     instance = cls.get_by_id(data.get("id"))


    # CUSTOM API CLASSES ###################
    # Normal restless API is insufficient in this case, so we need a custom endpoint
    # @classmethod
    # def api_fileop(cls, fid):
    #     if request.method == "PUT":
    #         allowed_extensions = {'png', 'jpg', 'jpeg', 'gif'}
    #         filedata = request.files['file']
    #         if '.' in filedata.filename and filedata.filename.rsplit('.', 1)[1] in allowed_extensions:
    #             file = Platform.query.filter_by(id=fid).one()
    #             file.update(filedata=filedata.read())
    #             return jsonify({'message': "Success"}), 200, {'ContentType': 'application/json'}
    #
    #         return jsonify({'message': "File type not allowed. Must be one of: {}".format(list(allowed_extensions))}), \
    #                415, {'ContentType': 'application/json'}
    #
    #     if request.method == "GET":
    #         file = Platform.query.filter_by(id=fid).one()
    #         return send_file(BytesIO(file.filedata), attachment_filename=file.name, mimetype='image/png')  # image


    # class PlatformFileAPI(MethodView):
    #     # TODO: implement put method
    #     def put(self, fid):
    #         allowed_extensions = set(['png', 'jpg', 'jpeg', 'gif'])
    #         filedata = request.files['file']
    #         if '.' in filedata.filename and filedata.filename.rsplit('.', 1)[1] in allowed_extensions:
    #             file = Platform.query.filter_by(id=fid).one()
    #             file.update(filedata=filedata.read())
    #             return jsonify({'message': "Success"}), 200, {'ContentType': 'application/json'}
    #
    #         return jsonify({'message': "File type not allowed. Must be one of: {}".format(list(allowed_extensions))}), \
    #                415, {'ContentType': 'application/json'}
    #
    #     def get(self, fid):
    #         try:
    #             file = Platform.query.filter_by(id=fid).one()
    #             return send_file(BytesIO(file.filedata), attachment_filename=file.name, mimetype='image/png')  # image
    #         except ProcessingException as perror:
    #             return jsonify({'message': perror.description}), perror.code, {'ContentType': 'application/json'}
    #         except NoResultFound as perror:
    #             return jsonify({'message': "No result found"}), 404, {'ContentType': 'application/json'}


# class CampaignDeployentsAPI(Campaign, APIModel):
#     api_restless_methods = ['GET']  # , 'POST', 'DELETE', 'PATCH'
#
#     # api_exclude_columns = ['files', 'info', 'user_id',
#     #                        'media_path_pattern', 'thm_path_pattern',
#     #                        'platform.rawfiledata', 'platform.campaign_id', 'platform.iscompressed',
#     #                        'platform.data_json', 'platform.data', 'platform.created_at', 'platform.description',
#     #                        'platform.reference', 'platform.user_id',
#     #                        'workgroups_usergroups', 'info.campaign_id',
#     #                        'info.created_at', 'user', 'description',
#     #                        'deployments.latlon', 'deployments.created_at', 'deployments.user_id',
#     #                        'deployments.campaign_id']
#     api_include_columns = ['color', 'created_at', 'id', 'key', 'name', 'platform_id', 'deployments',
#                            'deployments.id', 'deployments.color', 'deployments.name', 'deployments.timestamp', 'platform','platform.name']
#     api_max_results_per_page = 10000
#     api_collection_name = '{}-deployments'.format(Campaign.__tablename__)


class CampaignDeploymentList(APIModel, ValidationMixin):
    """
    A special resource for listing / querying campaigns in a different format.
    """
    __tablename__ = "campaign-deployment"
    # cache = SimpleCache()
    # api_has_cache = True
    api_build_restless_endpoints = False  # prevent restless endpoints, since this is not linked to an ORM class

    api_custom_endpoints = [
        APIModelEndpoint("get_objects", endpoint='{tablename}/<resource>', methods=['GET']),
        APIModelEndpoint("get_objects", endpoint='{tablename}', methods=['GET'], defaults={"resource": "campaign-deployment"})
    ]

    api_max_deployment_results = 2000
    valid_resources = ["deployment", "campaign", "campaign-deployment"]

    # validations
    val.validates_constraints()

    @classmethod
    def get_objects(cls, resource="campaign-deployment"):
        """

        :param resource:
        :return:

        Examples:

        # search campaign-deployments for "gbr"
        /api/campaign-deployment/deployment?q={"filters":[{"or":[{"name":"key","op":"ilike","val":"%gbr%"},{"name":"campaign","op":"has","val":{"name":"key","op":"ilike","val":"%gbr%"}}]}]}
        # match list of deployment keys (campaign-deployment)
        /api/campaign-deployment?q={"filters":[{"name":"key","op":"in","val":["r20071004_013437_gbr_05_noggin_grids","r20071010_230932_gbr_09_hydrographers_leg"]}]}
        # as above but for deployments
        /api/campaign-deployment/deployment?q={"filters":[{"name":"key","op":"in","val":["r20071004_013437_gbr_05_noggin_grids","r20071010_230932_gbr_09_hydrographers_leg"]}]}
        # as above but for IDs and not keys
        /api/campaign-deployment/deployment?q={"filters":[{"name":"id","op":"in","val":[9495,9491]}]}
        """
        # EG: search campaign-deployments for "gbr"
        # /api/list/campaign-deployment?q={"filters":[{"name":"key","op":"ilike","val":"%gbr%"},{"name":"campaign","op":"has","val":{"name":"key","op":"ilike","val":"%gbr%"}}],"disjunction":true}
        # EG

        # TODO: determine whether this is necessary at all...
        # Now that the API can do "or" queries, we can probably do much of this with existing models...

        assert(resource in cls.valid_resources), "Invalid resource. Must be one of {}.".format(cls.valid_resources)

        q = json.loads(request.args.get("q", "{}"))
        s = request.args.get("s", "").split()
        object_list = []
        if resource == "campaign":
            query = search(db.session, Campaign, q)
            object_list = [cls.campaign2dict(c) for c in query]
        else:
            query = search(db.session, Deployment, q)
            for s_term in s:
                # NOTE: Deployment.campaign.has filter is MUCH faster than adding where clauses
                subquery = db.session.query(Deployment.id).filter(or_(Deployment.name.ilike("%{}%".format(s_term)),
                    Deployment.campaign.has(Campaign.name.ilike("%{}%".format(s_term))))).subquery()
                query = query.filter(Deployment.id.in_(subquery))
            # print(str(query))
            object_list = cls.get_nested_deployments(query, nested=(resource == "campaign-deployment"))

        result = dict(objects=object_list, num_results=len(object_list), page=1, total_pages=1)

        return render_data_response(data=result)


        # campaign_filters = json.loads(request.args.get("cfilt", "{}"))
        # deployment_filters = json.loads(request.args.get("dfilt", "{}"))
        # # campaign_id = request.args.get("campaign_id", None)
        # # platform_id = request.args.get("platform_id", None)
        #
        # # return deployments from a single campaign
        # object_list = []
        # errmsg = ""
        # if search_string.strip() != "":
        #     deployment_query = Deployment.query
        #     for search_term in search_string.split():   # split on white space to handle multiple search terms
        #         deployment_query = deployment_query.filter(or_(Deployment.name.ilike("%{}%".format(search_term)),
        #                                                    and_(Deployment.campaign_id == Campaign.id,
        #                                                         Campaign.name.ilike("%{}%".format(search_term)))))
        #
        #     object_list = cls.get_nested_deployments(deployment_query, resource)
        # elif deployment_filters:
        #     deployment_query = Deployment.query
        #     if "campaign_ids" in deployment_filters:
        #         deployment_query = deployment_query.filter(
        #             Deployment.campaign_id.in_(deployment_filters.get("campaign_ids", [])))
        #     if "platform_ids" in deployment_filters:
        #         deployment_query = deployment_query.filter(
        #             Deployment.campaign_id == Campaign.id, Campaign.platform_id.in_(deployment_filters.get("platform_ids", [])))
        #     if "search" in deployment_filters:
        #         search_list = deployment_filters.get("search","").split()  # split on white space to handle multiple search terms
        #         for search_term in search_list:
        #             deployment_query = deployment_query.filter(
        #                 or_(Deployment.name.ilike("%{}%".format(search_term)),
        #                 and_(Deployment.campaign_id == Campaign.id, Campaign.name.ilike("%{}%".format(search_term)))))
        #     if "deployment_ids" in deployment_filters or "deployment_keys" in deployment_filters:
        #         deployment_query = deployment_query.filter(
        #             or_(Deployment.id.in_(deployment_filters.get("deployment_ids", [])), Deployment.key.in_(deployment_filters.get("deployment_keys", []))))
        #
        #     if deployment_query.count() > cls.api_max_deployment_results:
        #         errmsg = "Too many results. Found {}, but the limit is {}. Try narrowing search.".format(
        #             deployment_query.count(), cls.api_max_deployment_results)
        #         deployment_query = []
        #
        #     object_list = cls.get_nested_deployments(deployment_query, resource)
        #
        # elif resource == "campaigns":
        #     campaign_query = Campaign.query
        #     if "campaign_ids" in campaign_filters:
        #         campaign_query = campaign_query.filter(Campaign.id.in_(campaign_filters.get("campaign_ids", [])))
        #     if "platform_ids" in campaign_filters:
        #         campaign_query = campaign_query.filter(Campaign.platform_id.in_(campaign_filters.get("platform_ids", [])))
        #     object_list = [cls.campaign2dict(c) for c in campaign_query]
        #
        # return jsonify(objects=object_list, num_results=len(object_list), page=1, errmsg=errmsg)


    @classmethod
    def get_nested_deployments(cls, deployment_query, nested=True):
        # check if length of deployment list is too long, and if so, raise exception
        if deployment_query.count() > cls.api_max_deployment_results:
            raise ProcessingException("Too many results. Found {}, but the limit is {}. Try narrowing search.".format(
                    deployment_query.count(), cls.api_max_deployment_results))
        if nested is False:
            return [cls.deployment2dict(d, True) for d in deployment_query]
        else:
            campaign_dict = {}
            for d in deployment_query:  # generate dict lookup of keys
                if d.campaign_id not in campaign_dict:
                    campaign_dict[d.campaign_id] = cls.campaign2dict(d.campaign)
                campaign_dict[d.campaign_id]["deployments"].append(cls.deployment2dict(d))
            return list(campaign_dict.values())


    @staticmethod
    def campaign2dict(c):
        return dict(id=c.id, key=c.key, name=c.name, color=c.color, deployments=[])  #, deployment_count=c.deployment_count())

    @staticmethod
    def deployment2dict(d, include_campaign_id=False):
        dpl_dict = dict(name=d.name, id=d.id, key=d.key, color=d.color(), platform=dict(name=d.platform.name, id=d.platform.id),)
        if include_campaign_id:
            dpl_dict["campaign_id"] = d.campaign_id
        return dpl_dict


# class CampaignMapAPI(Campaign, APIModel):
#     api_restless_methods = ['GET']  # , 'POST', 'DELETE', 'PATCH'
#
#     api_include_columns = ['color', 'deployment_count', 'id']
#     api_include_methods = ['deployment_count', 'latlon']
#     api_max_results_per_page = 10000
#     api_collection_name = '{}/map'.format(Campaign.__tablename__)


class CampaignAPI(Campaign, ValidationMixin):
    """
    Campaign resource: a group of [deployments](#deployment). Eg: a specific expedition, cruise or data collection initiative
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']  # , 'POST', 'DELETE', 'PATCH'

    # api_exclude_columns = ['files.rawfiledata', 'files.campaign_id', 'files.iscompressed',
    #                        'platform.rawfiledata', 'platform.campaign_id', 'platform.iscompressed',
    #                        'platform.data_json',
    #                        'deployments', 'workgroups_usergroups', 'info.campaign_id',
    #                        'info.created_at']
    api_include_columns = ['color', 'id','key','name', 'description']
    api_include_methods = ['deployment_count', 'media_count', 'total_annotation_count', 'public_annotation_count', 'latlon'] #, 'platforms']
    # api_include_methods = ['deployment_count', 'timestamp_start', 'timestamp_end', 'media_count']
    api_max_results_per_page = 10000
    api_add_columns_single = ["user_id"]
    # api_add_methods_single = ["extent", "stats", "platforms", "total_annotation_count"]
    api_add_methods_single = ["stats", "platforms", "total_annotation_count", "public_annotation_count"]

    # cache = SimpleCache()
    api_cache_threshold = 100  # 1 week

    api_custom_endpoints = [
        APIModelEndpoint("get_map_cached", endpoint='{tablename}/map', methods=["GET"])
    ]

    # api_preprocessors = {"POST": ['set_color']}

    # validations
    val.validates_constraints()

    def __init__(self, **kwargs):
        """Add user_id to post payload if not supplied"""
        if "user_id" not in kwargs:
            kwargs["user_id"] = current_user.id
        super().__init__(**kwargs)



    @classmethod
    def get_map_cached(cls):
        """
        Deployment resource

        Minimal Deployment Resource for map data - result is cached for fast loading. Cache is cleared when new deployments are saved.

        :param GET: json: q: restless search query.
        :returns GET: json: a json string containing the structure of stuff
        """
        # q = """
        # SELECT
        #   campaign_centroids_mv.id,
        #   campaign_centroids_mv.color,
        #   campaign_centroids_mv.deployment_count,
        #   ST_X(campaign_centroids_mv.geom) as lon,
        #   ST_Y(campaign_centroids_mv.geom) as lat
        # FROM
        #   campaign_centroids_mv
        # """
        # result = db.session.execute(q)
        # campaigns_json = {
        #     "objects": [dict(latlon=dict(lat=c[4], lon=c[3]), id=c[0], color=c[1], deployment_count=c[2]) for c in result],
        #     "num_results": result.rowcount,
        #     "page": 1,
        #     "total_pages": 1
        # }

        campaigns_json = cls.get_cached_data()
        if campaigns_json is None:
            q = json.loads(request.args.get("q") if request.args.get("q", "").strip() else "{}")
            campaigns = search(db.session, Campaign, q)
            campaigns_json = {
                "objects": [dict(latlon=c.latlon, id=c.id, color=c.color, deployment_count=c.deployment_count,
                                 total_annotation_count=c.total_annotation_count) for c in campaigns.all()],
                "num_results": campaigns.count(),
                "page": 1,
                "total_pages": 1
            }
            # cls.set_cached_data(data=campaigns_json)
        return render_data_response(campaigns_json)

    # api_preprocessors = {'POST': ['post_preprocessor']}
    # @staticmethod
    # def post_preprocessor(data=None, **kw):
    #     if "color" not in data:
    #         data['color'] = "#%06x" % random.randint(0, 0xFFFFFF)


    # @classmethod
    # def register_api(cls, apimanager):
    #
    #     def post_preprocessor(data=None, **kw):
    #         if "color" not in data:
    #             data['color'] = "#%06x" % random.randint(0, 0xFFFFFF)
    #
    #     apimanager.create_api(cls, methods=['GET', 'POST', 'DELETE', 'PATCH'],
    #                           exclude_columns=['files.rawfiledata', 'files.campaign_id', 'files.iscompressed',
    #                                            'platform.rawfiledata', 'platform.campaign_id', 'platform.iscompressed', 'platform.data_json',
    #                                            'deployments', 'workgroups_usergroups', 'info.campaign_id',
    #                                            'info.created_at'],
    #                           include_methods=['deployment_count', 'file_count'],
    #                           preprocessors={'POST': [post_preprocessor]},
    #                           collection_name=cls.__tablename__,
    #                           max_results_per_page=10000)
    #
    #     apimanager.create_api(cls,
    #                           collection_name='{}-deployments'.format(cls.__tablename__),
    #                           methods=['GET'],
    #                           exclude_columns=['files', 'info', 'user_id',
    #                                            'media_path_pattern', 'thm_path_pattern',
    #                                            'platform.rawfiledata', 'platform.campaign_id', 'platform.iscompressed',
    #                                            'platform.data_json', 'platform.data', 'platform.created_at', 'platform.description',
    #                                            'platform.reference', 'platform.user_id',
    #                                            'workgroups_usergroups', 'info.campaign_id',
    #                                            'info.created_at', 'user', 'description',
    #                                            'deployments.latlon', 'deployments.created_at', 'deployments.user_id','deployments.campaign_id'],
    #                           max_results_per_page=10000)


class DeploymentEventAPI(DeploymentEvent, ValidationMixin):
    """
    Deployment Event resource: a freeform text log that can be associated with a [pose](#pose) and/or a [media](#media) object.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    api_include_columns = ['media','media.id','media.key','media.path_best','media.path_best_thm', 'user_id', 'description', 'timestamp', 'created_at', 'pose', 'pose.lat', 'pose.lon',
                           'pose.alt', 'pose.dep', 'pose.timestamp', 'deployment',
                           'deployment.name', 'deployment.key', 'deployment.id', 'deployment.created_at',
                           'deployment.campaign_id', 'deployment_event_type', 'user', 'user.username', 'user.info',
                           'user.first_name', 'user.last_name', 'user.email', 'user.id', "deployment_event_type_id",
                           "media_id", "deployment_id", "id"]
    # validations
    val.validates_constraints()


class DataSourceAPI(DataSource, ValidationMixin):
    """
    Datasource resource: the parameters that define the structure and format of the data on the
    `Datasource Repository` and which `Datasource Plugin` to use. Each [platform](#platform) must have at least one [datasource](#datasource).
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    # api_exclude_columns = ['deployment_events', 'data_json']
    api_include_columns = ["id", "name", "campaign_search", "datafile_pattern", "datafile_search", "deployment_search",
                           "media_pattern", "mediadir_search", "thumb_pattern", "thumbdir_search", "urlbase_browse",
                           "datafile_operations", "datasource_type", "datasource_type.id", "datasource_type.name",
                           "platform", "platform.id", "platform.name", "user_id", "created_at", "credential_key"]

    api_preprocessors = {
        "POST": ['assert_current_user_is_admin'],
        "PATCH_SINGLE": ['assert_current_user_is_admin'],
        "PATCH_MANY": ['assert_current_user_is_admin'],
        "DELETE_SINGLE": ['assert_current_user_is_admin']
    }

    # validations
    val.validates_constraints()


class DataSourceTypeAPI(DataSourceType, ValidationMixin):
    """
    Datasource Type resource: defines mapping to a datasource plugin.
    """
    api_restless_methods = ["GET"]
    api_include_columns = ["id", "name", "created_at"]
    val.validates_constraints()


class DeploymentEventTypeAPI(DeploymentEventType, ValidationMixin):
    """
    Deployment Event resource: timestamped freeform text observation with optional link to [media](#media) and [pose](#pose) objects.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    # api_exclude_columns = ['deployment_events', 'data_json']
    api_include_columns = ["name", "id", "data"]

    # validations
    val.validates_constraints()


class CampaignInfoAPI(CampaignInfo, ValidationMixin):
    """
    Deprecated.
    """
    api_restless_methods = ['GET', 'POST', 'DELETE']
    api_include_columns = ["name", "description", "created_at", "campaign_id", "id"]

    # validations
    val.validates_constraints()


class MapLayerAPI(MapLayer, ValidationMixin):
    """
    Map Layer resource: defines properties of additional map layers for integration into explore interface
    """
    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    # api_exclude_columns = ['rawfiledata', 'iscompressed', 'campaigns', 'data_json', 'user']
    #api_include_columns = ["created_at", "description", "id", "name", "reference", "user_id"]
    api_max_results_per_page = 1000

    api_preprocessors = {
        "POST": ['assert_current_user_is_admin'],
        "PATCH_SINGLE": ['assert_current_user_is_admin'],
        "PATCH_MANY": ['assert_current_user_is_admin'],
        "DELETE_SINGLE": ['assert_current_user_is_admin']
    }

    # validations
    val.validates_constraints()


class PlatformMapLayerAPI(PlatformMapLayer, ValidationMixin):
    """
    Platform Map Layer resource: defines properties of additional map layers for each platform filtered by configurable parameters
    """
    api_build_restless_endpoints = True
    api_restless_methods = ['GET', 'POST', 'DELETE', 'PATCH']
    # api_exclude_columns = ['rawfiledata', 'iscompressed', 'campaigns', 'data_json', 'user']
    api_max_results_per_page = 1000

    api_include_columns = ["id", "name", "baseurl", "layertype", "minzoom", "properties", "platform", "platform.id",
                           "platform.name", "platform.key", "platform.description"]

    api_preprocessors = {
        "POST": ['assert_current_user_is_admin'],
        "PATCH_SINGLE": ['assert_current_user_is_admin'],
        "PATCH_MANY": ['assert_current_user_is_admin'],
        "DELETE_SINGLE": ['assert_current_user_is_admin']
    }

    # validations
    val.validates_constraints()


# Register API function to be called from ...?
# def register_api(apimanager, app):
#     CampaignAPI.register_api(apimanager, app)
#     # CampaignMapAPI.register_api(apimanager, app)
#     # CampaignDeployentsAPI.register_api(apimanager, app)
#     CampaignFileAPI.register_api(apimanager, app)
#     CampaignDeploymentList.register_api(apimanager, app)
#     DeploymentAPI.register_api(apimanager, app)
#     DeploymentEventAPI.register_api(apimanager, app)
#     DeploymentEventTypeAPI.register_api(apimanager, app)
#     CampaignInfoAPI.register_api(apimanager, app)
#     PlatformAPI.register_api(apimanager, app)
#     PoseAPI.register_api(apimanager, app)
#     PoseDataAPI.register_api(apimanager, app)
#     MediaAPI.register_api(apimanager, app)
#     MediaPoseAssociationAPI.register_api(apimanager, app)
#     AnnotationLabelAPI.register_api(apimanager, app)
#     AnnotationPointAPI.register_api(apimanager, app)
#     AnnotationSetAPI.register_api(apimanager, app)
#     TagAnnotationAssociationAPI.register_api(apimanager, app)
#     MediaCollectionAPI.register_api(apimanager, app)
#     MediaCollectionMediaAssociationAPI.register_api(apimanager, app)
#     MediaTypeAPI.register_api(apimanager, app)
#     PlatformMapLayerAPI.register_api(apimanager, app)
#     MapLayerAPI.register_api(apimanager, app)


add_to_api(CampaignAPI)
add_to_api(CampaignFileAPI)
add_to_api(DeploymentFileAPI)
add_to_api(CampaignDeploymentList)
add_to_api(DeploymentAPI)
add_to_api(DeploymentEventAPI)
add_to_api(DeploymentEventTypeAPI)
add_to_api(CampaignInfoAPI)
add_to_api(PlatformAPI)
add_to_api(PoseAPI)
add_to_api(PoseDataAPI)
add_to_api(MediaAPI)
# add_to_api(MediaPoseAssociationAPI)
add_to_api(AnnotationLabelAPI)
add_to_api(AnnotationPointAPI)
add_to_api(AnnotationSetAPI)
add_to_api(AnnotationSetFileAPI)
add_to_api(TagAnnotationAssociationAPI)
add_to_api(MediaCollectionAPI)
add_to_api(MediaCollectionMediaAssociationAPI)
add_to_api(MediaTypeAPI)
add_to_api(PlatformMapLayerAPI)
add_to_api(MapLayerAPI)
add_to_api(DataSourceAPI)
add_to_api(DataSourceTypeAPI)

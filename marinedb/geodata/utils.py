from io import BytesIO  # StringIO
import random
from time import time

import requests
from pandas import DataFrame
import sys
from sqlalchemy.orm.exc import NoResultFound

from marinedb.extensions import db
from marinedb.geodata.models import AnnotationPoint, AnnotationLabel
from modules.datasample.annotations import AnnotationPointSampler


def subsample_media(media_query, sample_params=None):
    if sample_params is not None:
        if sample_params['method'] == 'random':
            return random.sample(media_query.all(), int(sample_params.get('n', 10)))
        elif sample_params['method'] == 'stratified':
            return media_query.all()[::int(sample_params.get('n', 10))]
        elif sample_params['method'] == "userprocess":
            Media = media_query._entity_zero().class_   # get Media class from query object
            Pose = Media.poses.property.mapper.class_   # get related Pose class from Media object
            poses_query = Pose.query.join(media_query.with_entities(Media.id).subquery()).with_entities(Pose.timestamp, Pose.lat, Pose.lon, Pose.media_id)
            poses = [p._asdict() for p in poses_query.all()]
            # TODO: SAMPLE POSES BASED ON POSITION
            # each pose has timestamp, lat, lon, media_id
            poses = random.sample(poses, int(sample_params.get('n', 5)))
            # /TODO
            media_ids = list(set([p.get("media_id") for p in poses]))
            new_media_query = Media.query.filter(Media.id.in_(media_ids))
            print(f"*** Converted {media_query.count()} media items into {poses_query.count()} poses "
                  f"and sampled {len(poses)} poses returning {new_media_query.count()} media items")
            return new_media_query.all()
    else:
        return media_query.all()


def get_points(media, annotation_set):  #, return_types=["points", "frame"]):
    point_list = media.annotations.filter(AnnotationPoint.annotation_set_id == annotation_set.id).all()
    # request_key = "{}-{}".format(annotation_set.id, media.id)
    # TODO: globally prevent deplicate requests to avoid duplicate creation requests
    if len(point_list) <= 0:  # if no annotations, create empty annotations
        data = annotation_set.data
        # TODO: handle media type!
        xys = AnnotationPointSampler().get_points(data.get('type', None), **data.get('params', {}))
        for xy in xys:
            point = AnnotationPoint(
                media_id=media.id, x=xy['x'], y=xy['y'], annotation_set_id=annotation_set.id,
                labels_per_point=data.get('labels_per_point'), user_id=annotation_set.user_id
            )
            point_list.append(point)
            media.annotations.append(point)

        if data.get('allow_wholeframe', False):
            point = AnnotationPoint(
                media_id=media.id, annotation_set_id=annotation_set.id,
                labels_per_point=data.get('labels_per_frame'), user_id=annotation_set.user_id)
            point_list.append(point)
            media.annotations.append(point)
        db.session.commit()

    return point_list

    # annotation_list = []
    # # TODO: make this use the pagination models, api_to_dict and q constructs from API
    # for a in point_list:  # loop through annotation points
    #     # check if point is frame ot point label and return accordingly
    #     if ("points" in return_types and a.x is not None and a.y is not None) \
    #             or ("frame" in return_types and a.x is None and a.y is None):
    #         a_info = dict(id=a.id, x=a.x, y=a.y, t=a.t, annotations=[], supplementary_annotations=[], data=a.data)
    #         for l in a.annotations:  # loop though labels
    #             # if annotation_set id for label matches annotation_set id for point, add to list of labels
    #             if l.annotation_set_id == annotation_set.id:
    #                 a_info['annotations'].append(
    #                     dict(color=l.color(), label_id=l.label_id, id=l.id, data=l.data))
    #             # if the label comes from a different annotation set, consider it to be a suggestion
    #             elif l.label_id is not None:
    #                 a_info['supplementary_annotations'].append(
    #                     dict(label={"name": l.label.name, "id": l.label_id}, color=l.color(), id=l.id,
    #                          annotation_set={"id": l.annotation_set_id}, data=l.data,
    #                          user={"full_name": l.user.full_name(), "id": l.user_id}))
    #         annotation_list.append(a_info)
    #
    # return annotation_list

def get_ecoregion(lat, lon, fields="*"):
    try:
        if lat and lon:
            url = "https://services.arcgis.com/F7DSX1DSNSiWmOqh/arcgis/rest/services/Marine_Ecoregions_Of_the_World_(MEOW)/FeatureServer/0/query" \
                  "?outFields={fields}&geometry={lon},{lat}&geometryType=esriGeometryPoint&inSR=4326&outSR=4326&f=json" \
                  "&spatialRel=esriSpatialRelIntersects".format(lat=lat, lon=lon, fields=fields)
            r = requests.get(url)
            if r.ok: return r.json()
    except Exception as e:
        pass
    return {}  # just return empty dict if this doesn't work

def deployments2dataframe(include_extras=False):
    from marinedb.geodata.api import Deployment
    import pandas as pd
    query = Deployment.query.filter(db.or_(Deployment.platform_id == 1, Deployment.platform_id == 5))
    result_count = query.count()
    lod = []
    for i, d in enumerate(query.all()):
        print("\rProcessing {}/{} ({:.2f}%)    ".format(i + 1, result_count, float(i + 1) / result_count * 100), end="")
        row = dict(deployment=d.key, campaign=d.campaign.key, platform=d.platform.name,
                   **dict(zip(['lat_start', 'lon_start'], d.latlon)), **d.pose_stats(), **d.pose_stats(extended=True))
        if include_extras:
            row.update(**get_ecoregion(lat=row.get('lat_start'), lon=row.get('lon_start'),
                                       fields="ECOREGION,PROVINCE,REALM,Lat_Zone")["features"][0]["attributes"])
        lod.append(row)
    return pd.DataFrame(lod)
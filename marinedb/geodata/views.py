# -*- coding: utf-8 -*-
import datetime
import json
from time import time

import requests
from flask import Blueprint, request, redirect, url_for, render_template, flash, make_response, \
    send_file, current_app, jsonify, Response, session
from flask.sessions import SecureCookieSessionInterface
from flask_login import login_required, current_user, login_user
from jinja2 import TemplateNotFound

from marinedb.geodata.api import AnnotationSetAPI, MediaAPI
from marinedb.user.models import User
from modules.flask_restless import ProcessingException

blueprint = Blueprint("geodata", __name__, url_prefix='/geodata', static_folder="../static", template_folder="templates")#, template_folder='../templates/geodata'


@blueprint.route("/models/<resource>", defaults={'mode': 'get', 'id': None})
@blueprint.route("/models/<resource>/<mode>", defaults={'id': None})
@blueprint.route("/models/<resource>/<mode>/<id>")
# @login_required
def model_templates(resource, mode, id):
    params = dict(list(request.args.items()))
    query_string = request.query_string
    try:
        return render_template("model_templates/"+resource+"/"+mode+".html", id=id, params=params, query_string=query_string, resource=resource, mode=mode)
    except TemplateNotFound as e:
        return render_template("model_templates/example_model/"+mode+".html", id=id, params=params, query_string=query_string, resource=resource, mode=mode)


@blueprint.route("/explore")
def explore():
    args = request.args.to_dict()
    action_messages = {"newcollection": "Use the options below to select a dataset, apply filters and create a Collection."}
    if args.get('action') in action_messages:
        flash(action_messages[args.get('action')], 'info')
    return render_template("geodata/explore.html", params=args, current_datetime=datetime.datetime.now())


# @blueprint.route("/collection")
# @login_required
# def collection():
#     return render_template("geodata/collection.html", params=request.args.to_dict())

@blueprint.route("/annotation_set/<int:annotation_set_id>")
def annotation_set(annotation_set_id):
    args = request.args.to_dict()
    data = AnnotationSetAPI.get_by_id(annotation_set_id)
    if data is None:
        raise ProcessingException(description="Not found", code=404)
    return render_template("geodata/annotation_set.html", args=args, data=data.instance_to_dict())


@blueprint.route("/annotate/<int:media_id>/<int:annotation_set_id>")
def annotate_media(media_id, annotation_set_id):
    args = request.args.to_dict()
    media_data = MediaAPI.get_by_id(media_id).instance_to_dict()
    annotation_set_data = AnnotationSetAPI.get_by_id(annotation_set_id).instance_to_dict()
    # add point_ids list to preselect point for annotation window loading
    try: args['point_ids'] = json.loads(args.get("point_ids")) if "point_ids" in args else []
    except: args['point_ids'] = []
    return render_template("geodata/annotate_media.html", args=args, media=media_data, annotation_set=annotation_set_data, rendered_at=int(time()))

@blueprint.route("/frame_player/<src>/<action>", methods=["GET", "POST"])
@blueprint.route("/frame_player/<action>", methods=["GET", "POST"], defaults={"src": "framegrabber"})
@blueprint.route("/frame_player", defaults={"action": None, "src": "framegrabber"}, methods=["GET", "POST"])
@login_required
def frame_player(action, src="framegrabber"):
    if action is not None:
        url = current_app.config.get("FRAME_LOGGER_ACTION").get(src) \
            if isinstance(current_app.config.get("FRAME_LOGGER_ACTION"), dict) \
            else current_app.config.get("FRAME_LOGGER_ACTION")
        if url is not None:
            try:
                url = url.format(action=action)
                if request.method == "POST":
                    data = request.get_json()
                    data["remote_addr"] = request.remote_addr
                    response = requests.post(url, json=data)
                else:   #if request.method == "GET":
                    params = request.args.to_dict()
                    params["remote_addr"] = request.remote_addr
                    response = requests.get(url, params=params) #, timeout=2.0)  # timeout after 1 second
                return Response(response=response.content, status=response.status_code, content_type=response.headers['Content-Type'])
            except Exception as e:
                return jsonify({"status": "error", "message": "Cannot reach {} ({})".format(url, e)}), 503
        return jsonify({"status": "error", "message": "URL for {} not configured".format(action)}), 404
    else:
        return render_template("geodata/frame_player.html")

@blueprint.route("/sqcapture")
def sqcapture():
    qsargs = dict(request.args.to_dict())
    if qsargs.get("api_token", False):
        if not current_user.is_authenticated():
            user = User.query.filter_by(api_token=qsargs.get("api_token", False))
            if user.count():
                login_user(user.one())
        # remove api_token from QS and redirect
        del qsargs["api_token"]
        return redirect(url_for('geodata.sqcapture', **qsargs))
    # session_cookie = SecureCookieSessionInterface().get_signing_serializer(current_app)
    # same_cookie = session_cookie.dumps(dict(session))
    response = render_template("geodata/sqcapture.html")
    # response.headers.add("Set-Cookie", f"my_cookie={same_cookie}; Secure; HttpOnly; SameSite=None; Path=/;")
    return response

@blueprint.route("/event_annotation_list/<int:media_collection_id>/<int:annotation_set_id>")
@login_required
def event_annotation_list(media_collection_id, annotation_set_id):
    return render_template("geodata/partials/event_annotation_list.html", annotation_set_id=annotation_set_id, media_collection_id=media_collection_id)



# import requests
# from flask import Response, stream_with_context
# @blueprint.route("/image_proxy")
# def image_proxy():
#     src = request.args.get("camanProxyUrl")
#     req = requests.get(src, stream=True)
#     return Response(stream_with_context(req.iter_content(2048)), content_type=req.headers['content-type'])

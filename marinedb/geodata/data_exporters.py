from collections import namedtuple

from flask import jsonify
from marinedb.geodata.models import *
from marinedb.geodata.utils import subsample_media
from marinedb.user.models import User
from marinedb.utils import encode_string
from modules.datatransform.models import DataTransformer, export_transformed_query
from modules.flask_restless import ProcessingException
from modules.flask_restless.mixins import docstring_parameter
from modules.flask_restless.search import QueryBuilder, SearchParameters, search
from marinedb.database import db
import pandas as pd
import numpy as np
from datetime import datetime
import time
import json


def nullifnan(value):
    """Convert NaN to None/null. Causes JSON rendering issues - needs to be fixed in import scripts."""
    return None if value is None or np.isnan(value) else value


@docstring_parameter(export_transformed_query.__doc__)
def export_annotations(query, metadata=None, filename_pattern="annotations-{hash}", limit=200000):
        """
        Export [annotations](#annotation) matching query define in `q`.
        Search queries are executed on the [annotation](#annotation) model. This endpoint also supports translating
        to a target `label_scheme` (when possible)

        %s

        Allowed fields include:

        `id`, `tag_names`, `comment`, `label.id`, `label.name`, `label.lineage_names`, `label.uuid`, `label.color`,
        `updated_at`, `user.username`, `needs_review`, `likelihood`,
        `point.id`, `point.x`, `point.y`, `point.t`, `point.data`, `point.media.id`, `point.media.key`,
        `point.media.path_best`, `point.media.timestamp_start`, `point.media.timestamp_start_local`,
        `point.media.path_best_thm`, `point.pose.timestamp`, `point.pose.timestamp_local`, `point.pose.lat`,
        `point.pose.lon`, `point.pose.alt`, `point.pose.dep`, `point.pose.data`, `point.pose.id`,
        `point.media.deployment.id`, `point.media.deployment.key`, `point.media.deployment.name`,`point.polygon`,
        `point.media.deployment.campaign.id`, `point.media.deployment.campaign.key`,
        `point.media.deployment.campaign.name`, `object_id`, `supplementary_label`

        See [annotation](#annotation) model columns, relations and attributes for more information on each of the included fields.

        **TRANSLATING TO OTHER LABEL_SCHEMES**

        This endpoint also supports the translation of labels from a source to a target label_scheme.
        another label_scheme using the semantic translation framework. In order to do this, you need to specify the
        additional `translate` url query parameter:

        ```
        &translate={"target_label_scheme_id":..., "vocab_registry_keys": ..., "mapping_override": ...}
        ```
        Where `target_label_scheme_id` [required] is an INT with the `id` of the target label scheme,
        `vocab_registry_keys` [optional] is a list containing the priority order of keys for `vocab_registries` for
        which to perform the semantic translation and `mapping_override` defines a set of key-value pairs containing
        `source label.id : target label.id` for which to override the translation.
        Note: for extended schemes, labels are translated through `tree_traversal` and no semantic translation is
        required. With translation parameters you will also need to specify additional columns in the `include_columns`
        parameter to obtain the translation output.

        Additional allowed columns include:

        `label.translated.id`, `label.translated.name`, `label.translated.lineage_names`,
        `label.translated.uuid`, `label.translated.translation_info`,

        These columns are ignored if no `translate` query parameter is supplied.
        """
        # Also possible to us `c` instead of `include_columns`
        # /api/annotation_set/2194/export?c=point.media_id&c=point.media.key&c=id&c=point.pose.timestamp&c=point.pose.lat&c=point.pose.lon&c=point.pose.alt&c=point.pose.dep&c=point.pose.data&c=point.media.path_best&c=point.media.path_best_thm&c=label.id&c=label.name&c=label.lineage_names&c=comment&c=updated_at&c=user.email&c=point.x&c=point.y&c=point.t&c=point.id&c=point.data&f={"operations":[{"module":"pandas","method":"json_normalize"}]}&disposition=inline&template=dataframe.csv
        allowed_columns = [
            "id", "tag_names", "comment", "label.id", "label.name", "label.lineage_names", "label.uuid", "label.color",
            "supplementary_label", "updated_at", "user.username", "object_id", "needs_review", "likelihood",
            "point.id", "point.x", "point.y", "point.t", "point.data", "point.media.id", "point.media.key",
            "point.polygon", "annotation_set.id", "annotation_set.name", "media_collection.id", "media_collection.name",
            "point.media.path_best", "point.media.timestamp_start", "point.media.timestamp_start_local",
            "point.media.path_best_thm", "point.pose.timestamp", "point.pose.timestamp_local",
            "point.pose.lat", "point.pose.lon", "point.pose.alt", "point.pose.dep", "point.pose.data", "point.pose.id",
            "point.media.deployment.campaign.id", "point.media.deployment.campaign.key",
            "point.media.deployment.campaign.name",
            "point.media.deployment.key", "point.media.deployment.id", "point.media.deployment.name", "point.pixels_tl"]
        allowed_translate_columns = [
            "label.translated.id", "label.translated.name", "label.translated.lineage_names", "label.translated.uuid",
            "label.translated.translation_info"]

        # filename_pattern = "annotations-{hash}"

        extra_ops = []

        # If we're translating, allow the translate columns
        translate = json.loads(request.args.get("translate", "{}") or "{}")
        if translate.get("target_label_scheme_id"):
            allowed_columns += allowed_translate_columns

        return export_transformed_query(query, allowed_columns=allowed_columns,
                                        filename_pattern=filename_pattern, metadata=metadata or dict(),
                                        extra_operations=extra_ops, limit=limit)


def media_collection_export_query(cls, id):
    tic = time.perf_counter()
    output_format = request.args.get("format", "json")
    output_type = request.args.get("file", "text")
    return_info = request.args.getlist("return")
    transform = request.args.get("transform", None)
    filters = request.args.getlist("filter")
    # filename_pattern = "collection-{metadata[name]}-{metadata[server_timestamp]}"
    filename_pattern = "collection-{metadata[user][username]}-{metadata[id]}-{metadata[name]}"

    media_collection = cls.get_by_id(id)
    metadata = {
        "name": media_collection.name,
        "id": media_collection.id,
        "created_at": media_collection.created_at.strftime("%Y%m%dT%H%M%S.%f"),
        "user": {
            "id": media_collection.user_id,
            "username": media_collection.user.username,
            "email": media_collection.user.email
        },
        # "user_id": media_collection.user_id,
        "data": media_collection.data,
        "description": media_collection.description,
        "server_timestamp": datetime.now().strftime("%Y%m%dT%H%M%S.%f"),
        "transform": transform
    }

    def resolve_media_path(row):
        media = namedtuple("Media", "key id")(row["media_key"], row["media_id"])
        deployment = namedtuple("Deployment", "key id")(row["deployment_key"], row["deployment_id"])
        campaign = namedtuple("Campaign", "key id")(row["campaign_key"], row["campaign_id"])
        path = row["path"] if row["path"] is not None \
            else row["campaign_path"].format(campaign=campaign, media=media, deployment=deployment) \
            if row["campaign_path"] is not None else None
        path_thm = row["path_thm"] if row["path_thm"] is not None \
            else row["campaign_path_thm"].format(campaign=campaign, media=media, deployment=deployment) \
            if row["campaign_path_thm"] is not None else None
        return [path, path_thm]

    # build filename (if appropriate)
    filename = filename_pattern.format(metadata=metadata) if output_type != "text" else None

    # initialise data munging sequence, with load
    dt = DataTransformer()

    # base query
    data_query = db.session.query(Media.id.label("media_id"), Media.key.label("media_key")).filter(
        MediaCollectionMediaAssociation.media_id == Media.id,
        MediaCollectionMediaAssociation.media_collection_id == id
    )

    # df from query
    dt.add_operation(module="pandas", method="query_to_dataframe")

    # add events
    if "events" in return_info:
        data_query = data_query.add_columns(DeploymentEventType.name.label("event_type"),
                                            DeploymentEvent.description.label("event"),
                                            DeploymentEvent.created_at.label("event_timestamp")).filter(
            DeploymentEventType.id == DeploymentEvent.deployment_event_type_id,
            DeploymentEvent.media_id == Media.id)

    # add dataset keys
    if "datasetkeys" in return_info or "mediapaths-resolved" in return_info:
        data_query = data_query.add_columns(Campaign.key.label("campaign_key"), Campaign.id.label("campaign_id"),
                                            Deployment.key.label("deployment_key"),
                                            Deployment.id.label("deployment_id"),
                                            Platform.name.label("platform_name"),
                                            Platform.id.label("platform_id")).filter(
            Media.deployment_id == Deployment.id, Campaign.id == Deployment.campaign_id,
            Platform.id == Deployment.platform_id)

    # add media path info and resolve paths if requested
    if "mediapaths" in return_info or "mediapaths-resolved" in return_info:
        data_query = data_query.add_columns(Media.path.label("media_path"), Media.path_thm.label("thm_path"))
        if "mediapaths-resolved" in return_info:
            # Resolve media paths
            data_query = data_query.add_columns(Campaign.media_path_pattern.label("campaign_path"),
                                                Campaign.thm_path_pattern.label("campaign_path_thm"))
            dt.add_operation(module="pandas", method="apply_multicol", columns=["media_path", "thm_path"],
                             columns_func=resolve_media_path, axis=1)
            dt.add_operation(method="drop", labels=["campaign_path", "campaign_path_thm"], axis=1)

    # add pose info
    if "pose" in return_info or "posedata" in return_info:
        data_query = data_query.add_columns(Pose.timestamp.label("pose_timestamp"), Pose.lat.label("pose_lat"),
                                            Pose.lon.label("pose_lon"), Pose.alt.label("pose_alt"),
                                            Pose.dep.label("pose_dep")).filter(
            Pose.media_id == Media.id)

        # add pose data
        if "posedata" in return_info:
            data_query = data_query.add_columns(PoseData.name.label("posedata_key"),
                                                PoseData.value.label("posedata_value")).filter(
                PoseData.pose_id == Pose.id)
            # Add operation to pivot data into correct format
            dt.add_operation(method="fillna", value="")  # need to fill nas for pivot_table
            dt.add_operation(module="pandas", method="pivot_table", columns="posedata_key",
                             values="posedata_value", columns_prefix="posedata_", aggfunc="first")

    # Create response
    dt.add_operation(module="flask", method="make_response_from_dataframe", metadata=metadata,
                     output_format=output_format, filename=filename)

    # Run data transformer
    output = dt.run_all(data_query)
    print(("Processing time: {}s".format(time.perf_counter() - tic)))
    return output


def deployment_export(cls, id):
    tic = time.perf_counter()
    output_format = request.args.get("format", "csv")
    output_type = request.args.get("file", "attachment")
    return_info = request.args.getlist("return")
    # filename_pattern = "deployment-{metadata[deployment_key]}-{metadata[server_timestamp]}"
    filename_pattern = "deployment-{metadata[deployment_key]}-nav"

    assert output_type in ["attachment", "text"], "Unsupported query string parameter (file={})".format(output_type)
    assert output_format in ["csv", "json"], "Unsupported query string parameter (format={})".format(output_format)

    # initialise data munging sequence, with load
    dt = DataTransformer()

    # Create meta data to send back in the response
    deployment = cls.get_by_id(id)
    campaign = deployment.campaign
    metadata = {
        "deployment_name": deployment.name,
        "deployment_key": deployment.key,
        "deployment_id": deployment.id,
        "deployment_created": deployment.created_at.strftime("%Y%m%dT%H%M%S.%f"),
        "campaign_name": campaign.name,
        "campaign_key": campaign.key,
        "campaign_id": campaign.id,
        "campaign_media_path": campaign.media_path_pattern,
        "campaign_thm_path": campaign.thm_path_pattern,
        "campaign_created": campaign.created_at.strftime("%Y%m%dT%H%M%S.%f"),
        "platform_name": deployment.platform.name,
        #"platform_id": deployment.platform.id,
        "server_timestamp": datetime.now().strftime("%Y%m%dT%H%M%S.%f")
    }

    # function resolve paths if path resolve required
    def resolve_media_path(row):
        media = namedtuple("Media", "key id")(row["media_key"], row["media_id"])
        cmp_path = metadata["campaign_media_path"]
        cmp_thm = metadata["campaign_thm_path"]
        if row["path"] is not None:
            path = row["path"]
        else:
            path = cmp_path.format(campaign=campaign, media=media, deployment=deployment)
        if row["path_thm"] is not None:
            path_thm = row["path_thm"]
        else:
            path_thm = cmp_thm.format(campaign=campaign, media=media, deployment=deployment)
        return [path, path_thm]

    # build filename (if appropriate)
    if output_type == "attachment":
        filename = filename_pattern.format(metadata=metadata)
    elif output_type == "text":
        filename = None
    else:
        raise ProcessingException("Unrecognised output file type requested: '{}'".format(output_type))

    # base query
    data_query = db.session.query(Media.id.label("media_id"), Media.key.label("media_key")).filter(
        Media.deployment_id == id)
    dt.add_operation(module="pandas", method="query_to_dataframe")

    # add dataset keys
    if "datasetkeys" in return_info:
        data_query = data_query.add_columns(Campaign.key.label("campaign_key"), Campaign.id.label("campaign_id"),
                                            Deployment.key.label("deployment_key"),
                                            Deployment.id.label("deployment_id"),
                                            Platform.name.label("platform_name"),
                                            Platform.id.label("platform_id")).filter(
            Media.deployment_id == Deployment.id, Campaign.id == Deployment.campaign_id,
            Platform.id == Deployment.platform_id)

    # add events
    if "events" in return_info:
        data_query = data_query.add_columns(DeploymentEventType.name.label("event_type"),
                                            DeploymentEvent.description.label("event")).filter(
            DeploymentEventType.id == DeploymentEvent.deployment_event_type_id, DeploymentEvent.media_id == Media.id)

    # add media path info and resolve paths if requested
    if "mediapaths" in return_info or "mediapaths-resolved" in return_info:
        data_query = data_query.add_columns(Media.path.label("path"), Media.path_thm.label("path_thm"))
        if "mediapaths-resolved" in return_info:
            dt.add_operation(module="pandas", method="apply_multicol", columns=["path", "path_thm"],
                             columns_func=resolve_media_path, axis=1)

    # add pose info
    if "pose" in return_info or "posedata" in return_info:
        data_query = data_query.add_columns(Pose.timestamp.label("pose_timestamp"), Pose.lat.label("pose_lat"),
                                            Pose.lon.label("pose_lon"), Pose.alt.label("pose_alt"),
                                            Pose.dep.label("pose_dep")).filter(
            Pose.media_id == Media.id)
        # add pose data
        if "posedata" in return_info:
            data_query = data_query.add_columns(PoseData.name.label("posedata_key"),
                                                PoseData.value.label("posedata_value")).filter(
                PoseData.pose_id == Pose.id)
            # Add operation to pivot data into correct format
            dt.add_operation(method="fillna", value="")  # need to fill nas for pivot_table
            dt.add_operation(module="pandas", method="pivot_table", columns="posedata_key",
                             values="posedata_value", columns_prefix="posedata_", aggfunc="first")

    # # add path info
    # if "mediapaths" in return_info:
    #     data_query = data_query.add_columns(Media.path_best.label("path_pattern"), Media.path_best_thm.label("path_pattern_thm"))
    # # Add dataset keys and IDS
    # # TODO: this is broken - conflicts with "mediapaths" hybrid_property in Media
    # # For now, if elif to avoid conflicts
    # elif "datasetkeys" in return_info:
    #     data_query = data_query.add_columns(Campaign.key.label("campaign_key"), Campaign.id.label("campaign_id"),Deployment.key.label("deployment_key"), Deployment.id.label("deployment_id"),Platform.name.label("platform_name"), Platform.id.label("platform_id")).filter(Media.deployment_id == Deployment.id,Campaign.id == Deployment.campaign_id,Platform.id == Campaign.platform_id)

    # CSV
    # if output_format == "csv":
    #     filename = "deployment-{metadata[deployment_key]}-{metadata[server_timestamp]}.csv"
    #     dt.add_operation(method="to_csv")
    #     dt.add_operation(method="flask.make_csv_response", kwargs={"filename": filename, "metadata": metadata})
    # # JSON
    # elif output_format == "json":
    #     dt.add_operation(method="to_dict", kwargs={"orient": "records"})
    #     dt.add_operation(method="flask.make_json_response", kwargs={"metadata": metadata})

    # response
    dt.add_operation(module="flask", method="make_response_from_dataframe",
                     metadata=metadata, output_format=output_format, filename=filename)
    output = dt.run_all(data_query)

    print(("Processing time: {}s".format(time.perf_counter() - tic)))

    return output


# def media_collection_addmedia(cls, id):
#     """
#     MediaCollection add media resource
#
#     Add media to a media collection.
#
#     :param URL: int: id: ID of collection
#     :param GET: json: q: restless search query
#     :returns PATCH: json: result of query
#
#     """
#     # """
#     # Add media to a media collection. Using Flask-Restless QueryBuilder to construct queries using same format as
#     # "media" endpoint with an addition parameter, "sample" which defines how to subset the
#     # Construct a query for Media using Flask-Restless QueryBuilder to have the same format as API for querying media.
#     # <br><br>
#     # """
#     max_stored_queries = 10
#     # parameter to select whether to replace or append
#     append = request.args.get('mode') == "append"
#     media_collection = MediaCollection.get_by_id(id)
#
#     # Construct query
#     q = json.loads(request.args['q'])
#     q_orig = json.loads(request.args['q'])
#     sample_params = q.pop('sample', None)
#
#     searchparams = SearchParameters.from_dictionary(q)
#     query = QueryBuilder.create_query(db.session, Media, searchparams)
#
#     # Subsample filtered query
#     media = subsample_media(query.all(), sample_params=sample_params)
#
#     # Update media collection and set custom data attribute
#     data = media_collection.data
#     if append:
#         media = media + media_collection.media
#         creation_queries = data['creation_queries'] if 'creation_queries' in data else []
#         if 'count' in creation_queries:
#             creation_queries['count'] += 1
#         elif len(creation_queries) >= max_stored_queries:
#             creation_queries = {'count': max_stored_queries + 1}
#         else:
#             creation_queries.append(q_orig)
#         if len(creation_queries) > max_stored_queries or isinstance(creation_queries, str):
#             creation_queries = 'lost count: > %d' % max_stored_queries
#     else:
#         creation_queries = [q_orig]
#         # media = query.all()
#     data['creation_queries'] = creation_queries
#     media_collection.update(media=media, data=data)  # replace query data
#     return jsonify({"query": q, 'number_added': query.count(), "media_collection_name": media_collection.name,
#                     "media_collection_id": media_collection.id})
#
#
# def media_collection_export(cls, id):
#     output_format = request.args.get("format", "json")
#     output_type = request.args.get("file", "text")
#     return_info = request.args.getlist("return")
#     transform = request.args.get("transform", None)
#     filters = request.args.getlist("filter")
#     filename_pattern = "collection-{metadata[user][username]}-{metadata[id]}-{metadata[name]}"
#
#     media_collection = cls.get_by_id(id)
#     metadata = {
#         "name": encode_string(media_collection.name),
#         "id": media_collection.id,
#         "created_at": media_collection.created_at.strftime("%Y%m%dT%H%M%S.%f"),
#         "user": {
#             "id": media_collection.user_id,
#             "username": encode_string(media_collection.user.username),
#             "email": encode_string(media_collection.user.email)
#         },
#         "data": media_collection.data,
#         "description": encode_string(media_collection.description),
#         "server_timestamp": datetime.now().strftime("%Y%m%dT%H%M%S.%f"),
#         "transform": transform
#     }
#
#     dt = DataTransformer()
#
#     # Get list of Media in Collection
#     # TODO: incorporate more sophisticated filtering options to match query construct from API
#     media_collection_media = Media.query.filter(MediaCollectionMediaAssociation.media_id == Media.id,
#                                                 MediaCollectionMediaAssociation.media_collection_id == id)
#     if "has-event" in filters:
#         # get list of media in collection that has events
#         media_collection_media = media_collection_media.filter(Media.id == DeploymentEvent.media_id)
#
#     media_items = []
#     for m in media_collection_media.all():
#         pose = m.pose  # Only use first pose  TODO: consider multi-pose options?
#         media_info = {"media_id": m.id, "media_key": encode_string(m.key)}  # media info
#         if "mediapaths" in return_info:
#             media_info.update({"media_path": encode_string(m.path_best), "thm_path": encode_string(m.path_best_thm)})
#         if "pose" in return_info:
#             media_info.update({"pose_timestamp": pose.timestamp, "pose_lat": pose.lat, "pose_lon": pose.lon, "pose_alt":pose.alt, "pose_dep":pose.dep})  #pose info
#         if "posedata" in return_info:
#             media_info.update({"posedata_"+pdata.name: pdata.value for pdata in pose._data})  # pose data
#         if "datasetkeys" in return_info:
#             media_info.update({"campaign_key": encode_string(m.deployment.campaign.key),
#                                "deployment_key": encode_string(m.deployment.key),
#                                "platform": encode_string(m.deployment.platform.name)})
#         if "events" in return_info:
#             if len(m.events) <= 0:
#                 media_items.append(media_info)
#             else:
#                 for e in m.events: # if there are multiple events, make copies and add new entries
#                     media_info_event = media_info.copy()
#                     media_info_event.update({"event_type": encode_string(e.deployment_event_type.name),
#                                              "event": encode_string(e.description),
#                                              "event_timestamp": e.timestamp})
#                     media_items.append(media_info_event)
#         else:
#             media_items.append(media_info)
#
#     dt.add_operation(module="pandas", method="from_dict")
#
#     # Create response
#     filename = filename_pattern.format(metadata=metadata) if output_type != "text" else None
#     dt.add_operation(module="flask", method="make_response_from_dataframe", metadata=metadata, output_format=output_format, filename=filename)
#     #dt.add_operation(module="flask", method="make_json_response", metadata=metadata, filename=filename, output_format=output_format)
#
#     # Run data transformer
#     output = dt.run_all(media_items)
#     return output
#
#
# def annotation_set_export(cls, id, output_type="text", output_format="json", transform="points", validation_id=None):
#     tic = time.perf_counter()
#     output_format = request.args.get("format", output_format)
#     output_type = request.args.get("file", output_type)
#     return_info = request.args.getlist("return")
#     transform = request.args.get("transform", transform) or transform
#     validation_id = request.args.get("validation_id", validation_id)
#     filters = request.args.getlist("filter")
#     q = json.loads(request.args.get("q", "{}") or "{}")
#     filename_pattern = "annotations-uid{metadata[user][id]}-{metadata[media_collection][name]}-{metadata[name]}-{metadata[id]}-{metadata[transform]}"
#     timestamp_format = "%Y-%m-%dT%H:%M:%S.%fZ"
#
#     assert validation_id is not None if transform=="validate" else True, "For transform=validate, you need to supply a validation_id"
#
#     # initialise data munging sequence, with load
#     dt = DataTransformer()
#
#     # Annotation set information
#     annotation_set = cls.get_by_id(id)
#     metadata = annotation_set.instance_to_dict()
#     metadata.update(dict(transform=transform, media_count=annotation_set.media_collection.media.count(),
#                          point_count=AnnotationPoint.query.filter(AnnotationPoint.annotation_set_id==id).count(),
#                          server_timestamp=datetime.utcnow().strftime(timestamp_format)))
#
#     # build filename (if appropriate)
#     if output_type == "attachment":
#         filename = filename_pattern.format(metadata=metadata)
#     elif output_type == "text":
#         filename = None
#     else:
#         raise ProcessingException("Unrecognised output file type requested: '{}'".format(output_type))
#
#     if transform == "classtally":
#         annotations = db.session.query(Label.name.label("label_name"), Label.color, Label.id.label("label_id"),
#             func.count(Label.id).label('count')).filter(AnnotationLabel.label_id == Label.id,
#             AnnotationLabel.annotation_set_id == id, AnnotationLabel.annotation_set_id == AnnotationSet.id) \
#             .group_by(Label.name, Label.color, Label.id).order_by(text("count DESC"))
#         # df from query
#         dt.add_operation(module="pandas", method="query_to_dataframe")
#     elif transform == "deploymenttally":
#         annotations = db.session.query(
#             Deployment.name.label("deployment_name"), Deployment.key.label("deployment_key"), Campaign.color,
#             Deployment.id.label("deployment_id"), func.count(AnnotationLabel.label_id).label('count')
#         ).filter(
#             AnnotationLabel.point_id == AnnotationPoint.id, AnnotationPoint.media_id == Media.id,
#             Media.deployment_id == Deployment.id,AnnotationLabel.annotation_set_id == id,
#             Deployment.campaign_id == Campaign.id, AnnotationLabel.annotation_set_id == AnnotationSet.id,
#         ).group_by(Deployment.name, Deployment.key, Deployment.id, Campaign.color).order_by(text("count DESC"))
#         # df from query
#         dt.add_operation(module="pandas", method="query_to_dataframe")
#     else:
#
#         # run search query for annotations
#         annotation_labels = search(db.session, AnnotationLabel, q).filter(AnnotationLabel.annotation_set_id == id)
#         # annotation_labels = AnnotationLabel.query.filter(AnnotationLabel.annotation_set_id == id)
#
#         if "has-label" in filters:
#             annotation_labels = annotation_labels.filter(AnnotationLabel.label_id != None)
#
#         if "has-xy" in filters:
#             annotation_labels = annotation_labels.filter(
#                 AnnotationPoint.x != None,
#                 AnnotationPoint.y != None,
#                 AnnotationLabel.point_id == AnnotationPoint.id
#             )
#         elif "doesnthave-xy" in filters:
#             annotation_labels = annotation_labels.filter(
#                 AnnotationPoint.x == None,
#                 AnnotationPoint.y == None,
#                 AnnotationLabel.point_id == AnnotationPoint.id
#             )
#
#         # if "has-event" in filters:
#         #     # get list of media in collection that has events
#         #     media_collection_media = media_collection_media.filter(Media.id == DeploymentEvent.media_id)
#
#         # Conditional returns based on data transforms
#         # remove labelinfo adn add pointinfo if validation
#         if transform == "validate":
#             if "labelinfo" in return_info:
#                 del return_info[return_info.index("labelinfo")]
#             if "pointinfo" not in return_info:
#                 return_info.append("pointinfo")
#
#         # remove point info if aggregation
#         elif transform == "aggregate" and "pointinfo" in return_info:
#             del return_info[return_info.index("pointinfo")]
#
#
#         # Create list of annotations
#         # TODO: DO THIS USING A QUERY INSTEAD OF ITERATING THROUGH ORM OBJECT!!!
#         annotations = []
#
#         # if output_format == "json":
#         #     paginated_results = APIModel.paginated_results(annotation_set, return_dicts=False)
#         #     annotation_labels =
#
#         for a in annotation_labels.all():  # annotation_set.annotations:
#             media = a.point.media
#             pose = media.pose  # Only use first pose  TODO: consider multi-pose options?
#             annotation = {
#                 "media_id": media.id,
#                 "media_key": encode_string(media.key),  # fix in case there are invalid chars
#                 "class_id": a.label_id,
#                 "label_id": a.id
#             }
#
#             # Conditional columns based on requested returns
#             if "pose" in return_info:
#                 annotation.update(
#                     {"pose_timestamp": pose.timestamp.strftime(timestamp_format), "pose_lat": pose.lat,
#                      "pose_lon": pose.lon, "pose_alt": pose.alt, "pose_dep": pose.dep})
#             if "posedata" in return_info:
#                 annotation.update({"posedata_" + d.name: d.value for d in pose._data})  # pose data
#             if "mediapaths" in return_info:
#                 annotation.update({"media_path": encode_string(media.path_best),
#                                    "thm_path": encode_string(media.path_best_thm)})
#             if "datasetkeys" in return_info:
#                 annotation.update(
#                     {"campaign_key": encode_string(media.deployment.campaign.key),
#                      "deployment_key": encode_string(media.deployment.key),
#                      "platform": encode_string(media.deployment.platform.name)})
#             # if "lineage" in return_info:
#             #     annotation.update({"class_lineage": encode_string(a.label.lineage_names()) if a.label_id is not None else None})  # pose data
#             if "labelinfo" in return_info:
#                 if a.label_id is not None:
#                     annotation.update({
#                         "class_name": encode_string(a.label.name) if a.label_id is not None else None,
#                         "class_name_path": encode_string(a.label.name_path) if a.label_id is not None else None,
#                         "tags": ",".join([encode_string(t.tag.name) for t in a.annotation_tags]),
#                         "comment": encode_string(a.comment),
#                         #"label_timestamp": a.created_at.strftime("%Y/%m/%dT%H:%M:%S.%fZ"),
#                         "class_uuid": str(a.label.uuid),
#                         "class_color": a.label.color
#                     })
#                 if transform != "aggregate":
#                     annotation.update({"label_timestamp": a.created_at.strftime("%Y/%m/%dT%H:%M:%S.%fZ")})
#             if "labelemail" in return_info:
#                 annotation.update({"labeler": encode_string(a.user.email)})
#             if "pointinfo" in return_info:
#                 annotation.update({
#                     "x": a.point.x,
#                     "y": a.point.y,
#                     "t": a.point.t,
#                     "point_id": a.point.id,
#                     "point_data": a.point.data_json
#                 })
#             annotations.append(annotation)
#
#         # Add op to create data frame from list of dicts
#         dt.add_operation(module="pandas", method="from_dict")
#
#         # Add additional data operations if annotations is not empty
#         if len(annotations) > 0:
#             # Prepare export and munge
#             # fix formatting of class_id
#             dt.add_operation(module="pandas", method="column_operation", column="class_id", operation="apply", args=[lambda x: int_or_empty(x)])
#
#             if transform == "validate" and validation_id is not None:
#                 # merge labels from validation set
#                 query_points_val = db.session.query(
#                     AnnotationPoint.id.label("point_id"), AnnotationLabel.label_id.label("class_id_val")).filter(
#                     AnnotationSet.id == validation_id, AnnotationSet.id == AnnotationLabel.annotation_set_id,
#                     AnnotationPoint.id == AnnotationLabel.point_id
#                 )
#                 if "labelemail" in return_info:
#                     query_points_val = query_points_val.add_columns(User.email.label("labeler_val")).filter(User.id == AnnotationLabel.user_id)
#
#                 #val_df = pd.read_sql(query_points_val.statement, query_points_val.session.bind)
#                 val_df = dt.transform_data(query_points_val, "query_to_dataframe", "pandas")
#                 dt.add_operation(module="pandas", method="merge", right=val_df, how="left", on="point_id")
#
#                 # move columns to end
#                 dt.add_operation(module="pandas", method="move_column", column="class_id")
#                 dt.add_operation(module="pandas", method="move_column", column="class_id_val")
#
#                 # reformat to int for convenience (displays as float for some reason)
#                 dt.add_operation(module="pandas", method="column_operation", column="class_id_val", operation="apply", args=[lambda x: int_or_empty(x)])
#
#             # if output_format == "csv" or transform == "aggregate":
#             #     # merge tags if outputting to CSV
#             #     if "labelinfo" in return_info:
#             #         dt.add_operation(module="pandas", method="column_operation", column="tags", operation="str.join", args=[","])
#             #         dt.add_operation(module="pandas", method="column_operation", column="comment", operation="fillna", kwargs=dict(value=""))
#
#             # aggregate columns
#             if transform == "aggregate":
#                 # if output_format == "json":
#                 #     dt.add_operation(method="reset_index")
#
#                 cols = ['class_id', 'class_name', 'class_name_path', 'class_uuid', 'class_color', 'comment', 'tags'] if "labelinfo" in return_info else ['class_id']
#                 dt.add_operation(method="fillna", value="-")
#                 if output_format == "json":
#                     dt.add_operation(module="pandas", method="groupby_count")
#                 else:
#                     dt.add_operation(module="pandas", method="count_unstack", columns=cols)
#
#                 # TODO: THIS IS NOT WORKING YET
#                 # if "aggregate-allclasses" in output_format:
#                 #     all_labels = [{'class_name':tg.name, 'class_id':tg.id, 'tags':tags_delim.join([t.name for t in tg.tags]), 'comment':''} for tg in annotation_set.label_scheme.labels]
#                 #     classlist = pd.MultiIndex.from_dict(all_labels)
#                 ##     query_labels = db.session.query(Label.name.label("class_name"), Label.id.label("class_id")).filter(Label.label_scheme_id==annotation_set.label_scheme_id).order_by(Label.name)
#                 ##     all_labels = pd.read_sql(query_labels.statement, query_labels.session.bind)
#                 ##     classlist = map(lambda t: (t, ''), list(all_labels['class_name']))  # Get classlist & add empty comment
#                 ##     classlist = pd.MultiIndex.from_tuples(classlist, names=['class_name', 'comment'])
#                 ##     df_points = pd.concat([output.reindex(columns=classlist),  # Full list of classes, NO COMMENTS
#                 ##         output.reindex(columns=(output.columns + classlist) - classlist)],  # ONLY modified classes
#                 ##         axis=1)
#
#     # Create response
#     dt.add_operation(module="flask", method="make_response_from_dataframe", metadata=metadata,
#                      output_format=output_format, filename=filename)
#
#     output = dt.run_all(annotations)
#     print(("Processing time: {}s".format(time.perf_counter() - tic)))
#     return output
#
#
# def int_or_empty(val):
#     try:
#         return int(val)
#     except Exception:
#         return ""
#
#
# def annotation_set_export_query(cls, id):
#     tic = time.perf_counter()
#     output_format = request.args.get("format", "json")
#     output_type = request.args.get("file", "text")
#     return_info = request.args.getlist("return")
#     transform = request.args.get("transform", None)
#     validation_id = request.args.get("validation_id", None)
#     filters = request.args.getlist("filter")
#     # filename_pattern = "annotations-{metadata[media_collection_name]}-{metadata[name]}-{metadata[server_timestamp]}"
#     filename_pattern = "annotations-{metadata[user][username]}-{metadata[media_collection][name]}-{metadata[id]}-{metadata[name]}-{metadata[transform]}"
#
#     assert validation_id is not None if transform == "validate" else True, "For transform=validate, you need to supply a validation_id"
#
#     # initialise data munging sequence, with load
#     dt = DataTransformer()
#
#     annotation_set = AnnotationSet.get_by_id(id)
#     metadata = {
#         "name": annotation_set.name,
#         "id": annotation_set.id,
#         "created_at": annotation_set.created_at.strftime("%Y%m%dT%H%M%S.%f"),
#         "media_collection_name": annotation_set.media_collection.name,
#         "media_collection_id": annotation_set.media_collection_id,
#         "user_id": annotation_set.user_id,
#         "data": annotation_set.data,
#         "description": annotation_set.description,
#         "label_scheme_id": annotation_set.label_scheme_id,
#         "label_scheme_name": annotation_set.label_scheme.name,
#         "server_timestamp": datetime.now().strftime("%Y%m%dT%H%M%S.%f"),
#         "transform": transform
#     }
#
#     # build filename (if appropriate)
#     filename = filename_pattern.format(metadata=metadata) if output_type != "text" else None
#
#     # conditional requirements for import
#     if "mediapaths" in return_info and "datasetkeys" not in return_info:
#         return_info.append("datasetkeys")
#
#     # df from query
#     dt.add_operation(module="pandas", method="query_to_dataframe")
#
#     # base query
#     data_query = db.session.query(Media.id.label("media_id"), Media.key.label("media_key"),
#         AnnotationPoint.id.label("point_id"), AnnotationLabel.label_id.label("class_id")).filter(
#         Media.id == AnnotationPoint.media_id, AnnotationPoint.id == AnnotationLabel.point_id,
#         AnnotationLabel.annotation_set_id == id, AnnotationPoint.annotation_set_id == id
#     )
#
#     if "labelinfo" in return_info:
#         data_query = data_query.add_columns(Label.name.label("class_name"), AnnotationLabel.comment.label("comment"),
#             #Tag.name.label("tags"),   # TODO: FIX - ADDING THIS CAUSES REPEATED ROW PER TAG
#             ).filter(Tag.id == TagAnnotationAssociation.tag_id,
#             AnnotationLabel.id == TagAnnotationAssociation.annotation_id,
#             AnnotationLabel.label_id == Label.id)
#         # dt.add_operation(module="pandas", method="groupby_concat", column="tags", delimeter=",")
#
#     if "labelemail" in return_info:
#         data_query = data_query.add_columns(User.email.label("labeler")).filter(User.id == AnnotationLabel.user_id)
#
#     if "pointinfo" in return_info:
#         data_query = data_query.add_columns(AnnotationPoint.x, AnnotationPoint.y, AnnotationPoint.t, AnnotationPoint.id.label("point_id"))
#
#     # add pose info
#     if "pose" in return_info:
#         data_query = data_query.add_columns(Pose.timestamp.label("pose_timestamp"), Pose.lat.label("pose_lat"),
#             Pose.lon.label("pose_lon"), Pose.alt.label("pose_alt"), Pose.dep.label("pose_dep")
#         ).filter(Pose.media_id == Media.id)
#
#     # add pose data
#     if "posedata" in return_info:
#         data_query = data_query.add_columns(PoseData.name.label("posedata_key"),
#             PoseData.value.label("posedata_value")).filter(PoseData.pose_id == Pose.id)
#         # Add operation to pivot data into correct format
#         dt.add_operation(module="pandas", method="pivot_table",
#                          columns="posedata_key", values="posedata_value", columns_prefix="data_")
#
#     #
#     if "datasetkeys" in return_info or "mediapaths" in return_info:
#         data_query = data_query.add_columns(Campaign.key.label("campaign_key"), Campaign.id.label("campaign_id"),
#             Deployment.key.label("deployment_key"), Deployment.id.label("deployment_id"),
#             Platform.name.label("platform_name"), Platform.id.label("platform_id")).filter(
#             Media.deployment_id == Deployment.id, Campaign.id == Deployment.campaign_id,
#             Platform.id == Deployment.platform_id)
#
#     # add path info
#     if "mediapaths" in return_info:
#         def resolve_media_path(row):
#             media = namedtuple("Media", "key id")(row["media_key"], row["media_id"])
#             deployment = namedtuple("Deployment", "key id")(row["deployment_key"], row["deployment_id"])
#             campaign = namedtuple("Campaign", "key id")(row["campaign_key"], row["campaign_id"])
#             if row["path"] is not None:
#                 path = row["path"]
#             else:
#                 path = row["campaign_path"].format(campaign=campaign, media=media, deployment=deployment)
#             if row["path_thm"] is not None:
#                 path_thm = row["path_thm"]
#             else:
#                 path_thm = row["campaign_path_thm"].format(campaign=campaign, media=media, deployment=deployment)
#             return [path, path_thm]
#
#         # Resolve media paths
#         data_query = data_query.add_columns(Media.path.label("path"), Media.path_thm.label("path_thm"),Campaign.media_path_pattern.label("campaign_path"),Campaign.thm_path_pattern.label("campaign_path_thm"))
#         dt.add_operation(module="pandas", method="apply_multicol", columns=["path", "path_thm"], func=lambda row: pd.Series(resolve_media_path(row)), axis=1)
#         dt.add_operation(method="drop", labels=["campaign_path", "campaign_path_thm"], axis=1)
#
#
#     # Create response
#     dt.add_operation(module="flask", method="make_response_from_dataframe", metadata=metadata, output_format=output_format, filename=filename)
#
#     output = dt.run_all(data_query)
#     print(("Processing time: {}s".format(time.perf_counter() - tic)))
#     return output

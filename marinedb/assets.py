# -*- coding: utf-8 -*-
from flask_assets import Bundle, Environment

css = Bundle(
    #"libs/bootstrap/dist/css/bootstrap-slate.css",
    #"libs/bootstrap/dist/css/bootstrap-superhero.min.css",
    "libs/bootstrap/dist/css/bootstrap-yeti.css",
    "libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css",
    "libs/font-awesome-4.7.0/css/font-awesome.min.css",
    "libs/select2-4.1.0-rc.0/dist/css/select2.min.css",             # required for ajax_select in plugins.js
    "api_libs/annotation/label_scheme.css",
    "css/dataset_select.css",
    "css/codehilight_emacs.css",
    "api_libs/geodata/media.css",
    "libs/trumbowyg-wysiwyg/dist/ui/trumbowyg.css",
    "libs/bootstrap-magnify/css/bootstrap-magnify.css",
    "css/style.css",
    filters="cssmin",
    output="public/css/common.css"
)

js = Bundle(
    "libs/jQuery/dist/jquery.js",
    "libs/bootstrap/dist/js/bootstrap.js",
    "api_libs/annotation/label_scheme.js",
    "js/apiform.js",
    "js/plugins.js",
    # "js/jquery.shorten.js",
    "libs/autocomplete/jquery.autocomplete.js",
    "api_libs/geodata/media2.js",
    "libs/select2-4.1.0-rc.0/dist/js/select2.full.min.js",          # required for ajax_select in plugins.js
    "libs/mustache-3.1.0/mustache.min.js",                          # required for ajax_select in plugins.js
    "js/common_modals.js",
    "libs/trumbowyg-wysiwyg/dist/trumbowyg.js",
    "libs/bootstrap-magnify/js/bootstrap-magnify.js",
    "libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js",
    filters='jsmin',
    output="public/js/common.js"
)

css_annotate = Bundle(
    "api_libs/geodata/annotate_media.css",
    "css/media_gui.css",
    filters='cssmin',
    output="public/css/common-annotate.css"
)

js_annotate = Bundle(
    "api_libs/geodata/annotate_media.js",
    "libs/jquery-mousewheel/jquery.mousewheel.js",
    "js/jquery.multiselect.js",
    filters='jsmin',
    output="public/js/common-annotate.js"
)



css_basic = Bundle(
    "libs/bootstrap/dist/css/bootstrap-yeti.css",
    "libs/font-awesome-4.7.0/css/font-awesome.min.css",
    "css/style.css",
    "css/dataset_select.css",
    filters="cssmin",
    output="public/css/common-basic.css"
)

js_basic = Bundle(
    "libs/jQuery/dist/jquery.js",
    "libs/bootstrap/dist/js/bootstrap.js",
    "js/plugins.js",
    "js/common_modals.js",
    filters='jsmin',
    output="public/js/common-basic.js"
)

assets = Environment()

assets.register("js_all", js)
assets.register("css_all", css)
assets.register("js_basic", js_basic)
assets.register("css_basic", css_basic)
assets.register("js_annotate", js_annotate)
assets.register("css_annotate", css_annotate)

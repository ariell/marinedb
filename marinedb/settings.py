# -*- coding: utf-8 -*-
import os
#from flask import request
import tempfile


class Config(object):
    #HOST_DOMAIN = "http://localhost:5000"
    APP_DIR = os.path.abspath(os.path.dirname(__file__))  # This directory
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    BCRYPT_LOG_ROUNDS = 13
    ASSETS_DEBUG = False   # True = Don't bundle/minify static assets
    API_DOC_DIR = os.path.join(PROJECT_ROOT, "docs", "api", "json")
    DEBUG_TB_ENABLED = False  # Disable Debug toolbar
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    CACHE_TYPE = 'simple'  # Can be "memcached", "redis", etc.
    SECRET_KEY = os.environ.get('MDB_SECRET_KEY', 'highly-secure')
    MEDIA_UPLOAD_EXTENSIONS = ['.png', '.jpg', '.jpeg']
    MEDIA_UPLOAD_URL = "/static/media/{path}"   # localhost, can either be changed or setup as reverse proxy in apache
    MEDIA_THM_CACHE_URL = "/api/media/{media.id}/thumbnail?size=350"
    MEDIA_THM_ALLOWED_SIZES = [200, 400, 800, 2048]
    MEDIA_THM_DEFAULT_SIZE = 400
    MEDIA_UPLOAD_PATH = "{deployment.platform.key}/{deployment.campaign.key}/{deployment.key}/{filename}"
    MEDIA_THM_CACHE_PATH = "{media.deployment.platform_id}/{media.deployment.campaign_id}/{media.deployment_id}/{media.id}_{params}.jpg"
    MEDIA_THM_BBOX_DEFAULT_SIZE = 0.2     # size of default point cropbox as a proportion of the image size
    MEDIA_THM_POINT_DEFAULT_SIZE = 0.015   # size of point circle overlay as a proportion of the image size
    PROJECT_NAME = os.environ.get('MDB_PROJECT_NAME', 'MDB+')
    INSTANCE_ID = "<a href='http://greybits.com.au' target='_blank'>GREYBITS</a>"
    MEDIA_UPLOAD_DIR = os.path.join(PROJECT_ROOT,"DATA/media/")
    MEDIA_THM_CACHE_DIR = os.path.join(PROJECT_ROOT,"DATA/thumbnails/")
    MEDIA_PLACEHOLDER_IMG = os.path.join(APP_DIR,"static/images/placeholder.jpg")
    CACHE_DIR = os.path.join(PROJECT_ROOT, "_cache")
    CACHE_DEFAULT_TIMEOUT = 60*60*24*30    # default cache timeout = 30 days
    IN_MAINTENANCE_MODE = False
    HUMANIZE_USE_UTC = True
    SHOW_STATS_ON_HOME = True
    LABEL_SCHEME_LAZY_LOAD_THRESH = 10      # the number of labels above which enforces lazy loading
    LABEL_SCHEME_MAX_SEARCH_RESULTS = 1000   # the maximum number of labels to return in a search

    # Wiki
    WIKI_DOC_DIR = os.path.join(PROJECT_ROOT, "sqwiki")
    WIKI_PAGES = {
        "about_info": "info/about",
        "annotation_help": "annotation/annotate_media",
        "media_download_help": "data_export/download_media_objects"
    }

    # Email settings
    MAIL_SERVER = "smtp.sendgrid.net"
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_DEBUG = False
    MAIL_USERNAME = "apikey"
    MAIL_PASSWORD = "set in: settings.cfg"
    MAIL_DEFAULT_SENDER = (PROJECT_NAME, "noreply@squidle.org")
    MAIL_MAX_EMAILS = None
    MAIL_SUPPRESS_SEND = False
    MAIL_ASCII_ATTACHMENTS = False
    MAIL_LOG_TO_DIR = None

    # translation parameters
    TRANSLATION_REGISTRY_KEY_ORDER = ["worms", "caab", "catami"]

    # Analytics
    ANALYTICS_GOOGLE_TAGID = None

    # REMOTE DEPLOYMENTS
    DB_BKP_PATH = os.path.join(PROJECT_ROOT, "dbbackups")
    DB_BKP_TEMPLATE_PATH = os.path.join(DB_BKP_PATH, "dbtemplate")
    DB_BKP_USER = "postgres"

    # # To enable iframe logins
    # SESSION_COOKIE_SAMESITE = 'None'
    # SESSION_COOKIE_SECURE = 'True'

class ProdConfig(Config):
    """Production configuration."""
    ENV = 'prod'
    DEBUG = False
    DEBUG_TB_ENABLED = False  # Disable
    #API_DOC_BUILD = False
    # TODO: this is an issue with fontawesome that needs to be fixed during minification
    #ASSETS_DEBUG = False   #False  # True: Don't bundle/minify static assets, False: minify
    SQLALCHEMY_DATABASE_URI = 'postgresql://localhost/marinedb'.format(Config.SECRET_KEY)


class DevConfig(Config):
    """Development configuration."""
    ENV = 'dev'
    DEBUG = True
    DEBUG_TB_ENABLED = False  # Debug toolbar
    ASSETS_DEBUG = True  # True = Don't bundle/minify static assets
    SQLALCHEMY_DATABASE_URI = 'postgresql://localhost/marinedb'
    # DB_NAME = 'dev.db' # Put the db file in project root
    # DB_PATH = os.path.join(Config.PROJECT_ROOT, DB_NAME)
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///{0}'.format(DB_PATH)


class TestConfig(Config):
    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    BCRYPT_LOG_ROUNDS = 1  # For faster tests
    WTF_CSRF_ENABLED = False  # Allows form testing

# -*- coding: utf-8 -*-
"""Database module, including the SQLAlchemy database object and DB-related
utilities.
"""

import requests
from flask_login import current_user
from sqlalchemy import text, Index, PrimaryKeyConstraint
from sqlalchemy.ext import compiler
from sqlalchemy.schema import DDLElement, PrimaryKeyConstraint

from sqlalchemy.orm import relationship, Query
from sqlalchemy.dialects.postgresql import UUID

from modules.datatransform.models import DataTransformer, get_data_transformer, render_data_transformer
# from modules.datatransform.munge import FileMunge
# # from modules.flask_restless import APIModelEndpoint  #, ProcessingException
from modules.datatransform.utils import DataTransformerException
from modules.flask_restless.views import render_data_template, render_data_response
from .extensions import db
from .compat import basestring
from zlib import compress, decompress
# import sys
from pandas import DataFrame, merge, read_csv, read_excel
# from datetime import datetime
import time
# from sqlalchemy.exc import IntegrityError
# import string
# # import json
# import json
# import re
from sqlalchemy.ext.hybrid import hybrid_property
from flask import request, jsonify, send_file
# from sqlalchemy.orm.exc import NoResultFound
import os
from io import BytesIO  # StringIO
import json
import traceback
from sqlalchemy.orm import backref
from sqlalchemy.schema import UniqueConstraint
from marinedb.compat import StringIO

# Include versioning for models
# https://sqlalchemy-continuum.readthedocs.io/en/latest/intro.html#basics
# Add `__versioned__ = {}` to models that require versioning
from sqlalchemy_continuum import make_versioned
make_versioned()


# Alias common SQLAlchemy names
Column = db.Column
relationship = relationship


class CRUDMixin(object):
    """Mixin that adds convenience methods for CRUD (create, read, update, delete)
    operations.
    """

    @classmethod
    def create(cls, commit=True, **kwargs):
        """Create a new record and save it the database."""
        instance = cls(**kwargs)
        return instance.save(commit=commit)

    @classmethod
    def get_or_create(cls, commit=True, **kwargs):
        """If record exists, return it, otherwise create it!"""
        instance = cls.query.filter_by(**kwargs).first()
        if instance:
            return instance, False
        else:
            instance = cls(**kwargs)
            return instance.save(commit=commit), True
        pass

    def update(self, commit=True, **kwargs):
        """Update specific fields of a record."""
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        """Save the record."""
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True):
        """Remove the record from the database."""
        db.session.delete(self)
        return commit and db.session.commit()


class BetterQuery(Query):
    def quick_count(self):
        count_q = self.statement.with_only_columns([db.func.count()]).order_by(None)
        return self.session.execute(count_q).scalar()

        # count_query = (self.statement.with_only_columns([db.func.count()]).order_by(None))
        # return self.session.execute(count_query).scalar()


class Model(CRUDMixin, db.Model):  #, APIBackupMixin):
    """Base model class that includes CRUD convenience methods."""
    __abstract__ = True
    query_class = BetterQuery   # override query class to include custom methods

    __table_args__ = {'extend_existing': True}

    # def query(self):
    #     pass


# From Mike Bayer's "Building the app" talk
# https://speakerdeck.com/zzzeek/building-the-app
class SurrogatePK(object):
    """A mixin that adds a surrogate integer 'primary key' column named
    `id` to any declarative-mapped class.
    """
    #__table_args__ = {'extend_existing': True}

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, doc="Primary key `id`")

    @classmethod
    def get_by_id(cls, id):
        if any((isinstance(id, basestring) and id.isdigit(), isinstance(id, (int, float))),):
            return cls.query.get(int(id))
        return None


class UUIDMixin(object):
    #__table_args__ = {'extend_existing': True}
    uuid = Column(UUID(as_uuid=True), unique=True, nullable=True, server_default=text('uuid_generate_v4()'),
                  doc="A universally unique identifier for this object")


class SaveCollectionMixin:

    last_save = Column(db.JSON, nullable=True, doc="Sequence of file operations performed at last save")

    def get_relation_collection(self, collection_path):
        class_ = self
        instance = self
        for i in collection_path.split("."):
            instance = getattr(instance, i)
            class_ = getattr(class_, i)
            if hasattr(class_, "mapper"):
                class_ = class_.mapper.class_
                # NOTE: do not need to check permission for collection mapper instances, since that is what we're creating
            elif isinstance(class_, Model):
                class_ = class_.__class__
                # check permission for each Model instance up the tree
                can_edit = instance.current_user_can_edit if hasattr(instance, 'current_user_can_edit') \
                    else (current_user.is_authenticated() and instance.user_id == current_user.id) \
                    if hasattr(instance,'user_id') else False
                if not can_edit:
                    raise PermissionError("You do not have edit permission for the '{}' instance, so you cannot perform this action.".format(class_.__tablename__))
            else:
                raise ValueError("Unknown collection type. Expecting type 'Model' or 'InstrumetedAttribute'")
        return instance, class_

    def dicts_to_database(self, dictlist, collection, update_existing=False, match_on=None, create_missing=True, skip_errors=False):
        instance_collection, collection_class = self.get_relation_collection(collection)
        created = 0
        updated = 0
        errors = 0
        error_rows = []
        count = 0
        if not isinstance(dictlist, list):
            raise TypeError("Unsupported data type '{}'. Bulk save requires list of dicts.".format(type(dictlist)))
        nrows = len(dictlist)
        for r in dictlist:
            count += 1
            if not isinstance(r, dict):
                raise TypeError("Unsupported data type '{}'. Bulk save requires list of dicts.".format(type(r)))
            try:
                if update_existing is True:
                    assert isinstance(match_on, list), \
                        "The `match_on` parameters needs to be a list of fields if `update_existing=True`"
                    filts = [getattr(collection_class, f) == r.get(f) for f in match_on]
                    # print(filts)
                    # print(match_on)
                    # print(collection_class)
                    item = instance_collection.filter(*filts).one_or_none()
                    if item:
                        # print("Found item: {}".format(r))
                        #item.update(commit=False, **r)
                        for attr, value in r.items():
                            setattr(item, attr, value)
                        updated += 1
                    elif create_missing is True:
                        # print("Creating missing")
                        instance_collection.append(collection_class(**r))
                        created += 1
                    else:
                        raise FileNotFoundError("Could not find match to update (match_on: {}".format(match_on))
                else:
                    instance_collection.append(collection_class(**r))
                    created += 1
            except Exception as e:
                errors += 1
                error_rows.append(count)
                print(traceback.format_exc())
                if skip_errors is True:
                    print("Error during batch save. {}, row {}/{}: {}... continuing...".format(e, count, nrows, r))
                else:
                    db.session.rollback()
                    raise DataTransformerException("Error during batch save. {}, row {}/{}: {}".format(e, count, nrows, r))
        return dict(message="Saved {} entries to {}".format(nrows, collection), error_rows=error_rows,
                    created=created, updated=updated, errors=errors, nrows=nrows, collection=collection)

    def batch_save(self, data, save_info=None, commit=True):
        tic = time.time()
        save = json.loads(request.args.get("save", "{}") or "{}")
        f = json.loads(request.args.get("f", "{}") or "{}")
        if save:
            if save_info is None: save_info = request.args.to_dict()  # all arguments
            with db.session.no_autoflush:   # prevent premature auto-flush with related objects
                result = self.dicts_to_database(data, **save)
                self.last_save = dict(save=save, f=f)
                if commit:
                    db.session.commit()
                result['duration'] = time.time() - tic   # add duration to results
                return result
        return None


class DBFileMixin(SurrogatePK, SaveCollectionMixin):
    """A mixin that adds fields and methods for database-stored file objects
    """
    #__table_args__ = {'extend_existing': True}

    name = Column(db.Text, unique=False, nullable=False,
                  doc="The name of the file (optional). Defaults to `basename` of either `file_url` or the path of the uploaded file")
    file_url = Column(db.Text, unique=False, nullable=True,
                  doc="The URL of the file to be loaded dynamically (if `data` field is not set)")
    description = Column(db.Text, nullable=True,
                  doc="The description of the file (optional)")
    _rawfiledata = Column(db.LargeBinary, nullable=True,
                  doc="The raw file storage for the file. This is not directly accessible through the API")
    iscompressed = Column(db.Boolean, nullable=True, default=False,
                  doc="Whether or not the file is compressed or not. If so, it will be automatically decompressed upon access.")

    def __init__(self, **kwargs):
        # if name is not supplied, attempt to set it by default
        if "name" not in kwargs:
            file_url = kwargs.get("file_url", None)
            kwargs["name"] = os.path.basename(file_url) if file_url else None
        db.Model.__init__(self, **kwargs)


    # Computed property for getting and setting file
    # based on compression flag
    @property
    def data(self):
        if self._rawfiledata is not None:
            content = self._rawfiledata
        elif self.file_url is not None:
            r = requests.get(self.file_url)
            r.raise_for_status()
            content = r.content
        else:
            raise FileNotFoundError("No valid content found for {} in {}".format(self.name, self.__tablename__))
        return decompress(content) if self.iscompressed else content

    @data.setter
    def data(self, value):
        # value.decode('utf-8')
        #value = unicode(value,'utf-8')
        # print "\n\n",value,"\n\n"
        self._rawfiledata = compress(value) if self.iscompressed else value  #value.decode('latin-1').encode("utf-8")

    @classmethod
    def get_data(cls, fid):
        """
        Get the data for the file resource matching `fid`. This endpoint also provides options to manipulate, transform
        and save data to the database. The querystring takes the following form:
        ```
        ?f={...}&save={...}&disposition=...&template=...
        ```
        where `f` is a JSON dictionary containing a sequence of operations for transforming the data
        (see [Data transformation API](#api_data_transform) for more info),
        `disposition` is optional and defines the download type which can be `inline` (default, displays in
        the browser) or `attachment` (triggers a download) ,
        `template` is optional depending on the output of the steps in `f` and can be used to define the export format,
        `save` is a JSON dictionary that defines batch save operations. This can be used to save the file content or the
        output of the operations in `f` to a related database model collection.

        """
        # prepare load data
        instance = cls.query.get(fid)
        data = BytesIO(instance.data)

        # create data transformer
        dt = get_data_transformer(data)

        # save the result if the save parameter is set
        save_result = instance.batch_save(dt.data)
        if save_result is not None:
            dt.set_data(save_result)

        # render result of data transformer
        return render_data_transformer(dt, filename_pattern=instance.name, default_disposition="inline")


    @classmethod
    def set_data(cls):
        """
        Save file data to database. Use this if it is not possible to dynamically link the file using the `file_url` or
        if the `file_url` is potentially unstable / ephemeral. This should be a multipart form post with `file`
        parameter set to the uploaded file and all other fields as in the object definition below.
        """
        upload = request.files.get("file")
        data_fields = dict(request.form)
        if "name" not in data_fields:
            data_fields["name"] = f"{time.strftime('%Y%m%d-%H%M%S')}-{os.path.basename(upload.filename)}"
        f = cls.create(data=upload.read(), **data_fields)
        # return jsonify({"id": f.id, "name": f.name})
        return render_data_response(f.instance_to_dict())


class JSONDataFieldMixin(object):

    #__table_args__ = {'extend_existing': True}

    data_json = Column(db.Text, nullable=True, doc="Text field containing JSON properties for this resource")

    @hybrid_property
    def data(self):
        """JSON data containing optional properties for this resource"""
        try:
            return json.loads(self.data_json)
        except Exception as e:
            return {}

    @data.setter
    def data(self, data):
        self.data_json = json.dumps(data)
        # self.description = jsond.dumps(data)


def get_model_by_table_name(tablename):
    for c in list(db.Model._decl_class_registry.values()):
        if hasattr(c, '__tablename__') and c.__tablename__ == tablename:
            return c


def ReferenceCol(tablename, nullable=False, cascade=False, pk_name='id', **kwargs):
    """Column that adds primary key foreign key reference.

    Usage: ::

        category_id = ReferenceCol('category')
        category = relationship('Category', backref='categories')
    """
    ondelete = 'CASCADE' if cascade==True else None
    return db.Column(
        db.ForeignKey("{0}.{1}".format(tablename, pk_name), ondelete=ondelete),
        nullable=nullable, **kwargs)


# def method_not_allowed_preprocessor(*args, **kw):
#     raise ProcessingException(description="This method is not allowed on this resource", code=405)


# def get_or_create(session, model, **kwargs):
#     instance = session.query(model).filter_by(**kwargs).first()
#     if instance:
#         return instance, False
#     else:
#         instance = model(**kwargs)
#         session.add(instance)
#         session.commit()
#         return instance, True

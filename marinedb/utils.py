# -*- coding: utf-8 -*-
'''Helper utilities and decorators.'''
from flask import flash, Flask

from marinedb.settings import ProdConfig


def flash_errors(form, category="warning"):
    '''Flash all errors for a form.'''
    for field, errors in list(form.errors.items()):
        for error in errors:
            flash("{0} - {1}".format(getattr(form, field).label.text, error), category)


def encode_string(txt):
    if txt:
        txt = txt.encode("utf-8").decode('utf-8','ignore')
    return txt


def init_app_config(config_object=ProdConfig):
    app = Flask(__name__)
    app.config.from_object(config_object)
    app.config.from_pyfile('settings.cfg', silent=True)  # extend settings from cfg file if available
    return app


def get_db_engine(config_object=ProdConfig):
    app = init_app_config(config_object)
    from sqlalchemy import create_engine  # for views
    return create_engine(app.config['SQLALCHEMY_DATABASE_URI'])


# def get_db_engine_without_app(config_object=ProdConfig):
#     # app = init_app_config(config_object)
#     # Create a config object without creating a full Flask app
#     root_path = os.path.abspath(__name__)
#     config = Config(root_path=root_path)
#     config.from_object(config_object)
#     config.from_pyfile(os.path.join(root_path), 'settings.cfg') #, silent=True)  # Load configuration from a file
#     return create_engine(config['SQLALCHEMY_DATABASE_URI'])

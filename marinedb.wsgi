# This file contains the WSGI configuration required to serve up your
# web application. It works by setting the variable 'application' to a
# WSGI handler of some description.

import sys
import os


# add your project directory to the sys.path
project_home = os.path.dirname(os.path.realpath(__file__))
#project_home = u'/var/www/marinedb/'


# activate virtual environment
activate_this = project_home+'/env/bin/activate_this.py'
#exec(compile(open(activate_this, "rb").read(), activate_this, 'exec'), dict(__file__=activate_this))
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

#print("\n\n{}\n{}\n{}\n\n".format(project_home,activate_this,sys.version))

if project_home not in sys.path:
    sys.path.insert(0, project_home)
    # sys.path = [project_home] + sys.path

# import flask app but need to call it "application" for WSGI to work
from marinedb.app import create_app
from marinedb.settings import DevConfig, ProdConfig


application = create_app(ProdConfig)
# application = create_app(DevConfig)

# def application(environ, start_response):
#     # pass the WSGI environment variables on through to os.environ
#     app = create_app(ProdConfig(secret=environ.get('MDB_SECRET', ''), project_name=environ.get('MDB_PROJECT_NAME', '')))
#     return app(environ, start_response)



# def application(environ, start_response):
#     # pass the WSGI environment variables on through to os.environ
#     os.environ['MDB_SECRET'] = environ.get('MDB_SECRET', '')
#     os.environ['MDB_PROJECT_NAME'] = environ.get('MDB_PROJECT_NAME', '')
#     app = create_app(ProdConfig())
#     return app(environ, start_response)

# def application(environ, start_response):
#     # list of environment variables to pass into app
#     # defined by SetEnv parameters in apache site config
#     ENVIRONMENT_VARIABLES = ['MDB_SECRET', 'MDB_PROJECT_NAME']
#     for key in ENVIRONMENT_VARIABLES:
#         if key in environ:
#             os.environ[key] = environ.get(key, '')
#
#     # Have to add env variables before importing configs
#     from marinedb.app import create_app
#     from marinedb.settings import DevConfig, ProdConfig
#     app = create_app(ProdConfig)
#     return app(environ, start_response)

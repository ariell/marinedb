sudo apt-get install build-essential python-all-dev

#wget http://download.osgeo.org/gdal/1.11.0/gdal-1.11.0.tar.gz
wget http://download.osgeo.org/gdal/CURRENT/gdal-2.0.2.tar.gz
tar xvfz gdal-2.0.2.tar.gz
cd gdal-2.0.2

./configure --with-python
make
sudo make install
#!/usr/bin/env bash


ARGS="$@"

REMOTE_CMDS="cd marinedb"   # && git pull && sudo service apache2 reload"

if [[ -z "${ARGS// }" ]]; then
    # Print usage if no args
    echo -e "Usage:\n   $0  pull updatedb reload taillog\n"
else
    # buld up commands from arguments
    if [[ $ARGS == *"pull"* ]]; then
        REMOTE_CMDS="$REMOTE_CMDS && git pull"
    fi
    if [[ $ARGS == *"updatedb"* ]]; then
        REMOTE_CMDS="$REMOTE_CMDS  && source env/bin/activate && python manage.py db migrate && python manage.py db upgrade"
    fi
    if [[ $ARGS == *"reload"* ]]; then
        REMOTE_CMDS="$REMOTE_CMDS  && sudo service apache2 reload"
    fi
    if [[ $ARGS == *"taillog"* ]]; then
        REMOTE_CMDS="$REMOTE_CMDS  && tail -n 50 /var/log/apache2/error.log"
    fi

    # Run commands on remote servers
    read -rep "*** Run on Nectar IMOS SQUIDLE+ PROD? [Y/n]" response
    if [[ $response =~ ^(Y|y| ) ]] || [[ -z $response ]]; then
        ssh -i ~/.ssh/cloud.key -t ubuntu@144.6.225.82 "$REMOTE_CMDS"           # Nectar IMOS Squidle Production
    fi

    read -rep "*** Run on Nectar IMOS SQUIDLE+ SANDBOX? [Y/n]" response
    if [[ $response =~ ^(Y|y| ) ]] || [[ -z $response ]]; then
        ssh -i ~/.ssh/cloud.key -t ubuntu@203.101.232.29 "$REMOTE_CMDS"         # Nectar IMOS SANDBOX
    fi

    read -rep "*** Run on Nectar SOI GCloud SQUIDLE+? [Y/n]" response
    if [[ $response =~ ^(Y|y| ) ]] || [[ -z $response ]]; then
        ssh -i ~/.ssh/gcloud-key ariell_friedman@130.211.155.120 "$REMOTE_CMDS"    # SOI GCloud
    fi

    read -rep "*** Run on Nectar SOI Falkor Nebula SQUIDLE? [Y/n]" response
    if [[ $response =~ ^(Y|y| ) ]] || [[ -z $response ]]; then
        ssh -i ~/.ssh/hpc-cloud-keygen_rsa ubuntu@10.23.14.51 "$REMOTE_CMDS"    # SOI Falkor
    fi

    read -rep "*** Run on Nectar SOI Shoreside Nebula SQUIDLE? [Y/n]" response
    if [[ $response =~ ^(Y|y| ) ]] || [[ -z $response ]]; then
        ssh -i ~/.ssh/hpc-cloud-keygen_rsa ubuntu@10.23.14.8 "$REMOTE_CMDS"    # SOI Shoreside
    fi
fi


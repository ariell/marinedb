#!/bin/bash

# SETUP GDAL
# ./install_gdal.sh


FILEIN=$1

fname=$(basename $FILEIN)
extension="${fname##*.}"
filename="${fname%.*}"
dname=$(dirname $FILEIN)

FILEOUT="${dname}/${filename}-converted.${extension}"

# Correct map projection and make background transparent
gdalwarp ${FILEIN} ${FILEOUT} -t_srs "+proj=longlat +ellps=WGS84" -wo NUM_THREADS=ALL_CPUS -dstnodata 255
tiff_files=`ls image_r*_c*_rs*_cs*.tif`
gdalbuildvrt -a_srs mosaic-corrected.vrt $tiff_files
#gdalwarp ${FILEIN} ${FILEOUT} -t_srs 'epsg:900913'  -wo NUM_THREADS=ALL_CPUS -dstnodata 255

# create tiles
TILEDIR="${dname}/${filename}-tiles/"
python gdal2tiles.py -s EPSG:4326 -w none ${FILEOUT} ${TILEDIR}


# USAGE:
#   bash sq_sos.sh username
#   eg: bash sq_sos.sh sshtunnel

# NOTE FOR XRDP
# sudo apt install xrdp
# sudo nano /etc/xrdp/xrdp.ini
# change: "port=3389" to "port=tcp://.:3389"


CERT_FILE="$HOME/.ssh/restricted-tunnel_rsa"
UNAME="$1"
TUNNEL_SERVER="131.217.172.56"
HOSTNUM=`expr $(hostname) : '[^0-9]*\([0-9]*\)' 2> /dev/null || :`  # eg: host: GBB005 -> 005
PORT_SSH=$((2200 + HOSTNUM))   # eg: 2200+005 = 2205
PORT_XRDP=$((3300 + HOSTNUM))   # eg: 2200+005 = 2205

if [ ! -f "$CERT_FILE" ]; then
  echo "$CERT_FILE does not exist. Creating certificate key-pair..."
  ssh-keygen -q -t rsa -N '' -f $CERT_FILE <<<y >/dev/null 2>&1
  echo -e "SSH key-pair generated. Please ensure your public key is configured on the server to enable tunneling"
  echo -e "Please email: \n\n${CERT_FILE}.pub\n\n to hello@greybits.com.au"

else
  echo "Connecting to tunnel server..."
  echo "This device will be accessible remotely from tunnel server on port: $PORT_SSH"
  echo "(CTL+C to end session)"
#  ssh -R ${PORT_SSH}:localhost:22 -R ${PORT_XRDP}:localhost:3389 -i ${CERT_FILE} ${UNAME}@${TUNNEL_SERVER} -N
  ssh -R ${PORT_SSH}:localhost:22 -i ${CERT_FILE} ${UNAME}@${TUNNEL_SERVER} -N
  echo -e "\nRemote tunnel session ended..."
fi


import csv
from marinedb.annotation.api import *

from_info_field_default = "code_short"
to_info_field_default = "code_short"

def convert_scheme(from_scheme_id, to_scheme_id, from_info_field=from_info_field_default, to_info_field=to_info_field_default, do_mapping=False):
    errors = dict()
    success = []
    labels_to_remove = []
    for from_tg in LabelSchemeAPI.get_by_id(from_scheme_id).labels:
        try:
            code = LabelInfo.query.filter_by(label_id=from_tg.id,name=from_info_field).one().value
        except Exception as e:
            log_error(errors, from_tg, e, msg="FIELD LOOKUP")
        else:
            try:
                to_tg = Label.query.filter(Label.info.any(name=to_info_field,value=code), Label.label_scheme_id==to_scheme_id).one()
                # new_tg = Label.query.filter(LabelInfo.value==code, LabelInfo.name==to_info_field, LabelInfo.label_id==Label.id, Label.label_scheme_id==to_scheme_id).one()
                print("MAPPING: {} >> {}  |  {}={}".format(from_tg.name, to_tg.name, from_info_field, code))
                annotation_count = remap_label_annotations(from_tg, to_tg, do_mapping=do_mapping)
                children_count = remap_label_children(from_tg, to_tg, do_mapping=do_mapping)
                labels_to_remove.append(from_tg)
                success.append(dict(from_name=from_tg.name_path, from_id=from_tg.id, to_name=to_tg.name_path, to_id=to_tg.id, annotation_count=annotation_count, children_count=children_count))
            except Exception as e:
                log_error(errors, from_tg, e, msg="LABEL MAPPING")
    if do_mapping:
        for tg_r in labels_to_remove:
            tg_r.delete()

    for error_type in errors.keys():
        write_csv(from_scheme_id, to_scheme_id, from_info_field, to_info_field, error_type, errors[error_type])

    write_csv(from_scheme_id, to_scheme_id, from_info_field, to_info_field, "MAPPED", success)


def remap_label_annotations(from_tg, to_tg, do_mapping=False):
    count = from_tg.annotations.count()
    if count > 0:
        print(" - Mapping {} annotations | {} >> {}".format(count, from_tg.name, to_tg.name))
        if do_mapping:
            # remap all annotations
            for a in from_tg.annotations:
                a.label_id = to_tg.id
            db.session.commit()
    return count

def remap_label_children(from_tg, to_tg, do_mapping=False):
    count = from_tg.children.count()
    if count > 0:
        print(" - Mapping {} children | {} >> {}".format(count, from_tg.name, to_tg.name))
        if do_mapping:
            # remap all child classes
            for c in from_tg.children:
                c.parent_id = to_tg.id
            db.session.commit()
    return count

def log_error(errors, tg, e, msg=""):
    if type(e).__name__ not in errors:
        errors[type(e).__name__] = []
    errors[type(e).__name__].append(
        dict(from_name=tg.name_path, from_id=tg.id, error="{} {}".format(msg, str(e))))


def write_csv(from_scheme_id, to_scheme_id, from_info_field, to_info_field, filename, dictlist):
    if len(dictlist)> 0:
        keys = dictlist[0].keys()
        with open('scheme_mapper_from_{}_{}_to_{}_{}_{}.csv'.format(
                from_scheme_id, from_info_field, to_scheme_id, to_info_field, filename
        ), 'w', newline='')  as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(dictlist)

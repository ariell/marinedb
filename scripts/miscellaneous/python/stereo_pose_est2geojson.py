import sys
import requests
import json
import os
import numpy as np
import string
import re

input_file = sys.argv[1]
fname = os.path.splitext(input_file)[0]
dirpath = os.path.dirname(input_file)

deployment = input_file.split('/')[-3]
campaign = input_file.split('/')[-4]

linestring_file = '{}/{}-linestring.json'.format(dirpath, deployment)
pointfeatures_file = '{}/{}-pointfeatures.json'.format(dirpath, deployment)
info_file = '{}/{}-info.json'.format(dirpath, deployment)

start_at = 57

col_config = {
    'geometry': {
        'lat': 2,
        'lon': 3
    },
    'properties': {
        'timestamp': 1,
        'depth': 6,
        'roll': 7,
        'pitch': 8,
        'heading': 9,
        'image1': 10,
        'image2': 11,
        'altitude': 12
    }
}

stats_properties = ['timestamp', 'depth', 'altitude']

f = open(input_file, 'r')
data = f.readlines()
f.close()

parseStr = lambda x: x.isalpha() and x or x.isdigit() and int(x) or re.match('(?i)^-?(\d+\.?e\d+|\d+\.\d*|\.\d+)$',x) and float(x) or x

points = []
for i in range(start_at, len(data)):
    row = data[i].split("\t")
    thispoint = {
        'type': 'Feature',
        'geometry': {'type': 'Point',
                     'coordinates': [parseStr(row[col_config['geometry']['lon']].strip()), parseStr(row[col_config['geometry']['lat']].strip())]},
        'properties': {k: parseStr(row[col_config['properties'][k]].strip()) for k in col_config['properties'].keys()}
    }
    points.append(thispoint)
    # print "LAT: ", lat, "LON: ", lon



# Format template
geojson_format = {
    "type": "FeatureCollection",
    "features": []
}


# prep info stats
info_properties = {}
for k in stats_properties:
    thisdata = [p['properties'][k] for p in points]
    info_properties[k + '_min'] = np.min(thisdata)
    info_properties[k + '_mean'] = np.mean(thisdata)
    info_properties[k + '_max'] = np.max(thisdata)

# create linestring file
linestring_coordinates = [p['geometry']['coordinates'] for p in points]
linestring_geojson = geojson_format.copy()
linestring_geojson["features"].append({
    "type": "Feature",
    "properties": info_properties,
    "geometry": {
        "type": "LineString",
        "coordinates": linestring_coordinates
    }
})
# opens an geoJSON file to write the output to
f = open(linestring_file, "w")
f.write(json.dumps(linestring_geojson))
f.close()

# create info file
info_properties['coord_end'] = points[-1]['geometry']['coordinates']
info_properties['campaign'] = campaign
info_properties['deployment'] = deployment
info_point = {
    'type': 'Feature',
    'geometry': {'type': 'Point',
                 'coordinates': points[0]['geometry']['coordinates']},
    'properties': info_properties
}
f = open(info_file, "w")
f.write(json.dumps(info_point))
f.close()


# create point features file
pointfeatures_geojson = geojson_format.copy()
pointfeatures_geojson["features"] = points
f = open(pointfeatures_file, "w")
f.write(json.dumps(pointfeatures_geojson))
f.close()


# outFileHandle = open(coord_file, "w")
# outFileHandle.write(json.dumps(start_coord))
# outFileHandle.close()
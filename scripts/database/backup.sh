#!/usr/bin/env bash
# full path of script
SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
BASEDIR=$(dirname "$(dirname "$SCRIPTDIR")");  # marinedb base dir
cd $BASEDIR || exit

FILENAME=`date '+%Y-%m-%dT%H-%M-%S'`
DBNAME="marinedb"
BKPDIR="$BASEDIR/dbbackups"
USR="postgres"
_PARTIAL_BACKUP='false'
MOVE_TO_CLOUD=''

print_usage() {
  echo -e "
Backup MARINEDB/SQ+ database

Usage:
  bash $(basename $0) [-b <BACKUP_DIR>] [-d <DATABASE_NAME>] [-u <USER>] [-p <PASS>] [-t]

  -b: backup location (default=$BKPDIR)
  -d: database to backup (default=$DBNAME)
  -f: backup file name (default=current datetime, eg:$FILENAME)
  -u: database user to perform backup (default=$USR)
  -t: partial / template backup - including data for users, label_schemes and user_groups, exclude all media, annotations and datasets info
  -c: move to cloud location, eg: aws:s3://gbe-squidle-backups/squidle.org/
"
}


while getopts b:d:f:u:c:t flag
do
    case "${flag}" in
        d) DBNAME=${OPTARG};;
        b) BKPDIR=${OPTARG};;
        f) FILENAME=${OPTARG};;
        u) USR=${OPTARG};;
        c) MOVE_TO_CLOUD=${OPTARG};;
        t) _PARTIAL_BACKUP='true';;
        *) print_usage
           exit 1 ;;
    esac
done


mkdir -p $BKPDIR/${FILENAME}

# backup database
if [[ "${_PARTIAL_BACKUP}" == "true" ]]; then
  echo "Running partial database backup..."
  pg_dump -h localhost -p 5432 -U ${USR} --exclude-table-data=annotation --exclude-table-data=annotation_set \
    --exclude-table-data=annotation_tags --exclude-table-data=campaign --exclude-table-data=campaign_file \
    --exclude-table-data=campaigninfo --exclude-table-data=datasource --exclude-table-data=deployment \
    --exclude-table-data=deployment_event --exclude-table-data=deployment_file --exclude-table-data=media \
    --exclude-table-data=media_collection --exclude-table-data=media_collection_media --exclude-table-data=platform \
    --exclude-table-data=platform_maplayer --exclude-table-data=point --exclude-table-data=pose \
    --exclude-table-data=posedata --exclude-table-data=usergroups_annotation_set \
    --exclude-table-data=usergroups_media_collection \
    -F c -b -f  "${BKPDIR}/${FILENAME}/${DBNAME}.backup" ${DBNAME}
else
  echo "Running full database backup..."
  pg_dump -h localhost -p 5432 -U ${USR} -F c -b -f  "${BKPDIR}/${FILENAME}/${DBNAME}.backup" ${DBNAME}
fi

#pg_dump -h localhost -p 5432 $EXCLUDE -F c -b -v -f  "${BKPDIR}/${FILENAME}/${DBNAME}.backup" ${DBNAME}
# pg_dump -h localhost -p 5432 --exclude-table-data=annotation --exclude-table-data=annotation_set --exclude-table-data=annotation_tags --exclude-table-data=campaign --exclude-table-data=campaign_file --exclude-table-data=campaigninfo --exclude-table-data=datasource --exclude-table-data=deployment --exclude-table-data=deployment_event --exclude-table-data=deployment_file --exclude-table-data=media --exclude-table-data=media_collection --exclude-table-data=media_collection_media --exclude-table-data=platform --exclude-table-data=platform_maplayer --exclude-table-data=point --exclude-table-data=pose --exclude-table-data=posedata -F c -b -v -f  "${BKPDIR}/${FILENAME}/${DBNAME}.backup" ${DBNAME}


# copy migrations
echo "Copying migrations..."
cp -r migrations ${BKPDIR}/${FILENAME}/

# copy git revision
echo "Saving GIT version..."
git rev-parse --verify HEAD > ${BKPDIR}/${FILENAME}/git-current.txt

# copy settings file
echo "Backing up setting file..."
cp marinedb/settings.cfg ${BKPDIR}/${FILENAME}/


# copy pip freeze for software versions
echo "Saving pip freeze dependency state..."
env/bin/pip freeze > ${BKPDIR}/${FILENAME}/pip-freeze.txt

# Copy apache config
echo "Backing up apache config..."
cp -rL /etc/apache2/sites-enabled ${BKPDIR}/${FILENAME}/

# MOVE TO CLOUD STORAGE AND DELETE
if [[ "${MOVE_TO_CLOUD:0:3}" == "aws" ]]; then
  echo "MOVING TO AWS: ${MOVE_TO_CLOUD:4}"
  CL=${MOVE_TO_CLOUD:4}
  aws s3 sync "${BKPDIR:?}/${FILENAME:?}" "$(dirname $CL)/$(basename $CL)/${FILENAME:?}" && rm -r "${BKPDIR:?}/${FILENAME:?}"
elif [ "${MOVE_TO_CLOUD:0:3}" == "gcs" ]; then
  echo "TODO: ADD COMMAND TO COPY TO GCS: ${MOVE_TO_CLOUD:4}"
else
  echo "Not backing up to cloud storage"
fi

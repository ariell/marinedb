DBNAME="marinedb"
USR="postgres"
CONC="CONCURRENTLY"

print_usage() {
  echo -e "
Update views

Usage:
  bash $(basename $0) [-d <DATABASE_NAME>] [-u <USER>]

  -d: database to refesh views (default=$DBNAME)
  -u: database user to refresh views (default=$USR)
  -i: initialize (do not refresh with 'CONCURRENTLY' option)
"
}

while getopts d:u:i flag
do
    case "${flag}" in
        d) DBNAME=${OPTARG};;
        u) USR=${OPTARG};;
        i) CONC="";;
        *) print_usage
           exit 1 ;;
    esac
done

echo -e "
REFRESH MATERIALIZED VIEW ${CONC} deployments_mv;
REFRESH MATERIALIZED VIEW ${CONC} campaigns_mv;
" | psql -U ${USR} -d ${DBNAME}
#!/usr/bin/env bash

# full path of script
SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
BASEDIR=$(dirname "$(dirname "$SCRIPTDIR")");  # marinedb base dir
cd "$BASEDIR" || exit


DBNAME="marinedb"
USR="postgres"
_RESET_SEQUENCES='false'
_REPLACE_MIGRATIONS='false'
_CHECKOUT_GIT='false'

print_usage() {
  echo -e "
Restore MARINEDB/SQ+ database

Usage:
  bash $(basename $0) -b <BACKUP_DIR> -d <DATABASE_NAME> [-u <USER>] [-r] [-m]

  -b: backup directory (required)
  -d: database to restore to (default=$DBNAME)
  -u: database user to perform backup (default=$USR)
  -g: optionally checkout git version
  -r: optionally reset sequences
  -m: optionally replace migrations directory
"
}


while getopts b:d:u:mrg flag
do
    case "${flag}" in
        b) BKP=${OPTARG};;
        d) DBNAME=${OPTARG};;
        r) _RESET_SEQUENCES='true';;
        g) _CHECKOUT_GIT='true';;
        m) _REPLACE_MIGRATIONS='true';;
        u) USR=${OPTARG};;
        *) print_usage
           exit 1 ;;
    esac
done

DB_BKP="`ls ${BKP}/*.backup`"
DB_MIGRATIONS="${BKP}/migrations"
GIT_HASH=`cat ${BKP}/git-current.txt`

echo -e "DB_BKP=$DB_BKP
DB_MIGRATIONS=$DB_MIGRATIONS
GIT_HASH=$GIT_HASH
"

# Check if database exists
if sudo -u $USR psql -lqt | cut -d \| -f 1 | grep -qw ${DBNAME}; then
  echo -e "\nDB EXISTS! Exiting to preserve data. Please rename or remove to restore.\n"
  exit 1
else
  echo -e "\nCreating the DB: ${DBNAME}...\n"
  sudo -u $USR createdb -U ${USR} ${DBNAME}
fi

# TODO: check if postgres user exists, if not, prompt to create it
# psql -d ${DBNAME} -c 'CREATE USER postgres SUPERUSER;'
TMP_DB_BKP="/tmp/`basename ${DB_BKP}`"
cp ${DB_BKP} ${TMP_DB_BKP}   #copy to accessible location for postgres user
sudo -u ${USR} pg_restore -h localhost -p 5432 -U ${USR} --no-owner --role=${USR} -d ${DBNAME}  ${TMP_DB_BKP} && echo -e "\n\nCompleted database restore: ${DB_BKP}\n\n"
rm ${TMP_DB_BKP} && echo "Removed temporary file '${TMP_DB_BKP}'"

# Replace migrations directory
if [[ "${_REPLACE_MIGRATIONS}" == "true" ]]; then
  [ -d "migrations" ] && mv migrations "migrations.bkp.`date '+%Y-%m-%dT%H-%M-%S'`" && echo -e "Backed up existing migrations..."
  cp -r ${DB_MIGRATIONS} . && echo -e "\n\nCopied migration directory: ${DB_MIGRATIONS}\n\n"
fi

# Checkout the relevant git revision
if [[ "${_CHECKOUT_GIT}" == "true" ]]; then
  git checkout ${GIT_HASH} && echo -e "\n\nChecked out git revision: ${GIT_HASH}\n\n"
fi

# TODO: change settings.cfg to $DBNAME

# Reset sequences. This may be necessary to ensure that the DB sequences have all been restored properly
# Get sequences and save to temp file
if [[ "${_RESET_SEQUENCES}" == "true" ]]; then
  echo "Resetting sequences..."
  psql ${DBNAME} -Atq -o temp -c "SELECT 'SELECT SETVAL(' || \
         quote_literal(quote_ident(PGT.schemaname) || '.' || quote_ident(S.relname)) || \
         ', COALESCE(MAX(' ||quote_ident(C.attname)|| '), 1) ) FROM ' || \
         quote_ident(PGT.schemaname)|| '.'||quote_ident(T.relname)|| ';' \
  FROM pg_class AS S, \
       pg_depend AS D, \
       pg_class AS T, \
       pg_attribute AS C, \
       pg_tables AS PGT \
  WHERE S.relkind = 'S' \
      AND S.oid = D.objid \
      AND D.refobjid = T.oid \
      AND D.refobjid = C.attrelid \
      AND D.refobjsubid = C.attnum \
      AND T.relname = PGT.tablename \
  ORDER BY S.relname;"
  # execute temp file
  psql ${DBNAME} -f temp
  # remove temp file
  rm temp
fi

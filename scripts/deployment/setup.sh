#!/usr/bin/env bash
set -e  # exit if any command fails
#set -x  # print everything for debugging

CURDIR=`pwd`;
SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)";
APPDIR=$(dirname "$(dirname "$SCRIPTDIR")");  # marinedb base dir

# Some defaults
SETTINGS_FILE="${SCRIPTDIR}/configs/greybitsbox.cfg";
PGPASS="something-really-secret";
DB_BACKUP="";
_SETUP_GEOSERVER='false';

print_usage() {
  echo -e "
Deploy marinedb (SQUIDLE+) software stack on linux server

Usage:
  bash $(basename $0) -s <SETTINGS_FILE> -p <POSTGRES_PASSWORD> [-d <path/to/dbbackup>] [-g]

  -s: path to settings file (default=$SETTINGS_FILE)
  -d: path to database backup to restore (optional, if omitted, will create empty db)
  -p: password got postgres user
  -g: optionally setup geoserver / tomcat for maps
  -h: print help / usage
"
}

# parse arguments
while getopts s:p:d:g flag
do
    case "${flag}" in
        s) SETTINGS_FILE=${OPTARG};;
        p) PGPASS=${OPTARG};;
        d) DB_BACKUP=${OPTARG};;
        g) _SETUP_GEOSERVER='true';;
        *) print_usage
           exit 1 ;;
    esac
done

########################################################################################################################
# SETUP
########################################################################################################################
echo "Loading settings file: $SETTINGS_FILE"
# shellcheck source=configs/greybitsbox.cfg
source "$SETTINGS_FILE";

########################################################################################################################
# INSTALL DEPENDENCIES
########################################################################################################################
# Install postgresql, git, pip and apache
sudo apt-get install postgresql git python3-psycopg2 libpq-dev python3-dev apache2 libapache2-mod-wsgi-py3 virtualenv aptitude
sudo aptitude install apache2-dev   # install apache2-dev using aptitude to resolve any dependency errors
# If you get errors with APACHE2 log saying something about mod_python, remove it as it is not needed
# sudo apt-get remove libapache2-mod-python

########################################################################################################################
# SETUP ENVIRONMENT
########################################################################################################################
# Set up site environment and enable site in apache:
echo "Setting up virtual environment..."
cd $APPDIR
#pip install virtualenv
virtualenv -p ${PYTHON_BIN} env
source env/bin/activate
pip install -r requirements/dev.txt       # set up dependencies
#pip install psycopg2  # needed this on latest install?? TODO: investigate


echo "Setting up directories and permissions..."
# Create directories
sudo mkdir -p ${MEDIA_DIR}
sudo mkdir -p ${THM_CACHE}

# Setup permissions:
sudo usermod -a -G www-data ${USER}          # add user to www-data group (ubuntu=local user)
#sudo chown ${USER}:www-data -R marinedb/     # configure directory permissions
sudo chown www-data:www-data ${THM_CACHE}
sudo chown www-data:www-data ${MEDIA_DIR}
sudo chgrp -R www-data ${APPDIR}
sudo chmod -R g+w ${APPDIR}/marinedb/static/


# MOVE DATABASE LOCATION IF LOCATION IS SUPPLIED
if [ -d "$( dirname "$DB_LOCATION")" ] ; then
  echo "Moving database location..."
  #current_data_dir=`sudo -u postgres psql -c "SHOW data_directory;" | sed -n 3p`  # Check current location
  sudo mkdir "${DB_LOCATION}"
  sudo systemctl stop postgresql                          # stop service
  sudo rsync -a /var/lib/postgresql "${DB_LOCATION}"        # copy data to new location
  sudo mv /var/lib/postgresql /var/lib/postgresql.bkp     # make backup
  sudo ln -s "${DB_LOCATION}"/postgresql /var/lib/postgresql           # link to new location
  sudo chown postgres:postgres "${DB_LOCATION}"             # change ownership / permission for dir
  sudo chown postgres:postgres /var/lib/postgresql        # change ownership / permission for link
  sudo systemctl start postgresql
  # sudo systemctl status postgresql  # check it is back up and running
fi


########################################################################################################################
# SITE CONFIG
########################################################################################################################
# get geoserver bit, if relevant

# create apache config
source $SCRIPTDIR/generate_apache_config.sh

# application config file
source $SCRIPTDIR/generate_app_config.sh

########################################################################################################################
# SETUP DATABASE
########################################################################################################################

#Setup the database. If using postgres, create the database using
sudo -u postgres psql -d template1 -c "ALTER USER postgres with encrypted password '${PGPASS}';"

# Create postgres password directory for scripted backups/restore
echo "localhost:5432:${DBNAME}:postgres:${PGPASS}" > ~/.pgpass
chmod 600 ~/.pgpass

# Create the database
if [ -d "$DB_BACKUP" ] ; then
  # Create DB from restoring a backup
  sudo -u postgres bash scripts/database/database_restore.sh -b "$DB_BACKUP" -d "$DBNAME" -m
else
  # create db and restart service
  sudo -u postgres createdb ${DBNAME}
  # convert DB to postgis
  sudo apt install postgis  #postgresql-12-postgis-3
  sudo -u postgres psql ${DBNAME} -c "CREATE EXTENSION IF NOT EXISTS postgis;";
  # Enable extension for UUID field in posgres database
  sudo -u postgres psql ${DBNAME} -c "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\"";
  #  sudo service postgresql restart

  # Initialise database
  env/bin/python manage.py db init
  env/bin/python manage.py db migrate
  env/bin/python manage.py db upgrade


  # Create materialized views
  sudo -u postgres psql $DBNAME -v host_url="'$HOST_URL'" < $SCRIPTDIR/create_db_views.sql
fi

########################################################################################################################
# SETUP GEOSERVER
########################################################################################################################
if [[ "${_SETUP_GEOSERVER}" == "true" ]]; then
  git submodule update --init --progress geoserver-config
  sudo bash "${APPDIR}/geoserver-config/setup.sh"
fi

########################################################################################################################
# Enable site and reload
########################################################################################################################
sudo a2dissite 000-default.conf         # disable default site
sudo a2ensite marinedb                  # enable site
sudo a2enmod headers                    # enable headers module in case not already enabled
sudo a2dismod wsgi
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo service apache2 reload             # reload apache

# Enable ports if closed (in 22.04, ports are closed by default)
sudo ufw allow 'Apache Full'
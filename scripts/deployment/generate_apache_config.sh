#!/usr/bin/env bash
echo -e "# mod_wsgi setup for virtual environment
`mod_wsgi-express module-config`

<VirtualHost *:80>
    DocumentRoot ${APPDIR}
    ServerName ${HOST_NAME}
    ServerAdmin ariell@greybits.com.au
    WSGIScriptAlias / ${APPDIR}/marinedb.wsgi
    <Directory ${APPDIR}/>
            Require all granted
    </Directory>
    # Setup route for static files
    Alias /static ${APPDIR}/marinedb/static
    <Directory ${APPDIR}/marinedb/static/>
            Options Indexes SymLinksIfOwnerMatch FollowSymLinks
            Require all granted
    </Directory>
    # Setup route for local media
    Alias /media ${MEDIA_DIR}
    <Directory ${MEDIA_DIR}>
            Options Indexes SymLinksIfOwnerMatch FollowSymLinks
            Require all granted
    </Directory>

    ## Geoserver support
    ProxyRequests Off
    ProxyPreserveHost On
    ProxyPass /geoserver http://localhost:8080/geoserver retry=0
    ProxyPassReverse /geoserver http://localhost:8080/geoserver

    # CORS support, to enable GetFeatureInfo, WFS, other ajax requests:
    <IfModule mod_headers.c>
      Header set Access-Control-Allow-Origin "*"
    </IfModule>

    # Logging
    ErrorLog \${APACHE_LOG_DIR}/error.log
    LogLevel warn
    CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
" | sudo tee /etc/apache2/sites-available/marinedb.conf
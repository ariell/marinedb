#!/usr/bin/env bash
echo -e "# Override settings for local installation
PROJECT_NAME = 'SQUIDLE+'
INSTANCE_ID = '${INSTANCE_ID}'
SECRET_KEY = '${PGPASS}'
FRAME_LOGGER_CAPTURE = False
SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:${PGPASS}@localhost/${DBNAME}'
MEDIA_UPLOAD_URL = '/media/{path}'
MEDIA_UPLOAD_DIR = '${MEDIA_DIR}'
MEDIA_THM_CACHE_DIR = '${THM_CACHE}'
REQUEST_CACHE_DIR = '${REQST_CACHE}'
#IN_MAINTENANCE_MODE = True
" | tee ${APPDIR}/marinedb/settings.cfg
-- DROP THE VIEWS UPON RERUN
DROP MATERIALIZED VIEW campaigns_mv;
DROP MATERIALIZED VIEW deployments_mv;

------------------------------------------------------------------------------
-- DEPLOYMENT MV
------------------------------------------------------------------------------
CREATE MATERIALIZED VIEW deployments_mv AS
    SELECT
        deployment.id                                                                        AS id,
        deployment.key                                                                       AS key,
        deployment.name                                                                      AS name,
        campaign.name                                                                        AS campaign_name,
        campaign.key                                                                         AS campaign_key,
        campaign.id                                                                          AS campaign_id,
        campaign.color                                                                       AS color,
        platform.name                                                                        AS platform_name,
        platform.key                                                                         AS platform_key,
        platform.id                                                                          AS platform_id,
        format(:host_url || '/api/deployment/%s'::text, deployment.id)    AS url,
        (SELECT pose.geom
            FROM pose, media
            WHERE pose.geom IS NOT NULL AND pose.media_id = media.id
              AND media.deployment_id = deployment.id LIMIT 1)                               AS point,
        (SELECT MIN(pose.dep)
            FROM pose, media
            WHERE pose.geom IS NOT NULL AND pose.media_id = media.id
              AND media.deployment_id = deployment.id)                                       AS dep_min,
        (SELECT MAX(pose.dep)
            FROM pose, media
            WHERE pose.geom IS NOT NULL AND pose.media_id = media.id
              AND media.deployment_id = deployment.id)                                       AS dep_max,
        (SELECT st_makeline(pose.geom ORDER BY pose."timestamp")
            FROM pose, media
            WHERE pose.geom IS NOT NULL AND pose.media_id = media.id
              AND media.deployment_id = deployment.id)                                       AS line,
        (SELECT pose."timestamp"
             FROM pose, media
             WHERE pose.media_id = media.id AND media.deployment_id = deployment.id
             LIMIT 1)::date                                                                  AS date,
        (SELECT count(media.id) AS count
            FROM media
            WHERE (media.deployment_id = deployment.id))                                     AS media_count,
        (SELECT count(a.id) AS count
            FROM annotation a, point p, media m
            WHERE a.label_id IS NOT NULL
              AND a.point_id = p.id
              AND p.media_id = m.id
              AND m.deployment_id = deployment.id)                                           AS total_annotation_count,
        (SELECT count(a.id) AS count
            FROM annotation a, point p, media m, annotation_set s,
                 usergroups_annotation_set u, groups g
            WHERE a.label_id IS NOT NULL
              AND a.point_id = p.id
              AND p.media_id = m.id
              AND m.deployment_id = deployment.id
              AND a.annotation_set_id = s.id
              AND s.id = u.annotation_set_id
              AND u.group_id = g.id
              AND g.is_public = true)                                                        AS public_annotation_count
    FROM
         deployment, campaign, platform
    WHERE
         deployment.campaign_id = campaign.id
         AND deployment.platform_id = platform.id
         AND deployment.is_valid = TRUE
    WITH NO DATA;

-- ALTER TABLE deployments_mv OWNER TO postgres;

CREATE UNIQUE INDEX ON deployments_mv (id);

------------------------------------------------------------------------------
-- CAMPAIGN MV
------------------------------------------------------------------------------
CREATE MATERIALIZED VIEW campaigns_mv AS
    SELECT
       campaign.id                                                                           AS id,
       campaign.name                                                                         AS name,
       campaign.key                                                                          AS key,
       campaign.color                                                                        AS color,
       format(:host_url || '/api/campaign/%s'::text, campaign.id)         AS url,
       (SELECT string_agg(DISTINCT platform.name::text, ', '::text)
            FROM platform, deployments_mv d
            WHERE d.campaign_id = campaign.id
              AND d.platform_id = platform.id)                                               AS platform_names,
       (SELECT ST_Centroid(ST_Collect(d.line))
            FROM deployments_mv d
            WHERE d.line IS NOT NULL  AND d.campaign_id = campaign.id)                       AS centroid,
       (SELECT st_convexhull(ST_Collect(st_startpoint(d.line)))
            FROM deployments_mv d
            WHERE d.line IS NOT NULL  AND d.campaign_id = campaign.id)                       AS polygon,
       (SELECT d.date
            FROM deployments_mv d
            WHERE d.campaign_id = campaign.id
            ORDER BY d.date asc LIMIT 1)                                                     AS date,
       (SELECT sum(d.media_count)
            FROM deployments_mv d
            WHERE d.campaign_id = campaign.id)                                               AS media_count,
       (SELECT count(d.id)
            FROM deployments_mv d
            WHERE d.campaign_id = campaign.id)                                               AS deployment_count,
--        (SELECT count(annotation.id) AS count
--             FROM annotation, point, media, deployment
--             WHERE annotation.label_id IS NOT NULL
--               AND annotation.point_id = point.id
--               AND point.media_id = media.id
--               AND media.deployment_id = deployment.id
--               AND deployment.campaign_id = campaign.id)                                      AS total_annotation_count,
--        (SELECT count(annotation.id) AS count
--             FROM annotation, point, media, deployment,
--                  annotation_set, usergroups_annotation_set, groups
--             WHERE annotation.label_id IS NOT NULL
--               AND annotation.point_id = point.id
--               AND point.media_id = media.id
--               AND media.deployment_id = deployment.id
--               AND annotation.annotation_set_id = annotation_set.id
--               AND annotation_set.id = usergroups_annotation_set.annotation_set_id
--               AND usergroups_annotation_set.group_id = groups.id
--               AND groups.is_public = true
--               AND deployment.campaign_id = campaign.id)                                      AS public_annotation_count
       (SELECT sum(d.total_annotation_count)
            FROM deployments_mv d
            WHERE d.campaign_id = campaign.id)                                               AS total_annotation_count,
       (SELECT sum(d.public_annotation_count)
            FROM deployments_mv d
            WHERE d.campaign_id = campaign.id)                                               AS public_annotation_count
    FROM campaign
    WITH NO DATA;

-- ALTER TABLE campaigns_mv OWNER TO postgres;
CREATE UNIQUE INDEX ON campaigns_mv (id);